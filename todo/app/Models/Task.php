<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = ['status', 'priority', 'title', 'description', 'user_id', 'created_at', 'completed_at', 'parent_id'];
    /**
     * @var string[]
     */
    protected $dates = ['created_at', 'completed_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subtasks()
    {
        return $this->hasMany(Task::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentTask()
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    /**
     * @param $query
     * @param $status
     * @return mixed
     */
    public function scopeByStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * @return bool
     */
    public function hasUncompletedSubtasks()
    {
        return $this->subtasks()->where('status', 'todo')->exists();
    }
}

