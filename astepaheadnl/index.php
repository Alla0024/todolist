<?php
/*
  $Id: index.php, v 7.12 07.12.2017 14:33:15 
  Solomono osCommerce
*/


  $milliseconds = round(microtime(true) * 1000);

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEFAULT);


  if (isset($_GET['keywords']) || isset($_GET['cPath']) || isset($_GET['manufacturers_id']) || isset($_GET['type'])) {
      if ($page_not_found){
          http_response_code(404);
          $content = CONTENT_ERROR_404;
          $sidebar_left = false;
      }else {
          $content = CONTENT_INDEX_PRODUCTS;

          if (ATTRIBUTES_PRODUCTS_MODULE_ENABLED == 'true') {
              if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {
                  require('ext/filter/filter_seo.php');
              } else {
                  require('ext/filter/filter.php');
              }
          }

          $current_cat_list = $cat_list[$current_category_id];
          $current_cat_list[]=$current_category_id;
          $current_cat_list_with_cat_as_keys = array_flip($current_cat_list);
          $productsToCurrentCat = [];
          foreach ($prodToCat as $pId=>$cId) if (isset($current_cat_list_with_cat_as_keys[$cId])){
              $productsToCurrentCat[] = $pId;
          }

          if (isset($_GET['manufacturers_id'])) {
              // current manufacturer info:

              $man_desc_query = tep_db_query("
                  select mi.manufacturers_url,mi.h1_manufacturer,mi.seo_text_top, m.manufacturers_image, m.manufacturers_name 
                  from manufacturers_info mi, manufacturers m 
                  where m.manufacturers_id = mi.manufacturers_id 
                      and languages_id = '".$languages_id."'   
                      and m.manufacturers_id = '".$_GET['manufacturers_id'] . "'"
              );
              $man_desc = tep_db_fetch_array($man_desc_query);

              $heading_text_box = trim($man_desc['h1_manufacturer']) != '' ? $man_desc['h1_manufacturer'] : $man_desc['manufacturers_name'];
              $seo_text_top = $man_desc['seo_text_top'];
              $cat_image = $man_desc['manufacturers_image'];
              $desc_text = $man_desc['manufacturers_url'];
              $where_manufacturers = "p.manufacturers_id = '" . (int)$_GET['manufacturers_id'] . "' and";
              $m_srch_action = 'manufacturers_id=' . (int)$_GET['manufacturers_id'];

          } else {

              if (isset($_GET['cPath'])) {
                  $hide_categories_mainpage = false;
                  $type_join = $where_type = "";
                  // current category info:
                  $current_category_query = tep_db_query("select cd.categories_name, cd.categories_heading_title, cd.categories_description, c.categories_image from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . $current_category_id . "' and cd.categories_id = c.categories_id and cd.language_id = '" . $languages_id . "'");
                  $current_category = tep_db_fetch_array($current_category_query);

                  $heading_text_box = $current_category['categories_heading_title'] ? $current_category['categories_heading_title'] : $current_category['categories_name'];
                  $cat_image = 'categories/' . $current_category['categories_image'];
                  $seo_text_top = $current_category['categories_description'];

                  // CURRENT subcategoriest columns:
                  $subcat_array = tep_get_subcategories_tree($cat_tree, $current_category_id);

                  if (isset($_GET['type'])) {
                      $hide_categories_mainpage = true;
                      switch ($_GET['type']) {
                          case 'featured':
                              $type_join = " inner join " . TABLE_FEATURED . " f on p.products_id = f.products_id and f.status = '1' ";
                              break;
                          case 'specials':
                              $type_join = " inner join " . TABLE_SPECIALS . " s ON p.products_id = s.products_id and s.status = '1' and (s.expires_date >= CURDATE() or s.expires_date = '0000-00-00 00:00:00' or s.expires_date is NULL) ";
                              break;
                          case 'new':
                              $where_type = "p.lable_2 = '1' and";
                              break;

                      }
                  }

                  if (isset($_GET['filter_id']) && constant('SEO_FILTER') != 'true') $m_srch_action = 'cPath=' . $_GET['cPath'] . '&filter_id=' . $_GET['filter_id'];
                  else $m_srch_action = 'cPath=' . $_GET['cPath'];

              } elseif (isset($_GET['keywords'])) {

                  $heading_text_box = tep_db_input($_GET['keywords']);
                  $keyword_arr = explode(' ', $heading_text_box);

                  // searchword_swap:
                  $sql_words = tep_db_query("SELECT * FROM searchword_swap");
                  if ($sql_words->num_rows) {
                      while ($sql_words_result = tep_db_fetch_array($sql_words)) {
                          $sql_words_array[$sql_words_result['sws_word']] = $sql_words_result['sws_replacement'];
                      }
                      foreach ($keyword_arr as $k => $keyword) $keyword_arr[$k] = strtr($keyword, $sql_words_array);
                  }

                  foreach ($keyword_arr as $k) {
                      $query_name[] = 'LOWER(pd.products_name) LIKE "%' . mb_strtolower($k) . '%"';
                      $query_model[] = 'LOWER(p.products_model) LIKE "%' . mb_strtolower($k) . '%"';
                      $query_description[] = 'LOWER(pd.products_description) LIKE "%' . mb_strtolower($k) . '%"';
                  }
                  $query_name = implode(' AND ', $query_name);
                  $query_model = implode(' AND ', $query_model);
                  $query_description = implode(' AND ', $query_description);
                  $where_keywords .= " (($query_name) or ($query_model) or ($query_description)) and";

                  // subcategories for search results:
                  if ($_GET['cid'] != '') {
                      $clear_pid = tep_db_input($_GET['cid']);

                      if ($cat_list[$clear_pid]) $all_search_cats = implode(',', $cat_list[$clear_pid]);
                      else $all_search_cats = $clear_pid;

                      $where_subcategories = " p2c.categories_id in(" . $all_search_cats . ") and";
                  }

                  $search_enhancements_keywords = addslashes(strip_tags(tep_db_input($_GET['keywords'])));
                  if ($search_enhancements_keywords != $last_search_insert) {
                      tep_db_query("insert into search_queries (search_text)  values ('" . $search_enhancements_keywords . "')");
                      tep_session_register('last_search_insert');
                      $last_search_insert = $search_enhancements_keywords;
                  }

                  // $m_srch_action = 'keywords='.$_GET['keywords'];
                  $m_srch_action = 'index.php';
              }

              if (DISPLAY_PRICE_WITH_TAX == 'true') {
                  $type_join .= "LEFT JOIN " . TABLE_TAX_RATES . " tr on p.products_tax_class_id = tr.tax_class_id";
              }
          }

          // show products from current category or from search
          $listing_sql = "select distinct p.products_id
                                 from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, 
                                      " . TABLE_PRODUCTS_DESCRIPTION . " pd, 
                                      " . TABLE_PRODUCTS . " p 
                                      " . $type_join . "
                                where " . $where_filters . "
                                      " . $where_keywords . " 
                                      " . $where_manufacturers . " 
                                      " . $where_type . "
                                      " . $where_subcategories . "
                                      p.products_status = 1
                                  and p.products_id = p2c.products_id 
                                  and pd.products_id = p2c.products_id 
                                  and pd.language_id = '" . (int)$languages_id . "'";

          switch ($_GET['sort']) {
              case 'name':
              default:
                  $listing_sql .= " order by p.products_quantity !=0 desc, p.products_sort_order, pd.products_name";
                  break;
              case 'price_ub':
                  $listing_sql .= " order by p.products_quantity !=0 desc, products_price desc";
                  break;
              case 'price_vozr':
                  $listing_sql .= " order by p.products_quantity !=0 desc, products_price";
                  break;
              case 'new':
                  $listing_sql .= " order by p.products_quantity !=0 desc, p.products_date_added desc";
                  break;
              case 'viewed':
                  $listing_sql .= " order by p.products_quantity !=0 desc, pd.products_viewed desc";
                  break;
          }

          $listing_sql_raw = $listing_sql;
          // split query to 2 small queries: 1) find all products ids, 2) get info for each product
          $pids_price_filter_excluded = tep_get_all_pids_price_exclude($listing_sql, $price_filter_statement);
          $listing_sql = tep_get_query_products_info($listing_sql);
          // define how much products to show on page
          if ($_GET['row_by_page'] != '') {
              if ($_GET['row_by_page'] == 'all') $row_by_page = 1000;
              elseif ((int)$_GET['row_by_page'] != 0) $row_by_page = (int)$_GET['row_by_page'];
              else $row_by_page = explode(';', $template->getMainconf('MAX_DISPLAY_SEARCH_RESULTS_TITLE'))[0];
          } else {
              $row_by_page = explode(';', $template->getMainconf('MAX_DISPLAY_SEARCH_RESULTS_TITLE'))[0];
          }

          $listing_split = new splitPageResults($listing_sql, $row_by_page?:10, 'p.products_id');
          $listing_query = tep_db_query($listing_split->sql_query);

          if (is_array($all_pids)) $all_pids_string = " p.products_id in(" . implode(',', $all_pids) . ") ";
          else $all_pids_string = '';


          if ($_GET['page'] != '') $heading_text_box .= ' - ' . PREVNEXT_TITLE_PPAGE . ' ' . $_GET['page'];

          //salemaker: get salemaker prices only for products from current page (uses only one sql-query)
          $salemakers_array = get_salemakers($listing_query);
          mysqli_data_seek($listing_query, 0);

          //$milliseconds = round(microtime(true) * 1000);
          //debug(round(microtime(true) * 1000) - $milliseconds);

          // PDF view:
          if ($_GET['pdf'] == 'true') {
              require(DIR_WS_INCLUDES . '/tfpdf/tfpdf_mysql.php');
              //if(tep_db_num_rows($listing_query)>500) $listing_sql .= " limit 500";
              $pdf_page = $_GET['page'] ?: '1';
              $pdf_offset = ($pdf_page - 1) * $row_by_page;
              $listing_sql .= " limit " . $pdf_offset . "," . $row_by_page . "";
              $pdf = new PDF_MySQL_Table();
              $prop = array('HeaderColor' => array(36, 139, 203), 'color1' => array(255, 255, 255), 'color2' => array(255, 255, 255), 'padding' => 2);
              $pdf->Table($listing_sql, $prop);
              $pdf->Output();

              exit();
          }
          // END PDF view

          $row_bypage_array = array();
          $display_results_array = explode(';', ($template->getMainconf('MAX_DISPLAY_SEARCH_RESULTS_TITLE') . ';' . FILTER_ALL));
          foreach ($display_results_array as $k => $res) {
              $row_bypage_array[] = array('id' => (($res != FILTER_ALL) ? $res : 'all'), 'text' => $res);
          }

          $r_sort_array = array(array('id' => 'name', 'text' => LISTING_SORT_NAME),
              array('id' => 'price_vozr', 'text' => LISTING_SORT_PRICE1),
              array('id' => 'price_ub', 'text' => LISTING_SORT_PRICE2),
              array('id' => 'new', 'text' => LISTING_SORT_NEW),
              array('id' => 'viewed', 'text' => LISTING_SORT_POPULAR));

          $r_display_array = array(array('id' => 'list', 'text' => LISTING_SORT_LIST),
              array('id' => 'columns', 'text' => LISTING_SORT_COLUMNS));

          if ($_GET['display'] == '') $display = $template->getMainconf('LIST_DISPLAY_TYPE') == 0 ? 'list' : 'columns';
          else $display = $_GET['display'];

          if ($display == 'list') $display_hover_list = 'hover';
          elseif ($display == 'columns') $display_hover_columns = 'hover';

          // ajax-form, where we adding hidden values of current selected attributes:
          foreach ($_GET as $k => $v) {
              if ($k == 'cPath' or $k == 'f' or $k == 'language' or $k == 'manufacturers_id') {
              } elseif (is_int($k)) $m_srch .= '<input id="pl_at' . $k . '_2" type="hidden" name="' . $k . '" value="' . $v . '" />';
              else $m_srch .= '<input id="' . $k . '2" type="hidden" name="' . $k . '" value="' . $v . '" />';
          }
          $m_srch = '<form name="m_srch" id="m_srch" action="' . tep_href_link(FILENAME_DEFAULT, $m_srch_action) . '" method="get" >' . $m_srch . '</form>';
          // END ajax-form;

          // Clear $desc_text seo filter
          if(!empty($_GET['filter_id']) || !empty($_GET['manufacturers_id'])) {
              $desc_text = "";
          }
      }
  } else { // default page


      $content = CONTENT_INDEX_DEFAULT;
      $breadcrumb->add(TITLE);

  }

      //$milliseconds = round(microtime(true) * 1000);

      // attributes arrays:
  		// get all attributes list from current category

      $counts_array = array();
      $counts_may_be = array(); // array with all selected attributes
      $show_in_filter = array();
      $show_in_product_listing = array();
      // get array with all attributes names
      $show_options_sql = tep_db_query("select products_options_id, products_options_name, products_options_length, products_options_comment, products_options_sort_order from ".TABLE_PRODUCTS_OPTIONS." where (products_options_length = 1 or products_options_comment = 1) and language_id = " . $languages_id); // products_options_length != 0
      $show_options_arr = [];

      while ($show_options = tep_db_fetch_array($show_options_sql)) {
        $show_options_arr[] = array('sort'=>$show_options['products_options_sort_order'],'id'=>$show_options['products_options_id']);
      //  $show_options_arr2[$show_options['products_options_id']] = $show_options['products_options_sort_order'];
        $attr_names_array[$show_options['products_options_id']] = $show_options['products_options_name'];

        // array with options to show in filter:
        if($show_options['products_options_length']=='1') $show_in_filter[] = $show_options['products_options_id'];
        // array with options to show in product listing:
        if($show_options['products_options_comment']=='1') $show_in_product_listing[] = $show_options['products_options_id'];
      }

      // sort attributes array:
      array_multisort($show_options_arr);
      foreach ($show_options_arr as $key => $val) {
          unset($val['sort']);
          $show_options_arr[$key] = $val['id'];
      }
      if (is_array($show_options_arr)) $attr_sort_orders = array_flip($show_options_arr);  // make array with sort orders for EACH attribute

      // array with attributes(options) values
      if(is_array($show_options_arr) && $show_options_arr) {
        $show_options_vals_sql = tep_db_query("select pov.products_options_values_id, pov.products_options_values_name, products_options_values_sort_order from ".TABLE_PRODUCTS_OPTIONS_VALUES." pov, ".TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS." pov2po where pov2po.products_options_values_id = pov.products_options_values_id and pov2po.products_options_id in(".implode(',',array_unique($show_options_arr)).") and pov.language_id = " . $languages_id . "");
        while ($show_options_vals = tep_db_fetch_array($show_options_vals_sql)) {
          $show_options_vals_arr[] = array('sort'=>$show_options_vals['products_options_values_sort_order'],'id'=>$show_options_vals['products_options_values_id']);
          $attr_vals_names_array[$show_options_vals['products_options_values_id']] = $show_options_vals['products_options_values_name'];
        }

        // sort attributes values array:
        if (is_array($show_options_vals_arr)) {
          array_multisort($show_options_vals_arr);
          foreach ($show_options_vals_arr as $key => $val) {
              unset($val['sort']);
              $show_options_vals_arr[$key] = $val['id'];
          }
          $attr_vals_sort_orders = array_flip($show_options_vals_arr);  // make array with sort orders for EACH attribute value
        }
        // search attributes for specific products:
        if($listing_sql!='') {
      /*    commented in 24.12.19
          $cleared_listing_sql = str_replace($where_filters,'',$listing_sql); // clear temporary filters subqueries

          $parse_froms = explode('from', strtolower($cleared_listing_sql));
          $parse_listing_from=explode('order by', $parse_froms[count($parse_froms)-1]);
          $parse_froms[count($parse_froms)-1] = $parse_listing_from[0];
          unset($parse_froms[0]);
          $parse_froms_str = implode('from',$parse_froms);

          $new_listing = "select distinct at.options_values_id, at.options_id, at.products_id from products_attributes at, ".$parse_froms_str." and at.options_id in(".implode(',',array_unique($show_options_arr)).") and at.products_id=p.products_id";
       */

          // NOT include filters results (only current categories):
          // $new_listing = "select at.options_values_id, at.options_id, at.products_id from ".TABLE_PRODUCTS_ATTRIBUTES." at inner join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on at.products_id=p2c.products_id where ".$where_subcategories." at.options_id in(".implode(',',array_unique($show_options_arr)).")";
          // include filters results:
//          $new_listing = "select distinct at.options_values_id, at.options_id, at.products_id from ".TABLE_PRODUCTS_ATTRIBUTES." at inner join " . TABLE_PRODUCTS . " p on at.products_id=p.products_id where ".($all_pids_string?$all_pids_string.' and':'')." at.options_id in(".implode(',',array_unique($show_options_arr)).")";

            $manufacturer = !empty($_GET['filter_id']) ?(int)$_GET['filter_id']:'';
            $manufacturer = !empty($_GET['manufacturers_id']) ?( int)$_GET['manufacturers_id']:($manufacturer?:'');

            $keywords_sql = "select distinct p.products_id
                                 from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c, 
                                      " . TABLE_PRODUCTS_DESCRIPTION . " pd, 
                                      " . TABLE_PRODUCTS . " p 
                                      $type_join
                                where 
                                      " . $where_filters . " 
                                      " . $where_keywords . " 
                                      " . $where_subcategories . " 
                                      p.products_status = 1
                                  and p.products_id = p2c.products_id 
                                  and pd.products_id = p2c.products_id 
                                  and pd.language_id = '" . (int)$languages_id . "'";
            $keywords_query = tep_db_query($keywords_sql);
            $keywords_products_id = [0];
            while ($row = tep_db_fetch_array($keywords_query)){
                $keywords_products_id[]=$row['products_id'];
            }
            $productsToCurrentCat = $keywords_products_id;
            $new_listing = "select distinct at.options_values_id, at.options_id, at.products_id from ".TABLE_PRODUCTS_ATTRIBUTES." at inner join " . TABLE_PRODUCTS . " p on at.products_id=p.products_id where p.products_id in(".implode(',',$productsToCurrentCat).")".($manufacturer ? " and p.manufacturers_id = '{$manufacturer}'" :'');

        } else {

        // main page:
          $new_listing = "select distinct at.options_values_id, at.options_id, at.products_id from ".TABLE_PRODUCTS_ATTRIBUTES." at where at.options_id in(".implode(',',array_unique($show_options_arr)).")";

        }
        $listing_count_sql = tep_db_query($new_listing);

        // fill in attributes arrays:
        while ($listing_count_pids = tep_db_fetch_array($listing_count_sql)) {

            if (in_array($listing_count_pids['options_id'], $show_in_filter) or in_array($listing_count_pids['options_id'], $show_in_product_listing)) { // show options only if we turn them on in admin
                $r_pid_attr_array[$listing_count_pids['products_id']][$listing_count_pids['options_id']][$attr_vals_sort_orders[$listing_count_pids['options_values_id']]] = $attr_vals_names_array[$listing_count_pids['options_values_id']];
                $attrs_array[$attr_sort_orders[$listing_count_pids['options_id']]] = $listing_count_pids['options_id'];
                if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {

                    $counts_array[$listing_count_pids['options_id']][$listing_count_pids['options_values_id']][] = $listing_count_pids['products_id']; // for attributes count in filter

                    // get all selected attribute values to one array $counts_may_be

                    if (isset($_GET[$listing_count_pids['options_id']])) {
                        foreach (explode('-', $_GET[$listing_count_pids['options_id']]) as $val) {
                            if ($listing_count_pids['options_values_id'] == $val) {
                                $counts_may_be[$listing_count_pids['options_id']][$listing_count_pids['options_values_id']][] = $listing_count_pids['products_id'];
                            }
                        }
                    } //*/

                } else {
                    $attr_vals_array[$listing_count_pids['options_id']][$listing_count_pids['options_values_id']] = $attr_vals_sort_orders[$listing_count_pids['options_values_id']];
                }
            }
        }

        if (is_array($attrs_array)) {
          $attrs_array = array_unique($attrs_array); // delete attributes - duplicates
          ksort($attrs_array); // sort array by sort_order
        }

      }
        $options_string = generateOptionsString($attr_names_array, $attr_vals_names_array);
        if(!empty($options_string)) {
            $heading_text_box .= $options_string;
        }

      // --------------- ATTRIBUTES COUNTER IN FILTER ----------------------------- //
        if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {

            require(DIR_WS_MODULES . '/attributes_counter.php');
        }
      // --------------- ATTRIBUTES COUNTER IN FILTER --END------------------------ //

  		// END get list of all attributes from current category

      if($_SESSION['view_order_success']) {
          unset($_SESSION['view_order_success']);
      }

if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
          require(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
      } else {
          require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);
      }




require(DIR_WS_INCLUDES . 'application_bottom.php');
?>