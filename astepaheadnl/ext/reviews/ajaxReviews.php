<?php
if (empty($_POST['action'])) die;

$rootPath = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])));
chdir('../../');
require('includes/application_top.php');
require('ext/reviews/reviews.php');
require('includes/languages/'.$language.'/product_info.php');
$reviews = new reviews();
//$reviews->setLanguageId($languages_id);
$postData = tep_db_prepare_input($_POST);
switch ($_POST['action']):
    case 'addReview':
        if (GOOGLE_RECAPTCHA_STATUS === 'true' && file_exists(DIR_WS_EXT . "recaptcha/recaptcha.php")) {
            if ($_SESSION['recaptcha'] !== true) {

                echo json_encode(
                    [
                        'html'   => '<div class = "h3">ReCaptcha error</div>',
                        'count'  => "({$rating['count']})",
                        'rating' => Reviews::drawRatingBlock($rating['average']).Reviews::drawQuantityBlock($rating['count'],TEXT_REVIEWSES)
                    ]
                );
                die;
            }
        }
        $data = [
          'review' => [
            'products_id'=>$postData['pid'],
            'customers_id'=>$postData['cid'],
            'customers_name'=>$postData['name'],
            'reviews_rating'=>$postData['rating'],
            'date_added'=>date('Y-m-d H:i:s'),
            'reviews_type'=>1,
          ],
          'review_desc' => [
              'reviews_text'=>$postData['text'],
              'languages_id'=>(int)$languages_id,
          ],
        ];
        $reviews->addReview($data);
        $reviews->setProductId($_POST['pid']);
        $reviews->getReviewContainer();
        $rating = Reviews::count_comments($postData['pid']);

        echo json_encode(
            [
                'html'   => $reviews->reviews_blocks . $reviews->pagination,
                'count'  => "({$rating['count']})",
                'rating' => Reviews::drawRatingBlock($rating['average']).Reviews::drawQuantityBlock($rating['count'],TEXT_REVIEWSES)
            ]
        );
        break;
    case 'getMore':
        if (!empty($_POST['pid'])) $reviews->setProductId($_POST['pid']);
        $reviews->getReviewContainer();
        echo json_encode(
          ['html'=>$reviews->reviews_blocks.$reviews->pagination]
        );
        die;
    case 'getAllMore':
        $reviews->setReviewsPerPage(20); // 1 - products
        $reviews->setShowImage(true); // 1 - products
        $reviews->getAllReviewContainer();
        echo json_encode(
          ['html'=>$reviews->reviews_blocks, 'lastPage'=>$reviews->lastPageArrived]
        );
    default:
        die;
endswitch;