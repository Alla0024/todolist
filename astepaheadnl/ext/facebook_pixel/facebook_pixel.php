<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
define('DEFAULT_PIXEL_CURRENCY', defined('CURRENCY_CODE') ? CURRENCY_CODE : 'USD');

function facebook_pixel($data="")
{
    echo '
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,"script","https://connect.facebook.net/en_US/fbevents.js");
        fbq("init", "'.FACEBOOK_PIXEL_ID.'");
        '.$data.'
        fbq("track", "PageView");
        $("body").append(\'<img alt="pixel" height="1" width="1" style="display:none;" src="https://www.facebook.com/tr?id='.FACEBOOK_PIXEL_ID.'&ev=PageView&noscript=1"/>\');';
}

// ���� ��������� �� �������� ��������
if($_GET['products_id'])
{
    $category_id_query = tep_db_query("SELECT categories_id FROM " . TABLE_PRODUCTS_TO_CATEGORIES . " WHERE products_id ='".(int)$_GET['products_id']."'");
    $category = $category_id_query->fetch_assoc();

    $data = "fbq('track', 'ViewContent', { 
               content_type: 'product',
               content_ids: ['".$_GET['products_id']."'],
               content_name: '".addslashes($product_info['products_name'])."',
               content_category: '".addslashes(tep_get_category_name($category['categories_id'], $languages_id))."',
               value: ".round(tep_add_tax($product_info['products_price'], tep_get_tax_rate($product_info['products_tax_class_id'])), 2).",
               currency: '".DEFAULT_PIXEL_CURRENCY."'
            });";

    facebook_pixel($data);
}
else if($_GET['order_id']) {
    $o_id = tep_db_prepare_input($_GET['order_id']);

    $order_products_query = tep_db_query("SELECT products_id, products_name, products_price, products_tax, products_quantity
                                                          FROM " . TABLE_ORDERS_PRODUCTS . "
                                                          WHERE orders_id = '" . $o_id . "'");

    $total_order = 0;
    $products_purchased_array = array();
    while ($order_row = $order_products_query->fetch_assoc())
    {
        $total_order += ($order_row['products_price'] * $order_row['products_quantity']) + $order_row['products_tax'];
        $products_purchased_array[] = "{id: ".$order_row['products_id'].", quantity: ".$order_row['products_quantity']."}";
    }


    $data = "fbq('track', 'Purchase',
              {
                value: ".$total_order.",
                currency: '".DEFAULT_PIXEL_CURRENCY."',
                contents: [
                  ".implode(', ', $products_purchased_array)."
                  ],
                content_type: 'product'
              }
            );";
    facebook_pixel($data);
}
else
{
    facebook_pixel();
}
