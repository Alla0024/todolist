<?php

if(!defined('FACEBOOK_PIXEL_MODULE_ENABLED'))
{
    /**
     * check.php -- ���� ��� ��������������� ���������� �������� ������ � ��
     */
    tep_db_query("INSERT INTO ".TABLE_CONFIGURATION."
        (configuration_title,
         configuration_key,
         configuration_value,
         configuration_description,
         configuration_group_id,
         sort_order,
         date_added,
         last_modified,
         use_function,
         set_function)
         VALUES
        ('FaceBook pixel',
        'FACEBOOK_PIXEL_MODULE_ENABLED',
        'facebook_pixel:false',
        'FaceBook pixel',
        '277',
        '1',
        '".date('Y-m-d H:i:s')."',
        '".date('Y-m-d H:i:s')."',
        'tep_check_modules_folder',
        'tep_cfg_select_option(array(\"facebook_pixel:true\", \"facebook_pixel:false\"),')
        ");

    tep_db_query("INSERT INTO " . TABLE_CONFIGURATION . "
        (configuration_title,
        configuration_key,
        configuration_value,
        configuration_description,
        configuration_group_id,
        sort_order,
        last_modified,
        date_added,
        use_function,
        set_function) 
        VALUES 
        ('Master Password', 
        'FACEBOOK_PIXEL_ID', 
        '', 
        'Facebook pixel ID', 
        1, 
        1002, 
        '".date('Y-m-d H:i:s')."', 
        '".date('Y-m-d H:i:s')."', 
        NULL, 
        NULL)");

    $module_installed = true;
}