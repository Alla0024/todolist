<?php

if(!defined('CARDS_ENABLED')) {
    tep_db_query("
        INSERT INTO " . TABLE_CONFIGURATION . "
        (configuration_title,
         configuration_key,
         configuration_value,
         configuration_description,
         configuration_group_id,
         sort_order,
         date_added,
         last_modified,
         use_function,
         set_function)
         VALUES
        ('Оплата картами',
        'CARDS_ENABLED',
        'acquiring:false',
        'Модуль оплаты картами',
        '277',
        '1',
        NOW(),
        NOW(),
        'tep_check_modules_folder',
        'tep_cfg_select_option(array(\"acquiring:true\", \"acquiring:false\"),')
    ");
}