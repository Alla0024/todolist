<?php 

  $r_mycode = $_POST['gv_redeem_code'];  
    
	if($r_mycode!='') { 
  	$coupon_array = redeemCoupon($r_mycode);
    $coupon_array = json_decode($coupon_array);

    if($coupon_array->success=='true') $coupon_text = '<div class="coupon_response_valid">'.SUB_TITLE_COUPON_VALID.'</div>';
    else $coupon_text = '<div class="coupon_response_invalid">'.$coupon_array->message.'</div>'; // '.SUB_TITLE_COUPON_INVALID.'<br />

  }
  
function redeemCoupon($code)
{
    //BOF KGT

    if (MODULE_ORDER_TOTAL_COUPON_STATUS == 'true') {

        //EOF KGT
        global $customer_id, $order, $credit_covers, $order_total_modules,$cart,$currencies;
        $error = false;
        if ($code) {
            // get some info from the coupon table
            $coupon_query = tep_db_query("select coupon_id, coupon_amount, coupon_type, coupon_minimum_order,uses_per_coupon, uses_per_user, restrict_to_products, restrict_to_categories from " . TABLE_COUPONS . " where coupon_code='" . $code . "' and coupon_active='Y'");
            $coupon_result = tep_db_fetch_array($coupon_query);

            if ($coupon_result['coupon_type'] != 'G') {
                if (tep_db_num_rows($coupon_query) == 0) {
                    $error = true;
                    $errMsg = ERROR_NO_INVALID_REDEEM_COUPON;
                }

                if ((float)$coupon_result['coupon_minimum_order'] > 0 && $cart->show_total() < (float)$coupon_result['coupon_minimum_order']){
                    $error = true;
                    $errMsg = sprintf(ERROR_MINIMUM_CART_AMOUNT,$currencies->display_price($coupon_result['coupon_minimum_order'],0,1,true));

                }

                $date_query = tep_db_query("select coupon_start_date from " . TABLE_COUPONS . " where coupon_start_date <= now() and coupon_code='" . $code . "'");
                if (tep_db_num_rows($date_query) == 0) {
                    $error = true;
                    $errMsg = ERROR_INVALID_STARTDATE_COUPON;
                }

                $date_query = tep_db_query("select coupon_expire_date from " . TABLE_COUPONS . " where coupon_expire_date >= now() and coupon_code='" . $code . "'");
                if (tep_db_num_rows($date_query) == 0) {
                    $error = true;
                    $errMsg = ERROR_INVALID_FINISDATE_COUPON;
                }

                $coupon_count = tep_db_query("select coupon_id from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . $coupon_result['coupon_id'] . "'");
                $coupon_count_customer = tep_db_query("select coupon_id from " . TABLE_COUPON_REDEEM_TRACK . " where coupon_id = '" . $coupon_result['coupon_id'] . "' and customer_id = '" . $customer_id . "' and customer_id>0");
                if (tep_db_num_rows($coupon_count) >= $coupon_result['uses_per_coupon'] && $coupon_result['uses_per_coupon'] > 0) {
                    $error = true;
                    $errMsg = ERROR_INVALID_USES_COUPON . $coupon_result['uses_per_coupon'] . TIMES;
                }

                if (tep_db_num_rows($coupon_count_customer) >= $coupon_result['uses_per_user'] && $coupon_result['uses_per_user'] > 0) {
                    $error = true;
                    $errMsg = ERROR_INVALID_USES_USER_COUPON . $coupon_result['uses_per_user'] . TIMES;
                }
                
                foreach(array_column($order->products, 'id') as $pid) {
                  $pids[] = tep_get_prid($pid);
                }

                if ($coupon_result['restrict_to_categories']) {

                  $cat_query = tep_db_query("select products_id from products_to_categories where products_id in(" . implode(',',$pids) . ") and categories_id in(" . $coupon_result['restrict_to_categories'] . ")");
                  if (tep_db_num_rows($cat_query) ==0 ) {
                    $error = true;
                    $errMsg = ERROR_NO_INVALID_REDEEM_COUPON;
                  }
                }
            
                if ($coupon_result['restrict_to_products']) {
                  $coupon_result['restrict_to_products'] = preg_replace('/\s+/', '', $coupon_result['restrict_to_products']);
                  $products_array = explode(',',$coupon_result['restrict_to_products']);
                  $intersect = array_intersect($products_array,$pids);
                  if(empty($intersect)) {
                    $error = true;
                    $errMsg = ERROR_NO_INVALID_REDEEM_COUPON;
                  }  
                }    
                
                if ($error === false) {
                    global $order_total_modules, $cc_id;
                    $cc_id = $coupon_result['coupon_id'];
                    if (!tep_session_is_registered('cc_id')) tep_session_register('cc_id');
                    $_SESSION['cc_id'] = $cc_id;
                    $order_total_modules->pre_confirmation_check();
                    if (!tep_session_is_registered('credit_covers')) {
                        tep_session_register('credit_covers');
                        $credit_covers = true;
                    }
                    return '{"success": "true"}';
                } else {
                    return '{"success": "false","message":"'.$errMsg.'"}';
                    if (tep_session_is_registered('credit_covers')) tep_session_unregister('credit_covers');
                }
            }
        }
        //BOF KGT
    } 
    //EOF KGT
    return '{"success": "false","message":"7. end"}';
}
  
?>