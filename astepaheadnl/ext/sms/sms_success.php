<?php
if (SMS_ENABLE) {

    $customerPhoneNumber = formatPhoneNumber($_SESSION['onepage']['customer']['telephone']);
    if ($customerPhoneNumber) {

        $enc_terminal = SMS_ENC;
        $client       = new SoapClient('http://vipsms.net/api/soap.html');
        $res          = $client->auth(SMS_LOGIN, SMS_PASSWORD);
        $sessid       = $res->message;
        $text         = HEADING_TITLE . ' #' . $_GET['order_id'] . '. ' . SMS_OWNER_TEL;
        if(defined('SMS_TEXT') && SMS_TEXT) {
            $text = SMS_TEXT;
        }

        if (SMS_CUSTOMER_ENABLE) {
            $res = $client->sendSmsOne($sessid, $customerPhoneNumber, SMS_SIGN, $text);
        }
        if (SMS_OWNER_ENABLE) {
            $res = $client->sendSmsOne($sessid, SMS_OWNER_TEL, SMS_SIGN, SMS_NEW_ORDER . ': #' . $_GET['order_id'] . '!');
        }
    }
}
?>