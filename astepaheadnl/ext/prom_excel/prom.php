<?php

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();
$exportLog = '';
$allowed_image_types = ['image/jpeg','image/gif','image/png','image/webp'];
$allowed_table_types = ['application/octet-stream','text/csv','application/excel','application/vnd.ms-excel','application/x-excel','application/x-msexcel','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

if (tep_not_null($_FILES) && in_array($_FILES['excelfile']['type'],$allowed_table_types)){
    $file_path = $_FILES['excelfile']['tmp_name'];
    $current_currency = $_POST['currency']?:'USD';
    $current_lang = $_POST['language']?:1;
    $upload_images = isset($_POST['uploadImages']);
    $truncate_table = isset($_POST['truncateTable']);
    include_once ('../ext/prom_excel/promua.php');
}

include_once('html-open.php');
include_once('header.php');
?>
<!--    <div class="container">-->
<!--        <div class="col-md-12">-->
<!--            --><?php //include __DIR__ . "/../../admin/includes/material/blocks/tabs/import_export.php" ?>
<!--        </div>-->
<!--    </div>-->
    <div class="container">
        <h1 class="h1">Импорт из Excel Prom.ua</h1>

        <?php if (tep_not_null($exportLog)){ ?>
        <?= $exportLog ?>
        <?php } else { ?>
        <form enctype="multipart/form-data" method="post">

            <div class="form-group"><label for="excelfile">Выберите .xlsx файл:</label>
                <input type="file" required class="form-control" name="excelfile" id="excelfile" accept=".xlsx">
            </div>
            <label for="currency">Основная валюта:</label>
            <select name="currency" class="form-group" required>
                <?php foreach ($currencies->currencies as $cur=>$data): ?>
                    <option value="<?=$cur?>"><?=$cur?></option>
                <?php endforeach; ?>
            </select>
            <label for="language">Основной язык:</label>
            <select name="language" class="form-group" required>
                <?php foreach ($languages as $data): ?>
                    <option value="<?=$data['id']?>"><?=$data['name']?></option>
                <?php endforeach; ?>
            </select>
            <div class="form-group">
                <input type="checkbox" name="uploadImages" id="uploadImages">
                <label for="uploadImages">Импортировать изображения? (займёт много времени)</label>
            </div>
            <div class="form-group">
                <input type="checkbox" name="truncateTable" id="truncateTable">
                <label for="truncateTable">Очистить текущую базу данных от товаров/категорий/производителей/аттрибутов</label>
            </div>
            <button type="submit" class="btn btn-info btn-rounded">Загрузить</button>
        </form>
    <?php } ?>
    </div>
<?
require('footer.php');
require('html-close.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');

