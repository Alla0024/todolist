<?php
// last viewed
if (is_array($_SESSION['visited_products2']) and count($_SESSION['visited_products2'])) {
    $listing_sql = "SELECT p.products_id
                     FROM " . TABLE_PRODUCTS . " p      
                LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.products_id = p2c.products_id                                   
                          " . ($new_products_from ? : '') . " 
                    WHERE p2c.categories_id in(" . $all_active_cats . ")                             
                      AND p.products_id in(" . implode(',', $_SESSION['visited_products2']) . ") 
                      AND p.products_status = '1' 
                 ORDER BY  " . ($tpl_settings['orderby'] ? : "FIND_IN_SET(p.products_id,'" . implode(',', array_reverse($_SESSION['visited_products2'])) . "')") . " 
                           " . ($tpl_settings['limit'] ? 'LIMIT ' . $tpl_settings['limit'] : '');

    $last_viewed_query = tep_get_query_products_info($listing_sql); // split query to 2 small queries: 1) find all products ids, 2) get info for each product

    $last_viewed = tep_db_query($last_viewed_query);
    $salemakers_array = get_salemakers($last_viewed);
    mysqli_data_seek($last_viewed, 0);

    if ($last_viewed->num_rows) {
        $tpl_settings['request'] = $last_viewed;
        include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
    }
}?>
