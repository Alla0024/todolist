<?php
if (is_file(__DIR__ . '/' . TEMPLATE_NAME . '/' . '/quick_order.php')) {
    require_once __DIR__ . '/' . TEMPLATE_NAME . '/' . '/quick_order.php';
} else {
    require_once __DIR__ . '/default/quick_order.php';
}