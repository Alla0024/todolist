<?php
/**
 * Created by PhpStorm.
 * User: 'Serhii.M'
 * Date: 12.04.2019
 * Time: 12:18
 */
ini_set('max_execution_time','0');

if (empty($file_path)){
    die('empty filepath');
}
$imageFolder = DIR_FS_CATALOG_IMAGES;
$file = $file_path;
$data = simplexml_load_file($file);
$lng = new language();
$upload_images = isset($upload_images)?$upload_images:false;
$truncate_table = isset($truncate_table)?$truncate_table:false;
$productsCounter = $categoriesCounter = $manufacturersCounter = $attributesCounter = $attrValuesCounter = 0;
$manufacturers = $products_manufacturers = [];
if ($truncate_table) {
    tep_db_query("TRUNCATE TABLE `products`;");
    tep_db_query("TRUNCATE TABLE `products_attributes`");
    tep_db_query("TRUNCATE TABLE `products_attributes_download`;");
    tep_db_query("TRUNCATE TABLE `products_attributes_groups`");
    tep_db_query("TRUNCATE TABLE `products_description`;");
    tep_db_query("TRUNCATE TABLE `products_notifications`");
    tep_db_query("TRUNCATE TABLE `products_options`;");
    tep_db_query("TRUNCATE TABLE `products_options_values`");
    tep_db_query("TRUNCATE TABLE `products_options_values_to_products_options`;");
    tep_db_query("TRUNCATE TABLE `products_to_categories`");
    tep_db_query("TRUNCATE TABLE `products_xsell`;");
    tep_db_query("TRUNCATE TABLE `categories_description`");
    tep_db_query("TRUNCATE TABLE `categories`;");
    tep_db_query("TRUNCATE TABLE `manufacturers`;");
    tep_db_query("TRUNCATE TABLE `manufacturers_info`;");
}
//-------------------currencies--------------------------------
$currencies_array = array();
foreach ($data->shop->currencies as $currencies_arr) {
    foreach ($currencies_arr as $currency) if($rate = intval($currency['rate'])){
        $currency_name = strval($currency['id']);
        $currencies_array[$currency_name] = $rate;
    }
}

$categories_array = array();
//----------------------categories--------------------------------
foreach ($data->shop->categories->category as $row) {
    $id = intval($row['id']);
    $parent_id = intval($row['parentId']);
    $name = strval($row);
    $categories_array[$parent_id][$id] = $name;
}
ksort($categories_array);
$categories_array = tep_db_prepare_input($categories_array);
foreach ($categories_array as $parent_id => $childs) {
    foreach ($childs as $child_id => $name) {
        $categoriesCounter++;
        $categories_array = [
            'categories_id'=>$child_id,
            'parent_id'=>$parent_id,
            'categories_status'=>1,
        ];
        tep_db_perform(TABLE_CATEGORIES,$categories_array, 'insertodku');
        foreach ($lng->catalog_languages as $lang) {
            $categories_description_array = [
                'categories_id'=>$child_id,
                'categories_name'=>$name,
                'language_id'=>$lang['id'],
            ];
            tep_db_perform(TABLE_CATEGORIES_DESCRIPTION,$categories_description_array, 'insertodku');
        }
    }
}
//}
//----------------------products--------------------------------
$i = 0;
$attr_array = $attributes_array = array();

$imageDownloadCounter = 0;
function saveImg($url,$download = false) {
    global $imageFolder,$imageDownloadCounter;
    $imageFolder = $imageFolder ?: DIR_WS_IMAGES;
    $filename_arr = explode('/', $url);
    $filename = end($filename_arr);
    if (!$download) return $filename;
    if (!file_exists($imageFolder . 'products/' . $filename)) {
        $imageDownloadCounter++;
        $url = explode('/',$url);
        $url[count($url)-1] = str_replace('+','%20',urlencode($url[count($url)-1]));
        $url = trim(implode('/',$url));
        $img = file_get_contents($url);
        $imagesize = getimagesize($url);
        $end = '';
        if ($imagesize["mime"] == 'image/gif') {
            $end = ".gif";
        } elseif ($imagesize["mime"] == 'image/png') {
            $end = ".png";
        } elseif ($imagesize["mime"] == 'image/jpeg') {
            $end = ".jpg";
        }
        $filename = explode('.', $filename);
        array_pop($filename);
        $filename = implode('_', $filename) . $end;
        if (!file_exists($imageFolder . 'products/')) {
            mkdir($imageFolder . 'products/');
        }
        $path = $imageFolder . 'products/' . $filename;
        file_put_contents($path, $img);
    }
    return $filename;
}

function priceToFloat($price){
    $stringPrice = (string)$price;
    $stringPrice = str_replace(',','.',$stringPrice); //replace invalid comma
    return (float)$stringPrice;
}

foreach ($data->shop->offers->offer as $row) {
    $product_attributes = array();
    foreach ($row->param as $attribute){
        $name = (string)$attribute['name'];
        $unit = (string)$attribute['unit'];
        $fullname = $name.($unit?', '.$unit:'');
        $product_attributes[$fullname] = (string)$attribute;
        $attributes_array[$fullname][(string)$attribute] = (string)$attribute;
    }
    $images = array();
    foreach ($row->picture as $picture){
        $images[] = saveImg(strval($picture),$upload_images);
    }

    // id - идентификатор предложения.
    $id = intval($row['id']);

    // available - статус товара «в наличии» / «на заказ».
    $available = intval((bool)$row['available']);

    // url - URL страницы товара на сайте магазина.
    $url = strval($row->url);


    // oldprice - старая цена.
    //    $oldprice = strval($row->oldprice);
    //    $oldprice = priceToFloat($row->price_old);

    // currencyId - валюта.
    $currencyId = strval($row->currencyId);
    // price - актуальная цена.
    $price = priceToFloat($row->price);
    //    $price = priceToFloat($row->price) * $currencies_array[$currencyId];

    // currencyId - идентификатор категории товара.
    $categoryId = intval($row->categoryId);

    $quantity = intval($row->stock_quantity);

    // name - название товара.
    $name = strval($row->name);

    // vendor - название производителя.
    $vendor = strval($row->vendor);


    // description - описание.
    $description = nl2br(strval($row->description));


    $manufacturers[$id] = $vendor;
    $products_manufacturers[$vendor] = $vendor;

    $products_array = [
        'products_id'=>$id,
        'products_model'=>$id,
        'products_status'=>$available,
        'products_quantity'=>$quantity,
        'products_image'=>implode(';',$images),
        'products_price'=>$price
    ];
    tep_db_perform(TABLE_PRODUCTS,$products_array, 'insertodku');
    $productsCounter++;
    $pId = $id;
    $products_to_categories_array = [
        'categories_id'=>$categoryId,
        'products_id'=>$pId,
    ];
    tep_db_perform(TABLE_PRODUCTS_TO_CATEGORIES,$products_to_categories_array, 'insertodku');
    foreach ($lng->catalog_languages as $lang) {
        $products_descriptions = [
            'products_id' => $pId,
            'products_name' => $name,
            'products_description' => $description,
            'language_id' => $lang['id']
        ];
        tep_db_perform(TABLE_PRODUCTS_DESCRIPTION, $products_descriptions, 'insertodku');
    }
    $attr_array[$pId] = $product_attributes;
    $i++;
    //    if ($i == $limit){
    //        break;
    //        var_dump($attributes_array);die;
    //        die;
    //    }
}
$option_name_to_id = array();
$option_values_name_to_id = array();
$option_id = (int)tep_db_fetch_array(tep_db_query("SELECT MAX(`products_options_id`) as max FROM products_options"))['max'];
foreach ($attributes_array as $option_name => $values_array){
    $option_id++;
    foreach ($lng->catalog_languages as $lang) {
        $option = [
            'products_options_id' => $option_id,
            'language_id' => $lang['id'],
            'products_options_name' => $option_name,
            'products_options_type' => 0
        ];
        tep_db_perform(TABLE_PRODUCTS_OPTIONS, $option, 'insertodku');
    }
    $attributesCounter++;
    //        $option_id = tep_db_insert_id();
    $option_name_to_id[$option_name] = $option_id;
    foreach ($values_array as $value){
        foreach ($lng->catalog_languages as $lang) {
            $oValues = [
                'language_id' => $lang['id'],
                'products_options_values_name' => $value
            ];
            if (isset($ovalue_id)){
                $oValues['products_options_values_id'] = $ovalue_id;
            }
            tep_db_perform(TABLE_PRODUCTS_OPTIONS_VALUES, $oValues, 'insertodku');
            $ovalue_id = tep_db_insert_id();
        }
        $attrValuesCounter++;
        $option_values_name_to_id[$option_id][$value] = $ovalue_id;
        $optionsValuesToOption = [
            'products_options_id'=>$option_id,
            'products_options_values_id'=>$ovalue_id
        ];
        tep_db_perform(TABLE_PRODUCTS_OPTIONS_VALUES_TO_PRODUCTS_OPTIONS,$optionsValuesToOption, 'insertodku');
        unset($ovalue_id);
    };

}
foreach ($attr_array as $pId=>$attr){
    foreach ($attr as $option_name=>$option_value){
        $oId = $option_name_to_id[$option_name];
        $ovId = $option_values_name_to_id[$oId][$option_value];
        $prod_attr_array = [
            'products_id'=>$pId,
            'options_id'=>$oId,
            'options_values_id'=>$ovId,
            'options_values_price'=>'',
            'pa_qty'=>1
        ];
        tep_db_perform(TABLE_PRODUCTS_ATTRIBUTES,$prod_attr_array, 'insertodku');
    }
}
$brand_name_to_id_array = [];
if ($products_manufacturers) {
    $manufacturersQuoted = array_map(
        function ($m) {
            return "'{$m}'";
        },
        $products_manufacturers
    );
    $checkManufacturer = tep_db_query(
        "SELECT manufacturers_id FROM " . TABLE_MANUFACTURERS . " WHERE manufacturers_name in (" . implode(
            ',',
            $manufacturersQuoted
        ) . ")"
    );

    foreach ($products_manufacturers as $manufacturer_name) {
        $manufacturersCounter++;
        $manufacturer_array = [
            'manufacturers_name' => $manufacturer_name,
            'status' => 1
        ];
        $checkManufacturer = tep_db_query(
            "SELECT manufacturers_id FROM " . TABLE_MANUFACTURERS . " WHERE manufacturers_name = '{$manufacturer_name}'"
        );
        if ($checkManufacturer->num_rows) {
            $manufacturer_array['manufacturers_id'] = tep_db_fetch_array($checkManufacturer)['manufacturers_id'];
        }
        tep_db_perform(TABLE_MANUFACTURERS, $manufacturer_array, 'insertodku');
        $mId = tep_db_insert_id();
        foreach ($lng->catalog_languages as $lang) {
            $manufacturer_info_array = [
                'manufacturers_id' => $mId,
                'languages_id' => $lang['id']
            ];
            $brand_name_to_id_array[$manufacturer_name] = $mId;
            tep_db_perform(TABLE_MANUFACTURERS_INFO, $manufacturer_info_array, 'insertodku');
        }
    }
    foreach ($manufacturers as $pId => $manufacturer_name) {
        $mId = $brand_name_to_id_array[$manufacturer_name];
        tep_db_query("UPDATE products SET manufacturers_id = $mId WHERE products_id = '{$pId}'");
    }
}
$exportLog = <<<EXPORTLOG
Добавлено товаров: $productsCounter      </br>
Добавлено категорий: $categoriesCounter    </br>
Добавлено производителей: $manufacturersCounter </br> 
Добавлено атрибутов: $attributesCounter    </br>
Добавлено значений атрибутов: $attrValuesCounter    </br>

EXPORTLOG;
