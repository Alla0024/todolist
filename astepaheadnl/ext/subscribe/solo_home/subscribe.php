<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-12">
    <div class="subscribe_news">
        <div class="row">
            <div class="col-lg-4 col-sm-4 col-xs-12">
                <p><?php echo HOME_MAIN_NEWS_SUBSCRIBE; ?></p>
            </div>
            <div class="col-lg-8 col-sm-8 col-xs-12">
                <form class="form_subscribe_news" action="subscripbe.php" method="POST">
                    <input type="hidden" name="podpiska" value="yes">
                    <input type="email" class="form-control form_subscribe_input" required autocomplete="off"
                           placeholder="<?php echo HOME_MAIN_NEWS_EMAIL; ?>" name="email_address">
                    <button type="submit" class="btn btn-default"><?php echo MAIN_NEWS_SUBSCRIBE_BUT; ?></button>
                </form>
            </div>
        </div>
    </div><!-- END NEWS SUBMIT FORM -->
</div>