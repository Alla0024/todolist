<?php 
    
    function getLabel($listing) {
        global $spec_price;
      if ($listing['lable_1']) {
          $label = LABEL_TOP;
          $label_class = 1;   
      } elseif ($listing['lable_2']) {
          $label = LABEL_NEW;
          $label_class = 2;
      } elseif ($listing['lable_3']) {
          
          $label_class = 3;
          // Discount in % if sales module enables
          if ( $spec_price!='' && ((double) $listing['products_price']) ) {
              $discount = '-' . round((($listing['products_price'] - $spec_price) / $listing['products_price']) * 100) . '%';
          } else {
              $discount = '';
          }
          $label = $discount ?: LABEL_SPECIAL;
      } else {    
          $label = '';             
          $label_class = '';
      }
      
      return array('name'=>$label,'class'=>$label_class);
    }
?>