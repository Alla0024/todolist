jQuery(document).ready(function() {

    /* WISHLIST */
    $(document).on('click', '.wishlisht_button', function(event) {
        event.preventDefault();

        var $data = $(this).data();
        var $text = $(this).find('label');
        var $input = $(this).find('input');
        $.ajax({
            url: './ext/wishlist/ajax_wishlist.php',
            type: 'POST',
            dataType: 'json',
            data: {
                request: 'wishlist',
                action: $input.is(':checked')?'delete':'add',
                id: $data.id
            },
            success: function(callback) {
                $text.html(callback.text);
                $input.is(':checked')?$input.prop('checked', false):$input.prop('checked', true);
                $('[name="wishlist_'+$data.id+'"]').prop('checked',$input.is(':checked'));
                // Wishlish box
                $.get('./ext/wishlist/r_wishlist_box2.php', {
                    method: 'ajax',
                    wishlist_id: $data.id
                }, function(data2) {
                    //	if (data2 == '') $('#wishlist_box2').fadeOut(300);
                    //	else {
                    $('#wishlist_box2').replaceWith(data2);
                    //	if ($('#wishlist_box2').css('display') == 'none') $('#wishlist_box2').fadeIn(300);
                    //	}
                });
            }
        });
    });
    /* /WISHLIST */
});
