<?php
/**
 * Created by PhpStorm.
 * User: Juliya
 * Date: 03.04.2019
 * Time: 10:11
 */

$lang = isset($_POST['lang']) ? $_POST['lang'] : 'en';
switch ($lang) {
    case 'ru':
        define('FOLDER_PERMISSION', 'Права на запись');
        define('RECOMMENDED_TO_INSTALL', 'Рекомендуется при инсталляции установить в');
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Рекомендуем при инсталляции на сервере включить следующие директивы для PHP'
        );
        define('REFRESH_PAGE', 'Обновите страницу');
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Мы обнулили sql_mode для текущей сесси в MYSQL. 
            <br/>Это сделано для успешной установки базы данных и начального использования магазина. 
            <br/>Ваши значения - %s вернутся после перезагрузки mysql сервера. 
            <br/>Для успешной работы магазина рекомендуем вам обнулить переменную sql_mode на сервере'
        );
        define('DB_LOGIN_ERROR', 'Поле "Имя пользователя" обязательно для заполнения');
        define('DB_PASS_ERROR', 'Поле "Пароль" обязательно для заполнения');
        define('DB_HOST_ERROR', 'Поле "Хост" обязательно для заполнения');
        define('DB_NAME_ERROR', 'Поле "Имя базы данных" обязательно для заполнения');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz Существует');
        define('INSTALL_REQUIREMENTS', 'Требования к установке');
        define(
            'MYSQL_ERORO',
            'Ошибки в файле базы данных или версия Mysql не подходит 
            для данного установщика, для продолжения устанвки нужно исправить следующую ошибку "%s"'
        );
        define(
            'DB_MAX_ALLOWED_PACKET',
            'Размер файла с базой данных больше, чем в переменной max_allowed_packet, 
            увеличьте значение max_allowed_packet до "%s" на сервере MySQL'
        );
        define('EXT_PHAR', 'Расширение Phar');
        define('EXT_CURL', 'Расширение Curl');
        define('PHP_VERSION_CHECK', 'Версия PHP >= ' . errors::PHP_MIN_VERSION  . ' или <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'Расширение MySQLi');
        define('STEP_1', 'Выбрать язык');
        define('STEP_2', 'Настройка базы данных');
        define('STEP_3', 'Общие настройки');
        define('STEP_4', 'Установка завершена!');
        define('TITLE', 'Установить магазин Solomono');
        define('SELECT_LANGUAGE', 'Выбрать язык');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Ваш язык');
        define('STEP_1_TEXT_3', 'Пожалуйста, выберите ваш язык');
        define('NEXT_STEP', 'Следующий шаг');
        define('STEP_2_TEXT_1', 'Настройка базы данных');
        define(
            'STEP_2_TEXT_2',
            'Пожалуйста, введите свои учетные данные базы данных. 
            Если вы не знаете эту информацию, пожалуйста, свяжитесь с вашим хостингом.'
        );
        define('STEP_2_FORM_INPUT_1', 'Хост базы данных');
        define('STEP_2_FORM_INPUT_HELP_1', 'Скорее всего, localhost');
        define('STEP_2_FORM_INPUT_2', 'Имя базы данных');
        define('STEP_2_FORM_INPUT_HELP_2', 'Имя базы данных');
        define('STEP_2_FORM_INPUT_3', 'Имя пользователя базы данных');
        define('STEP_2_FORM_INPUT_HELP_3', 'Имя базы данных, обычно root');
        define('STEP_2_FORM_INPUT_4', 'Пароль базы данных');
        define('STEP_2_FORM_INPUT_HELP_4', 'Оставьте пустым для пустого пароля');
        define('STEP_2_FORM_INPUT_5', 'Подпапка веб-сайта');
        define('STEP_2_FORM_INPUT_6', 'Папка администратора');
        define('STEP_3_TEXT_1', 'Общие настройки');
        define('STEP_3_TEXT_2', 'Пожалуйста, введите общую информацию о вашем сайте');
        define('STEP_3_FORM_INPUT_1', 'Имя магазина');
        define('STEP_3_FORM_INPUT_HELP_1', 'Название сайта');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'Адрес электронной почты администратора');
        define('STEP_3_FORM_INPUT_3', 'Пароль');
        define('STEP_3_FORM_INPUT_HELP_3', 'Пароль администратора');
        define('STEP_3_FORM_INPUT_4', 'Использовать HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Удалить продукты по умолчанию');
        define('STEP_4_TEXT_1', 'Поздравляем, ваш магазин готов!');
        define('STEP_4_TEXT_2', 'Ссылка на ваш магазин');
        define('STEP_4_TEXT_3', 'Перейти в админ-панель');
        define('INSTALL_MESSAGE', 'Вы устанавливаете %s %s');
        define('INSTALL_HELP', 'Нужна помощь?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'У текущего пользователя отсутствуют права на запись в папку, 
            расспакуйте архив с сайтом самостоятельно и запустите install.php снова.'
        );
        break;
    case 'uk':
        define('FOLDER_PERMISSION', 'Права на запис');
        define('RECOMMENDED_TO_INSTALL', 'Рекомендується при інсталяції встановити в');
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Рекомендуємо при інсталяції на сервері включити такі директиви для PHP'
        );
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Ми обнулили sql_mode для поточної сесії в MYSQL. 
            <br/>Це зроблено для успішної установки бази даних і початкового використання магазину. 
            <br/>Ваші значення - %s повернуться після перезавантаження mysql сервера. 
            <br/>Для успішної роботи магазину рекомендуємо вам обнулити змінну sql_mode на сервері'
        );
        define('DB_LOGIN_ERROR', 'Поле "Ім\'я користувача" обов\'язково для заповнення');
        define('DB_PASS_ERROR', 'Поле "Пароль" обов\'язково для заповнення');
        define('DB_HOST_ERROR', 'Поле "Хост" обов\'язково для заповнення');
        define('DB_NAME_ERROR', 'Поле "Ім\'я бази даних" обов\'язково для заповнення');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz Існує');
        define('REFRESH_PAGE', 'Оновіть сторінку');
        define('INSTALL_REQUIREMENTS', 'Вимоги до встановлення');
        define('EXT_PHAR', 'Розширення Phar');
        define('EXT_CURL', 'Розширення Curl');
        define('PHP_VERSION_CHECK', 'Версія PHP >= ' . errors::PHP_MIN_VERSION  . ' або <= ' . errors::PHP_MAX_VERSION);
        define(
            'MYSQL_ERORO',
            'Помилки у файлі бази даних або версія Mysql не підходить для даного установника, 
            для продовження Устанвка потрібно виправити таку помилку "%s"'
        );
        define(
            'DB_MAX_ALLOWED_PACKET',
            'Розмір файлу з базою даних більше, ніж у змінній max_allowed_packet, 
            збільште значення max_allowed_packet до "%s" на сервері MySQL'
        );
        define('EXT_MYSQLI', 'Розширення MySQLi');
        define('STEP_1', 'Вибрати мову');
        define('STEP_2', 'Налаштування бази даних');
        define('STEP_3', 'Загальні налаштування');
        define('STEP_4', 'Установка завершена!');
        define('TITLE', 'Встановити магазин Solomono');
        define('SELECT_LANGUAGE', 'Вибрати мову');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Ваша мова');
        define('STEP_1_TEXT_3', 'Будь ласка, оберіть вашу мову');
        define('NEXT_STEP', 'Наступний крок');
        define('STEP_2_TEXT_1', 'Налаштування бази даних');
        define(
            'STEP_2_TEXT_2',
            'Будь ласка, введіть свої облікові дані бази даних. 
            Якщо ви не знаєте цю інформацію, будь ласка, зв\'яжіться з вашим хостингом.'
        );
        define('STEP_2_FORM_INPUT_1', 'Хост бази даних');
        define('STEP_2_FORM_INPUT_HELP_1', 'Швидше за все, localhost');
        define('STEP_2_FORM_INPUT_2', 'Ім\'я бази даних');
        define('STEP_2_FORM_INPUT_HELP_2', 'Ім\'я бази даних');
        define('STEP_2_FORM_INPUT_3', 'Ім\'я користувача бази даних');
        define('STEP_2_FORM_INPUT_HELP_3', 'Ім\'я бази даних, зазвичай, root');
        define('STEP_2_FORM_INPUT_4', 'Пароль бази даних');
        define('STEP_2_FORM_INPUT_HELP_4', 'Залиште порожнім для порожнього пароля');
        define('STEP_2_FORM_INPUT_5', 'Підпапка веб-сайту');
        define('STEP_2_FORM_INPUT_6', 'Папка адміністратора');
        define('STEP_3_TEXT_1', 'Загальні налаштування');
        define('STEP_3_TEXT_2', 'Будь ласка, введіть загальну інформацію про вашому сайті');
        define('STEP_3_FORM_INPUT_1', 'Ім\'я магазину');
        define('STEP_3_FORM_INPUT_HELP_1', 'Назва сайту');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'Адреса електронної пошти адміністратора');
        define('STEP_3_FORM_INPUT_3', 'Пароль');
        define('STEP_3_FORM_INPUT_HELP_3', 'Пароль адміністратора');
        define('STEP_3_FORM_INPUT_4', 'Використовувати HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Видалити продукти за замовчуванням');
        define('STEP_4_TEXT_1', 'Вітаємо, ваш магазин готовий!');
        define('STEP_4_TEXT_2', 'Посилання на ваш магазин');
        define('STEP_4_TEXT_3', 'Перейти в адмін-панель');
        define('INSTALL_MESSAGE', 'Ви встановлюєте %s %s');
        define('INSTALL_HELP', 'Потрібна допомога?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'У поточного користувача відсутні права на запис в папку, 
            розпакуйте архів з сайтом самостійно і запустіть install.php знову.'
        );
        break;
    case 'pl':
        define('FOLDER_PERMISSION', 'Write Rights');
        define('RECOMMENDED_TO_INSTALL', 'Zaleca się zainstalowanie w');
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Opróżniliśmy sql_mode dla bieżącej sesji w MYSQL.
             <br/> Ma to na celu pomyślną instalację bazy danych i początkowe korzystanie ze sklepu.
             <br/> Twoje wartości - %s zostanie zwrócone po ponownym uruchomieniu serwera mysql.
             <br/> Aby sklep działał poprawnie, zalecamy opróżnienie zmiennej sql_mode na serwerze'
        );
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Zalecamy, aby podczas instalacji na serwerze uwzględnić następujące dyrektywy dla PHP'
        );
        define('REFRESH_PAGE', 'Odśwież stronę');
        define('DB_LOGIN_ERROR', 'Pole „Nazwa użytkownika” jest wymagane');
        define('DB_PASS_ERROR', 'Pole „Hasło” jest wymagane');
        define('DB_HOST_ERROR', 'Pole „Host” jest wymagane');
        define('DB_NAME_ERROR', 'Pole „Nazwa bazy danych” jest wymagane');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz Exists');
        define('INSTALL_REQUIREMENTS', 'Zainstaluj wymagania');
        define('EXT_PHAR', 'Rozszerzenie Phar');
        define('EXT_CURL', 'Rozszerzenie Curl');
        define(
            'MYSQL_ERORO',
            'Błędy w pliku bazy danych lub wersja MySQL nie jest odpowiednia dla tego instalatora, 
            aby kontynuować instalację, należy naprawić następujący błąd "%s"'
        );
        define(
            'DB_MAX_ALLOWED_PACKET',
            'Rozmiar pliku bazy danych jest większy niż w zmiennej max_allowed_packet, 
            zwiększ wartość max_allowed_packet do "%s" na serwerze MySQL'
        );
        define('PHP_VERSION_CHECK', 'Версия PHP >= ' . errors::PHP_MIN_VERSION  . ' lub <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'MySQLi Extension');
        define('STEP_1', 'Wybrać język');
        define('STEP_2', 'Konfigurowanie bazy danych');
        define('STEP_3', 'Ustawienia ogólne');
        define('STEP_4', 'Instalacja zakończona!');
        define('TITLE', 'Zainstalować sklep Solomono');
        define('SELECT_LANGUAGE', 'Wybrać język');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Twój język');
        define('STEP_1_TEXT_3', 'Proszę, wybierz swój język');
        define('NEXT_STEP', 'Następny krok');
        define('STEP_2_TEXT_1', 'Konfigurowanie bazy danych');
        define(
            'STEP_2_TEXT_2',
            'Proszę, wprowadź swoje dane logowania do bazy danych. 
            Jeśli nie znasz tych informacji, proszę skontaktować się ze swoim hostingiem.'
        );
        define('STEP_2_FORM_INPUT_1', 'Host bazy danych');
        define('STEP_2_FORM_INPUT_HELP_1', 'Najprawdopodobniej localhost');
        define('STEP_2_FORM_INPUT_2', 'Nazwa bazy danych');
        define('STEP_2_FORM_INPUT_HELP_2', 'Nazwa bazy danych');
        define('STEP_2_FORM_INPUT_3', 'Nazwa użytkownika bazy danych');
        define('STEP_2_FORM_INPUT_HELP_3', 'Nazwa bazy danych, zwykle root');
        define('STEP_2_FORM_INPUT_4', 'Hasło do bazy danych');
        define('STEP_2_FORM_INPUT_HELP_4', 'Pozostaw pole puste, aby pustego hasła');
        define('STEP_2_FORM_INPUT_5', 'Podfolder stronie internetowej');
        define('STEP_2_FORM_INPUT_6', 'Folder administratora');
        define('STEP_3_TEXT_1', 'Ustawienia ogólne');
        define('STEP_3_TEXT_2', 'Proszę podać ogólne informacje o swojej stronie');
        define('STEP_3_FORM_INPUT_1', 'Nazwa sklepu');
        define('STEP_3_FORM_INPUT_HELP_1', 'Tytuł strony');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'Adres e-mail administratora');
        define('STEP_3_FORM_INPUT_3', 'Hasło');
        define('STEP_3_FORM_INPUT_HELP_3', 'Hasło administratora');
        define('STEP_3_FORM_INPUT_4', 'Korzystać z protokołu HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Usunąć produkty domyślnie');
        define('STEP_4_TEXT_1', 'Gratulacje, twój sklep jest gotowy!');
        define('STEP_4_TEXT_2', 'Link do sklepu');
        define('STEP_4_TEXT_3', 'Przejdź do panelu administracyjnego');
        define('INSTALL_MESSAGE', 'Instalujesz %s %s');
        define('INSTALL_HELP', 'Potrzebujesz pomocy?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'Bieżący użytkownik nie ma uprawnień do zapisu w folderze, 
            sam rozpakuj archiwum z witryną i ponownie uruchom install.php.'
        );
        break;
    case 'es':
        define('FOLDER_PERMISSION', 'Derechos de escritura');
        define('RECOMMENDED_TO_INSTALL', 'Se recomienda instalar en formato');
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Hemos vaciado sql_mode para la sesión actual en MYSQL.
             <br/> Esto es para una instalación exitosa de la base de datos y un uso inicial de la tienda.
             <br/> Sus valores: %s se devolverá después de reiniciar el servidor mysql.
             <br/> Para el funcionamiento exitoso de la tienda, 
             le recomendamos que vacíe la variable sql_mode en el servidor'
        );
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Recomendamos que habilite las siguientes directivas PHP al instalar en el servidor'
        );
        define('DB_LOGIN_ERROR', 'El campo "Nombre de usuario" es obligatorio');
        define('DB_PASS_ERROR', 'El campo "Contraseña" es obligatorio');
        define('DB_HOST_ERROR', 'El campo "Host" es obligatorio');
        define('DB_NAME_ERROR', 'El campo "Nombre de la base de datos" es obligatorio');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz Existe');
        define('REFRESH_PAGE', 'Actualizar la página');
        define('EXT_PHAR', 'Extensión Phar');
        define('EXT_CURL', 'Extensión Curl');
        define('INSTALL_REQUIREMENTS', 'Instalar requisitos');
        define(
            'MYSQL_ERORO',
            'Los errores en el archivo de la base de datos o la versión de Mysql no es adecuada para este instalador, 
            para continuar con la instalación, debe corregir el siguiente error "%s"'
        );
        define(
            'DB_MAX_ALLOWED_PACKET',
            'El tamaño del archivo de la base de datos es mayor que en la variable max_allowed_packet, 
            aumenta el valor de max_allowed_packet a "%s" en el servidor MySQL'
        );
        define('PHP_VERSION_CHECK', 'Versión de PHP >= ' . errors::PHP_MIN_VERSION  . ' o <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'Extensión MySQLi');
        define('STEP_1', 'Seleccionar el idioma');
        define('STEP_2', 'Configuración de la base de datos');
        define('STEP_3', 'La configuración general de');
        define('STEP_4', 'La instalación ha sido completada.');
        define('TITLE', 'Instalar la tienda de Solomono');
        define('SELECT_LANGUAGE', 'Seleccionar el idioma');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Su idioma');
        define('STEP_1_TEXT_3', 'Por favor, seleccione su idioma');
        define('NEXT_STEP', 'El siguiente paso');
        define('STEP_2_TEXT_1', 'Configuración de la base de datos');
        define(
            'STEP_2_TEXT_2',
            'Por favor, escriba sus credenciales de la base de datos. 
            Si no conoce esta información, por favor, póngase en contacto con su empresa de alojamiento.'
        );
        define('STEP_2_FORM_INPUT_1', 'El host de la base de datos');
        define('STEP_2_FORM_INPUT_HELP_1', 'Probablemente, localhost');
        define('STEP_2_FORM_INPUT_2', 'El nombre de la base de datos');
        define('STEP_2_FORM_INPUT_HELP_2', 'El nombre de la base de datos');
        define('STEP_2_FORM_INPUT_3', 'El nombre de usuario de la base de datos');
        define('STEP_2_FORM_INPUT_HELP_3', 'El nombre de la base de datos, normalmente root');
        define('STEP_2_FORM_INPUT_4', 'La contraseña de la base de datos');
        define('STEP_2_FORM_INPUT_HELP_4', 'Déjelo en blanco para una contraseña en blanco');
        define('STEP_2_FORM_INPUT_5', 'En una subcarpeta del sitio web');
        define('STEP_2_FORM_INPUT_6', 'La carpeta de administrador');
        define('STEP_3_TEXT_1', 'La configuración general de');
        define('STEP_3_TEXT_2', 'Por favor, introduzca la información sobre su sitio web');
        define('STEP_3_FORM_INPUT_1', 'Nombre de la tienda');
        define('STEP_3_FORM_INPUT_HELP_1', 'El nombre del sitio');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'La dirección de correo electrónico del administrador');
        define('STEP_3_FORM_INPUT_3', 'La contraseña');
        define('STEP_3_FORM_INPUT_HELP_3', 'La contraseña de administrador');
        define('STEP_3_FORM_INPUT_4', 'El uso de HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Eliminar los productos de forma predeterminada');
        define('STEP_4_TEXT_1', 'Enhorabuena, tu tienda listo!');
        define('STEP_4_TEXT_2', 'El enlace a su tienda');
        define('STEP_4_TEXT_3', 'Ir al panel de administración de wordpress');
        define('INSTALL_MESSAGE', 'Usted está instalando %s %s');
        define('INSTALL_HELP', 'Necesita ayuda?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'El usuario actual no tiene permisos de escritura en la carpeta, 
            descomprima el archivo con el sitio usted mismo y ejecute install.php nuevamente.'
        );
        break;
    case 'fr':
        define(
            'DB_MAX_ALLOWED_PACKET',
            'La taille du fichier de base de données est plus grande que dans la variable max_allowed_packet, 
            augmentez la valeur de max_allowed_packet  à "%s" sur le serveur MySQL'
        );
        define('FOLDER_PERMISSION', 'Droits d\'écriture');
        define('RECOMMENDED_TO_INSTALL', 'Il est recommandé d\'installer dans');
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Nous avons vidé sql_mode pour la session en cours dans MYSQL.
             <br/>Ceci est pour une installation réussie de la base de données et une première utilisation du magasin.
             <br/>Vos valeurs - %s seront renvoyées après le redémarrage du serveur mysql.
             <br/>Pour le bon fonctionnement du magasin, nous vous recommandons de vider 
             la variable sql_mode sur le serveur'
        );
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Nous vous recommandons d\'activer les directives PHP suivantes lors de l\'installation sur le serveur'
        );
        define('DB_LOGIN_ERROR', 'Le champ "Nom d\'utilisateur" est obligatoire');
        define('DB_PASS_ERROR', 'Le champ "Mot de passe" est obligatoire');
        define('DB_HOST_ERROR', 'Le champ "Hôte" est obligatoire');
        define('DB_NAME_ERROR', 'Le champ "Nom de la base de données" est obligatoire');
        define('MISSING_SITE_ARCHIVE', 'Site . tar . gz Exists');
        define('EXT_PHAR', 'Phar Extension');
        define('EXT_CURL', 'Curl Extension');
        define('INSTALL_REQUIREMENTS', 'Installer les exigences');
        define('REFRESH_PAGE', 'Actualiser la page');
        define(
            'MYSQL_ERORO',
            'Les erreurs dans le fichier de base de données ou la version de Mysql ne conviennent pas à ce 
            programme d\'installation, pour continuer l\'installation, vous devez corriger l\'erreur suivante "%s"'
        );
        define('PHP_VERSION_CHECK', 'PHP version >= ' . errors::PHP_MIN_VERSION  . ' ou <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'MySQLi Extension');
        define('STEP_1', 'Choisir la langue');
        define('STEP_2', 'Configuration de la base de données');
        define('STEP_3', 'Paramètres généraux');
        define('STEP_4', 'L\'installation est terminée!');
        define('TITLE', 'Installer une boutique Solomono');
        define('SELECT_LANGUAGE', 'Choisir la langue');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Votre langue');
        define('STEP_1_TEXT_3', 'S\'il vous plaît sélectionnez votre langue');
        define('NEXT_STEP', 'L\'étape suivante');
        define('STEP_2_TEXT_1', 'Configuration de la base de données');
        define(
            'STEP_2_TEXT_2',
            'S\'il vous plaît entrez vos informations d\'identification de la base de données. 
            Si vous ne connaissez pas ces informations, veuillez contacter votre hébergement.'
        );
        define('STEP_2_FORM_INPUT_1', 'L\'hôte de la base de données');
        define('STEP_2_FORM_INPUT_HELP_1', 'Probablement localhost');
        define('STEP_2_FORM_INPUT_2', 'Nom de la base de données');
        define('STEP_2_FORM_INPUT_HELP_2', 'Nom de la base de données');
        define('STEP_2_FORM_INPUT_3', 'Le nom d\'utilisateur de la base de données');
        define('STEP_2_FORM_INPUT_HELP_3', 'Le nom de la base de données, généralement root');
        define('STEP_2_FORM_INPUT_4', 'Le mot de passe de la base de données');
        define('STEP_2_FORM_INPUT_HELP_4', 'Laissez vide pour un mot de passe vide');
        define('STEP_2_FORM_INPUT_5', 'Sous-dossier d\'un site web');
        define('STEP_2_FORM_INPUT_6', 'Le dossier de l\'administrateur');
        define('STEP_3_TEXT_1', 'Paramètres généraux');
        define('STEP_3_TEXT_2', 'S\'il vous plaît, entrez des informations générales à propos de votre site');
        define('STEP_3_FORM_INPUT_1', 'Nom de la boutique');
        define('STEP_3_FORM_INPUT_HELP_1', 'Le nom du site');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'Adresse e-mail de l\'administrateur');
        define('STEP_3_FORM_INPUT_3', 'Le mot de passe');
        define('STEP_3_FORM_INPUT_HELP_3', 'Le mot de passe de l\'administrateur');
        define('STEP_3_FORM_INPUT_4', 'Utiliser HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Supprimer des produits par défaut');
        define('STEP_4_TEXT_1', 'Félicitations, votre magasin de prêt!');
        define('STEP_4_TEXT_2', 'Lien vers votre boutique');
        define('STEP_4_TEXT_3', 'Aller au panneau d\'administration');
        define('INSTALL_MESSAGE', 'Vous installez %s %s');
        define('INSTALL_HELP', 'Besoin d\'aide?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'L\'utilisateur actuel ne dispose pas d\'autorisations en écriture sur le dossier, 
            décompressez vous-même l\'archive avec le site et exécutez à nouveau install.php.'
        );
        break;
    case 'de':
        define(
            'DB_MAX_ALLOWED_PACKET',
            'Die Datenbankdatei ist größer als in der Variablen max_allowed_packet, 
            erhöhen Sie den Wert von max_allowed_packet  tot "%s" auf dem MySQL-Server'
        );
        define('FOLDER_PERMISSION', 'Schreibrechte');
        define('RECOMMENDED_TO_INSTALL', 'Es wird empfohlen, in zu installieren');
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Wir empfehlen, dass Sie bei der Installation auf dem Server die folgenden PHP-Anweisungen aktivieren'
        );
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Wir haben sql_mode für die aktuelle Sitzung in MYSQL geleert.
             <br/> Dies ist für eine erfolgreiche Datenbankinstallation und die erstmalige Verwendung des Speichers.
             <br/> Ihre Werte - %s werden nach dem Neustart des MySQL-Servers zurückgegeben.
             <br/> Für den erfolgreichen Betrieb des Speichers empfehlen wir, 
             die Variable sql_mode auf dem Server zu leeren'
        );
        define('REFRESH_PAGE', 'Seite aktualisieren');
        define('INSTALL_REQUIREMENTS', 'Installationsanforderungen');
        define('DB_LOGIN_ERROR', 'Das Feld "Benutzername" ist erforderlich');
        define('DB_PASS_ERROR', 'Das Feld "Passwort" ist erforderlich');
        define('DB_HOST_ERROR', 'Das Feld "Host" ist erforderlich');
        define('DB_NAME_ERROR', 'Das Feld "Datenbankname" ist erforderlich');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz Exists');
        define('EXT_PHAR', 'Phar Extension');
        define('EXT_CURL', 'Phar Curl');
        define(
            'MYSQL_ERORO',
            'Fehler in der Datenbankdatei oder in der Version von MySQL sind für dieses Installationsprogramm 
            nicht geeignet. Um die Installation fortzusetzen, müssen Sie den folgenden Fehler beheben "%s"'
        );
        define('PHP_VERSION_CHECK', 'PHP version >= ' . errors::PHP_MIN_VERSION  . ' oder <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'MySQLi Extension');
        define('STEP_1', 'Wählen Sie die Sprache');
        define('STEP_2', 'Konfiguration der Datenbank');
        define('STEP_3', 'Allgemeine Einstellungen');
        define('STEP_4', 'Die Installation ist abgeschlossen!');
        define('TITLE', 'Installieren Solomono Shop');
        define('SELECT_LANGUAGE', 'Wählen Sie die Sprache');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Ihre Sprache');
        define('STEP_1_TEXT_3', 'Bitte wählen Sie Ihre Sprache');
        define('NEXT_STEP', 'Der nächste Schritt');
        define('STEP_2_TEXT_1', 'Konfiguration der Datenbank');
        define(
            'STEP_2_TEXT_2',
            'Bitte geben Sie Ihre Anmeldeinformationen für die Datenbank. 
            Wenn Sie nicht wissen, diese Informationen, bitte Kontaktieren Sie Ihren Hosting.'
        );
        define('STEP_2_FORM_INPUT_1', 'Host-Datenbank');
        define('STEP_2_FORM_INPUT_HELP_1', 'Wahrscheinlich localhost');
        define('STEP_2_FORM_INPUT_2', 'Der name der Datenbank');
        define('STEP_2_FORM_INPUT_HELP_2', 'Der name der Datenbank');
        define('STEP_2_FORM_INPUT_3', 'Benutzername für die Datenbank');
        define('STEP_2_FORM_INPUT_HELP_3', 'Der name der Datenbank, in der Regel root');
        define('STEP_2_FORM_INPUT_4', 'Passwort-Datenbank');
        define('STEP_2_FORM_INPUT_HELP_4', 'Leeren Sie das Feld leer lassen Passwort');
        define('STEP_2_FORM_INPUT_5', 'Unterordner Web-site');
        define('STEP_2_FORM_INPUT_6', 'Der Ordner Admin');
        define('STEP_3_TEXT_1', 'Allgemeine Einstellungen');
        define('STEP_3_TEXT_2', 'Bitte geben Sie Allgemeine Informationen über Ihre Website');
        define('STEP_3_FORM_INPUT_1', 'Der name des Geschäftes');
        define('STEP_3_FORM_INPUT_HELP_1', 'Der name der Website');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'E-Mail-Adresse des Administrators');
        define('STEP_3_FORM_INPUT_3', 'Passwort');
        define('STEP_3_FORM_INPUT_HELP_3', 'Admin-Passwort');
        define('STEP_3_FORM_INPUT_4', 'HTTPS verwenden');
        define('STEP_3_FORM_INPUT_5', 'Entfernen Standard-Produkte');
        define('STEP_4_TEXT_1', 'Herzlichen Glückwunsch, Ihr Shop ist fertig!');
        define('STEP_4_TEXT_2', 'Link auf Ihren Shop');
        define('STEP_4_TEXT_3', 'Gehen Sie im Admin-Panel');
        define('INSTALL_MESSAGE', 'Sie installieren %s %s');
        define('INSTALL_HELP', 'Brauchen Sie Hilfe?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'Der aktuelle Benutzer hat keine Schreibrechte für den Ordner, 
            entpacke das Archiv mit der Site selbst und starte install.php erneut.'
        );
        break;
    case 'el':
        define('FOLDER_PERMISSION', 'Δικαιώματα εγγραφής');
        define('RECOMMENDED_TO_INSTALL', 'Συνιστάται η εγκατάσταση στο');
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'Σας συνιστούμε να ενεργοποιήσετε τις ακόλουθες οδηγίες PHP κατά την εγκατάσταση στον διακομιστή'
        );
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'Έχουμε αδειάσει το sql_mode για την τρέχουσα περίοδο λειτουργίας στο MYSQL.
             <br/> Αυτό είναι για μια επιτυχημένη εγκατάσταση βάσης δεδομένων και αρχική χρήση του καταστήματος.
             <br/> Οι τιμές σας - %s θα επιστραφούν μετά την επανεκκίνηση του διακομιστή mysql.
             <br/> Για την επιτυχή λειτουργία του καταστήματος, 
             σας συνιστούμε να αδειάσετε τη μεταβλητή sql_mode στο διακομιστή'
        );
        define('REFRESH_PAGE', 'Ανανέωση της σελίδας');
        define('INSTALL_REQUIREMENTS', 'Εγκαταστήστε τις απαιτήσεις');
        define('DB_LOGIN_ERROR', 'Το πεδίο "Όνομα χρήστη" απαιτείται');
        define('DB_PASS_ERROR', 'Το πεδίο "Κωδικός πρόσβασης" απαιτείται');
        define('DB_HOST_ERROR', 'Απαιτείται το πεδίο "Host"');
        define('DB_NAME_ERROR', 'Απαιτείται το πεδίο "Όνομα βάσης δεδομένων"');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz Exists');
        define(
            'MYSQL_ERORO',
            'Τα σφάλματα στο αρχείο βάσης δεδομένων ή στην έκδοση του Mysql δεν είναι κατάλληλα για αυτό το 
            πρόγραμμα εγκατάστασης, για να συνεχίσετε την εγκατάσταση πρέπει να διορθώσετε το ακόλουθο σφάλμα "%s"'
        );
        define(
            'DB_MAX_ALLOWED_PACKET',
            'Το μέγεθος του αρχείου βάσης δεδομένων είναι μεγαλύτερο από ό, 
            τι στη μεταβλητή max_allowed_packet, αυξήστε την τιμή του max_allowed_packet σε "%s" στον διακομιστή MySQL'
        );
        define('EXT_PHAR', 'Phar Extension');
        define('EXT_CURL', 'Curl Extension');
        define('PHP_VERSION_CHECK', 'PHP version >= ' . errors::PHP_MIN_VERSION  . ' ή <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'MySQLi Extension');
        define('STEP_1', 'Να επιλέξετε τη γλώσσα της');
        define('STEP_2', 'Ρύθμιση της βάσης δεδομένων');
        define('STEP_3', 'Γενικές ρυθμίσεις');
        define('STEP_4', 'Η εγκατάσταση ολοκληρώθηκε!');
        define('TITLE', 'Εγκαταστήσετε το κατάστημα Solomono');
        define('SELECT_LANGUAGE', 'Να επιλέξετε τη γλώσσα της');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Η γλώσσα σας');
        define('STEP_1_TEXT_3', 'Παρακαλώ, επιλέξτε τη γλώσσα σας');
        define('NEXT_STEP', 'Το επόμενο βήμα');
        define('STEP_2_TEXT_1', 'Ρύθμιση της βάσης δεδομένων');
        define(
            'STEP_2_TEXT_2',
            'Παρακαλώ, εισάγετε τα διαπιστευτήρια της βάσης δεδομένων. 
            Αν δεν γνωρίζετε αυτές τις πληροφορίες, παρακαλώ επικοινωνήστε με τον φιλοξενεί.'
        );
        define('STEP_2_FORM_INPUT_1', 'Υποδοχής της βάσης δεδομένων');
        define('STEP_2_FORM_INPUT_HELP_1', 'Πιθανότατα, το localhost');
        define('STEP_2_FORM_INPUT_2', 'Το όνομα της βάσης δεδομένων');
        define('STEP_2_FORM_INPUT_HELP_2', 'Το όνομα της βάσης δεδομένων');
        define('STEP_2_FORM_INPUT_3', 'Το όνομα χρήστη της βάσης δεδομένων');
        define('STEP_2_FORM_INPUT_HELP_3', 'Το όνομα της βάσης δεδομένων, συνήθως root');
        define('STEP_2_FORM_INPUT_4', 'Τον κωδικό πρόσβασης βάσης δεδομένων');
        define('STEP_2_FORM_INPUT_HELP_4', 'Αφήστε το κενό για το κενό κωδικό πρόσβασης');
        define('STEP_2_FORM_INPUT_5', 'Υποφάκελο ιστοσελίδα');
        define('STEP_2_FORM_INPUT_6', 'Ο φάκελος admin');
        define('STEP_3_TEXT_1', 'Γενικές ρυθμίσεις');
        define('STEP_3_TEXT_2', 'Παρακαλώ εισάγετε γενικές πληροφορίες για την ιστοσελίδα σας');
        define('STEP_3_FORM_INPUT_1', 'Το όνομα του καταστήματος');
        define('STEP_3_FORM_INPUT_HELP_1', 'Το όνομα της ιστοσελίδας');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'Η διεύθυνση ηλεκτρονικού ταχυδρομείου διαχειριστή');
        define('STEP_3_FORM_INPUT_3', 'Τον κωδικό πρόσβασης');
        define('STEP_3_FORM_INPUT_HELP_3', 'Τον κωδικό πρόσβασης διαχειριστή');
        define('STEP_3_FORM_INPUT_4', 'Να χρησιμοποιήσετε το HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Αφαιρέσετε τα προϊόντα από προεπιλογή');
        define('STEP_4_TEXT_1', 'Συγχαρητήρια, το κατάστημά σας είναι έτοιμο!');
        define('STEP_4_TEXT_2', 'Το link για το κατάστημά σας');
        define('STEP_4_TEXT_3', 'Πηγαίνετε στο admin panel');
        define('INSTALL_MESSAGE', 'Μπορείτε να εγκαταστήσετε το %s %s');
        define('INSTALL_HELP', 'Χρειάζεστε βοήθεια;');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'Ο τρέχων χρήστης δεν έχει δικαιώματα εγγραφής στο φάκελο, 
            αποσυνδέστε τον εαυτό σας από τον ιστότοπο και εκκινήστε ξανά το install.php.'
        );
        break;
    case 'nl':
        define('INSTALL_REQUIREMENTS', 'Installeer vereisten');
        define('RECOMMENDED_TO_INSTALL', 'Het wordt aanbevolen om in te installeren');
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'We raden u aan om de volgende PHP-richtlijnen in te schakelen wanneer u op de server installeert'
        );
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'We hebben sql_mode leeggemaakt voor de huidige sessie in MYSQL.
             <br/> Dit is voor een succesvolle database-installatie en het eerste gebruik van de winkel.
             <br/> Uw waarden - %s worden geretourneerd na het herstarten van de mysql-server.
             <br/> Voor een succesvolle werking van de winkel raden we 
             je aan om de variabele sql_mode op de server leeg te maken'
        );
        define('FOLDER_PERMISSION', 'Schrijfrechten');
        define('REFRESH_PAGE', 'Vernieuw de pagina');
        define('DB_LOGIN_ERROR', 'Het veld "Gebruikersnaam" is verplicht');
        define('DB_PASS_ERROR', 'Het veld "Wachtwoord" is verplicht');
        define('DB_HOST_ERROR', 'Het veld "Host" is verplicht');
        define('DB_NAME_ERROR', 'Het veld "Databasenaam" is verplicht');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz bestaat');
        define(
            'MYSQL_ERORO',
            'Fouten in het databasebestand of de versie van Mysql is niet geschikt voor dit installatieprogramma, 
            om door te gaan met de installatie moet u de volgende fout herstellen "%s"'
        );
        define(
            'DB_MAX_ALLOWED_PACKET',
            'De databasebestandsgrootte is groter dan in de variabele max_allowed_packet, 
            verhoog de waarde van max_allowed_packet  tot "%s" op de MySQL-server'
        );
        define('EXT_PHAR', 'Phar-extensie');
        define('EXT_CURL', 'Curl-extensie');
        define('PHP_VERSION_CHECK', 'PHP-versie >= ' . errors::PHP_MIN_VERSION  . ' of <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'MySQLi-extensie');
        define('STEP_1', 'Selecteer de gewenste taal');
        define('STEP_2', 'Inrichten Database');
        define('STEP_3', 'Algemene instellingen');
        define('STEP_4', 'De installatie is nu compleet!');
        define('TITLE', 'Stel winkel context koop');
        define('SELECT_LANGUAGE', 'Selecteer de gewenste taal');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Uw taal');
        define('STEP_1_TEXT_3', 'Selecteer uw taal');
        define('NEXT_STEP', 'De volgende stap');
        define('STEP_2_TEXT_1', 'Inrichten Database');
        define(
            'STEP_2_TEXT_2',
            'Vul uw databasereferenties. Als u deze gegevens niet weet, neem dan contact op met uw hosting.'
        );
        define('STEP_2_FORM_INPUT_1', 'Database host');
        define('STEP_2_FORM_INPUT_HELP_1', 'Het meest waarschijnlijk is, localhost');
        define('STEP_2_FORM_INPUT_2', 'De naam van de database');
        define('STEP_2_FORM_INPUT_HELP_2', 'De naam van de database');
        define('STEP_2_FORM_INPUT_3', 'De naam van de database gebruiker');
        define('STEP_2_FORM_INPUT_HELP_3', 'De naam van de database, meestal root');
        define('STEP_2_FORM_INPUT_4', 'Het database wachtwoord');
        define('STEP_2_FORM_INPUT_HELP_4', 'Laat leeg voor een leeg wachtwoord');
        define('STEP_2_FORM_INPUT_5', 'Submap van de website');
        define('STEP_2_FORM_INPUT_6', 'De admin-map');
        define('STEP_3_TEXT_1', 'Algemene instellingen');
        define('STEP_3_TEXT_2', 'Voer Algemene informatie over uw website');
        define('STEP_3_FORM_INPUT_1', 'Naam winkel');
        define('STEP_3_FORM_INPUT_HELP_1', 'De naam van de site');
        define('STEP_3_FORM_INPUT_2', 'E-mail');
        define('STEP_3_FORM_INPUT_HELP_2', 'E-mail adres van de beheerder');
        define('STEP_3_FORM_INPUT_3', 'Wachtwoord');
        define('STEP_3_FORM_INPUT_HELP_3', 'Het wachtwoord van de beheerder');
        define('STEP_3_FORM_INPUT_4', 'Gebruik HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Verwijder producten standaard');
        define('STEP_4_TEXT_1', 'Gefeliciteerd, uw winkel is klaar!');
        define('STEP_4_TEXT_2', 'Link naar uw winkel');
        define('STEP_4_TEXT_3', 'Ga naar het admin panel');
        define('INSTALL_MESSAGE', 'Stelt u %s %s');
        define('INSTALL_HELP', 'Hulp nodig?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'De huidige gebruiker heeft geen schrijfrechten voor de map, 
            pak het archief zelf uit met de site en voer install.php opnieuw uit.'
        );
        break;
    default:
        define('FOLDER_PERMISSION', 'Access write');
        define('RECOMMENDED_TO_INSTALL', 'It is recommended to install in');
        define(
            'RECOMMENDED_TO_INSTALL_ON_SERVER',
            'We recommend that you enable the following PHP directives when installing on the server'
        );
        define('REFRESH_PAGE', 'Refresh page');
        define(
            'MYSQL_SQL_MODE_MESSAGE',
            'We have emptied out sql_mode for the current session in MYSQL. 
            <br/>This is for a successful database installation and initial use of the store. 
            <br/>Your values - %s will returned after restarting the mysql server. 
            <br/>For the successful operation of the store, 
            we recommend that you empty the sql_mode variable on the server'
        );
        define('INSTALL_REQUIREMENTS', 'Install requirements');
        define('DB_LOGIN_ERROR', 'The "Username" field is required');
        define('DB_PASS_ERROR', 'The "Password" field is required');
        define('DB_HOST_ERROR', 'The "Host" field is required');
        define('DB_NAME_ERROR', 'The "Database name" field is required');
        define('MISSING_SITE_ARCHIVE', 'Site.tar.gz exist');
        define('EXT_PHAR', 'Extension Phar');
        define('EXT_CURL', 'Extension Curl');
        define(
            'MYSQL_ERORO',
            'Errors in the database file or the version of Mysql is not suitable for this installer, 
            to continue the installation you need to fix the following error "%s"'
        );
        define('PHP_VERSION_CHECK', 'PHP version >= ' . errors::PHP_MIN_VERSION  . ' or <= ' . errors::PHP_MAX_VERSION);
        define('EXT_MYSQLI', 'Extension MySQLi');
        define(
            'DB_MAX_ALLOWED_PACKET',
            'The database file size is larger than in the max_allowed_packet variable, 
            increase the value of max_allowed_packet to "%s" on the MySQL server'
        );
        define('STEP_1', 'Select Language');
        define('STEP_2', 'Database Setup');
        define('STEP_3', 'General Settings');
        define('STEP_4', 'Installation Complete!');
        define('TITLE', 'Install Solomono store');
        define('SELECT_LANGUAGE', 'Select Language');
        define('STEP_1_TEXT_1', '');
        define('STEP_1_TEXT_2', 'Your Language');
        define('STEP_1_TEXT_3', 'Please choose your language');
        define('NEXT_STEP', 'Next step');
        define('STEP_2_TEXT_1', 'Database Setup');
        define('STEP_2_TEXT_2',
            'Please fill in your database credentials. If you dont know this info, please contact your hosting.');
        define('STEP_2_FORM_INPUT_1', 'Database Host');
        define('STEP_2_FORM_INPUT_HELP_1', 'Most likely localhost');
        define('STEP_2_FORM_INPUT_2', 'Database Name');
        define('STEP_2_FORM_INPUT_HELP_2', 'Database Name');
        define('STEP_2_FORM_INPUT_3', 'Database Username');
        define('STEP_2_FORM_INPUT_HELP_3', 'The database root, usually root');
        define('STEP_2_FORM_INPUT_4', 'Database Password');
        define('STEP_2_FORM_INPUT_HELP_4', 'Leave blank for empty password');
        define('STEP_2_FORM_INPUT_5', 'Website Subfolder');
        define('STEP_2_FORM_INPUT_6', 'Admin Folder');
        define('STEP_3_TEXT_1', 'General Settings');
        define('STEP_3_TEXT_2', 'Please enter your general website info');
        define('STEP_3_FORM_INPUT_1', 'Store Name');
        define('STEP_3_FORM_INPUT_HELP_1', 'Website title');
        define('STEP_3_FORM_INPUT_2', 'Email');
        define('STEP_3_FORM_INPUT_HELP_2', 'Admin’s email address');
        define('STEP_3_FORM_INPUT_3', 'Password');
        define('STEP_3_FORM_INPUT_HELP_3', 'Admin’s password');
        define('STEP_3_FORM_INPUT_4', 'Use HTTPS');
        define('STEP_3_FORM_INPUT_5', 'Remove default products');
        define('STEP_4_TEXT_1', 'Congratulations, your store is ready!');
        define('STEP_4_TEXT_2', 'Your store link');
        define('STEP_4_TEXT_3', 'Go to admin panel');
        define('INSTALL_MESSAGE', 'You’re installing %s %s');
        define('INSTALL_HELP', 'Need help?');
        define(
            'TEXT_FOLDER_PERMISSION_DENIED',
            'The current user does not have write permissions to the folder. 
            Unzip the archive with the site manually and run install.php again.'
        );
        break;
}

if (isset($_POST['action']) && $_POST['action'] === 'chceckDb') {
    $errors = new errors;
    $db_host = tep_db_prepare_input($_POST['dbHost']);
    $db_username = tep_db_prepare_input($_POST['dbLogin']);
    $db_password = tep_db_prepare_input($_POST['dbPass']);
    $res = [];
    $connection = new db();
    $connection->setErrorHandler($errors);
    @$connection->connect($db_host, $db_username, $db_password);
    if ($connection->connection->connect_errno === 0) {
        $result = $connection->query("SHOW VARIABLES LIKE 'sql_mode'");
        $res = ['result' => 'success', 'message' => ''];
        if ($result->num_rows) {
            if ($val = $result->fetch_assoc()['Value']) {
                $val = str_replace(',', ',<br/>', $val);
                $res = [
                    'result' => 'success',
                    'message' => sprintf(htmlspecialchars(MYSQL_SQL_MODE_MESSAGE, ENT_QUOTES), $val)
                ];
                $connection->query("SET GLOBAL sql_mode='', SESSION sql_mode=''");
            }
        }
    } else {
        $error = '';
        foreach ($errors->getCustomErrors() as $errorText => $params) {
            $error .= defined($errorText) ? $errors->getCustomError($errorText) : $errorText;
            $error .= '<br/>';
        }

        $res = ['result' => 'error', 'message' => $error];
    }

    header('Content-type: application/json', true, 200);
    $json = json_encode($res, JSON_UNESCAPED_SLASHES);
    echo $json;
    die;
}

define('RECOMMENDED_DISPLAY_ERRORS', '1');
define('RECOMMENDED_MAX_EXEXUTION_TIME', '0');
define('RECOMMENDED_MEMORY_LIMIT', '-1');
define('RECOMMENDED_DISPLAY_STARTUP_ERRORS', '1');
ini_set('display_errors', RECOMMENDED_DISPLAY_ERRORS);
ini_set('max_execution_time', RECOMMENDED_MAX_EXEXUTION_TIME);
ini_set('memory_limit', RECOMMENDED_MEMORY_LIMIT);
ini_set('display_startup_errors', RECOMMENDED_DISPLAY_STARTUP_ERRORS);
error_reporting(E_ALL & ~E_NOTICE);
if (basename(__DIR__) === 'admin') {
    die('Use this only for Solomono setup');
}

if (!file_exists('site.tar.gz') && !file_exists('.env')) {
    die('missing site.tar.gz');
}

if (!class_exists('PharData')) {
    die('class PharData not found');
}

function tep_db_prepare_input($string)
{
    if (is_string($string)) {
        return trim(tep_sanitize_string(stripslashes($string)));
    } elseif (is_array($string)) {
        reset($string);
        foreach ($string as $key => $value) {
            $string[$key] = tep_db_prepare_input($value);
        }

        return $string;
    } else {
        return $string;
    }
}

function tep_sanitize_string($string)
{
    $string = preg_replace('/ +/', ' ', trim($string));

    return preg_replace("/[<>]/", '_', $string);
}

function arr2ini(array $a, array $parent = [])
{
    $out = '';
    foreach ($a as $k => $v) {
        if (is_array($v)) {
            //subsection case
            //merge all the sections into one array...
            $sec = array_merge((array)$parent, (array)$k);
            //add section information to the output
            $out .= '[' . join('.', $sec) . ']' . PHP_EOL;
            //recursively traverse deeper
            $out .= arr2ini($v, $sec);
        } else {
            //plain key->value case
            $out .= "$k=$v" . PHP_EOL;
        }
    }

    return $out;
}

function prepare_dot_env()
{
    $dotenv = parse_ini_file('.env');
    $dotenv['APP_ENV'] = 'prod';
    $dotenv['DB_HOST'] = $_POST['db_host'];
    $dotenv['DB_DATABASE'] = $_POST['db_database'];
    $dotenv['DB_USERNAME'] = $_POST['db_username'];
    $dotenv['DB_PASSWORD'] = $_POST['db_password'];
    $dotenv['ADMIN_FOLDER'] = $_POST['admin_folder'];
    $dotenv['CATALOG_FOLDER'] = str_replace(
        [$_SERVER['HTTP_ORIGIN'] . '/', 'install.php'],
        '',
        $_SERVER['HTTP_REFERER']
    );
    if (substr($dotenv['CATALOG_FOLDER'], -1) === '/') {
        $dotenv['CATALOG_FOLDER'] = substr($dotenv['CATALOG_FOLDER'], 0, -1);
    }

    if (!empty($dotenv['CATALOG_FOLDER'])) {
        rewriteBase('RewriteBase /' . $dotenv['CATALOG_FOLDER'] . '/');
    }

    if (!file_exists($dotenv['ADMIN_FOLDER']) && file_exists('admin')) {
        rename('admin', $dotenv['ADMIN_FOLDER']);
    }

    file_put_contents('.env', arr2ini($dotenv));
}

function rewriteBase($base)
{
    $file = '.htaccess';
    $handle = fopen($file, "r+");
    $searching = 'RewriteBase /';
    if ($handle) {
        $result = [];
        while (($line = fgets($handle)) !== false) {
            if (strpos($line, $searching) !== false) {
                $line = $base . "\r\n";
            }

            $result[] = $line;
        }

        file_put_contents($file, implode('', $result));
        fclose($handle);
    }
}

function rewriteHtaccess($flag, $search, $line_quantity)
{
    $file = '.htaccess';
    $handle = fopen($file, "r+");
    $result = [];
    $searching = $search . (($flag == 'true') ? ' OFF#' : ' ON#');
    $lines = 0;
    if ($handle) {
        while (($line = fgets($handle)) !== false) {
            if ($lines > 0 && $lines <= $line_quantity) {
                $lines++;
                $line = $flag == 'true' ? substr($line, 1) : "#$line";
            }

            if (strpos($line, $searching) !== false) {
                $lines++;
                $line = $search . (($flag == 'true') ? ' ON#' : ' OFF#') . "\r\n";
            }

            $result[] = $line;
        }

        file_put_contents($file, implode('', $result));
        fclose($handle);
    }
}

function tep_rand($min = null, $max = null)
{
    static $seeded;

    if (!$seeded) {
        mt_srand((double)microtime() * 1000000);
        $seeded = true;
    }

    if (isset($min) && isset($max)) {
        if ($min >= $max) {
            return $min;
        } else {
            return mt_rand($min, $max);
        }
    } else {
        return mt_rand();
    }
}

function tep_encrypt_password($plain)
{
    $password = '';

    for ($i = 0; $i < 10; $i++) {
        $password .= tep_rand();
    }

    $salt = substr(md5($password), 0, 2);
    $password = md5($salt . $plain) . ':' . $salt;

    return $password;
}

function new_admin_sql($admin_name, $admin_email, $admin_password)
{
    $pass = tep_encrypt_password($admin_password);
    $data = [];
    $data['admin_firstname'] = "Admin";
    $data['admin_lastname'] = $admin_name;
    $data['admin_email_address'] = $admin_email;
    $data['admin_cat_access'] = "ALL";
    $data['admin_created'] = date("Y-m-d H:i:s");
    $data['admin_modified'] = date("Y-m-d H:i:s");
    $data['admin_logdate'] = date("Y-m-d H:i:s");
    $data['admin_trylog_date'] = date("Y-m-d H:i:s");
    $data['admin_password'] = $pass;
    $data['admin_groups_id'] = 1;
    $data['admin_right_access'] = MD5('first login');
    $query = [];
    foreach ($data as $key => $value) {
        $value = tep_db_prepare_input($value);
        $query[] = "`{$key}` = " . '\'' . $value . '\'';
    }

    $query = implode(',', $query);

    return "INSERT INTO `admin` SET $query ON DUPLICATE KEY UPDATE {$query}";
}

function delTree($dir)
{
    $files = array_diff(scandir($dir), ['.', '..']);
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }

    return rmdir($dir);
}

/**
 * return true if there is a redirect from Http to Https
 * @return bool
 */
function isRedirectHttpToHttps() {
    $res = false;
    $url = 'http://' . $_SERVER['SERVER_NAME'];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    $out = curl_exec($ch);
    curl_close($ch);
    // line endings is the wonkiest piece of this whole thing
    $out = str_replace("\r", "", $out);

    // only look at the headers
    $headers_end = strpos($out, "\n\n");
    if( $headers_end !== false ) {
        $out = substr($out, 0, $headers_end);
    }

    $headers = explode("\n", $out);
    foreach($headers as $header) {
        if( substr($header, 0, 10) == "Location: " ) {
            $target = substr($header, 10);

            $firstProtocol = explode(":", $url)[0];
            $secondProtocol = explode(":", $target)[0];
            if ($firstProtocol !== $secondProtocol) {
                $res = true;
            }

            break;
        }
    }

    return $res;
}

class db
{

    var $connection;
    var $errorHandler;

    function setErrorHandler(errors &$handler)
    {
        $this->errorHandler = $handler;
    }

    function connect($db_host, $db_username, $db_password)
    {
        $this->connection = new mysqli($db_host, $db_username, $db_password);
        $this->error();
    }

    function query($query)
    {
        $query = $this->connection->query($query);
        $this->error();
        return $query;
    }

    function select_db($db)
    {
        $this->connection->select_db($db);
        $this->error();
    }

    function error()
    {
        global $step;
        if ($this->connection->connect_errno > 0) {
            --$step;
            $errorText = "MYSQL error no: " . $this->connection->connect_errno . "\n" . "Error: " . $this->connection->connect_error . "\n";
            if ($this->errorHandler) {
                $this->errorHandler->addCustomError($errorText);
            } else {
                echo $errorText;
            }
        }
    }

    function multi_query($query)
    {
        $this->connection->multi_query($query);
        $this->error();
    }
}

function extractSite()
{
    if (file_exists('site.tar.gz')) {

        try {
            $phar_data = new PharData('site.tar.gz');
            $phar_data->extractTo('.', null, true);
        } catch (Exception $e) {
            var_dump('ERROR: ' . $e->getMessage());
            die;
        }

        if (file_exists('install_temp')) {
            delTree('install_temp');
        }

        unlink('site.tar.gz');
    }
}

function deleteProductsImages($imageList)
{
    $images = explode(';', $imageList);
    foreach ($images as $image) {
        @unlink('images/products/' . $image);
    }
}

$step = isset($_POST['step']) ? (int)$_POST['step'] : 1;
$errors = new errors;

class errors
{
    const PHP_MIN_VERSION = 5.6;
    const PHP_MAX_VERSION = 7.4;
    public $errors = [];
    public $customErrors = [];

    public function __construct()
    {
        $this->errors = [
            'EXT_PHAR' => !extension_loaded('phar'),
            'EXT_CURL' => !extension_loaded('curl'),
            'PHP_VERSION_CHECK' => ((float)PHP_VERSION < errors::PHP_MIN_VERSION || (float)PHP_VERSION > errors::PHP_MAX_VERSION),
            'EXT_MYSQLI' => !extension_loaded('mysqli'),
        ];
        if (!file_exists('.env')) {
            $this->errors['FOLDER_PERMISSION'] = !file_exists('site.tar.gz');
            $this->errors['MISSING_SITE_ARCHIVE'] = !file_exists('site.tar.gz');
        }
    }

    public function addError($constName, $check)
    {
        $this->errors[(string)$constName] = (bool)$check;
    }

    public function addCustomError($error, $additionalParams = [])
    {
        $this->customErrors[(string)$error] = $additionalParams;
    }

    public function checkRequirements()
    {
        return $this->errors;
    }

    public function clearErrors()
    {
        $this->errors = [];
    }

    public function getCustomErrors()
    {
        return $this->customErrors;
    }

    public function getCustomError($error)
    {

        return vsprintf(constant($error), $this->customErrors[$error]);
    }

    public function isHaveErrors()
    {
        return array_reduce($this->errors, function ($error, $init) {
            return $error;
        }, false);
    }
}

switch ($step) {
    case 2:
        if (!file_exists('.env') && (!$errors->isHaveErrors())) {
            $errors->clearErrors();
            extractSite();
            if (isRedirectHttpToHttps()) {
                rewriteHtaccess('true', '#HTTPS', 8);
            }
        } elseif (file_exists('.env')) {
            $errors->clearErrors();
        }

        break;
    case 3:
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $db_host = tep_db_prepare_input($_POST['db_host']);
            $db_database = tep_db_prepare_input($_POST['db_database']);
            $db_username = tep_db_prepare_input($_POST['db_username']);
            $db_password = tep_db_prepare_input($_POST['db_password']);
            $errors->clearErrors();
            $connection = new db();
            $connection->setErrorHandler($errors);
            @$connection->connect($db_host, $db_username, $db_password);
            if ($connection->connection->connect_errno === 0) {
                $sqlsize = filesize('db.sql');
                $max_alloved_packet = $connection->query("SHOW VARIABLES like 'max_allowed_packet'");
                $max_alloved_packet = $max_alloved_packet->fetch_assoc();
                $max_alloved_packet = $max_alloved_packet['Value'];
                if ($max_alloved_packet_check = $sqlsize > (int)$max_alloved_packet) {
                    $errors->addCustomError('DB_MAX_ALLOWED_PACKET', [$sqlsize + 1000000]);
                    $step = 2;
                } else {
                    $sql_dump = file_get_contents('db.sql');
                    $connection->query("DROP DATABASE IF EXISTS " . $db_database);
                    $connection->query(
                        "CREATE DATABASE IF NOT EXISTS " . $db_database . " character set UTF8 collate utf8_general_ci"
                    );
                    $connection->select_db($db_database);
                    $cumulative_rows = 0;
                    if ($connection->connection->multi_query($sql_dump)) {
                        do{
                            if (is_numeric($connection->connection->affected_rows)) {
                                $cumulative_rows += $connection->connection->affected_rows;
                            }
                        } while($connection->connection->more_results() && $connection->connection->next_result());
                    }

                    if ($error_mess = $connection->connection->errno) {
                        $errors->addCustomError('MYSQL_ERORO', ["Error: $error_mess" . $connection->connection->error. PHP_EOL]);
                        $step = 2;
                    } else {
                        @unlink('db.sql');
                        prepare_dot_env();
                    }
                }
            }

            break;
        }
    case 4:
        //        $dotenv1 = parse_ini_file('.env');
        $r = explode(PHP_EOL, file_get_contents('.env'));
        $r = array_map(function ($r) {
            return explode('=', $r);
        }, $r);
        $dotenv = [];
        foreach ($r as $arr) {
            if ($arr[0]) {
                $dotenv[$arr[0]] = $arr[1];
            }
        }

        $errors->clearErrors();
        $connection = new db();
        $connection->setErrorHandler($errors);
        $connection->connect($dotenv['DB_HOST'], $dotenv['DB_USERNAME'], $dotenv['DB_PASSWORD']);
        $connection->select_db($dotenv['DB_DATABASE']);
        $store_name = tep_db_prepare_input($_POST['store_name']);
        $admin_email = tep_db_prepare_input($_POST['admin_email']);
        $admin_password = $_POST['admin_password'];
        $connection->query("UPDATE configuration SET configuration_value = '$store_name' WHERE configuration_id = 1");
        $connection->query("UPDATE configuration SET configuration_value = '$store_name' WHERE configuration_id = 2");
        $connection->query("UPDATE configuration SET configuration_value = '$admin_email' WHERE configuration_id = 3");
        $connection->query("UPDATE configuration SET configuration_value = '$admin_email' WHERE configuration_id = 4");
        $connection->query(new_admin_sql($store_name, $admin_email, $admin_password));
        if (isset($_POST['https']) && $_POST['https'] == 'on') {
            rewriteHtaccess('true', '#HTTPS', 8);
            $connection->query("UPDATE configuration SET configuration_value = 'true' WHERE configuration_id = 2013");

        } else {
            $connection->query("UPDATE configuration SET configuration_value = 'false' WHERE configuration_id = 2013");
            rewriteHtaccess('', '#HTTPS', 8);
        }

        if (isset($_POST['remove_products']) && $_POST['remove_products'] == 'on') {
            $result = $connection->query("SELECT products_image FROM products WHERE products_image != ''");
            if ($result->num_rows) {
                while ($images = $result->fetch_assoc()) {
                    deleteProductsImages($images['products_image']);
                }
            }

            $table_to_truncate = [
                'categories',
                'featured',
                'specials',
                'categories_description',
                'products',
                'products_attributes',
                'products_attributes_download',
                'products_attributes_groups',
                'products_description',
                'products_notifications',
                'products_options',
                'products_options_values',
                'products_options_values_to_products_options',
                'products_to_categories',
                'products_xsell',
            ];
            foreach ($table_to_truncate as $table) {
                $connection->query("TRUNCATE TABLE `$table`");
            }
        }

        rewriteHtaccess('', '#INSTALL', 4);
        @unlink(__FILE__);
        break;
    default:
        break;
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= TITLE ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .warning, .success, .error {
            border-radius: 10px;
            padding: 0.8em;
            margin-bottom: 1em;
            background: #f9f9fa;
        }

        .warning {
            border: 2px solid #f8e343;
        }

        .success {
            border: 2px solid #1cca87;
        }

        .error {
            border: 2px solid #f00;
        }

        /*progress bar*/
        .progress_container {
            width: 805px;
            margin: 70px auto 90px;
            position: relative;
        }

        .progress {
            position: relative;
            display: flex;
        }

        .progress-track {
            position: absolute;
            top: 6px;
            width: 100%;
            height: 2px;
            background-color: #DEDEDE;
            z-index: -1;
        }

        .progress-step {
            position: relative;
            width: 100%;
            color: #979798;
        }

        .progress-step:last-child {
            width: auto;
        }

        .progress-step span {
            font-size: 80%;
            font-family: PTSans-Bold, sans-serif;
            position: absolute;
            transform: translateX(-50%);
            left: 4%;
        }

        .progress-step:last-child span {
            width: 125px;
        }

        .progress-step:last-child:after {
            display: none;
        }

        .progress-step:before {
            content: "";
            display: flex;
            margin: 0 auto 10px 0;
            width: 10px;
            height: 10px;
            background: #DEDEDE;
            border: 2px solid #DEDEDE;
            border-radius: 100%;
            color: #fff;
        }

        .progress-step:last-child::before {
            margin: 0 0 10px;
        }

        .progress-step:after {
            content: "";
            position: absolute;
            top: 6px;
            left: 3%;
            width: 0%;
            /*transition: width 1s ease-in;*/
            height: 2px;
            background: #DEDEDE;
            z-index: -1;
        }

        .progress-step.is-active {
            color: #555555;
        }

        .progress-step.is-active:before {
            border: 2px solid #979797;
            animation: pulse 2s infinite;
            background: #979797;
        }

        .progress-step.is-complete {
            color: #555555;
        }

        .progress-step.is-complete:before {
            color: #fff;
            background: #1CCA87;
            border: 2px solid transparent;
        }

        .progress-step.is-complete:after {
            background: #1CCA87;
            width: 100%;
            /*animation: nextStep 1s;*/
            /*animation-fill-mode: forwards;*/
        }

        .progress_container button {
            position: absolute;
            left: 10px;
            top: 58px;
        }

        @keyframes pulse {
            0% {
                box-shadow: 0 0 0 0 rgba(151, 151, 151, 0.4);
            }
            70% {
                box-shadow: 0 0 0 10px rgba(151, 151, 151, 0);
            }
            100% {
                box-shadow: 0 0 0 0 rgba(151, 151, 151, 0);
            }
        }

        @keyframes nextStep {
            0% {
                width: 0%;
            }
            100% {
                width: 100%;
            }
        }

        /*end progress bar*/

        body {
            background: #F9F9FA;
            font-family: PTSans-Regular, sans-serif;
        }

        .install_form-wrapper {
            max-width: 580px;
            margin: 0 auto;
        }

        .install_form-wrapper .install_message {
            margin-left: 20px;
            color: #9B9B9B;
            font-size: 13px;
        }

        .install_form-wrapper .install_help {
            color: #9B9B9B;
            font-size: 13px;
            margin-right: 20px;
            float: right;
        }

        .install_form-block {
            max-width: 540px;
            margin: 0 auto 10px;
            background: #fff;
            padding: 20px;
            border-radius: 5px;
            -webkit-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.3);
            -moz-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.3);
            box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.3);
        }

        .install_form-block .h2 {
            color: #555;
            font-size: 24px;
            letter-spacing: 0.6px;
            position: relative
        }

        .install_form-block p {
            color: #9B9B9B;
            font-size: 13px;
            letter-spacing: 0.6px;
            line-height: 16px;
        }

        .install_form .form-group, .form-group {
            display: flex;
            justify-content: space-between;
            align-items: center;
            flex-direction: row;
            margin-bottom: 20px;
            position: relative;
        }

        .install_form .form-group:last-child {
            margin-bottom: 0;
            display: block;
            height: 32px;
        }

        .install_form .next_step, .install_form .form-group input[type="checkbox"] {
            float: right;
        }

        .install_form .form-group input[type="checkbox"] {
            margin-right: 275px;
            margin-left: 0;
            margin-top: 12px;
            width: auto;
            cursor: pointer;
        }

        .install_form .form-group:last-child .h5 {
            display: inline-block;
            height: 32px;
            line-height: 32px;
        }

        .install_form .form-group .h5 {
            color: #555555;
            font-size: 15px;
            letter-spacing: 0.4px;
            display: flex;
            flex-direction: column;
        }

        .install_form .form-group span {
            color: #9B9B9B;
            font-size: 13px;
            margin-top: 3px;
        }

        .install_form .form-group input,
        .install_form .form-group select {
            width: 266px;
            padding: 9px 10px 6px;
            border: 2px solid #eee;
            border-radius: 5px;
            color: #555555;
            -webkit-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.1);
            -moz-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.1);
            box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.1);
        }

        .install_form .form-group input[type="text"], .install_form .form-group input[type="password"] {
            min-width: 266px;
        }

        .next_step, .prev_step {
            position: absolute;
            top: 0;
            background: #F8E342;
            color: #111111;
            padding: 7px 15px;
            border: 1px solid #f8e342;
            border-radius: 4px;
            text-decoration: none;
            font-size: 13px;
            -webkit-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.2);
            -moz-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.2);
            box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.2);
        }

        .next_step {
            right: 0;
        }

        .prev_step {
            left: 0;
            background: transparent;
            color: #5B5B5B;
            border: 1px solid #eee;
        }

        .install_form .form-group input::-webkit-input-placeholder {
            color: #CCCCCC;
            font-size: 13px;
            letter-spacing: 0.4px;
        }

        .install_form .form-group input::-moz-placeholder {
            color: #CCCCCC;
            font-size: 13px;
            letter-spacing: 0.4px;

        }

        .install_form .form-group input:-ms-input-placeholder {
            color: #CCCCCC;
            font-size: 13px;
            letter-spacing: 0.4px;
        }

        .install_form .form-group input:-moz-placeholder {
            color: #CCCCCC;
            font-size: 13px;
            letter-spacing: 0.4px;
        }

        .install_form .form-group .database_host::-webkit-input-placeholder, .install_form .form-group .port::-webkit-input-placeholder {
            color: #333;
        }

        .install_form .form-group .database_host::-moz-placeholder, .install_form .form-group .port::-moz-placeholder {
            color: #333;
        }

        .install_form .form-group .database_host:-ms-input-placeholder, .install_form .form-group .port:-ms-input-placeholder {
            color: #333;
        }

        .install_form .form-group .database_host:-moz-placeholder, .install_form .form-group .port:-moz-placeholder {
            color: #333;
        }

        .next_action-block {
            display: flex;
            margin: 20px 0;
            align-items: center;
            justify-content: space-evenly;
        }

        .next_action-block .store_link, .next_action-block .admin_panel {
            display: inline-block;
            background: #F8E342;
            color: #111111;
            padding: 7px 15px;
            border: 1px solid #f8e342;
            border-radius: 4px;
            text-decoration: none;
            font-size: 16px;
            -webkit-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.2);
            -moz-box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.2);
            box-shadow: 0px 0px 22px -5px rgba(51, 51, 51, 0.2);
        }

        @media (max-width: 992px) {
            .progress_container {
                width: 85%;
            }

            .install_form-wrapper {
                max-width: 78%;
            }

            .install_form-block {
                max-width: initial;
            }

            .install_form .form-group input[type="checkbox"] {
                float: right;
            }

            .install_form .form-group:last-child .h5 {
                display: inline-block;
            }
        }

        @media (max-width: 768px) {
            .progress_container {
                display: none;
            }

            .install_form-wrapper {
                max-width: 90%;
            }
        }

        @media (max-width: 680px) {
            .install_form .form-group {
                flex-direction: column;
                align-items: flex-start;
                margin-bottom: 10px;
            }

            .install_form .form-group.fg_check {
                flex-direction: row;
            }

            .install_form .form-group .h5 {
                margin-bottom: 5px;
            }

            .install_form .form-group input,
            .install_form .form-group select {
                max-width: 100%;
                width: calc(100% - 24px);
            }

            .install_form .form-group input[type="text"], .install_form .form-group input[type="password"] {
                min-width: unset;
            }

            .install_form .form-group:last-child {
                height: 64px;
            }

            .form-group:last-child .next_step, .form-group:last-child .prev_step {
                bottom: 0;
                top: auto;
            }

            .install_form .form-group input[type="checkbox"] {
                margin-right: 0;
            }

            .install_form-wrapper .install_message {
                margin-left: 0;
            }

            .install_form-wrapper .install_help {
                margin-right: 0;
            }
        }
    </style>
</head>
<body>
<div class="progress_container">
    <div class="progress">
        <div class="progress-track"></div>
        <?php
        $step_arr = [
            1 => STEP_1,
            2 => STEP_2,
            3 => STEP_3,
            4 => STEP_4,
        ];
        foreach ($step_arr as $step_id => $step_text) {
            if ($step < $step_id) {
                $class = '';
            } elseif ($step == $step_id) {
                $class = ' is-active';
            } else {
                $class = ' is-complete';
            }

            echo '<div id="step', $step_id, '" class="progress-step', $class, '">', '<span>', $step_text, '</span>', '</div>';
        } ?>
    </div>
    <!--        <button onClick=next()>Next Step</button>-->
</div>
<?php
if (file_exists('.env') && !file_exists('db.sql') && file_exists('install.php')) {
    $step = 3;
}
?>
<div class="install_form-wrapper">
    <div id="install_form-block" class="install_form-block">
        <?php
        if ($errors->isHaveErrors() || $step == 1) {
            $refreshPage = false;
            echo '<p>', INSTALL_REQUIREMENTS, '</p>';
            foreach ($errors->checkRequirements() as $error => $check) {
                $class = ($check ? 'warning' : 'success');
                if ($check) {
                    $refreshPage = true;
                }
                echo '<div class="', $class, '">', constant($error), '</div>';
            }

            echo '<p>', RECOMMENDED_TO_INSTALL_ON_SERVER, ':</p>' . PHP_EOL;
            if (($ini = ini_get('display_errors')) !== RECOMMENDED_DISPLAY_ERRORS) {
                echo '<div class="warning">';
                echo 'display_errors = ', $ini, '</br>', RECOMMENDED_TO_INSTALL, ' 1</br>';
                echo '</div>';
            }

            if (($ini = ini_get('max_execution_time')) !== RECOMMENDED_MAX_EXEXUTION_TIME) {
                echo '<div class="warning">';
                echo 'max_execution_time = ', $ini, '</br>', RECOMMENDED_TO_INSTALL, ' 0</br>';
                echo '</div>';
            }

            if (($ini = ini_get('memory_limit')) !== RECOMMENDED_MEMORY_LIMIT) {
                echo '<div class="warning">';
                echo 'memory_limit = ', $ini, '</br>', RECOMMENDED_TO_INSTALL, ' -1</br>';
                echo '</div>';
            }

            if (($ini = ini_get('display_startup_errors')) !== RECOMMENDED_DISPLAY_STARTUP_ERRORS) {
                echo '<div class="warning">';
                echo 'display_startup_errors = ', $ini, '</br>', RECOMMENDED_TO_INSTALL, ' 1</br>';
                echo '</div>';
            }
        }

        foreach ($errors->getCustomErrors() as $errorText => $params) {
            echo '<div class="error">', defined($errorText) ? $errors->getCustomError($errorText) : $errorText, '</div>';
        }

        if ($step === 1): ?>
            <div class="h2"><?= SELECT_LANGUAGE ?></div>
            <p><?= STEP_1_TEXT_1 ?></p>
            <form method="post" class="install_form">
                <input type="hidden" name="step" id="step" value="<?= $step ?>">
                <div class="form-group">
                    <div class="h5"><?= STEP_1_TEXT_2 ?><span><?= STEP_1_TEXT_3 ?></span></div>
                    <?php $lang_array = [
                        'en' => 'English',
                        'uk' => 'Українська',
                        'ru' => 'Русский',
                        'pl' => 'Polish',
                        'es' => 'Spanish',
                        'fr' => 'Français',
                        'de' => 'German',
                        'el' => 'Greek',
                        'nl' => 'Nederlands',
                    ];
                    ?>
                    <select name="lang" id="install_languages" onchange="this.form.submit()">
                        <?php foreach ($lang_array as $lang_id => $lang_text): ?>
                            <option value="<?= $lang_id ?>"<?= $lang_id == $lang ? ' selected' : '' ?>><?= $lang_text ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <?php if ($refreshPage): ?>
                        <a href="install.php"><?= REFRESH_PAGE ?></a>
                    <?php else: ?>
                        <button type="submit" class="next_step"
                                onclick="this.nextElementSibling.style.display = 'block';this.style.display = 'none';document.getElementById('step').value = parseInt(document.getElementById('step').value)+1"><?= NEXT_STEP ?>
                            →
                        </button>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                             style="display : none; margin: auto; width: 55px;height: 55px; shape-rendering: auto;"
                             width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
                            <g transform="rotate(0 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.9166666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(30 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.8333333333333334s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(60 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s"
                                             repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(90 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.6666666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(120 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.5833333333333334s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(150 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s"
                                             repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(180 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.4166666666666667s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(210 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.3333333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(240 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s"
                                             repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(270 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.16666666666666666s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(300 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s"
                                             begin="-0.08333333333333333s" repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                            <g transform="rotate(330 50 50)">
                                <rect x="47" y="24" rx="3" ry="6" width="6" height="12" fill="#feee71">
                                    <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s"
                                             repeatCount="indefinite"></animate>
                                </rect>
                            </g>
                        </svg>
                    <?php endif; ?>
                </div>
            </form>
        <?php endif; ?>
        <?php if ($step === 2): ?>
        <div class="h2"><?= STEP_2_TEXT_1 ?></div>
        <p><?= STEP_2_TEXT_2 ?></p>
        <?= '<div style="display: none;" class="warning">', '</div>' ?>
        <form id="formMysql" name="sql" method="post" class="install_form">
            <input type="hidden" name="step" id="step" value="<?= $step + 1 ?>">
            <input type="hidden" name="lang" id="lang" value="<?= $lang ?>">
            <div class="form-group">
                <div class="h5"><?= STEP_2_FORM_INPUT_1 ?><span><?= STEP_2_FORM_INPUT_HELP_1 ?></span></div>
                <input type="text" required pattern="[\.a-zA-Z0-9\-]{1,64}" name="db_host" class="database_host"
                       value="<?= isset($_POST['db_host']) ? $_POST['db_host'] : 'localhost' ?>">
            </div>
            <div class="form-group">
                <div class="h5"><?= STEP_2_FORM_INPUT_2 ?><span><?= STEP_2_FORM_INPUT_HELP_2 ?></span></div>
                <input type="text" required pattern="[a-zA-Z0-9_-]{1,64}" name="db_database"
                       value="<?= isset($_POST['db_database']) ? $_POST['db_database'] : '' ?>">
            </div>
            <div class="form-group">
                <div class="h5"><?= STEP_2_FORM_INPUT_3 ?><span><?= STEP_2_FORM_INPUT_HELP_3 ?></span></div>
                <input type="text" required pattern="[a-zA-Z0-9_-]{1,64}" name="db_username"
                       value="<?= isset($_POST['db_username']) ? $_POST['db_username'] : '' ?>">
            </div>
            <div class="form-group">
                <div class="h5"><?= STEP_2_FORM_INPUT_4 ?><span><?= STEP_2_FORM_INPUT_HELP_4 ?></span></div>
                <input type="password" name="db_password"
                       value="<?= isset($_POST['db_password']) ? $_POST['db_password'] : '' ?>">
            </div>
            <!--            <div class="form-group">-->
            <!--                <div class="h5">--><? //= STEP_2_FORM_INPUT_5 ?><!--:<span>-->
            <? //=$_SERVER['HTTP_ORIGIN']?><!--/</span></div>-->
            <!--                <input type="text" name="subfolder" pattern="[a-zA-Z0-9_-]{0,64}" value="-->
            <? //= isset($_POST['subfolder']) ? $_POST['subfolder'] : '' ?><!--">-->
            <!--            </div>-->
            <div class="form-group">
                <div class="h5"><?= STEP_2_FORM_INPUT_6 ?></div>
                <input type="text" required name="admin_folder" pattern="[a-zA-Z0-9_-]{0,64}"
                       value="<?= isset($_POST['admin_folder']) ? $_POST['admin_folder'] : 'admin' ?>">
            </div>
            <div class="form-group">
                <button id="sql-button" type="submit" class="next_step"><?= NEXT_STEP ?> →</button>
            </div>
            <?php endif; ?>
            <?php if ($step === 3): ?>
                <div class="h2"><?= STEP_3_TEXT_1 ?></div>
                <p><?= STEP_3_TEXT_2 ?></p>
                <form method="post" class="install_form">
                    <input type="hidden" name="step" id="step" value="<?= $step + 1 ?>">
                    <input type="hidden" name="lang" id="lang" value="<?= $lang ?>">
                    <div class="form-group">
                        <div class="h5"><?= STEP_3_FORM_INPUT_1 ?><span><?= STEP_3_FORM_INPUT_HELP_1 ?></span></div>
                        <input type="text" name="store_name" required placeholder="Solomono" maxlength="32">
                    </div>
                    <div class="form-group">
                        <div class="h5"><?= STEP_3_FORM_INPUT_2 ?><span><?= STEP_3_FORM_INPUT_HELP_2 ?></span></div>
                        <input type="email" name="admin_email"
                               pattern="([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})" required
                               placeholder="example@store.com">
                    </div>
                    <div class="form-group">
                        <div class="h5"><?= STEP_3_FORM_INPUT_3 ?><span><?= STEP_3_FORM_INPUT_HELP_3 ?></span></div>
                        <input type="password" name="admin_password" required minlength="1" maxlength="32">
                    </div>
                    <div class="form-group fg_check">
                        <div class="h5"><?= STEP_3_FORM_INPUT_4 ?></div>
                        <input <?= isRedirectHttpToHttps() ? 'checked' : '' ?> type="checkbox" name="https">
                    </div>
                    <div class="form-group fg_check">
                        <div class="h5"><?= STEP_3_FORM_INPUT_5 ?></div>
                        <input type="checkbox" name="remove_products">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="next_step"><?= NEXT_STEP ?> →</button>
                    </div>
                </form>
            <?php endif; ?>
            <?php if ($step === 4): ?>
                <div class="h2"><?= STEP_4_TEXT_1 ?></div>
                <div class="next_action-block">
                    <a href="/<?= $dotenv['CATALOG_FOLDER'] ? $dotenv['CATALOG_FOLDER'] . '/' : '' ?>" target="_blank"
                       class="store_link"><?= STEP_4_TEXT_2 ?></a>
                    <a href="/<?= ($dotenv['CATALOG_FOLDER'] ? $dotenv['CATALOG_FOLDER'] . '/' : '') . $dotenv['ADMIN_FOLDER'] . '/?token=' . MD5('first login') ?>"
                       target="_blank" class="admin_panel"><?= STEP_4_TEXT_3 ?></a>
                </div>
            <?php endif; ?>
    </div>
    <span class="install_message"><?= sprintf(INSTALL_MESSAGE, 'Solomono', 'v1.0') ?></span>
    <a href="https://solomono.net/instructions-t-23.html" class="install_help" target="_blank"><?= INSTALL_HELP ?></a>
    <script>
        if (document.getElementsByName("sql").length) {
            document.getElementById('sql-button').onclick = function() {
                var	dbLogin = document.querySelector("form[name='sql'] input[name='db_username']").value;
                var dbPass = document.querySelector("form[name='sql'] input[name='db_password']").value;
                var dbHost = document.querySelector("form[name='sql'] input[name='db_host']").value;
                var dbName = document.querySelector("form[name='sql'] input[name='db_database']").value;
                var dbObj = {
                    dbLogin: dbLogin,
                    dbPass: dbPass,
                    dbHost: dbHost,
                    dbName: dbName
                };
                var errors = chechEmptyInputObj(dbObj);
                if (!Object.keys(errors).length) {
                    var request = new XMLHttpRequest();
                    var url = 'install.php';
                    var params = "action=chceckDb&dbLogin=" + dbLogin + "&dbPass=" + dbPass  + "&dbHost=" + dbHost + "&lang=<?= $lang ?>";
                    request.open("POST", url, true);
                    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    request.addEventListener("readystatechange", () => {
                        if(request.readyState === 4 && request.status === 200) {
                            var r = JSON.parse(request.responseText);
                            var boxElem = document.querySelector(".error");
                            if (r.result === "error") {
                                if (boxElem) {
                                    boxElem.innerHTML = r.message;
                                } else {
                                    var parentElement = document.getElementById('install_form-block');
                                    var theFirstChild = parentElement.firstChild;
                                    var newElement = createDiv(r.message);
                                    parentElement.insertBefore(newElement, theFirstChild);
                                }
                            } else {
                                if (boxElem) {
                                    boxElem.remove();
                                }

                                if (typeof r.message !== 'undefined' && r.message !== '') {
                                    document.querySelector(".warning").innerHTML = r.message;
                                    document.querySelector(".warning").style.display = 'block';
                                } else {
                                    document.querySelector(".warning").style.display = 'none';
                                    document.getElementById('formMysql').submit();
                                }
                            }
                        }
                    });
                    request.send(params);
                } else {
                    var boxElem = document.querySelector(".error");
                    var message = Object.values(errors).join('<br/>');
                    if (boxElem) {
                        boxElem.innerHTML = message;
                    } else {
                        var parentElement = document.getElementById('install_form-block');
                        var theFirstChild = parentElement.firstChild;
                        var newElement = createDiv(message);
                        parentElement.insertBefore(newElement, theFirstChild);
                    }
                }
                return false;
            };
        }

        function createDiv(message) {
            var div = document.createElement("div");
            div.className = 'error';
            div.innerHTML = message;
            return div;
        }

        function isBlank(str) {
            return (!str || /^\s*$/.test(str) || str.length === 0);
        }

        function chechEmptyInputObj (obj) {
            var errors = {
                dbLogin: "<?= htmlspecialchars(DB_LOGIN_ERROR, ENT_QUOTES) ?>",
                dbPass: "<?= htmlspecialchars(DB_PASS_ERROR, ENT_QUOTES) ?>",
                dbHost: "<?= htmlspecialchars(DB_HOST_ERROR, ENT_QUOTES) ?>",
                dbName: "<?= htmlspecialchars(DB_NAME_ERROR, ENT_QUOTES) ?>"
            };

            for (var prop in obj) {
                if (!isBlank(obj[prop])) {
                    delete errors[prop];
                }
            }

            return errors;
        }
    </script>
</div>
</body>
</html>