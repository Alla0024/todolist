<?php

require('includes/application_top.php');

require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_DEALER);


// BOF: BugFix: Spam mailer exploit
$sanita = array("|([\r\n])[\s]+|","@Content-Type:@");
$enquiry = '';
foreach ($_POST as $key => $value) {
    $value = tep_db_prepare_input(preg_replace($sanita, " ", $value));
    if (mb_strtoupper($key) !== 'G-RECAPTCHA-RESPONSE') {
        $enquiry .= constant(mb_strtoupper($key)) . ' ' . $value . '<br/>';
    }
}
// EOF: BugFix: Spam mailer exploit

$error = false;

if (isset($_GET['action']) && ($_GET['action'] == 'send')) {
    if (GOOGLE_RECAPTCHA_STATUS === 'true') {
        if ($_SESSION['recaptcha'] !== true) {

            $messageStack->add('contact', 'reCaptcha error');
            $message = $messageStack->render('contact', 'div', true);
            echo json_encode([
                'status' => 'fail',
                'msg' => $message
            ]);
            die;
        }
    }
    $email_address = tep_db_prepare_input($_POST['email']);
    $enquiry .= EMAIL_SUBJECT . " " . $_SERVER['SERVER_NAME'] . "<br />IP: " . $_SERVER['REMOTE_ADDR'];

    if (tep_validate_email($email_address)) {
        tep_mail(STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS, EMAIL_SUBJECT, $enquiry, $name, STORE_OWNER_EMAIL_ADDRESS);

        $messageStack->add('dealer', TEXT_SUCCESS, 'success');
        $message = $messageStack->render('dealer', 'div', true);
        unset($_SESSION['recaptcha']);
        echo json_encode(array('status'=>'success','msg'=>$message));
    } else {
        $messageStack->add('dealer', ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
        $message = $messageStack->render('dealer', 'div', true);
        echo json_encode(array('status'=>'fail','msg'=>$message));
    }

    die();
}

$breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_DEALER));

$content = CONTENT_DEALER;

require(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/' . TEMPLATENAME_MAIN_PAGE);

require(DIR_WS_INCLUDES . 'application_bottom.php');
