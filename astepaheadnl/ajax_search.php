<?php

/*
* Last modified kzm.pianist 
* Date: 16.09.2013
*/

require('includes/application_top.php');

die();
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    if ($_GET['q']) {
        $_GET['q'] = tep_db_input($_GET['q']);
        $name      = explode(' ', $_GET['q']);
        foreach ($name as $n) {
            $query_name[]  = 'LOWER(pd.products_name) LIKE "%' . mb_strtolower($n) . '%"';
            $query_model[] = 'LOWER(p.products_model) LIKE "%' . mb_strtolower($n) . '%"';
        }
        $query_name  = implode(' AND ', $query_name);
        $query_model = implode(' AND ', $query_model);
        $time        = microtime(true);

        $sql   = "SELECT pd.products_id
            FROM products_description pd
            LEFT JOIN products p ON pd.products_id = p.products_id
            WHERE ((" . $query_name . ")  or   (" . $query_model . ")   )
																 AND p.products_status=1 and pd.language_id = '{$languages_id}' ";
        $milliseconds = round(microtime(true) * 1000);    
        $query = tep_db_query($sql);
        if ($query->num_rows) {
            $ids = [];

            while ($row = tep_db_fetch_array($query)) {
                $ids[] = $row['products_id'];
            }
            $ids = implode(',', $ids);
            
            $spec_array = get_specials(" p.products_id in(".$ids.") ");

            $search_query = tep_db_query('SELECT p.products_id, 
          																			 pd.products_name, 
          																		 	 p.products_model, 
          																			 p.products_image, 
                                                 p.products_tax_class_id,
                                                 p.manufacturers_id, 
                                                 p.products_price
          															 	  FROM products p, products_description pd 
          																 WHERE p.products_id in(' . $ids . ') and pd.products_id=p.products_id and pd.language_id = ' . $languages_id . ' and ' . time() . '
                                           ORDER BY p.products_quantity !=0 desc');
                                 
            $salemakers_array = get_salemakers($search_query);
            mysqli_data_seek($search_query, 0);

            while ($search_product = tep_db_fetch_array($search_query)) {
                $search_id       = $search_product['products_id'];
                $search_name     = $search_product['products_name'];
                $search_model    = $search_product['products_model'];
                
                if($spec_array[$search_product['products_id']]) $spec_price = $spec_array[$search_product['products_id']];
          	    elseif($salemakers_array[$search_product['products_id']]) $spec_price = $salemakers_array[$search_product['products_id']];
          	    else $spec_price = $search_product['products_price']; 
                 
                $search_price    = $currencies->display_price($spec_price, tep_get_tax_rate($search_product['products_tax_class_id']));
                $search_category = $cat_names[$prodToCat[$search_product['products_id']]];

                // image
                $search_image = explode(';', $search_product['products_image']);
                if (empty($search_image[0])) {
                    $search_image[0] = 'default.png';
                }
                $search_image_filename = 'getimage/80x80/products/' . $search_image[0];
                // end image
                //    print $search_name.'|'.$search_category.'|'.$search_image_filename.'|'.$search_id.'|'.$search_price."\n";
                //      print '<span class="search__name">'.$search_name.'</span> ('.$search_model.')|'.$search_category.'|'.$search_image_filename.'|'.$search_id.'|'.$search_price."\n";
                print $search_name . ' (' . $search_model . ')|' . $search_category . '|' . $search_image_filename . '|' . $search_id . '|' . $search_price . "\n";
            }
        }
        //print round(microtime(true) * 1000)-$milliseconds;
        //echo $query_total_time;
    }
}
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
