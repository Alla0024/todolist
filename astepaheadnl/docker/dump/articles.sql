-- Adminer 4.8.1 MySQL 8.0.12 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `articles_id` int(11) NOT NULL AUTO_INCREMENT,
  `articles_date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `articles_last_modified` datetime DEFAULT NULL,
  `articles_date_available` datetime DEFAULT NULL,
  `articles_link` varchar(255) NOT NULL DEFAULT '',
  `articles_image` varchar(255) NOT NULL DEFAULT '',
  `articles_image_mobile` varchar(255) NOT NULL DEFAULT '',
  `articles_status` enum('0','1') NOT NULL DEFAULT '1',
  `articles_robots_status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `authors_id` int(11) DEFAULT '0',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `articles_code` varchar(100) DEFAULT NULL,
  `store_id` int(11) DEFAULT '0',
  PRIMARY KEY (`articles_id`),
  KEY `idx_articles_date_added` (`articles_date_added`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `articles` (`articles_id`, `articles_date_added`, `articles_last_modified`, `articles_date_available`, `articles_link`, `articles_image`, `articles_image_mobile`, `articles_status`, `articles_robots_status`, `authors_id`, `sort_order`, `articles_code`, `store_id`) VALUES
(68,	'2020-01-14 00:03:16',	'2023-02-25 22:53:19',	'0000-00-00 00:00:00',	'',	'',	'',	'1',	1,	NULL,	1,	'mainpage',	0),
(70,	'2020-01-14 00:03:16',	'2021-09-08 08:00:58',	'0000-00-00 00:00:00',	'',	'',	'',	'1',	1,	NULL,	2,	'banner_long',	0),
(71,	'2020-01-14 00:03:16',	'2021-07-29 08:38:53',	'0000-00-00 00:00:00',	'',	'articles/free-shipping.png',	'',	'1',	1,	NULL,	5,	'banner_2',	0),
(72,	'2020-01-14 00:03:16',	'2020-01-14 00:03:16',	'0000-00-00 00:00:00',	'',	'',	'',	'0',	1,	NULL,	4,	'banner_3',	0),
(73,	'2020-01-14 00:03:16',	'2021-07-29 08:17:35',	'0000-00-00 00:00:00',	'',	'',	'',	'1',	1,	NULL,	3,	'banner_1',	0),
(75,	'2020-01-14 00:03:16',	'2023-02-24 17:12:33',	'0000-00-00 00:00:00',	'',	'',	'',	'1',	1,	NULL,	1,	'phones',	0),
(76,	'2020-01-14 00:03:16',	'2023-02-08 16:33:49',	'0000-00-00 00:00:00',	'contact_us.php',	'',	'',	'0',	1,	NULL,	11,	'contacts',	0),
(77,	'2020-01-14 00:03:16',	'2020-01-14 00:03:16',	'0000-00-00 00:00:00',	'/',	'',	'',	'1',	1,	NULL,	1,	'',	0),
(79,	'2020-01-14 00:03:16',	'2023-02-08 17:21:17',	'2023-02-08 00:00:00',	'',	'',	'',	'1',	1,	NULL,	8,	'shipping_product_info_tab',	0),
(80,	'2020-01-14 00:03:16',	'2023-02-08 17:04:49',	'2018-03-15 00:00:00',	'',	'articles/LOGO_Apple_2.jpg',	'',	'1',	1,	NULL,	7,	'',	0),
(81,	'2020-01-14 00:03:16',	'2023-02-08 16:33:55',	'2018-04-06 00:00:00',	'',	'',	'',	'0',	1,	NULL,	10,	'',	0),
(82,	'2020-01-14 00:03:16',	'2022-08-11 11:50:38',	'2018-04-19 00:00:00',	'',	'',	'',	'1',	1,	NULL,	1,	'shipping_product_info',	0),
(83,	'2020-01-14 00:03:16',	'2023-02-26 00:39:15',	'0000-00-00 00:00:00',	'',	'',	'',	'1',	1,	NULL,	1,	'contacts_footer',	0),
(86,	'2020-01-14 00:03:16',	'2023-02-25 20:22:29',	'0000-00-00 00:00:00',	'',	'',	'',	'1',	1,	NULL,	1,	'',	0),
(108,	'2020-01-14 00:03:16',	'2022-06-06 10:44:21',	'0000-00-00 00:00:00',	'https://solomono.net',	'',	'articles/e0de6d4799985e65b7dbc649d1da9a05slider_images_rus_mob-33.png',	'0',	1,	NULL,	3,	'',	0),
(90,	'2020-01-14 00:03:16',	'2022-06-06 10:21:46',	'0000-00-00 00:00:00',	'',	'',	'articles/a669c6ff5e4db04bc7e0f7d5e387fb06slider_images_rus_22.png',	'0',	1,	NULL,	2,	'',	0),
(98,	'2020-01-14 00:03:16',	NULL,	'0000-00-00 00:00:00',	'',	'articles/Redmi Y.jpg',	'',	'0',	1,	NULL,	0,	NULL,	0),
(95,	'2020-01-14 00:03:16',	NULL,	NULL,	'',	'articles/news_1.png',	'',	'0',	1,	NULL,	0,	NULL,	0),
(109,	'2020-01-14 00:03:16',	NULL,	'0000-00-00 00:00:00',	'',	'',	'',	'0',	1,	NULL,	1,	NULL,	0),
(99,	'2020-01-14 00:03:16',	NULL,	'0000-00-00 00:00:00',	'',	'articles/Redmi Y.jpg',	'',	'0',	1,	NULL,	0,	NULL,	0),
(100,	'2020-01-14 00:03:16',	NULL,	'0000-00-00 00:00:00',	'',	'articles/Redmi Y.jpg',	'',	'0',	1,	NULL,	0,	NULL,	0),
(120,	'2020-01-14 00:03:16',	'2022-07-06 11:45:53',	'0000-00-00 00:00:00',	'',	'articles/televizor_1.jpg',	'',	'0',	1,	0,	1,	'',	0),
(121,	'2020-01-14 00:03:16',	'2022-12-05 21:13:17',	'0000-00-00 00:00:00',	'/garantija/a-80.html',	'',	'',	'1',	1,	0,	2,	'',	0),
(110,	'2020-01-14 00:03:16',	NULL,	'0000-00-00 00:00:00',	'',	'',	'',	'0',	1,	NULL,	1,	NULL,	0),
(111,	'2020-01-14 00:03:16',	'2020-01-14 00:03:16',	'2018-05-26 00:00:00',	'',	'',	'',	'1',	1,	NULL,	1,	'store-rules',	0),
(117,	'2020-01-14 00:03:16',	'2020-01-14 00:03:16',	'0000-00-00 00:00:00',	'',	'articles/rekord_1.jpg',	'',	'0',	1,	0,	1,	'',	0),
(118,	'2020-01-14 00:03:16',	'2020-01-14 00:03:16',	'0000-00-00 00:00:00',	'',	'articles/tranzistori_2.jpg',	'',	'0',	1,	0,	1,	'',	0),
(119,	'2020-01-14 00:03:16',	'2020-01-14 00:03:16',	'0000-00-00 00:00:00',	'',	'articles/Mixcder_1.jpg',	'',	'0',	1,	0,	1,	'',	0),
(122,	'2020-01-14 00:03:16',	'2022-12-05 21:14:20',	'0000-00-00 00:00:00',	'/remonty/a-1172.html',	'',	'',	'0',	1,	0,	3,	'',	0),
(123,	'2020-04-24 05:15:57',	'2020-04-24 05:45:05',	'2020-04-24 00:00:00',	'',	'',	'',	'1',	1,	0,	1,	'alert_message',	0),
(124,	'2021-03-30 06:35:51',	'2021-07-29 08:32:39',	'2021-03-30 00:00:00',	'',	'',	'',	'0',	1,	0,	5,	'https://www.youtube.com/watch?v=RsAxvvmHvJY',	0);
