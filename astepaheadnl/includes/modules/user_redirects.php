<?php


$redirectQuery = tep_db_query("SELECT 
r.redirect_from, r.redirect_to
FROM redirects r
WHERE r.status = 1");

while ($row = tep_db_fetch_array($redirectQuery)){
    if(strpos($_SERVER['REQUEST_URI'], $row['redirect_from'])!==False){
        header("HTTP/1.0 301 Moved Permanently");
        header("Location: " . $row['redirect_to']);
        tep_exit();
    }
}
