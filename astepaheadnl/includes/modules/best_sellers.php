<?php
$listing_sql = "SELECT p.products_id 
                     FROM " . TABLE_PRODUCTS . " p      
                LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.products_id = p2c.products_id                                   
                    WHERE p2c.categories_id in(" . $all_active_cats . ")                             
                      AND p.products_status = '1' 
                      AND p.products_ordered > 0
                 ORDER BY  " . ($tpl_settings['orderby'] ? : 'p.products_quantity !=0 desc, p.products_ordered desc') . " 
                           " . ($tpl_settings['limit'] ? 'LIMIT ' . $tpl_settings['limit'] : '');

$listing_sql = tep_get_query_products_info($listing_sql); // split query to 2 small queries: 1) find all products ids, 2) get info for each product
$best_sellers = tep_db_query($listing_sql);
$salemakers_array = get_salemakers($best_sellers);
mysqli_data_seek($best_sellers, 0);

if ($best_sellers->num_rows and $tpl_settings['disable_listing'] != true) {
    $tpl_settings['request'] = $best_sellers;
    include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
}
?>