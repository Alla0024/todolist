<?php

$rootDirectory = __DIR__ . "/../../../";
$pathToPaymentModule   = 'ext/acquiring/wayforpay/wayforpay.php';

if (CARDS_ENABLED == 'true' && file_exists($rootDirectory . $pathToPaymentModule)) {
    require_once $rootDirectory . $pathToPaymentModule;
} else {
    class wayforpay {
        var $code, $title, $description, $enabled;

        function __construct() {
        }

        function check() {
            return false;
        }
    }
}
