<?php
if ($_GET['products_id']) {

    if (is_array($all_active_cats = $cat_list[0])) $all_active_cats = implode(',', $all_active_cats);
    else $all_active_cats = 0;
    if (empty($all_active_cats)) $all_active_cats = 0;
    $xsell_query = "select distinct xp.discount, p.products_id
                    from " . TABLE_PRODUCTS_XSELL . " xp," . TABLE_PRODUCTS . " p      
                LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.products_id = p2c.products_id                                   
                    WHERE p2c.categories_id in(".$all_active_cats.")
                      AND xp.products_id = '" . $_GET['products_id'] . "' 
                      AND p.products_status = '1'
                      and xp.xsell_id = p.products_id 
                 ORDER BY  ".($tpl_settings['orderby']?:'p.products_quantity !=0 desc, xp.sort_order asc')." 
                    limit " . (defined('MAX_DISPLAY_ALSO_PURCHASED')?MAX_DISPLAY_ALSO_PURCHASED:10);

    $module_products_query = tep_get_query_products_info($xsell_query);
    $module_products       = tep_db_query($module_products_query);
    $salemakers_array      = get_salemakers($module_products);
    mysqli_data_seek($module_products, 0);

    if ($module_products->num_rows and $tpl_settings['disable_listing'] != true) {
        $tpl_settings = array(
            'request' => $module_products,
            'id'      => 'xsell',
            'classes' => array('product_slider'),
            'cols'    => '1;2;4;4',
            'title'   => PROD_XSELL_TITLE,
        );
        include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
    }
}
?>