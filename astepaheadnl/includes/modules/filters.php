<?php
$allPidsByCat = $attr_vals_array = [];
$uniqueOptionsArray = implode(',', array_unique($show_options_arr));

if(!function_exists('getOptionValueData')) {
    function getOptionValueData($at_id,$at_val_id,$at_val_name){
        global $redirectOptionsIdsArrayForCheck,$counts_array_true,$current_category_id,$tempSeoFilterInfo;
        $checked_attr = '';
        $currentOptionVals = $redirectOptionsIdsArrayForCheck;
        $splitAttr = explode('-', $_GET[$at_id]);
        foreach ($splitAttr as $val) {
            if ($at_val_id == $val) {
                $checked_attr = 'checked';
            }
        }
        if (isset($currentOptionVals[$at_id])){
            if (($keyToRemove = array_search($at_val_id,$splitAttr)) !== false){
                unset($splitAttr[$keyToRemove]);
            }else{
                $splitAttr[]=$at_val_id;
            }
            if ($splitAttr) {
                $currentOptionVals[$at_id] = implode('-', $splitAttr);
            } else {
                unset($currentOptionVals[$at_id]);
            }
        }else{
            $currentOptionVals[$at_id] = $at_val_id;
        }
        $currentAttributesList = [];
        foreach ($currentOptionVals as $val){
            $currentAttributesList = array_merge($currentAttributesList,explode('-',$val));
        }
        // output values of current attribute (option)
        $counts_number = $counts_array_true[$at_id][$at_val_id] ? ' ('.$counts_array_true[$at_id][$at_val_id].')' : '';
        //if($counts_array_true[$at_id][$at_val_id]!=0) {
        //                                    if ($checked_attr) $counts_number = '';
        sort($currentAttributesList);

        if (isCustomSeoUrlExist($current_category_id,$_GET['filter_id'],$currentAttributesList)) {
            unset($tempSeoFilterInfo);
            $filterText = '<a href="' . getFilterUrl($_GET['cPath'], $_GET['filter_id'], $currentOptionVals) . '">' . $at_val_name . $counts_number . '</a>';
        }else{
            $filterText = $at_val_name . $counts_number;
        }
        return [
            'count'=>$counts_number,
            'checked'=>$checked_attr,
            'text'=>$filterText,
        ];
    }
}

$uniqueOptionsString = '';
if (!empty($uniqueOptionsArray)) {
    $uniqueOptionsString = "and pa.options_id in({$uniqueOptionsArray})";
}

$allPidsByCat      = $attr_vals_array = [];
$allPidsByCatQuery = tep_db_query("
            SELECT  p2c.products_id FROM products_to_categories p2c 
                LEFT JOIN products p 
                    ON p2c.products_id = p.products_id               
            WHERE $where_subcategories p.products_status = 1 ");
while ($row = tep_db_fetch_array($allPidsByCatQuery)) {
    $allPidsByCat[$row['products_id']] = $row['products_id'];
}
if ($allPidsByCat) {
    $allPidsByCatList  = implode(',', $allPidsByCat);
    $allPidsByCatQuery = tep_db_query("
            SELECT pa.options_values_id, pa.options_id FROM products_attributes pa                    
            WHERE pa.products_id in (" . $allPidsByCatList . ") $uniqueOptionsString");
    while ($row = tep_db_fetch_array($allPidsByCatQuery)) {
        $attr_vals_array[$row['options_id']][$row['options_values_id']] = $row['options_values_id'];
    }
}

$r_content = '';

// show max price (for Price Range Filter):     //  " .$ifs. " deleted from query
if (!empty($pids_price_filter_excluded)) {
    $all_pids_price_excluded = implode(', ', $pids_price_filter_excluded);
    $all_pids_price_excluded = " p.products_id in ($all_pids_price_excluded) ";
}
if ($all_pids_price_excluded != '') {

    if (DISPLAY_PRICE_WITH_TAX == 'true') {
        $listing_sql_max_query = tep_db_query("select MAX(`p`.`products_price` * ((100 + if(`p`.`products_tax_class_id` != 0,`tr`.`tax_rate`,0)) / 100)) AS `max_price`, MIN(`p`.`products_price` * ((100 + if(`p`.`products_tax_class_id` != 0,`tr`.`tax_rate`,0)) / 100)) AS `min_price` from " . TABLE_PRODUCTS . " p LEFT JOIN " . TABLE_TAX_RATES . " tr on p.products_tax_class_id = tr.tax_class_id where " . $all_pids_price_excluded . " ");
    } else {
        $listing_sql_max_query = tep_db_query("select MAX(p.products_price) as max_price, MIN(p.products_price) as min_price from " . TABLE_PRODUCTS . " p where " . $all_pids_price_excluded . " ");
    }

    $listing_sql_max = tep_db_fetch_array($listing_sql_max_query);
    $listing_sql_max['max_price'] = ceil($ccr * $listing_sql_max['max_price']);  // with current_currency_rate
    $listing_sql_max['min_price'] = floor($ccr * $listing_sql_max['min_price']);  // with current_currency_rate
}
$rmin = (($_GET['rmin'] != '') ? $_GET['rmin'] : $listing_sql_max['min_price'] + 0);
$rmax = (($_GET['rmax'] != '') ? $_GET['rmax'] : $listing_sql_max['max_price'] + 0);
$price_fltr = ($currencies->currencies[$currency]['symbol_left'] ? $currencies->currencies[$currency]['symbol_left'] : $currencies->currencies[$currency]['symbol_right']);

//$allPidsByCat = $allPidsByCat?:$all_pids;
// Show manufacturers of current category
if (is_array($allPidsByCat) && $allPidsByCat) $all_pids_string=" and p.products_id in (" . implode(',',$allPidsByCat).")";
else $all_pids_string='';
//$redirectOptionsIdsArrayForCheck тут поточні options_values

$manuf_sql = tep_db_query("select distinct m.manufacturers_id, m.manufacturers_name 
    		                             from " . TABLE_PRODUCTS . " p 
    		                             left join " . TABLE_MANUFACTURERS . " m on p.manufacturers_id = m.manufacturers_id 
                            where m.manufacturers_name !='' " . $all_pids_string . " order by m.manufacturers_name");
$filterManufacturers = [];
$filterManCount = [];
$productsToCurrentCatAsKey = array_flip($productsToCurrentCat);
foreach ($manufacturersToProductsId as $id=>$arr){
    $second = array_flip($arr);
    $x = array_intersect_key($productsToCurrentCatAsKey, $second);
    $filterManCount[$id] = count($x);
}
while ($manufacturers_values = tep_db_fetch_array($manuf_sql)) {
    $manufacturers_values['check'] = !empty($_GET['filter_id']) && (int)$manufacturers_values['manufacturers_id'] == (int)$_GET['filter_id'] ? 'checked' : '';
    $manufacturers_values['href'] = getFilterUrl($_GET['cPath'],$manufacturers_values['manufacturers_id'],[]);
    $manCount = $filterManCount[$manufacturers_values['manufacturers_id']];
    $manCount = $manCount ? " (".$manCount.")" : '';
    if($manCount) {
        $manufacturers_values['count'] = $manCount;
        $filterManufacturers[] = $manufacturers_values;
    }
}

$attr_vals_array_tmp = [];
if (is_array($attr_vals_array)) {
    foreach ($attr_vals_array as $at_id => $at_vals) {
        // natsort($at_vals);
        asort($at_vals);
        foreach ($at_vals as $at_val_id => $at_val_sort) $at_vals[$at_val_id] = $attr_vals_names_array[$at_val_id];

        $attr_vals_array_tmp[$at_id] = $at_vals;
    }
    $attr_vals_array = $attr_vals_array_tmp;
}