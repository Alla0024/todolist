<?php
$listing_sql = "SELECT p.products_id
                     FROM " . TABLE_PRODUCTS . " p      
                LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.products_id = p2c.products_id
                LEFT JOIN " . TABLE_SPECIALS . " s ON p.products_id = s.products_id AND s.status = '1' and (s.expires_date >= CURDATE() or s.expires_date = '0000-00-00 00:00:00')                                   
                    WHERE p2c.categories_id in(" . $all_active_cats . ")                             
                      AND s.status = '1'
                      AND p.products_status = '1' 
                 ORDER BY  " . ($tpl_settings['orderby'] ? : 'p.products_date_added DESC') . " 
                           " . ($tpl_settings['limit'] ? 'LIMIT ' . $tpl_settings['limit'] : '');

$listing_sql = tep_get_query_products_info($listing_sql); // split query to 2 small queries: 1) find all products ids, 2) get info for each product

$specials = tep_db_query($listing_sql);
$salemakers_array = get_salemakers($specials);
mysqli_data_seek($specials, 0);

if ($specials->num_rows and $tpl_settings['disable_listing'] != true) {
    $tpl_settings['request'] = $specials;
    include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
}
?>