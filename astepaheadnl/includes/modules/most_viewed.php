<?php
$listing_sql = "SELECT p.products_id
                     FROM " . TABLE_PRODUCTS . " p      
                LEFT JOIN " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on p.products_id = p2c.products_id
                left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.products_id = pd.products_id 
                    WHERE p2c.categories_id in(" . $all_active_cats . ")                             
                      AND p.products_status = '1' 
                      and pd.products_viewed > 0
                 ORDER BY  " . ($tpl_settings['orderby'] ? : 'p.products_quantity !=0 desc, pd.products_viewed DESC, pd.products_name') . " 
                           " . ($tpl_settings['limit'] ? 'LIMIT ' . $tpl_settings['limit'] : '');

$most_viewed_query = tep_get_query_products_info($listing_sql); // split query to 2 small queries: 1) find all products ids, 2) get info for each product

$most_viewed = tep_db_query($most_viewed_query);
$salemakers_array = get_salemakers($most_viewed);
mysqli_data_seek($most_viewed, 0);

if ($most_viewed->num_rows) {
    $tpl_settings['request'] = $most_viewed;
    include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING_COL);
}
?>