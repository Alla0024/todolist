<?php
/*
  $Id: english.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
@setlocale(LC_TIME, 'es_ES');
define('OG_LOCALE', 'es_ES');
define('GO_COMPARE', 'Ir a comparar');
define('IN_WHISHLIST', 'En la lista de deseos123');
define('COMPARE', 'Comparar');
define('WHISH', 'En la lista de deseos');

//define('DATE_FORMAT_SHORT', '%m/%d/%Y');  // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
//define('DATE_FORMAT', 'm/d/Y'); // this is used for date()
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


define('TEXT_DAY_1','Lunes');
define('TEXT_DAY_2','Martes');
define('TEXT_DAY_3','Miércoles');
define('TEXT_DAY_4','Jueves');
define('TEXT_DAY_5','El viernes');
define('TEXT_DAY_6','Sábado');
define('TEXT_DAY_7','Domingo');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1','Enero');
define('TEXT_MONTH_BASE_2','Febrero');
define('TEXT_MONTH_BASE_3','Marzo');
define('TEXT_MONTH_BASE_4','Abril');
define('TEXT_MONTH_BASE_5','Mayo');
define('TEXT_MONTH_BASE_6','Junio');
define('TEXT_MONTH_BASE_7','Julio');
define('TEXT_MONTH_BASE_8','Agosto');
define('TEXT_MONTH_BASE_9','Septiembre');
define('TEXT_MONTH_BASE_10','Octubre');
define('TEXT_MONTH_BASE_11','Noviembre');
define('TEXT_MONTH_BASE_12','Diciembre');
define('TEXT_MONTH_1','De enero');
define('TEXT_MONTH_2','De febrero de');
define('TEXT_MONTH_3','De marzo de');
define('TEXT_MONTH_4','De abril de');
define('TEXT_MONTH_5','De mayo');
define('TEXT_MONTH_6','De junio de');
define('TEXT_MONTH_7','Julio');
define('TEXT_MONTH_8','De agosto de');
define('TEXT_MONTH_9','De septiembre de');
define('TEXT_MONTH_10','De octubre de');
define('TEXT_MONTH_11','De noviembre de');
define('TEXT_MONTH_12','De diciembre de');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'EUR');

// Global entries for the <html> tag
define('HTML_PARAMS','dir="LTR" lang="es"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Nombre de su tienda');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Crea una cuenta');
define('HEADER_TITLE_CHECKOUT', 'Revisa');
define('HEADER_TITLE_TOP', 'Top');
define('HEADER_TITLE_LOGOFF', 'Desconectarse');

define('HEAD_TITLE_LOGIN', 'Iniciar sesión');
define('HEAD_TITLE_COMPARE', 'Comparación');
define('HEAD_TITLE_WISHLIST', 'Productos favoritos');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Olvidé mi contraseña');

define('HEAD_TITLE_CHECKOUT','Revisa');
define('HEAD_TITLE_CHECKOUT_SUCCESS','Su pedido ha sido creado!');
define('HEAD_TITLE_ACCOUNT','Mi cuenta');
define('HEAD_TITLE_ACCOUNT_HISTORY','Mis ordenes');
define('HEAD_TITLE_ACCOUNT_EDIT','Editar mis datos');
define('HEAD_TITLE_ADDRESS_BOOK','Directorio');
define('HEAD_TITLE_ACCOUNT_PASSWORD','Cambia la contraseña');
define('HEAD_TITLE_ALLCOMMENTS','Todos los comentarios');
define('HEAD_TITLE_CONTACT_US','Contactos de la tienda '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML sitemap - '.STORE_NAME);
define('HEAD_TITLE_404','Error 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Error 403 - '.STORE_NAME);

// text for gender
define('MALE', 'Masculino');
define('FEMALE', 'Hembra');


// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'El carrito esta vacío');

// notifications box text in includes/boxes/products_notifications.php

// manufacturer box text
// information box text in includes/boxes/information.php
define('BOX_INFORMATION_CONTACT', 'Contáctenos');

define('BOX_INFORMATION_PRICE_XLS', 'Lista de precios (Excel)');
define('BOX_INFORMATION_PRICE_HTML', 'Lista de precios (HTML)');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Ver todos los artículos');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Seleccione');
define('PULL_DOWN_COUNTRY', 'Area:');
define('TYPE_BELOW', 'Tipo abajo');

// javascript messages
define('JS_ERROR', 'Se han producido errores durante el proceso de su formulario. \ N \ nPor favor, haga las siguientes correcciones: \ n \ n');


define('JS_FIRST_NAME', '* El \'Nombre \' debe contener un mínimo de' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters.\n');
define('JS_LAST_NAME', '* El \'Apellido \' debe contener un mínimo de ' . ENTRY_LAST_NAME_MIN_LENGTH . ' characters.\n');

define('JS_ERROR_SUBMITTED', 'Este formulario ya ha sido enviado. Presione Aceptar y espere a que se complete este proceso.');

define('CATEGORY_COMPANY', 'Detalles de la compañía');
define('CATEGORY_PERSONAL', 'Tus detalles personales');
define('CATEGORY_ADDRESS', 'Su dirección');
define('CATEGORY_CONTACT', 'Tu información de contacto');
define('CATEGORY_OPTIONS', 'Opciones');

define('ENTRY_COMPANY', 'Nombre de empresa:');
define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER', 'Género:');
define('ENTRY_GENDER_ERROR', 'Por favor seleccione su género.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME', 'Nombre:');
define('ENTRY_TEXT_FOM_IMAGE', 'Ingresa el texto de la imagen');
define('ENTRY_FIRST_NAME_ERROR', 'Su Nombre debe contener un mínimo de' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caracteres.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Apellido:');
define('ENTRY_LAST_NAME_ERROR', 'Su apellido debe contener un mínimo de' . ENTRY_LAST_NAME_MIN_LENGTH . ' caracteres.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Fecha de nacimiento:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Su Fecha de Nacimiento debe estar en este formato: MM/DD/AAAA (Por ejemplo 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (Por ejemplo 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'Dirección de correo electrónico:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Su dirección de correo electrónico debe contener un mínimo de' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caracteres.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Su dirección de correo electrónico no parece válida. Por favor, haga las correcciones necesarias.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Su dirección de correo electrónico ya existe en nuestros registros. Inicie sesión con la dirección de correo electrónico o cree una cuenta con una dirección diferente.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Dirección:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Su dirección debe contener un mínimo de' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caracteres.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB', 'Colonia:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'RUC:');
define('ENTRY_POST_CODE_ERROR', 'Su RUC debe contener un mínimo de ' . ENTRY_POSTCODE_MIN_LENGTH . ' caracteres.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Ciudad');
define('ENTRY_CITY_ERROR', 'El nombre de tu ciudad debe contener un mínimo de ' . ENTRY_CITY_MIN_LENGTH . ' caracteres.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Estado / provincia:');
define('ENTRY_STATE_ERROR', 'Su nombre de Estado debe contener un mínimo de ' . ENTRY_STATE_MIN_LENGTH . ' caracteres.');
define('ENTRY_STATE_ERROR_SELECT', 'Seleccione un estado en el menú desplegable Estados.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'País:');
define('ENTRY_COUNTRY_ERROR', 'Debe seleccionar un país en el menú desplegable Países.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Número de teléfono:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Su número de teléfono debe contener un mínimo de' . ENTRY_TELEPHONE_MIN_LENGTH . ' caracteres.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Número de fax');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Hoja informativa:');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Suscrito');
define('ENTRY_NEWSLETTER_NO', 'No suscripto');
define('ENTRY_PASSWORD', 'Contraseña:');
define('ENTRY_PASSWORD_ERROR', 'Su contraseña debe contener un mínimo de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'La confirmación de la contraseña debe coincidir con su contraseña.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Confirmación de contraseña:');
define('ENTRY_PASSWORD_CURRENT', 'Contraseña actual:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Su contraseña debe contener un mínimo de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW', 'Nueva contraseña:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Su nueva contraseña debe contener un mínimo de ' . ENTRY_PASSWORD_MIN_LENGTH . ' caracteres.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'La confirmación de la contraseña debe coincidir con su nueva contraseña.');

define('FORM_REQUIRED_INFORMATION', '* Información requerida');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Result Pages:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> products)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> orders)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> specials)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> featured products)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Pagina anterior');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Siguiente página');
define('PREVNEXT_TITLE_PAGE_NO', 'Página %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Conjunto anterior de% d Páginas');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Next Set of %d Pages');
define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;Prev]');
define('PREVNEXT_BUTTON_NEXT', '[Next&nbsp;&gt;&gt;]');

define('PREVNEXT_TITLE_PPAGE', 'Página');
define('PREVNEXT_TITLE_FROM', 'de');

define('ART_XSELL_TITLE', 'Contenido similar');
define('PROD_XSELL_TITLE', 'Productos similares');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Añadir dirección');
define('IMAGE_BUTTON_BACK', 'Atrás');
define('IMAGE_BUTTON_CHECKOUT', 'Revisa');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Confirmar pedido');
define('IMAGE_BUTTON_CONTINUE', 'Continuar');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continue Shopping');
define('IMAGE_BUTTON_DELETE', 'Borrar');
define('IMAGE_BUTTON_LOGIN', 'Registrarse');

define('IMAGE_BUTTON_IN_CART', 'En el carrito');
define('IMAGE_BUTTON_ADDTO_CART', 'Comprar');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Comprar');
define('IMAGE_BUTTON_TEST_TEMPLATE', 'Prueba gratis!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Compre esta tienda ahora');

define('LOGO_IMAGE_TITLE','Logo');

define('IMAGE_BUTTON_UPDATE', 'Actualizar');
define('IMAGE_REDEEM_VOUCHER', 'Aplicar');

define('SMALL_IMAGE_BUTTON_VIEW', 'Ver');

define ('CUSTOM_PANEL_EDIT', 'Editar ');
define ('CUSTOM_PANEL_EDIT_PRODUCT', 'producto');
define ('CUSTOM_PANEL_EDIT_MANUF', 'productor');
define ('CUSTOM_PANEL_EDIT_ARTICLE', 'artículo');
define ('CUSTOM_PANEL_EDIT_SECTION', 'sección');
define ('CUSTOM_PANEL_EDIT_CATEGORY', 'categoría');
define ('CUSTOM_PANEL_EDIT_SEO', 'seotext');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Panel de administración');
define('CUSTOM_PANEL_ADD', 'Agregar');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Producto');
define('CUSTOM_PANEL_ADD_PAGE', 'Pagina');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Descuento');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Categoría');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Estadísticas');
define('CUSTOM_PANEL_ADD_ONLINE', 'En el sitio: ');
define('CUSTOM_PANEL_PALETTE', 'Paleta del sitio');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Color del texto');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Fondo header');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Fondo footer');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Color de enlace');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Color del botón');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Color de fondo');
define('CUSTOM_PANEL_ORDERS', 'Pedidos');
define('CUSTOM_PANEL_ORDERS_NEW', 'Nuevo: ');


define('CUSTOM_PANEL_DATE1', 'día');
define('CUSTOM_PANEL_DATE2', 'días');
define('CUSTOM_PANEL_DATE3', 'días');


define('ICON_ERROR', 'Error');
define('ICON_SUCCESS', 'Éxito');
define('ICON_WARNING', 'Advertencia');

define('TEXT_GREETING_PERSONAL', 'Welcome back <span class="greetUser">%s!</span> Would you like to see which <a href="%s"><u>new products</u></a> are available to purchase?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Nuestro saludo al cliente');
define('TEXT_GREETING_GUEST', 'Welcome <span class="greetUser">Guest!</span> Would you like to <a href="%s"><u>log yourself in</u></a>? Or would you prefer to <a href="%s"><u>create an account</u></a>?');

define('TEXT_SORT_PRODUCTS', 'Ordenar productos:');
define('TEXT_DESCENDINGLY', 'Descendiendo');
define('TEXT_ASCENDINGLY', 'ascendente');
define('TEXT_BY', ' Por ');

define('TEXT_UNKNOWN_TAX_RATE', 'Tasa de impuesto desconocida');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'AVISO: Este sitio web estará deshabilitado para mantenimiento en: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'AVISO: el sitio web está actualmente deshabilitado para el público');

define('WARNING_INSTALL_DIRECTORY_EXISTS', '');
define('WARNING_CONFIG_FILE_WRITEABLE', '');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', '');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', '');
define('WARNING_SESSION_AUTO_START', '');


define('TEXT_CCVAL_ERROR_INVALID_DATE', '');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', '');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', '');

define('BOX_LOGINBOX_HEADING', 'Su cuenta');
define('LOGIN_BOX_MY_CABINET','Mi cuenta');
define('LOGIN_BOX_ADDRESS_BOOK','Directorio');
define('LOGIN_BOX_LOGOFF','Desconectarse');

define('LOGIN_FROM_SITE','Iniciar sesión');
define('RENDER_LOGIN_WITH', 'Login with');



// Article Manager
define('BOX_ALL_ARTICLES', 'Iniciar sesión');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> articles)');
define('NAVBAR_TITLE_DEFAULT', 'Artículos');
define('TEXT_NAVIGATION_BRANDS','Todos los fabricantes');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT','');
//define('PRICES_LOGGED_IN_TEXT','Debe estar registrado para los precios!');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO','El pedido mínimo es: ');
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART','El pedido mínimo es: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO','Orden en Unidades de: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART','Pedido en Unidades de: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT','');
define('ERROR_PRODUCTS_QUANTITY_INVALID','Cantidad no válida: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT','');
define('ERROR_PRODUCTS_UNITS_INVALID','Unidades no válidas: ');

// Comments
define('ADD_COMMENT_HEAD_TITLE', 'Añade tu comentario');

// Poll Box Text
define('_RESULTS', 'Resultados');
define('_VOTE', 'Votar');
define('_VOTES', 'Votos:');

define('IMAGE_BUTTON_NEXT', 'Siguiente artículo');
define('PREV_NEXT_PRODUCT', 'Producto ');
define('PREV_NEXT_CAT', ' de la categoría ');
define('PREV_NEXT_MB', ' del fabricante ');


// English names of boxes

define('BOX_HEADING_CATEGORIES', 'Categorías');
define('BOX_HEADING_INFORMATION', 'Información');
define('BOX_HEADING_TEMPLATE_SELECT', 'Selección de tema');
define('BOX_HEADING_MANUFACTURERS', 'Fabricantes');
define('BOX_HEADING_SPECIALS', 'Especiales');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Especiales %s');
define('BOX_HEADING_SEARCH', 'Búsqueda rápida');
define('BOX_OPEN_SEARCH_FORM', 'Open Search Form');
define('BOX_HEADING_WHATS_NEW', 'Últimos productos');
define('BOX_HEADING_NEWSBOX', 'Noticias');
define('BOX_HEADING_FEATURED', 'Productos Destacados');
define('BOX_HEADING_ARTICLES', 'Artículos');
define('BOX_HEADING_LINKS', 'Campo de golf');
define('HELP_HEADING', 'Ayuda');
define('BOX_HEADING_WISHLIST', 'Lista de deseos');
define('BOX_HEADING_BESTSELLERS', 'Los más vendidos');
define('BOX_HEADING_CURRENCY', 'Moneda');
define('_POLLS', 'Centro');
define('BOX_HEADING_CALLBACK', 'Llamar de vuelta');
define('BOX_HEADING_CONSULTANT', 'Consultor en línea');
define('BOX_HEADING_PRODUCTS', 'artículos');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Agregar categoría de elementos.');

define('BOX_BESTSELLERS_NO', 'Todavía no hay bestsellers');


  define('LOW_STOCK_TEXT1','Advertencia de bajas existencias: ');
  define('LOW_STOCK_TEXT2','N º de Modelo: ');
  define('LOW_STOCK_TEXT3','Cantidad: ');
  define('LOW_STOCK_TEXT4','URL del producto: ');
  define('LOW_STOCK_TEXT5','Límite de orden inferior actual es ');

// wishlist box text in includes/boxes/wishlist.php

  define('BOX_TEXT_NO_ITEMS', 'No hay productos en su lista de deseos.');
define('BUTTON_SHOPPING_CART_OPEN', 'Open Shopping Cart');


define('TOTAL_TIME', 'Tiempo de ejecución: ');
  define('TOTAL_CART', 'Total');
    
// otf 1.71 defines needed for Product Option Type feature.
  define('TEXT_PREFIX', 'txt_');
  define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES
  
  // Product tabs

define('ALSO_PURCHASED', 'También compró');

define('FORWARD', 'Adelante');
define('BACKWARD', 'Hacia atrás');

define('COMP_PROD_NAME','Nombre');
define('COMP_PROD_IMG','Imagen');
define('COMP_PROD_PRICE','Precio');
define('COMP_PROD_CLEAR','Limpiar todo');
define('COMP_PROD_BACK','Atrás');
define('COMP_PROD_ADD_TO','Añadir productos para comparar!');
define('TEXT_PASSWORD_FORGOTTEN', 'Se te olvidó tu contraseña?');
define('QUICK_ORDER','Pedido rápido');
define('QUICK_ORDER_SUCCESS','Su solicitud es enviada, nos pondremos en contacto con usted pronton');
define('QUICK_ORDER_BUTTON','Pedido rápido');
define('SEND_MESSAGE','Enviar');

define('LABEL_NEW','Nuevo');
define('LABEL_TOP','TOP');
define('LABEL_SPECIAL','Especial');

define('FILTER_BRAND','Marca');
define('FILTER_ALL','todas');

define('SELECT_TEMPLATE','Seleccionar plantilla');
define('WISHLIST_PC','pc.');

define('FOOTER_INFO','Información');
define('FOOTER_CATEGORIES','Categorías');
define('FOOTER_ARTICLES','Artículos');
define('FOOTER_CONTACTS','Contactos');
define('FOOTER_SITEMAP','Mapa del sitio');
define('FOOTER_DEVELOPED','Desarrollado por');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Mostrar todas las categorías');
define('SHOW_ALL_ARTICLES','Mostrar todos los artículos');

define('MAIN_NEWS','Noticias');
define('MAIN_NEWS_ALL','Todas las noticias');
define('MAIN_NEWS_SUBSCRIBE','Suscribirse <span> para las noticias </ span>');
define('MAIN_NEWS_SUBSCRIBE_BUT','Suscribir');
define('MAIN_NEWS_EMAIL','Tu correo electrónico');

define('MAIN_BESTSELLERS','Los más vendidos');
define('MAIN_REVIEWS','Comentarios');
define('MAIN_REVIEWS_ALL','Todos los comentarios');
define('MAIN_MOSTVIEWED','Mas visto');

define('LIST_TEMP_INSTOCK','En stock');
define('LIST_TEMP_OUTSTOCK','Agotado');

define('HIGHSLIDE_CLOSE','Cerca');
define('BUTTON_CANCEL','Cancelar');
define('BUTTON_SEND','Enviar');
define('BOX_HEADING_LASTVIEWED', 'Últimos productos vistos');

define('LISTING_PER_PAGE','Por página:');
define('LISTING_SORT_NAME','Alfabéticamente, A-Z');
define('LISTING_SORT_PRICE1','Precios de barato a caro');
define('LISTING_SORT_PRICE2','Precio: de alto a bajo');
define('LISTING_SORT_NEW','Por más nuevo');
define('LISTING_SORT_POPULAR','Mejor valorados');
define('LISTING_SORT_LIST','lista');
define('LISTING_SORT_COLUMNS','Columnas');

define('TEXT_HEADING_PRICE', 'Catálogo:');
define('TEXT_HEADING_CATALOG', 'Catálogo');

define('PROD_DRUGIE','Productos similares');
define('PROD_FILTERS','Filtros');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Attributes');
define('TEXT_PRODUCTS_QTY', 'Products Quantity');

define('PAGE404_TITLE','Error 404');
define('PAGE404_DESC','Página no encontrada');
define('PAGE404_REASONS','Puede haber varias razones:');
define('PAGE404_REASON1','Esta página se ha movido o ha cambiado de nombre');
define('PAGE404_REASON2','Esta página ya no existe en este sitio');
define('PAGE404_REASON3','Su dirección URL es incorrecta');
define('PAGE404_BACK1','Volver a la página anterior');
define('PAGE404_BACK2','Volver a la página de inicio');

// page 403
define('PAGE403_DESC', 'No tienes permiso para ver esta página.');
define('PAGE403_TITLE','Error 403');
define('PAGE403_TO_HOME_PAGE','Regresar a la página principal');


define('SB_SUBSCRIBER','Abonado');
define('SB_EMAIL_NAME','Solicitud de boletines');
define('SB_EMAIL_USER','Usuario');
define('SB_EMAIL_WAS_SUBSCRIBED','Fue suscribirse a boletines');
define('SB_EMAIL_ALREADY','ya se suscribieron en nuestros boletines!');
define("LOAD_MORE_BUTTON", "Carga más");
define("TIME_LEFT", "Hasta el final de la versión de prueba");

define("LOAD_MORE_REFRESHING", "Refrescante...");


define('IMAGE_BUTTON_ADDTO_CART_CLO', 'Añadir a la cesta');

define('CLO_NEW_PRODUCTS', 'Productos recientemente agregados');
define('CLO_FEATURED', 'Estos productos te interesarán');
define('CLO_DESCRIPTION_BESTSELLERS', 'Ventas TOP');
define('CLO_DESCRIPTION_SPECIALS', 'Descuentos');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Más visto');

define('SHOPRULES_AGREE', 'estoy de acuerdo con');
define('SHOPRULES_AGREE2', 'reglas de la tienda');

define("SHOW_ALL_SUBCATS", "ver todo");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Envío gratis");

//home
define('HOME_BOX_HEADING_SEARCH', 'buscar...');
define('HEADER_TITLE_OR', 'o');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'añadir');
define('HOME_IMAGE_BUTTON_IN_CART', 'al carrito');
define('HOME_ARTICLE_READ_MORE', 'leer más');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'Suscríbase a un nuevo boletín informativo y obtenga <span> un 25% de descuento </span> para un producto en un pedido en línea');
define('HOME_MAIN_NEWS_EMAIL', 'Ingrese su correo electrónico');
define('HOME_FOOTER_DEVELOPED', 'creación de la tienda en línea');
define('HOME_FOOTER_CATEGORIES', 'Catálogo');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Añadir un comentario al producto');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Primer nombre');
define('HOME_ADD_COMMENT_FORM', 'Comentar');
define('HOME_REPLY_TO_COMMENT', 'Responder al comentario');
define('HOME_PROD_DRUGIE', 'Productos vistos');
define("HOME_LOAD_MORE_INFO", "mas");
define("HOME_LOAD_ROLL_UP", "enrollar");
define("HOME_TITLE_LEFT_CATEGORIES", "bienes");
define('HOME_BOX_HEADING_SELL_OUT', 'Venta de liquidación');
define("HOME_LOAD_MORE_BUTTON", "descargar más productos");
define("HOME_BOX_HEADING_WISHLIST", "Productos favoritos");
define("HOME_PROD_VENDOR_CODE", "número de artículo ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Promociones');
define('HOME_FOOTER_SOCIAL_TITLE', 'estamos en las redes sociales:');
define('HOME_HEADER_SOME_LIST', 'Ver también');

define('SEARCH_LANG', 'es/');



define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'All results');
define('ENTER_KEY', 'Enter keywords');
define('TEXT_LIMIT_REACHED', 'Maximum products in compares reached: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Product was successfully added to your cart!');
define('CHOOSE_ADDRESS', 'Elige la dirección');

//default2

define('DEMO2_TOP_PHONES', 'Telefonos');
define('DEMO2_TOP_CALLBACK', 'Callback');
define('DEMO2_TOP_ONLINE', 'Online-Chat');
define('DEMO2_SHOPPING_CART', 'Cesta');
define('DEMO2_LOGIN_WITH', 'Ingresar con ');
define('DEMO2_WISHLIST_LINK', 'Lista de deseos');
define('DEMO2_FOOTER_CATEGORIES', 'Catalogo de productos');
define('DEMO2_MY_INFO', 'Editar datos');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Cambiar dirección de envío');
define('DEMO2_LEFT_CAT_TITLE', 'Las categorías');
define('DEMO2_LEFT_ALL_GOODS', 'Todos los bienes');
define('DEMO2_TITLE_SLIDER_LINK', 'Ver todos');
define('DEMO2_READ_MORE', 'Leer mas <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Enrollar <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Todas las categorias');
define('DEMO2_SHOW_ALL_ARTICLES','Todos los articulos');
define('DEMO2_BTN_COME_BACK','Ir hacia atrás');
define('DEMO2_SHARE_TEXT','Comparte esto');
define('DEMO2_QUICK_ORDER_BUTTON', 'En un clic');
















