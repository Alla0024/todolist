<?php
/*
  $Id: account_history_info.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Mi cuenta');
define('NAVBAR_TITLE_2', 'Historia');
define('NAVBAR_TITLE_3', 'Orden #%s');

define('HEADING_TITLE', 'Informacion del pedido');

define('HEADING_ORDER_NUMBER', 'Orden #%s');
define('HEADING_ORDER_DATE', 'Fecha de orden:');
define('HEADING_ORDER_TOTAL', 'Total del pedido:');

define('HEADING_DELIVERY_ADDRESS', 'Direccion de entrega');
define('HEADING_SHIPPING_METHOD', 'Metodo de envio');

define('HEADING_PRODUCTS', 'Productos');
define('HEADING_TAX', 'Impuesto');
define('HEADING_TOTAL', 'Total');

define('HEADING_PAYMENT_METHOD', 'Informacion del pedido');

define('HEADING_ORDER_HISTORY', 'Historial de pedidos');

define('TABLE_HEADING_DOWNLOAD_DATE', 'El vinculo caduca: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' Descargas restantes');
define('TABLE_HEADING_DATE_ADDED', 'Fecha');
define('TABLE_HEADING_STATUS', 'Estado del pedido');
define('TABLE_HEADING_COMMENTS', 'Comentarios');
?>
