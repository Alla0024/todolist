<?php
/*
  $Id: index.php,v 1.1 2003/06/11 17:38:00 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');
define('TABLE_HEADING_DATE_EXPECTED', 'Fecha prevista');
define('TABLE_HEADING_DEFAULT_SPECIALS', 'Descuentos %s');

if ( ($category_depth == 'products') || (isset($_GET['manufacturers_id'])) ) {
  define('HEADING_TITLE', 'A ver que tenemos aqui');
  define('TABLE_HEADING_IMAGE', '');
  define('TABLE_HEADING_MODEL', 'Modelo');
  define('TABLE_HEADING_PRODUCTS', 'nombre del producto');
  define('TABLE_HEADING_MANUFACTURER', 'Fabricante');
  define('TABLE_HEADING_QUANTITY', 'Cantidad');
  define('TABLE_HEADING_PRICE', 'Precio');
  define('TABLE_HEADING_WEIGHT', 'Peso');
  define('TEXT_ALL_MANUFACTURERS', 'Todos los fabricantes');
} elseif ($category_depth == 'top') {
  define('HEADING_TITLE', '?Que hay de nuevo aqui?');
} elseif ($category_depth == 'nested') {
  define('HEADING_TITLE', 'Categorias');
}
 	// BOF Wolfen featured sets
  define('TABLE_HEADING_PRICE', 'Precio');
  define('TEXT_NO_FEATURED_PRODUCTS', 'No hay productos destacados');
 	// EOF Wolfen featured sets
?>