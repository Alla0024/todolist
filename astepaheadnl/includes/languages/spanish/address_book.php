<?php
/*
  $Id: address_book.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Mi cuenta');
define('NAVBAR_TITLE_2', 'Directorio');

define('HEADING_TITLE', 'Mi libreta de direcciones personal');

define('PRIMARY_ADDRESS_TITLE', 'Direccion primaria');
define('PRIMARY_ADDRESS_DESCRIPTION', 'Esta direccion se utiliza como la direccion de envio y facturacion preseleccionada para los pedidos realizados en esta tienda. <br> <br> Esta direccion tambien se utiliza como base para calculos de impuestos sobre productos y servicios.');

define('ADDRESS_BOOK_TITLE', 'Entradas de la libreta de direcciones');

define('PRIMARY_ADDRESS', '(direccion primaria)');

define('TEXT_MAXIMUM_ENTRIES', '<span><b>NOTE:</b></span> Se permite un maximo de% s entradas de la libreta de direcciones.');