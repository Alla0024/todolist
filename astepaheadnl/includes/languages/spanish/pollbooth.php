<?php
/*
  $Id: pollbooth.php,v 1.5 2003/04/06 21:45:33 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Copyright (c) 2000,2001 The Exchange Project

  Released under the GNU General Public License
*/
if (!isset($_GET['op'])) {
	$_GET['op']="list";
	}
if ($_GET['op']=='results') {
  define('HEADING_TITLE', 'Vea lo que piensan los demas');
}
if ($_GET['op']=='list') {
  define('HEADING_TITLE', 'Valoramos tus pensamientos');
}
if ($_GET['op']=='vote') {
  define('HEADING_TITLE', 'Nuestros clientes importan');
}
if ($_GET['op']=='comment') {
  define('HEADING_TITLE', 'Comenta esta encuesta');
}
define('_WARNING', 'Advertencia: ');
define('_ALREADY_VOTED', 'Usted ha votado recientemente en esta encuesta.');
define('_NO_VOTE_SELECTED', 'No ha seleccionado una opcion para votar.');
define('_TOTALVOTES', 'Total de votos emitidos');
define('_OTHERPOLLS', 'Otras encuestas');
define('NAVBAR_TITLE_1', 'Cabina electoral');
define('_POLLRESULTS', 'Haga clic aqui para ver los resultados de la encuesta');
define('_VOTING', 'Vota ahora');
define('_RESULTS', 'Resultados');
define('_VOTES', 'Votos');
define('_VOTE', 'VOTAR');
define('_COMMENT', 'Comentario');
define('_COMMENTS_BY', 'Comentario hecho por ');
define('_COMMENTS_ON', ' en ');
define('_YOURNAME', 'Tu nombre');
define('_OTZYV', 'Comentario:');
define('TEXT_CONTINUE', 'Continuar');
define('_PUBLIC','Publico');
define('_PRIVATE','Privado');
define('_POLLOPEN','Encuesta abierta');
define('_POLLCLOSED','Encuesta cerrada');
define('_POLLPRIVATE','Piscina Privada, debes iniciar sesion para votar');
define('_ADD_COMMENTS', 'Agregar comentario');
define('TEXT_DISPLAY_NUMBER_OF_COMMENTS', 'Mostrando <b>%d</b> to <b>%d</b> (of <b>%d</b> comentarios)');
?>
