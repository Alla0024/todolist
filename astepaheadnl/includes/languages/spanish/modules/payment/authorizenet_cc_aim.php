<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_PUBLIC_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_DESCRIPTION', '<a href="https://www.authorize.net" target="_blank" style="text-decoration: underline; font-weight: bold;">Visite el sitio web de Authorize.net</a>');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_OWNER', 'Propietario de la tarjeta de crédito:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_NUMBER', 'Número de tarjeta de crédito:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_EXPIRES', 'Fecha de vencimiento de la tarjeta de crédito:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_CVC', 'Código de verificación de tarjeta (CVC):');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_TITLE', 'Ha habido un error al procesar su tarjeta de crédito');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_GENERAL', 'Intente nuevamente y si los problemas persisten, intente con otro método de pago');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_DECLINED', 'Esta transacción con tarjeta de crédito ha sido rechazada. Inténtalo de nuevo y si los problemas persisten, prueba con otra tarjeta de crédito o método de pago');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_INVALID_EXP_DATE', 'La fecha de vencimiento de la tarjeta de crédito no es válida. Verifique la información de la tarjeta y vuelva a intentarlo');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_EXPIRED', 'La tarjeta de crédito ha caducado. Inténtalo de nuevo con otra tarjeta o método de pago');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_CVC', 'El número de verificación de la tarjeta de crédito (CVC) no es válido. Verifique la información de la tarjeta y vuelva a intentarlo');
?>
