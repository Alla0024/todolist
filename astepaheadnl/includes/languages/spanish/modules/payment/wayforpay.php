<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Descripción');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Activar / Desactivar módulo');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Estado predeterminado');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Cuenta comercial');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Secreto mercantil');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'El orden de clasificación');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Estado de pago');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'Verdadero: habilitar, Falso: deshabilitar');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'El estado que se establece en el pedido cuando se crea');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Cuenta comercial');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Secreto mercantil');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Orden de clasificación del módulo en el bloque con módulos de pago');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'El estado que se establece en el pedido tras el pago exitoso');