<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Iniciar sesion');
define('HEADING_TITLE', 'Bienvenido, Inicia sesion');
define('HEADING_NEW_CUSTOMER', 'Nuevo cliente');
define('TEXT_NEW_CUSTOMER', 'Soy un nuevo cliente.');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'Al crear una cuenta en ' . STORE_NAME . ' Usted podra hacer sus compras mas rapido, estar al dia sobre el estado de sus pedidos y realizar un seguimiento de los pedidos que ha realizado anteriormente.');
define('TEXT_RETURNING_CUSTOMER', 'Yo soy cliente.');
define('TEXT_PASSWORD_FORGOTTEN', 'Contraseña olvidada? Haga clic aqui.');
define('TEXT_LOGIN_ERROR', 'Error: no coincide con direccion de correo electronico y / o contrasena.');
// guest_account start
define('NO_CUSTOMER', 'No hay tal usuario en nuestra base de datos');
define('NO_PASS', 'Contrasena incorrecta');
define('SOC_VK', 'VK');
define('SOC_SITE', 'sitio');
define('SOC_ENTER_FROM_OTHER', 'Inicie sesion desde otra red social o ingrese su nombre de usuario / contrasena.');
define('SOC_NOW_FROM', 'Intentas iniciar sesion desde');
define('SOC_NEED_FROM', 'Te registraste a traves de');
define('SOC_LOGIN_THX', 'Has ingresado satisfactoriamente');