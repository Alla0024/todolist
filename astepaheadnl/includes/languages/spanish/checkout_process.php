<?php
/*
  $Id: checkout_process.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('EMAIL_TEXT_SUBJECT', 'Orden en proceso');
define('EMAIL_TEXT_ORDER_NUMBER', 'Numero de orden:');
define('EMAIL_TEXT_INVOICE_URL', 'Factura detallada:');
define('EMAIL_TEXT_DATE_ORDERED', 'Fecha del pedido:');
define('EMAIL_TEXT_PRODUCTS', 'Productos');
define('EMAIL_TEXT_DELIVERY_ADDRESS', 'Direccion de entrega');
define('EMAIL_TEXT_BILLING_ADDRESS', 'Direccion de Envio');
define('EMAIL_TEXT_PAYMENT_METHOD', 'Metodo de pago');
define('EMAIL_SEPARATOR', '------------------------------------------------------');
define('EMAIL_TEXT_CUSTOMER_NAME', 'Cliente:');
define('EMAIL_TEXT_CUSTOMER_EMAIL_ADDRESS', 'Email:');
define('EMAIL_TEXT_CUSTOMER_TELEPHONE', 'Telefono:');