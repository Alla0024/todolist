<?php
/*
  $Id: article_info.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_ARTICLE_NOT_FOUND', 'Articulo no encontrado');
define('TEXT_ARTICLE_NOT_FOUND', 'Lo sentimos, pero el articulo que solicito no esta disponible en este sitio.');
define('TEXT_DATE_ADDED', 'Este articulo fue publicado en% s.');
define('TEXT_DATE_AVAILABLE', 'Este articulo sera publicado en %s.');
define('TEXT_BY', 'por ');
