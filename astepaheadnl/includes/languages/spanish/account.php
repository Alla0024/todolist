<?php
/*
  $Id: account.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Mi cuenta');
define('HEADING_TITLE', 'Informacion de mi cuenta');


define('MY_ACCOUNT_TITLE', 'Mi cuenta');
define('MY_ACCOUNT_ADDRESS_BOOK', 'Ver o cambiar entradas en mi libreta de direcciones.');
define('MY_ACCOUNT_PASSWORD', 'Cambiar la contrasena de mi cuenta.');

define('MY_ORDERS_VIEW', 'Vea las ordenes que he hecho.');

define('MY_NAVI', 'Menu');
define('MY_INFO', 'Mi informacion');
define('EDIT_ADDRESS_BOOK', 'Editar mi direccion de envio');
