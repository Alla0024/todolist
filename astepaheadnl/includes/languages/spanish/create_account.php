<?php
/*
  $Id: create_account.tpl.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// guest_account start
if ($guest_account == false) { // Not a Guest Account
define('NAVBAR_TITLE', 'Crea una cuenta');
} else {
  define('NAVBAR_TITLE', 'Revisa');
}
// guest_account end
define('HEADING_TITLE', 'Informacion de mi cuenta');
// guest_account start
define('CR_ENTER', 'Iniciar sesion');
define('CR_THX', '?Gracias! Usted esta registrado.');
define('CR_IF', 'Si ya esta registrado.');
// guest_account end
define('TEXT_ORIGIN_LOGIN', '<span><small><b>NOTE:</b></span></small> Si ya tiene una cuenta con nosotros, inicie sesion en <a href="%s"> <u> pagina de inicio de sesion </ u> </a>.');
define('EMAIL_SUBJECT', 'Bienvenido a ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Estimado Senor. %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Estimado Senora. %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Estimado %s' . ";");
define('EMAIL_WELCOME', ' le damos la bienvenida a la tienda en línea de <b>' . STORE_NAME . '</b>.<br>');
define('EMAIL_TEXT', 'Ahora procederemos a verificar sus datos. Una vez completado este proceso podrá iniciar sesión y disfrutar de los servicios que ofrecemos:<ul><li>Promociones exclusivas</li><li>Precios preferenciales</li><li>Atención personalizada</li></ul>');

define('EMAIL_CONTACT', 'Para obtener ayuda con cualquiera de nuestros servicios en línea, envíe un correo electrónico al correo ' . STORE_OWNER_EMAIL_ADDRESS . '.<br><br>');
define('EMAIL_WARNING', '<b>Nota:</b> En caso de no obtener respuesta inmediata a su solicitud o si desea cancelar su cuenta, informe su inconveniente al correo ' . STORE_OWNER_EMAIL_ADDRESS . '.<br>');
/* ICW Credit class gift voucher begin */
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Como parte de nuestra bienvenida a nuevos clientes, le hemos enviado un vale de regalo electronico de %s');
define('EMAIL_GV_REDEEM', 'El codigo de canje del vale de regalo electronico es% s, puedes ingresar el codigo de canjear al salir mientras realizas una compra');
define('EMAIL_GV_LINK', 'O siguiendo este enlace ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Enhorabuena, para que su primera visita a nuestra tienda en linea sea una experiencia mas gratificante, le estamos enviando un cupon de descuento electronico.' . "\n" .
                                        'A continuacion se detallan los detalles del cupon de descuento creado para usted' . "\n");
define('EMAIL_COUPON_REDEEM', 'Para usar el cupon, ingrese el codigo de canje que es% s durante la compra mientras realiza una compra');
define('CR_LOGIN', 'Su nombre de usuario (e-mail)');
define('CR_PASS', 'Tu contrasena');
define('CR_ADD_EMAIL', 'Ingrese su <b> correo electronico:</b>');
define('CR_SUBMIT', 'Enviar');
/* ICW Credit class gift voucher end */