<?php
/*
  $Id: account_password.php,v 1.1.1.1 2003/09/18 19:04:31 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Mi cuenta');
define('NAVBAR_TITLE_2', 'Cambia la contrasena');

define('HEADING_TITLE', 'Mi contrasena');

define('MY_PASSWORD_TITLE', 'My Password');

define('SUCCESS_PASSWORD_UPDATED', 'Su contrasena ha sido actualizada satisfactoriamente.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Su contrasena actual no coincide con la contrasena de nuestros registros. Vuelve a intentarlo.');
?>
