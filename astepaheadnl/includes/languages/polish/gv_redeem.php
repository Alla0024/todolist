<?php
/*
  $Id: gv_redeem.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Aktywuj kartę podarunkową');
define('HEADING_TITLE', 'Aktywuj kartę podarunkową');
define('TEXT_INFORMATION', '');
define('TEXT_INVALID_GV', 'Podana karta podarunkowa nie istnieje lub był już używany. Jeśli masz jakieś pytania, skontaktuj się z nami.');
define('TEXT_VALID_GV', 'Pomyślnie aktywowałeś swoją kartę płatniczą na kwotę% s! Teraz możesz użyć swoją kartę  podarunkową do robienia zakupów w naszym sklepie internetowym.');
?>
