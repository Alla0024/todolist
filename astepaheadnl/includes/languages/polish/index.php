<?php
/*
  $Id: index.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '&nbsp;');
define('TABLE_HEADING_DATE_EXPECTED', 'Data wpłynięcia');
define('TABLE_HEADING_DEFAULT_SPECIALS', 'Zniżka %s');

if ( ($category_depth == 'products') || (isset($_GET['manufacturers_id'])) ) {
  define('HEADING_TITLE', 'Lista produktów');

  define('TABLE_HEADING_IMAGE', '<span class="sort">Sortuj według:</span>');
  define('TABLE_HEADING_MODEL', 'Kod towaru');
  define('TABLE_HEADING_PRODUCTS', '<span class="cena">Nazwa</span>');
  define('TABLE_HEADING_MANUFACTURER', 'Producent');
  define('TABLE_HEADING_QUANTITY', 'Ilość');
  define('TABLE_HEADING_PRICE', '<span class="cena">Cena</span>');
  define('TABLE_HEADING_WEIGHT', 'Waga');
  define('TABLE_HEADING_PRODUCT_SORT', 'Kolejność');
  define('TEXT_ALL_MANUFACTURERS', 'Wszyscy producenci');
} elseif ($category_depth == 'top') {
  define('HEADING_TITLE', 'Witamy');
} elseif ($category_depth == 'nested') {
  define('HEADING_TITLE', 'Sekcje');
}
