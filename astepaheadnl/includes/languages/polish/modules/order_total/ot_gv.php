<?php
/*
  $Id: ot_gv.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_GV_TITLE', 'Karta podarunkowa');
define('MODULE_ORDER_TOTAL_GV_HEADER', 'Karta podarunkowa/Kupon');
define('MODULE_ORDER_TOTAL_GV_DESCRIPTION', 'Karta podarunkowa');
define('MODULE_ORDER_TOTAL_GV_USER_PROMPT', 'Możesz użyć swojej karty podarunkowej, zaznaczając to pole i klikając "Zastosuj":&nbsp;');
define('TEXT_ENTER_GV_CODE', 'Kod karty podarunkowej&nbsp;&nbsp;');
