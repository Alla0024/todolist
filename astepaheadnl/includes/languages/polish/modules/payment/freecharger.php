<?php
/*
  WebMakers.com Added: Free Payments and Shipping
  Written by Linda McGrath osCOMMERCE@WebMakers.com
  http://www.thewebmakerscorner.com

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_FREECHARGER_TEXT_TITLE', 'Darmowy załadunek');
define('MODULE_PAYMENT_FREECHARGER_TEXT_DESCRIPTION', 'Przeznaczony jest do darmowych wirtualnych produktów');
define('MODULE_PAYMENT_FREECHARGER_TEXT_EMAIL_FOOTER', 'Darmowy załadunek');