<?php
/*
  $Id: webmoney_merchant.php 1778 2008-01-09 23:37:44Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_LIQPAY_TEXT_TITLE', 'LiqPAY');
define('MODULE_PAYMENT_LIQPAY_TEXT_PUBLIC_TITLE', 'Visa/Mastercard LiqPay');
define('MODULE_PAYMENT_LIQPAY_TEXT_DESCRIPTION', 'Po kliknięciu przycisku Potwierdź zamówienie przejdziesz na stronę systemu płatności,  po dokonaniu płatności Twoje zamówienie zostanie zrealizowane.');
define('MODULE_PAYMENT_LIQPAY_TEXT_ADMIN_DESCRIPTION', 'Moduł płatności LiqPAY..');