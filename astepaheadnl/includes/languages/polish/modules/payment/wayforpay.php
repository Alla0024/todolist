<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Opis');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Włącz / wyłącz moduł');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Status domyślny');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Konto sprzedawcy');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Tajemnica handlowa');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'Porządek sortowania');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Stan płatności');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'Prawda - włącz, False - wyłącz');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Stan, który jest ustawiany zgodnie z kolejnością podczas jego tworzenia');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Konto sprzedawcy');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Tajemnica handlowa');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Kolejność sortowania modułu w bloku z modułami płatności');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'Status ustawiony dla zamówienia po pomyślnej płatności');