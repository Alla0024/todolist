<?php
/*
  $Id: percent.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2001,2002 osCommerce

  Released under the GNU General Public License
*/
define('MODULE_SHIPPING_PERCENT_TEXT_TITLE', 'Dostawa procentowa');
define('MODULE_SHIPPING_PERCENT_TEXT_DESCRIPTION', 'Wartość dostawy procentowej');
define('MODULE_SHIPPING_PERCENT_TEXT_WAY', 'Razem');