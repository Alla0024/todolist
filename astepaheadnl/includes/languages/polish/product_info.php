<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Nie znaleziono produktu!');
define('TEXT_DATE_ADDED', 'Ten produkt został dodany do naszego katalogu. %s');
define('TEXT_DATE_AVAILABLE', '<span>Produkt będzie dostępny w magazynie %s</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Produkt został pomyślnie odłożony!');
define('TEXT_COLOR', 'Kolor');
define('TEXT_SHARE', 'Udostępnij znajomym');
define('TEXT_REVIEWSES', 'opinie');
define('TEXT_REVIEWSES2', 'Opinie');
define('TEXT_DESCRIPTION', 'Opis');
define('TEXT_ATTRIBS', 'Сharakterystyka');
define('TEXT_PAYM_SHIP', 'Płatności i dostawa');
define('SHORT_DESCRIPTION', 'Krótki opis');


//home
define('HOME_TEXT_PAYM_SHIP', 'Gwarancja');