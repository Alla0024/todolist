<?php
/*
  $Id: account_password.php,v 1.1.1.1 2003/09/18 19:04:31 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Moje dane');
define('NAVBAR_TITLE_2', 'Zmienić hasło');
define('HEADING_TITLE', 'Moje hasło');
define('MY_PASSWORD_TITLE', 'Moje hasło');
define('SUCCESS_PASSWORD_UPDATED', 'Twoje hasło zostało zmienione.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Twoje hasło jest nieprawidłowe, podaj hasło, które było używane przy rejestracji.');
