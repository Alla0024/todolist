<?php
/*
  $Id: gv_faq.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Informacje na temat korzystania z kart podarunkowych');
define('HEADING_TITLE', 'Informacje na temat korzystania z kart podarunkowych');

define('TEXT_INFORMATION', '<a name="Top"></a>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=1','NONSSL').'">Jak kupić kartę podarunkową?</a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=2','NONSSL').'">Jak wysłać kartę podarunkową innej osobie?</a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=3','NONSSL').'">Dlaczego potrzebujesz kartę podarunkową?</a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=4','NONSSL').'">Jak korzystać z karty podarunkowej podczas składania zamówienia?</a><br>
  <a href="'.tep_href_link(FILENAME_GV_FAQ,'faq_item=5','NONSSL').'">Co zrobić, jeśli występują problemy, dotyczące korzystania z kart podarunkowych?</a><br>
');
switch ($_GET['faq_item']) {
  case '1':
define('SUB_HEADING_TITLE','Jak kupić kartę podarunkową?');
define('SUB_HEADING_TEXT','Karty podarunkowe są kupowane w taki samy sposób, jak i towary zwykłe, to znaczy, musisz dodać kartę podarunkową do koszyku i złożyć zamówienie, i wszystko! Po otrzymaniu płatności twoja karta podarunkowa zostanie aktywowany i będziesz mógł dokonywać zakupów za pomocą karty lub przekazać swoją kartę podarunkową do swojej rodziny lub znajomych. Po aktywacji karty podarunkowej otrzymasz powiadomienie na swój adres e-mail.');
  break;
  case '2':
define('SUB_HEADING_TITLE','Jak wysłać kartę podarunkową innej osobie?');
define('SUB_HEADING_TEXT','Zostaniesz poproszony o przesłanie pozostałej kwoty na kartę podarunkowej po złożeniu zamówienia, musisz wypełnić formularz i nacisnąć przycisk "Kontynuuj".');
  break;
  case '3':
  define('SUB_HEADING_TITLE','Dlaczego potrzebujesz kartę podarunkową?');
  define('SUB_HEADING_TEXT','Karta podarunkowa może być wykorzystany do pełnej lub częściowej zapłaty za zamówienie (w zależności od kwoty karty podarunkowej) wystawionej w naszym sklepie internetowym, a saldo karty podarunkowej się nie pali, pozostałe pieniądze można wykorzystać do dalszych zakupów w naszym sklepie internetowym, dodatkowo , możesz przekazać swój certyfikat do swojej rodzinie i przyjaciołom.');
  break;
  case '4':
  define('SUB_HEADING_TITLE','Jak korzystać z karty podarunkowej podczas składania zamówienia?');
  define('SUB_HEADING_TEXT','W procesie składania zamówienia w naszym sklepie internetowym zostaniesz poproszony o skorzystanie z karty podarunkowej.');
  break;
  case '5':
  define('SUB_HEADING_TITLE','Co zrobić, jeśli występują problemy, dotyczące korzystania z kart podarunkowych?');
  define('SUB_HEADING_TEXT','Jeśli masz jakiekolwiek problemy lub pytania dotyczące używania kart rabatowych, zadaj nam swoje pytania na stronie'. STORE_OWNER_EMAIL_ADDRESS . '. Na pewno dostaniesz odpowiedź ');
  break;
  default:
  define('SUB_HEADING_TITLE','');
  define('SUB_HEADING_TEXT','Wybierz pytanie, które cię interesuje.');

  }
?>
