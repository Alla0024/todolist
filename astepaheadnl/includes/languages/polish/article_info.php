<?php
/*
  $Id: article_info.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_ARTICLE_NOT_FOUND', 'Nie znaleziono artykułu');
define('TEXT_ARTICLE_NOT_FOUND', 'Przepraszamy, ale żądany artykuł nie jest dostępny!');
define('TEXT_DATE_ADDED', 'Ten artykuł został opublikowany %s.');
define('TEXT_DATE_AVAILABLE', 'Ten artykuł będzie dostępny %s.');
define('TEXT_BY', 'by ');
