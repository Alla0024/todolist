<?php
/*
  $Id: account.php
*/

define('NAVBAR_TITLE', 'Moje dane');
define('HEADING_TITLE', 'Moje dane');
define('MY_ACCOUNT_TITLE', 'Moje dane');

define('MY_ACCOUNT_PASSWORD', 'Zmienić hasło.');
define('MY_ORDERS_VIEW', 'Moje zakupy.');
define('MY_NAVI', 'Menu');
define('MY_INFO', 'Moja informacja');
define('EDIT_ADDRESS_BOOK', 'Edytować adres');
