<?php
/*
  $Id: address_book.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Moje adresy');
define('NAVBAR_TITLE_2', 'Książka adresowa');
define('HEADING_TITLE', 'Moja książka adresowa');
define('PRIMARY_ADDRESS_TITLE', 'Twój adres');
define('PRIMARY_ADDRESS_DESCRIPTION', 'Ten adres jest ustawiony jako domyślny. On będzie używany przy dokonywaniu zakupów i wszelkich innych czynnościach w naszym sklepie.');
define('ADDRESS_BOOK_TITLE', 'Pozycje książki adresowej');
define('PRIMARY_ADDRESS', '(*)');
define('TEXT_MAXIMUM_ENTRIES', '<b>UWAGA:</b> Maksymalny rozmiar książki adresowej - <b>%s</b> pozycji');
