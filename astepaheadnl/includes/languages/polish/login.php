<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Hasło');
define('HEADING_TITLE', 'Pozwól mi wejść!');

define('TEXT_NEW_CUSTOMER', 'Zarejestruj się teraz!');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'Rejestracja w naszym sklepie pozwoli na znacznie <b>szybsze i wygodniejsze</b> dokonywanie zakupów, ponadto możesz monitorować realizację oraz historię swoich zamówień.');
define('TEXT_RETURNING_CUSTOMER', 'Jestem już zarejestrowany!');
define('TEXT_LOGIN_ERROR', '<span><b>BŁĄD:</b></span> Nieprawidłowy \'E-Mail Adres\' i/lub \'Hasło\'.');
// guest_account start
define('NO_CUSTOMER', 'W bazie danych nie ma takiego użytkownika');
define('NO_PASS', 'Nieprawidłowe hasło');
define('SOC_VK', 'Vkontakte');
define('SOC_SITE', 'witryna');
define('SOC_ENTER_FROM_OTHER', 'Zaloguj się przez media społecznościowe lub przez swój login/hasło.');
define('SOC_NOW_FROM', 'Teraz spróbujesz zalogować się przez');
define('SOC_NEED_FROM', 'Zarejestrowałeś się przez');
define('SOC_LOGIN_THX', 'Dziękujemy, udało ci się wejść!');
