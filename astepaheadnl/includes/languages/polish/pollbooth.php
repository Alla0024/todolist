<?php
/*
  $Id: pollbooth.php,v 1.5 2003/04/06 21:45:33 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Copyright (c) 2000,2001 The Exchange Project

  Released under the GNU General Public License
*/
if (!isset($_GET['op'])) {
	$_GET['op']="list";
	}
if ($_GET['op']=='results') {
  define('HEADING_TITLE', 'Wyniki ankiety');
}
if ($_GET['op']=='list') {
  define('HEADING_TITLE', 'Wyniki ankiety');
}
if ($_GET['op']=='vote') {
  define('HEADING_TITLE', 'Wyniki ankiety');
}
if ($_GET['op']=='comment') {
  define('HEADING_TITLE', 'Opinie');
}
define('_WARNING', 'Ostrzeżenie: ');
define('_ALREADY_VOTED', 'Już głosowałeś.');
define('_NO_VOTE_SELECTED', 'Nie wybrałeś odpowiedzi do głosowania.');
define('_TOTALVOTES', 'Łączna liczba głosów');
define('_OTHERPOLLS', 'Inne ankiety');
define('NAVBAR_TITLE_1', 'Wyniki ankiety');
define('_POLLRESULTS', 'Wyniki ankiety');
define('_VOTING', 'Głosuj');
define('_RESULTS', 'Wyniki');
define('_VOTES', 'Głosów');
define('_VOTE', 'Głosuj');
define('_COMMENT', 'Opinie');
define('_COMMENTS_BY', 'Opinie dodał ');
define('_COMMENTS_ON', '  ');
define('_YOURNAME', 'Twoje imię:');
    define('_OTZYV', 'Opinie:');
define('TEXT_CONTINUE', 'Dodaj opinię');
define('_PUBLIC','Otwarte głosowanie');
define('_PRIVATE','Zamknięte głosowanie');
define('_POLLOPEN','Ankieta jest otwarta');
define('_POLLCLOSED','Ankieta dla zarejestrowanych użytkowników');
define('_POLLPRIVATE','Ankieta dla zarejestrowanych użytkowników, zaloguj się do sklepu.');
define('_ADD_COMMENTS', 'Dodać opinię');
define('TEXT_DISPLAY_NUMBER_OF_COMMENTS', 'Wyświetlono <b>%d</b> - <b>%d</b> (łącznie <b>%d</b> opinii)');
