<?php
/*
  Created by: Linda McGrath osCOMMERCE@WebMakers.com
  
  Update by: fram 05-05-2003
  Updated by: Donald Harriman - 08-08-2003 - MS2

  down_for_maintenance.php v1.1
  
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Sklep jest zamknięty z powodów technicznych.');
define('HEADING_TITLE', 'Sklep jest tymczasowo zamknięty ...');
define('DOWN_FOR_MAINTENANCE_TEXT_INFORMATION', 'Są wykonywane prace techniczne na serwerze, sprawdź później.');
define('TEXT_MAINTENANCE_ON_AT_TIME', 'Data zamknięcia: ');
define('TEXT_MAINTENANCE_PERIOD', 'Sklep wznowi pracę za: ');
define('DOWN_FOR_MAINTENANCE_STATUS_TEXT', 'Kliknij przycisk "Kontynuuj", sklep może być już pracuje:');
