<?php
/*
  $Id: account_history.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Moje dane');
define('NAVBAR_TITLE_2', 'Historia zamówień');
define('HEADING_TITLE', 'Historia zamówień');
define('TEXT_ORDER_STATUS', 'Status zamówienia');
define('TEXT_ORDER_DATE', 'Data zamówienia');
define('TEXT_ORDER_SHIPPED_TO', 'Adres dostawy:');
define('TEXT_ORDER_BILLED_TO', 'Kupujący:');
define('TEXT_ORDER_COST', 'Suma');
define('TEXT_ORDER_PC', 'szt.');
define('TEXT_NO_PURCHASES', 'Niestety, nie masz jeszcze żadnych zakupów w naszym sklepie');

