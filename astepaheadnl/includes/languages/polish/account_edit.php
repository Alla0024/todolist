<?php
/*
  $Id: account_edit.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Moje dane');
define('NAVBAR_TITLE_2', 'Edycja');
define('HEADING_TITLE', 'Moje dane');
define('MY_ACCOUNT_TITLE', 'Moje dane');
define('SUCCESS_ACCOUNT_UPDATED', 'Twoje dane zaktualizowane.');
