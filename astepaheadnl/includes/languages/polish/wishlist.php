<?php
/*
  $Id: wishlist.php,v 3.0  2005/04/20 Dennis Blake
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Produkty odłożone');
define('BOX_TEXT_PRICE', 'Koszt');
define('BOX_TEXT_PRODUCT', 'Nazwa');
define('BOX_TEXT_IMAGE', 'Zdjęcie');
define('BOX_TEXT_NO_ITEMS', 'Brak odłożonych produktów.');
define('TEXT_EMAIL', 'Email: ');
define('TEXT_MESSAGE', 'Wiadomość: ');
define('TEXT_ITEM_IN_CART', 'Produkt w koszyku');
define('TEXT_ITEM_NOT_AVAILABLE', 'Produkt jest niedostępny.');
define('WISHLIST_EMAIL_SUBJECT', 'wysłał do ciebie wiadomość');  //Customers name will be automatically added to the beginning of this.
define('WISHLIST_SENT', 'Towary odłożone są pomyślnie wysłane.');
define('WISHLIST_EMAIL_LINK', '

Lista odłożonych produktów dla odwiedzających $from_name:
$link

Dziękujemy, ' . STORE_NAME); //$from_name = Customers name  $link = public wishlist link

define('WISHLIST_EMAIL_GUEST', 'Dziękujemy, ' . STORE_NAME);

define('ERROR_YOUR_NAME' , 'Wpisz swoje imię.');
define('ERROR_YOUR_EMAIL' , 'Podaj swój adres e-mail.');
define('ERROR_VALID_EMAIL' , 'Wprowadź poprawny adres e-mail.');
define('ERROR_ONE_EMAIL' , 'Musisz podać przynajmniej jedno imię i jeden adres e-mail.');
define('ERROR_ENTER_EMAIL' , 'Wprowadź adres e-mail.');
define('ERROR_ENTER_NAME' , 'Podaj nazwy odbiorców.');
define('ERROR_MESSAGE', 'Dodaj wiadomość.');
