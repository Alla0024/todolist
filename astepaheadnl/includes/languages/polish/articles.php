<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');

if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
  define('HEADING_TITLE', $topics['topics_name']);
  define('TEXT_NO_ARTICLES', 'Obecnie nie ma artykułów w tej sekcji.');
  define('TEXT_ARTICLES', 'Lista artykułów:');
  define('TEXT_DATE_ADDED', 'Opublikowano:');
  define('TEXT_TOPIC', 'Sekcja');
  define('TEXT_BY', 'Autor:');
} elseif ($topic_depth == 'top') {
  define('HEADING_TITLE', 'Wszystkie artykuły');
  define('TEXT_CURRENT_ARTICLES', '');
  define('TEXT_UPCOMING_ARTICLES', 'Artykuły, które będą opublikowane w najbliższym czasie.');
  define('TEXT_NO_ARTICLES', 'Brak dostępnych artykułów.');
  define('TEXT_DATE_ADDED', 'Opublikowano:');
  define('TEXT_DATE_EXPECTED', 'Oczekiwano:');
  define('TEXT_TOPIC', 'Sekcja:');
  define('TEXT_BY', 'by');
} elseif ($topic_depth == 'nested') {
  define('HEADING_TITLE', 'Artykuły');
}
  define('TOTAL_ARTICLES', 'Wszystkich artykułów');
  define('HEADING_TITLE', 'Artykuły');
