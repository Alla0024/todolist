<?php 
define('INVOICE_TEXT_THANK_YOU','Grazie per aver approfittato di un negozio');
define('TEXT_INFO_ORDERS_STATUS_NAME','Stato dell\'ordine:');
define('TABLE_HEADING_PRODUCTS_MODEL','Codice #');
define('INVOICE_TEXT_INVOICE_NR','Ordine #');
define('INVOICE_TEXT_INVOICE_DATE','Data Ordine:');
define('INVOICE_TEXT_NUMBER_SIGN','#');
define('INVOICE_TEXT_DASH','-');
define('INVOICE_TEXT_COLON',':');
define('INVOICE_TEXT_INVOICE','Conto');
define('INVOICE_TEXT_ORDER','Ordine');
define('INVOICE_TEXT_DATE_OF_ORDER','Data');
define('INVOICE_TEXT_DATE_DUE_DATE','Data di pagamento');
define('ENTRY_SOLD_TO','Indirizzo di fatturazione:');
define('ENTRY_SHIP_TO','Indirizzo di spedizione:');
define('ENTRY_PAYMENT_METHOD','Pagamento:');
define('TABLE_HEADING_PRODUCTS','Prodotti');
define('TABLE_HEADING_UNIT_PRICE','pz');
define('TABLE_HEADING_TOTAL','Tutto');
define('ENTRY_SUB_TOTAL','Importo:');
define('ENTRY_SHIPPING','Spedizione:');
define('ENTRY_TAX','Tassa di soggiorno:');
define('ENTRY_TOTAL','Solo:');


define('PRINT_HEADER_COMPANY_NAME', 'Nome della ditta:');
define('PRINT_HEADER_ADDRESS', 'Indirizzo:');
define('PRINT_HEADER_PHONE', 'Telefono:');
define('PRINT_HEADER_EMAIL_ADDRESS', 'E-mail:');
define('PRINT_HEADER_WEBSITE', 'Sito web');
define('PDF_TITLE_TEXT', 'Fattura');
define('INVOICE_CUSTOMER_NUMBER', 'Cliente:');
define('TABLE_HEADING_PRODUCTS_PC', 'Quantità');
define('PDF_FOOTER_TEXT', 'Il tuo testo in fondo alla pagina');
