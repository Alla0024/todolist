<?php 
define('MODULE_SHIPPING_ZONES_TEXT_TITLE','Le tariffe per la zona');
define('MODULE_SHIPPING_ZONES_TEXT_DESCRIPTION','zonale tariffa base');
define('MODULE_SHIPPING_ZONES_TEXT_WAY','Consegna a');
define('MODULE_SHIPPING_ZONES_TEXT_UNITS','Kg.');
define('MODULE_SHIPPING_ZONES_INVALID_ZONE','Per il paese selezionato nessuna possibilità di spedizione ');
define('MODULE_SHIPPING_ZONES_UNDEFINED_RATE','Il costo della spedizione per ora non può essere determinato ');
