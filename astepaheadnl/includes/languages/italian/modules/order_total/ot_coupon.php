<?php 
define('MODULE_ORDER_TOTAL_COUPON_TITLE','Coupon');
define('MODULE_ORDER_TOTAL_COUPON_HEADER','Certificati/Tagliandi');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION','Coupon');
define('ERROR_NO_INVALID_REDEEM_COUPON','Non valido il codice coupon');
define('ERROR_MINIMUM_CART_AMOUNT','Importo minimo di ordine per questo coupon: %s');
define('ERROR_INVALID_STARTDATE_COUPON','Specificato coupon ancora non è valido');
define('ERROR_INVALID_FINISDATE_COUPON','Il coupon è scaduto');
define('ERROR_INVALID_USES_COUPON','Il coupon è già stato utilizzato ');
define('TIMES',' volta.');
define('ERROR_INVALID_USES_USER_COUPON','Si è utilizzato un coupon maggior numero di volte possibile.');
define('TEXT_ENTER_COUPON_CODE','Il codice:&nbsp;&nbsp;');
