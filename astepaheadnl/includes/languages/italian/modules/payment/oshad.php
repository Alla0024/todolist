<?php 
define('MODULE_PAYMENT_OSHAD_TEXT_TITLE','Oshad');
define('MODULE_PAYMENT_OSHAD_TEXT_PUBLIC_TITLE','Oshadbank Visa/Mastercard');
define('MODULE_PAYMENT_OSHAD_TEXT_DESCRIPTION','Dopo aver premuto il pulsante di Confermare l\'ordine Si va al sito web del sistema di pagamento per il pagamento dell\'ordine, dopo il pagamento, il Vostro ordine verrà eseguito.');
define('MODULE_PAYMENT_OSHAD_TEXT_ADMIN_DESCRIPTION','Modulo di pagamento Oshad..');
