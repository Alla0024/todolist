<?php 
define('MODULE_PAYMENT_LIQPAY_TEXT_TITLE','LiqPAY');
define('MODULE_PAYMENT_LIQPAY_TEXT_PUBLIC_TITLE','Visa/Mastercard LiqPay');
define('MODULE_PAYMENT_LIQPAY_TEXT_DESCRIPTION','Dopo aver premuto il pulsante di Confermare l\'ordine Si va al sito web del sistema di pagamento per il pagamento dell\'ordine, dopo il pagamento, il Vostro ordine verrà eseguito.');
define('MODULE_PAYMENT_LIQPAY_TEXT_ADMIN_DESCRIPTION','Modulo di pagamento LiqPAY..');
