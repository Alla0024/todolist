<?php 
define('NAVBAR_TITLE_1','I miei dati');
define('NAVBAR_TITLE_2','Cambiare la password');
define('HEADING_TITLE','La mia password');
define('MY_PASSWORD_TITLE','La mia password');
define('SUCCESS_PASSWORD_UPDATED','Si può cambiato la password.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING','Hai inserito una password errata, digitare la password che Hai inserito durante la registrazione.');
