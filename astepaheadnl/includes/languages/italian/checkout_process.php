<?php 
define('EMAIL_TEXT_SUBJECT','Il vostro ordine');
define('EMAIL_TEXT_ORDER_NUMBER','Numero dell\'ordine:');
define('EMAIL_TEXT_INVOICE_URL','Informazioni sull\'ordine:');
define('EMAIL_TEXT_DATE_ORDERED','Data ordine:');
define('EMAIL_TEXT_PRODUCTS','Avete ordinato:');
define('EMAIL_TEXT_DELIVERY_ADDRESS','L\'indirizzo di spedizione');
define('EMAIL_TEXT_BILLING_ADDRESS','L\'indirizzo dell\'acquirente');
define('EMAIL_TEXT_PAYMENT_METHOD','Metodo di pagamento');
define('EMAIL_SEPARATOR','------------------------------------------------------');
define('EMAIL_TEXT_CUSTOMER_NAME','Acquirente:');
define('EMAIL_TEXT_CUSTOMER_EMAIL_ADDRESS','Email:');
define('EMAIL_TEXT_CUSTOMER_TELEPHONE','Telefono:');
