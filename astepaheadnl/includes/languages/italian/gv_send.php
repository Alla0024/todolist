<?php 
define('HEADING_TITLE','Inviare il certificato');
define('NAVBAR_TITLE','Inviare il certificato');
define('EMAIL_SUBJECT','Un messaggio da un negozio Online');
define('HEADING_TEXT','<br>Per inviare il certificato, È necessario compilare il seguente modulo.<br>');
define('ENTRY_NAME','Il nome del destinatario:');
define('ENTRY_EMAIL','Indirizzo E-Mail del destinatario:');
define('ENTRY_MESSAGE','Messaggio:');
define('ENTRY_AMOUNT','L\'importo del certificato:');
define('ERROR_ENTRY_AMOUNT_CHECK','&nbsp;&nbsp;<span class=\"errorText\">Errata importo</span>');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK','&nbsp;&nbsp;<span class=\"errorText\">non Valido indirizzo Email</span>');
define('MAIN_MESSAGE','Si decide di inviare il certificato per l\'importo %s vostro amico di %s, il suo indirizzo Email: %s<br><br>il Destinatario del certificato riceverà il seguente messaggio:<br><br>Caro %s<br><br>  Ti inviato un certificato per un importo di %s, mittente: %s');
define('PERSONAL_MESSAGE','%s scrive:');
define('TEXT_SUCCESS','Complimenti, il Tuo certificato stato inviato con successo');
define('EMAIL_SEPARATOR','----------------------------------------------------------------------------------------');
define('EMAIL_GV_TEXT_HEADER','Complimenti, Hai ottenuto il certificato per l\'importo di %s');
define('EMAIL_GV_TEXT_SUBJECT','Regalo di %s');
define('EMAIL_GV_FROM','Il mittente di questo certificato di %s');
define('EMAIL_GV_MESSAGE','Messaggio del mittente: ');
define('EMAIL_GV_SEND_TO','Ciao, %s');
define('EMAIL_GV_REDEEM','Per intensificare il certificato di aprire il link che si trova sotto. Il codice del certificato: %s');
define('EMAIL_GV_LINK','Cliccare qui per intensificare il certificato ');
define('EMAIL_GV_FIXED_FOOTER','Se Si verificano problemi durante l\'attivazione di un certificato utilizzando il link di cui sopra, Si consiglia di inserire il codice del certificato al momento dell\'ordine, e non attraverso il link di cui sopra.');
define('EMAIL_GV_SHOP_FOOTER','');
