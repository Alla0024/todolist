<?php 
define('NAVBAR_TITLE','La password');
define('HEADING_TITLE','Permettetemi di entrare!');
define('TEXT_NEW_CUSTOMER','Iscriversi!');
define('TEXT_NEW_CUSTOMER_INTRODUCTION','Registrazione presso il nostro negozio, sarai in grado di acquistare molto di <b>più veloce e più conveniente</b>, inoltre, È in grado di seguire l\'esecuzione degli ordini, visualizzare la cronologia degli ordini.');
define('TEXT_RETURNING_CUSTOMER','Sono già registrato!');
define('TEXT_LOGIN_ERROR','<span><b>ERRORE:</b></span> non Valido \'Indirizzo E-Mail e/o Password\'.');
define('NO_CUSTOMER','Questo utente non esiste nel database');
define('NO_PASS','Password errata');
define('SOC_VK','Vkontakte');
define('SOC_SITE','sito');
define('SOC_ENTER_FROM_OTHER','Vai da un altro soc. rete o sotto il tuo username/password.');
define('SOC_NOW_FROM','Ora si sta cercando di andare da');
define('SOC_NEED_FROM','Registrato dal');
define('SOC_LOGIN_THX','Grazie, è entrato con successo!');
