<?php 
define('NAVBAR_TITLE','Intensificare il certificato');
define('HEADING_TITLE','Intensificare il certificato');
define('TEXT_INFORMATION','');
define('TEXT_INVALID_GV','Il certificato specificato non esiste o è stato già utilizzato. Se Avete domande, non esitate a contattarci.');
define('TEXT_VALID_GV','Si può intensificato il certificato per l\'importo %s! Ora È possibile utilizzare il certificato per effettuare acquisti nel nostro negozio online.');
