<?php 
define('NAVBAR_TITLE_1','I miei dati');
define('NAVBAR_TITLE_2','Rubrica');
define('NAVBAR_TITLE_ADD_ENTRY','Nuovo record');
define('NAVBAR_TITLE_MODIFY_ENTRY','Modificare la voce');
define('NAVBAR_TITLE_DELETE_ENTRY','Eliminare la voce');
define('HEADING_TITLE_ADD_ENTRY','Nuovo record');
define('HEADING_TITLE_MODIFY_ENTRY','Modificare la voce');
define('HEADING_TITLE_DELETE_ENTRY','Eliminare la voce');
define('DELETE_ADDRESS_TITLE','Rimuovere l\'indirizzo');
define('DELETE_ADDRESS_DESCRIPTION','Vuoi davvero cancellare indirizzi selezionati dalla rubrica?');
define('NEW_ADDRESS_TITLE','Nuovo record');
define('SELECTED_ADDRESS','L\'indirizzo selezionato');
define('SET_AS_PRIMARY','Fare default.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED','L\'indirizzo selezionato rimosso dalla rubrica.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED','La rubrica aggiornata.');
define('WARNING_PRIMARY_ADDRESS_DELETION','L\'indirizzo predefinito non può essere rimosso. Impostare lo stato di default su un altro indirizzo e provare ancora una volta.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY','La rubrica non è stata trovata.');
define('ERROR_ADDRESS_BOOK_FULL','La rubrica è piena. Rimuovere inutile indirizzo e solo allora Si sarà in grado di aggiungere un nuovo indirizzo.');
