<?php 
define('TEXT_PRODUCT_NOT_FOUND','Il prodotto non è stato trovato!');
define('TEXT_DATE_ADDED','Questo prodotto è stato aggiunto al nostro catalogo il %s');
define('TEXT_DATE_AVAILABLE','<span>la Merce è disponibile a magazzino in %s</span>');
define('PRODUCT_ADDED_TO_WISHLIST','La merce può accantonato!');
define('TEXT_COLOR','Colore');
define('TEXT_SHARE','Condividi con i tuoi amici');
define('TEXT_REVIEWSES','recensioni');
define('TEXT_REVIEWSES2','Recensioni');
define('TEXT_DESCRIPTION','Descrizione');
define('TEXT_ATTRIBS','Caratteristiche');
define('TEXT_PAYM_SHIP','Il pagamento e la spedizione');
define('SHORT_DESCRIPTION','Breve descrizione');
define('HOME_TEXT_PAYM_SHIP','Garanzia');
