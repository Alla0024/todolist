<?php 
define('TEXT_MAIN','');
if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
    define('HEADING_TITLE', $topics['topics_name']);
    define('TEXT_NO_ARTICLES', 'Al momento non ci sono articoli in questo argomento.');
    define('TEXT_ARTICLES', 'Di seguito è riportato un elenco di articoli con i più recenti elencati per primi.');
    define('TEXT_DATE_ADDED', 'Pubblicato:');
    define('TEXT_TOPIC', 'Argomento:');
    define('TEXT_BY', 'di');
} elseif ($topic_depth == 'top') {
    define('HEADING_TITLE', 'Tutti gli articoli');
    define('TEXT_CURRENT_ARTICLES', 'Articoli attuali');
    define('TEXT_UPCOMING_ARTICLES', 'Prossimi articoli');
    define('TEXT_NO_ARTICLES', 'Al momento non ci sono articoli elencati.');
    define('TEXT_DATE_ADDED', 'Pubblicato:');
    define('TEXT_DATE_EXPECTED', 'Previsto:');
    define('TEXT_TOPIC', 'Argomento:');
    define('TEXT_BY', 'di');
} elseif ($topic_depth == 'nested') {
    define('HEADING_TITLE', 'Articoli');
}
define('TOTAL_ARTICLES','Tutti gli articoli');
define('HEADING_TITLE','Articoli');
