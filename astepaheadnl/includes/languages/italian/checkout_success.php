<?php 
define('NAVBAR_TITLE_1','Effettuare l\'ordine');
define('NAVBAR_TITLE_2','Successo');
define('HEADING_TITLE','Il vostro ordine è decorata!');
define('TEXT_SUCCESS','Il tuo ordine è andato a buon fine!');
define('TEXT_THANKS_FOR_SHOPPING','Il tuo ordine è andato a buon fine!');
define('TABLE_HEADING_COMMENTS','In breve tempo sarete contattati da un nostro responsabile.');
define('TABLE_HEADING_DOWNLOAD_DATE','Il link è valido fino a: ');
define('TABLE_HEADING_DOWNLOAD_COUNT',' volte è possibile scaricare il file.');
define('SMS_NEW_ORDER','Avete un nuovo ordine');
define('TEXT_SUCCESS_INFO','Tutte le ulteriori istruzioni e coordinate bancarie per il pagamento inviati per e-mail');
