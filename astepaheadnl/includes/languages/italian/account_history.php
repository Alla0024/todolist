<?php 
define('NAVBAR_TITLE_1','I miei dati');
define('NAVBAR_TITLE_2','La storia degli ordini');
define('HEADING_TITLE','La storia degli ordini');
define('TEXT_ORDER_STATUS','Lo stato dell\'ordine');
define('TEXT_ORDER_DATE','La data dell\'ordine');
define('TEXT_ORDER_SHIPPED_TO','Indirizzo di spedizione:');
define('TEXT_ORDER_BILLED_TO','Acquirente:');
define('TEXT_ORDER_COST','L\'importo');
define('TEXT_ORDER_PC','pz');
define('TEXT_NO_PURCHASES','Purtroppo, non Hai ancora acquistato nel nostro negozio.');
