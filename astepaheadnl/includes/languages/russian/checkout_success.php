<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Оформление заказа');
define('NAVBAR_TITLE_2', 'Успешно');
define('HEADING_TITLE', 'Ваш заказ оформлен!');
define('TEXT_SUCCESS', 'Ваш заказ успешно оформлен!'); 
define('TEXT_THANKS_FOR_SHOPPING', 'Ваш заказ успешно оформлен!');
define('TABLE_HEADING_COMMENTS', 'В ближайшее время с вами свяжется наш менеджер.');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Ссылка действительна до: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' раз можно загрузить файл.');
define('SMS_NEW_ORDER', 'У Вас новый заказ');
define('TEXT_SUCCESS_INFO', 'Все дальнейшие инструкции и реквизиты для оплаты отправлены вам на почту');