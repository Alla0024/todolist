<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');

if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
  define('HEADING_TITLE', $topics['topics_name']);
  define('TEXT_NO_ARTICLES', 'На данный момент нет статей в данном разделе.');
  define('TEXT_ARTICLES', 'Список статей:');
  define('TEXT_DATE_ADDED', 'Опубликовано:');
  define('TEXT_TOPIC', '\'Раздел123');
  define('TEXT_BY', 'Автор:');
} elseif ($topic_depth == 'top') {
  define('HEADING_TITLE', 'Все статьи');
  define('TEXT_CURRENT_ARTICLES', '');
  define('TEXT_UPCOMING_ARTICLES', 'Статьи, которые будут опубликованы в ближайшее время.');
  define('TEXT_NO_ARTICLES', 'Нет доступных статей.');
  define('TEXT_DATE_ADDED', 'Опубликовано:');
  define('TEXT_DATE_EXPECTED', 'Ожидается:');
  define('TEXT_TOPIC', 'Раздел:');
  define('TEXT_BY', 'by');
} elseif ($topic_depth == 'nested') {
  define('HEADING_TITLE', 'Статьи');
}
  define('TOTAL_ARTICLES', 'Всего статей');
  define('HEADING_TITLE', 'Статьи');
