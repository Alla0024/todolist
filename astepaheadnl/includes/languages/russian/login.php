<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Пароль');
define('HEADING_TITLE', 'Позвольте войти!');

define('TEXT_NEW_CUSTOMER', 'Зарегистрироваться!');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'Зарегистрировавшись в нашем магазине, Вы сможете совершать покупки намного <b>быстрее и удобнее</b>, кроме того, Вы сможете следить за выполнением заказов, смотреть историю своих заказов.');
define('TEXT_RETURNING_CUSTOMER', 'Я уже зарегистрирован!');
define('TEXT_LOGIN_ERROR', '<span><b>ОШИБКА:</b></span> Неверный \'E-Mail Адрес\' и/или \'Пароль\'.');
// guest_account start
define('NO_CUSTOMER', 'Такого пользователя нет в базе');
define('NO_PASS', 'Неверный пароль');
define('SOC_VK', 'Вконтакте');
define('SOC_SITE', 'сайта');
define('SOC_ENTER_FROM_OTHER', 'Зайдите из другой соц. сети или под своим логином/паролем.');
define('SOC_NOW_FROM', 'Сейчас вы пытаетесь зайти из');
define('SOC_NEED_FROM', 'Вы регистрировались из');
define('SOC_LOGIN_THX', 'Спасибо, вы успешно вошли!');