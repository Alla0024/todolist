<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Содержимое корзины');
define('HEADING_TITLE', 'Моя корзина');
define('TABLE_HEADING_REMOVE', 'Удалить');
define('TABLE_HEADING_REMOVE_FROM', 'из корзины');
define('TABLE_HEADING_QUANTITY', 'Кол-во');
define('TABLE_HEADING_IMAGE', 'Картинка');
define('TABLE_HEADING_NAME', 'Наименование');
define('TABLE_HEADING_PRICE', 'Цена');
define('TABLE_HEADING_MODEL', 'Код товара');
define('TABLE_HEADING_PRICE_TOTAL', 'Стоимость');
define('TABLE_HEADING_PRODUCTS', 'Товары');
define('TABLE_HEADING_TOTAL', 'Стоимость');
define('TEXT_CART_EMPTY', 'Ваша корзина пуста!');
define('SUB_TITLE_COUPON', 'Купон');
define('SUB_TITLE_COUPON_SUBMIT', 'Применить');
define('SUB_TITLE_COUPON_VALID', 'Поздравляем! <br />Вы ввели верный купон!');
define('SUB_TITLE_COUPON_INVALID', 'Вы ввели неверный купон!');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'Товары, выделенные ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' имеются на нашем складе в недостаточном для Вашего заказа количестве.<br>Пожалуйста, измените количество продуктов выделенных (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), благодарим Вас');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Товары, выделенные ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' имеются на нашем складе в недостаточном для Вашего заказа количестве.<br>Тем не менее, Вы можете купить их и проверить количество имеющихся в наличии для поэтапной доставки в процессе выполнения Вашего заказа.');