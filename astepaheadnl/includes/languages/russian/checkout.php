<?php
/*
  One Page Checkout, Version: 1.08

  I.T. Web Experts
  http://www.itwebexperts.com

  Copyright (c) 2009 I.T. Web Experts

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Оформление заказа');
define('NAVBAR_TITLE_1', 'Оформление заказа');
define('HEADING_TITLE', 'Оформление заказа');
define('TABLE_HEADING_SHIPPING_ADDRESS', 'Адрес доставки');
define('TABLE_HEADING_BILLING_ADDRESS', 'Адрес оплаты');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Наименование');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Цена за шт.');
define('TABLE_HEADING_PRODUCTS', 'Корзина');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Всего');
define('ENTRY_TELEPHONE', 'Телефон');
define('ENTRY_COMMENT', 'Комментарий к заказу');
define('TABLE_HEADING_SHIPPING_METHOD', 'Способ доставки');
define('TABLE_HEADING_PAYMENT_METHOD', 'Способ оплаты');
define('TEXT_CHOOSE_SHIPPING_METHOD', '');
define('TEXT_SELECT_PAYMENT_METHOD', '');
define('TEXT_ERROR_SHIPPING_METHOD', 'Пожалуйста, введите ваш адрес доставки, чтобы увидеть <b>%s</b>');
define('TEXT_ENTER_SHIPPING_INFORMATION', ''); //This is currently the only shipping method available to use on this order.
define('TEXT_ENTER_PAYMENT_INFORMATION', '');
define('TABLE_HEADING_COMMENTS', 'Комментарии:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Продолжить оформление заказа');
define('EMAIL_SUBJECT', 'Рады приветствовать Вас в интернет-магазине ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Уважаемая %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Уважаемый %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Уважаемый(ая) %s <br /><br/>');
define('EMAIL_WELCOME', 'Рады приветствовать Вас в интернет-магазине <b>' . STORE_NAME . '</b>. <br /><br/>');
define('EMAIL_TEXT', 'Теперь Вы имеете доступ к некоторым дополнительным возможностям, которые доступны зарегистрированным пользователям:' . "\n\n" . '<li><b>Корзина</b> - любые продукты, добавленые в корзину, остаются там, пока Вы не удалите их или не оформите заказ.' . "\n" . '<li><b>Адресная книга</b> - теперь мы можем отправлять свою продукцию на адрес, который Вы указали в пункте "Адрес доставки".' . "\n" . '<li><b>История заказов</b> - у Вас есть возможность просматривать историю заказов в нашем магазине.<br><br>');
define('EMAIL_CONTACT', 'Если у Вас возникли какие-либо вопросы, пишите: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '');
// Start - CREDIT CLASS Gift Voucher Contribution
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'As part of our welcome to new customers, we have sent you an e-Gift Voucher worth %s');
define('EMAIL_GV_REDEEM', 'The redeem code for the e-Gift Voucher is %s, you can enter the redeem code when checking out while making a purchase');
define('EMAIL_GV_LINK', 'or by following this link ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Congratulations, to make your first visit to our online shop a more rewarding experience we are sending you an e-Discount Coupon.' . "\n" .
										' Below are details of the Discount Coupon created just for you' . "\n");
define('EMAIL_COUPON_REDEEM', 'To use the coupon enter the redeem code which is %s during checkout while making a purchase');
// End - CREDIT CLASS Gift Voucher Contribution
define('TEXT_PLEASE_SELECT', 'Выберите');
define('TEXT_PASSWORD_FORGOTTEN', 'Забыли пароль?');
define('IMAGE_LOGIN', 'Войти');
define('TEXT_HAVE_COUPON_KGT', 'Есть купон?');
define('TEXT_DIFFERENT_SHIPPING', 'Адрес оплаты отличается от адреса доставки?');
define('TEXT_DIFFERENT_BILLING', 'Адрес доставки отличается от адреса оплаты?');
// Points/Rewards Module V2.1rc2a BOF

define('TEXT_MIN_SUM', 'МИНИМАЛЬНАЯ СУММА ЗАКАЗА');
define('TEXT_NEW_CUSTOMER', 'Новый покупатель');
define('TEXT_LOGIN_SOCIAL', 'Войти через соц. сети');
define('TEXT_REGISTRATION_OFF', 'Оформить заказ без регистрации');


define('TEXT_EMAIL_EXISTS', 'Такой e-mail уже существует, пожалуйста');
define('TEXT_EMAIL_EXISTS2', 'войдите');
define('TEXT_EMAIL_EXISTS3', 'под указанным e-mail');
define('TEXT_EMAIL_WRONG', 'вы ввели неверный e-mail.');
define('TEXT_ORDER_PROCESSING', 'Заказ обрабатывается, подождите...');
define('TEXT_EMAIL_LOGIN', 'Логин');
define('TEXT_EMAIL_PASS', 'Пароль');

define('TEXT_CHANGE_ADDRESS', 'Изменить адрес');
define('ADDRESS_BOOK', 'Адресная книга');
define('BILLING_ADDRESS_THE_SAME', 'Тот же');

define('CH_JS_REFRESH', 'Обновление');
define('CH_JS_REFRESH_METHOD', 'Обновление способа');
define('CH_JS_SETTING_METHOD', 'Установка метода');
define('CH_JS_SETTING_ADDRESS', 'Установка адреса');
define('CH_JS_SETTING_ADDRESS_BIL', 'Оплаты');
define('CH_JS_SETTING_ADDRESS_SHIP', 'Доставки');

define('CH_JS_ERROR_SCART', 'В процессе обновления корзины возникла ошибка, пожалуйста, проинформируйте');
define('CH_JS_ERROR_SOME1', 'В процессе обновления');
define('CH_JS_ERROR_SOME2', 'возникла ошибка, пожалуйста, проинформируйте');

define('CH_JS_ERROR_SET_SOME1', 'Возникла ошибка в процессе установки способа');
define('CH_JS_ERROR_SET_SOME2', ', пожалуйста, проинформируйте');
define('CH_JS_ERROR_SET_SOME3', 'об этой ошибке.');

define('CH_JS_ERROR_REQ_BIL', 'Заполните, пожалуйста, необходимые поля в разделе "Адрес оплаты"');
define('CH_JS_ERROR_ERR_BIL', 'Проверьте, пожалуйста, корректность ввода данных в разделе "Адрес оплаты"');
define('CH_JS_ERROR_REQ_SHIP', 'Заполните, пожалуйста, все необходимые поля в "Адресе доставки"');
define('CH_JS_ERROR_ERR_SHIP', 'Проверьте, пожалуйста, корректность ввода данных в разделе "Адрес доставки"');
define('CH_JS_ERROR_ADDRESS', 'Ошибка адреса');
define('CH_JS_ERROR_PMETHOD', 'Ошибка выбора способа оплаты');
define('CH_JS_ERROR_SELECT_PMETHOD', 'Вы должны выбрать способ оплаты.');
define('CH_JS_CHECK_EMAIL', 'Проверка E-mail адреса');
define('CH_JS_ERROR_EMAIL', 'В процессе проверки email адреса возникла ошибка, пожалуйста, проинформируйте');