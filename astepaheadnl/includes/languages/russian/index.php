<?php
/*
  $Id: index.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '&nbsp;');
define('TABLE_HEADING_DATE_EXPECTED', 'Дата поступления');
define('TABLE_HEADING_DEFAULT_SPECIALS', 'Скидки %s');   

  define('HEADING_TITLE', 'Список товаров');

  define('TABLE_HEADING_IMAGE', '<span class="sort">Сортировать по:</span>');       
  define('TABLE_HEADING_MODEL', 'Код товара');
  define('TABLE_HEADING_PRODUCTS', '<span class="cena">Наименование</span>');
  define('TABLE_HEADING_MANUFACTURER', 'Производитель');
  define('TABLE_HEADING_QUANTITY', 'Количество');
  define('TABLE_HEADING_PRICE', '<span class="cena">Цена</span>');
  define('TABLE_HEADING_WEIGHT', 'Вес');
  define('TABLE_HEADING_PRODUCT_SORT', 'Порядок');
  define('TEXT_ALL_MANUFACTURERS', 'Все производители');