<?php
/*
  $Id: ot_coupon.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Купон');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Сертификаты/Купоны');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Купон');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Неверный код купона');
define('ERROR_MINIMUM_CART_AMOUNT', 'Минимальная сумма заказа для этого купона: %s');
define('ERROR_INVALID_STARTDATE_COUPON', 'Указанный купон еще недействителен');
define('ERROR_INVALID_FINISDATE_COUPON', 'У данного купона истёк срок действия');
define('ERROR_INVALID_USES_COUPON', 'Купон уже был использован ');
define('TIMES', ' раз.');
define('ERROR_INVALID_USES_USER_COUPON', 'Вы использовали купон максимально возможное количество раз.');
define('TEXT_ENTER_COUPON_CODE', 'Ваш код:&nbsp;&nbsp;');