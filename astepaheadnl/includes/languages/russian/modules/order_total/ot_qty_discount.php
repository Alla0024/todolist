<?php
/*
  $Id: ot_gv.php,v 1.0 2002/04/03 23:09:49 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_QTY_DISCOUNT_TITLE', 'Скидка');
define('MODULE_QTY_DISCOUNT_DESCRIPTION', 'Скидка в зависимости от количества купленного товара.');