<?php
/*
  $Id: webmoney_merchant.php 1778 2008-01-09 23:37:44Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa/Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Description');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');

define("MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE", "Включить/Выключить модуль");
define("MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE", "Статус по умолчанию");
define("MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE", "Merchant account");
define("MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE", "Merchant secret");
define("MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE", "Порядок сортировки");
define("MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE", "Статус оплаты");


define("MODULE_PAYMENT_WAYFORPAY_STATUS_DESC", "True - включить, False - выключить");
define("MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC", "Статус который устанавливается заказу при его создании");
define("MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC", "Merchant account");
define("MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC", "Merchant secret");
define("MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC", "Порядок сортировки модуля в блоке с модулями оплаты");
define("MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC", "Статус который утсанавливается заказу при успешной оплате");
