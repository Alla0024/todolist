<?php
/*
  WebMakers.com Added: loginbox.php
  Originally written by: Aubrey Kilian <aubrey@mycon.co.za>
  Re-written by Linda McGrath osCOMMERCE@WebMakers.com
  http://www.thewebmakerscorner.com

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('BOX_LOGINBOX_HEADING', 'Вход');
define('IMAGE_BUTTON_LOGIN', 'Войти');
define('LOGIN_BOX_ADDRESS_BOOK','Адресная книга');
define('LOGIN_BOX_LOGOFF','Выход');