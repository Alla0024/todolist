<?php
/*
  $Id: print_my_invoice.php,v 6.1 2005/06/05 18:17:59 PopTheTop Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

//// START Edit the following defines to your liking ////

// Footing
define('INVOICE_TEXT_THANK_YOU', 'Спасибо что воспользовались магазином'); // Printed at the bottom of your invoices
define('TEXT_INFO_ORDERS_STATUS_NAME', 'Статус заказа:');

// Product Table Info Headings
define('TABLE_HEADING_PRODUCTS_MODEL', 'Код #'); // Change this to "Model #" or leave it as "SKU #"

//// END Editing the above defines to your liking ////

define('INVOICE_TEXT_INVOICE_NR', 'Заказ #');
define('INVOICE_TEXT_INVOICE_DATE', 'Дата Заказа:');
// Misc Invoice Info
define('INVOICE_TEXT_NUMBER_SIGN', '#');
define('INVOICE_TEXT_DASH', '-');
define('INVOICE_TEXT_COLON', ':');

define('INVOICE_TEXT_INVOICE', 'Счет');
define('INVOICE_TEXT_ORDER', 'Заказ');
define('INVOICE_TEXT_DATE_OF_ORDER', 'Дата');
define('INVOICE_TEXT_DATE_DUE_DATE', 'Дата оплаты');

// Customer Info
define('ENTRY_SOLD_TO', 'Адрес плательщика:');
define('ENTRY_SHIP_TO', 'Адрес доставки:');
define('ENTRY_PAYMENT_METHOD', 'Оплата:');

// Product Table Info Headings
define('TABLE_HEADING_PRODUCTS', 'Товары');
define('TABLE_HEADING_UNIT_PRICE', 'шт.');
define('TABLE_HEADING_TOTAL', 'Всего');

// Order Total Details Info
define('ENTRY_SUB_TOTAL', 'Сумма:');
define('ENTRY_SHIPPING', 'Доставка:');
define('ENTRY_TAX', 'Налог:');
define('ENTRY_TOTAL', 'Всего:');


define('PRINT_HEADER_COMPANY_NAME', 'Название компании:');
define('PRINT_HEADER_ADDRESS', 'Адрес:');
define('PRINT_HEADER_PHONE', 'Телефон:');
define('PRINT_HEADER_EMAIL_ADDRESS', 'Электронное письмо:');
define('PRINT_HEADER_WEBSITE', 'Веб-сайт');
define('PDF_TITLE_TEXT', 'Выставленный счет');
define('INVOICE_CUSTOMER_NUMBER', 'Клиент:');
define('TABLE_HEADING_PRODUCTS_PC', 'Количество');
define('PDF_FOOTER_TEXT', 'Ваш текст в нижнем колонтитуле');
