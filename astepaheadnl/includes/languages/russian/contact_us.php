<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
define('HEADING_TITLE', 'КОНТАКТЫ');
define('HEADING_SUBTITLE', 'Свяжитесь с нами сейчас и наши специалисты ответят нам в ближайшее время!');
define('NAVBAR_TITLE', 'Написать');
define('TEXT_SUCCESS', 'Ваше сообщение успешно отправлено!');
define('EMAIL_SUBJECT', 'Сообщение от ' . STORE_NAME);
define('ENTRY_NAME', 'Ваше имя:');
define('ENTRY_EMAIL', 'E-Mail адрес:');
define('ENTRY_PHONE', 'Ваш телефон:');
define('ENTRY_ENQUIRY', 'Сообщение:');
define('SEND_TO_TEXT', 'Письмо в:');
define('POP_CONTACT_US','Оставьте заявку, и мы Вам перезвоним');