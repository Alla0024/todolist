<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Товар не знайдено!');
define('TEXT_DATE_ADDED', 'Товар був доданий в наш каталог %s');
define('TEXT_DATE_AVAILABLE', '<span>Товар буде в наявності %s</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Товар успішно відкладений!');
define('TEXT_COLOR', 'Колір');
define('TEXT_SHARE', 'Поділіться с друзями');
define('TEXT_REVIEWSES', 'відгуків');
define('TEXT_REVIEWSES2', 'Відгуки');
define('TEXT_DESCRIPTION', 'Опис');
define('TEXT_ATTRIBS', 'Характеристики');
define('TEXT_PAYM_SHIP', 'Оплата і доставка');
define('SHORT_DESCRIPTION', 'Короткий опис');

//home
define('HOME_TEXT_PAYM_SHIP', 'Гарантія');