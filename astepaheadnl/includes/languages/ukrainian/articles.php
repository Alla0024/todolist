<?php
/*
  $Id: articles.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');

if ( ($topic_depth == 'articles') || (isset($_GET['authors_id'])) ) {
  define('HEADING_TITLE', $topics['topics_name']);
  define('TEXT_NO_ARTICLES', 'На даний момент нема статей в даному розділі.');
  define('TEXT_ARTICLES', 'Список статей:');
  define('TEXT_DATE_ADDED', 'Опубліковано:');
  define('TEXT_TOPIC', 'Розділ');
  define('TEXT_BY', 'Автор:');
} elseif ($topic_depth == 'top') {
  define('HEADING_TITLE', 'Всі статті');
  define('TEXT_CURRENT_ARTICLES', '');
  define('TEXT_UPCOMING_ARTICLES', 'Статті, які будуть опубліковані найближчим часом.');
  define('TEXT_NO_ARTICLES', 'Нема доступних статей.');
  define('TEXT_DATE_ADDED', 'Опубліковано:');
  define('TEXT_DATE_EXPECTED', 'Очікується:');
  define('TEXT_TOPIC', 'Розділ:');
  define('TEXT_BY', 'by');
} elseif ($topic_depth == 'nested') {
  define('HEADING_TITLE', 'Статті');
}
  define('TOTAL_ARTICLES', 'Всього статей');
  define('HEADING_TITLE', 'Статті');
