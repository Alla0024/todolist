<?php
/*
  $Id: gv_send.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Надіслати сертифікат');
define('NAVBAR_TITLE', 'Надіслати сертифікат');
define('EMAIL_SUBJECT', 'Повідомлення від Інтернет-магазину');
define('HEADING_TEXT','<br>Щоб відправити сертифікат, Ви повинні заповнити наступну форму.<br>');
define('ENTRY_NAME', 'Ім\'я одержувача:');
define('ENTRY_EMAIL', 'E-Mail адреса одержувача:');
define('ENTRY_MESSAGE', 'Повідомлення:');
define('ENTRY_AMOUNT', 'Сума сертифікату:');
define('ERROR_ENTRY_AMOUNT_CHECK', '&nbsp;&nbsp;<span class="errorText">Невірна сума</span>');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK', '&nbsp;&nbsp;<span class="errorText">Невірна Email адреса</span>');
define('MAIN_MESSAGE', 'Ви вирішили відправити сертифікат на суму %s своєму знайомому %s, його Email адреса: %s<br><br>Одержувач сертифіката отримає наступне повідомлення:<br><br>Поважний %s<br><br>
                        Вам відправлений сертифікат на суму %s, відправник: %s');
define('PERSONAL_MESSAGE', '%s пише:');
define('TEXT_SUCCESS', 'Вітаємо, Ваш сертифікат успішно відправлений');
define('EMAIL_SEPARATOR', '----------------------------------------------------------------------------------------');
define('EMAIL_GV_TEXT_HEADER', 'Вітаємо, Ви отримали сертифікат на суму %s');
define('EMAIL_GV_TEXT_SUBJECT', 'Подарунок від %s');
define('EMAIL_GV_FROM', 'Відправник цього сертифіката - %s');
define('EMAIL_GV_MESSAGE', 'Повідомлення відправника: ');
define('EMAIL_GV_SEND_TO', 'Вітаю, %s');
define('EMAIL_GV_REDEEM', 'Щоб активізувати сертифікат, відкрийте посилання, яке розташоване нижче. Код сертифікату: %s');
define('EMAIL_GV_LINK', 'Натисніть тут, щоб активізувати сертифікат ');
define('EMAIL_GV_FIXED_FOOTER', 'Якщо у Вас виникають проблеми при активації сертифіката за допомогою посилання, зазначеного вище, ' . "\n" .
                                'Ми рекомендуємо вводити код сертифіката при оформленні замовлення, а не через посилання, вказаного вище.' . "\n\n");
define('EMAIL_GV_SHOP_FOOTER', '');