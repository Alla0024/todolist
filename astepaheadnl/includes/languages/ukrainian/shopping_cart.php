<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Вміст кошика');
define('HEADING_TITLE', 'Мій кошик');
define('TABLE_HEADING_REMOVE', 'Видалити');
define('TABLE_HEADING_REMOVE_FROM', 'із кошика');
define('TABLE_HEADING_QUANTITY', 'Кіл-ть');
define('TABLE_HEADING_IMAGE', 'Зображення');
define('TABLE_HEADING_NAME', 'Найменування');
define('TABLE_HEADING_PRICE', 'Ціна');
define('TABLE_HEADING_PRICE_TOTAL', 'Разом');
define('TABLE_HEADING_MODEL', 'Код товару');
define('TABLE_HEADING_PRODUCTS', 'Товари');
define('TABLE_HEADING_TOTAL', 'Вартість');
define('TEXT_CART_EMPTY', 'Ваш кошик порожній!');
define('SUB_TITLE_COUPON', 'Купон');
define('SUB_TITLE_COUPON_SUBMIT', 'Застосувати');
define('SUB_TITLE_COUPON_VALID', 'Вітаємо! <br />Ви ввели вірний купон!');
define('SUB_TITLE_COUPON_INVALID', 'Ви ввели невірний купон!');
define('OUT_OF_STOCK_CANT_CHECKOUT', 'Товари, виділені ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' є на нашому складі в недостатньому для Вашого замовлення кількості. <br> ласка, поміняйте кількість продуктів виділених (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), дякуємо Вам');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Товари, виділені ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' є на нашому складі в недостатньому для Вашого замовлення кількості.<br> Проте, Ви можете купити їх і перевірити кількість наявних для поетапної доставки в процесі виконання Вашого замовлення.');