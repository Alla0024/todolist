<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Пароль');
define('HEADING_TITLE', 'Дозвольте увійти!');

define('TEXT_NEW_CUSTOMER', 'Зареєструватися!');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'Зареєструвавшись в нашому магазині, Ви зможете здійснювати покупки набагато <b>швидше і зручніше</b>, крім того, Ви зможете стежити за виконанням замовлень, дивитися історію своїх замовлень.');
define('TEXT_RETURNING_CUSTOMER', 'Я вже зареєстрований!');
define('TEXT_LOGIN_ERROR', '<span><b>ПОМИЛКА:</b></span> Невірна \'E-Mail Адреса\' та/або \'Пароль\'.');
// guest_account start
define('NO_CUSTOMER', 'Такого користувача нема в базі');
define('NO_PASS', 'Невірний пароль');
define('SOC_VK', 'Вконтакте');
define('SOC_SITE', 'сайту');
define('SOC_ENTER_FROM_OTHER', 'Зайдіть з іншої соц. мережі або під своїм логіном / паролем.');
define('SOC_NOW_FROM', 'Зараз ви намагаєтеся зайти з');
define('SOC_NEED_FROM', 'Ви реєструвалися з');
define('SOC_LOGIN_THX', 'Дякуємо, ви успішно увійшли!');