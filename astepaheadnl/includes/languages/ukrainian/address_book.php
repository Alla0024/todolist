<?php
/*
  $Id: address_book.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Мої адреси');
define('NAVBAR_TITLE_2', 'Адресна книга');
define('HEADING_TITLE', 'Моя адресна книга');
define('PRIMARY_ADDRESS_TITLE', 'Ваш адрес');
define('PRIMARY_ADDRESS_DESCRIPTION', 'Дана адреса встановлена за замовчуванням.<br>Він буде використовуватися при здійсненні покупок та будь-яких інших дій у нашому магазині.');
define('ADDRESS_BOOK_TITLE', 'Записи адресної книги');
define('PRIMARY_ADDRESS', '(*)');
define('TEXT_MAXIMUM_ENTRIES', '<b>ЗАУВАЖЕННЯ:</b> Максимальний обсяг адресної книги - <b>%s</b> записів');
