<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Безготівковий розрахунок');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', 'Наші банківські реквізити:<br><br>Отримувач: &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')  . '<br>ЄДРПОУ/ІПН: &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')  . '<br>Назва банку : &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')  . '<br>Номер рахунку/карти: &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')  . '<br>МФО банку: &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')  . '<br>Призначення платежу : &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')  . '<br><br>Після оплати замовлення обов\'язково повідомте нам по електронній пошті <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS . '</a> про факт оплати. Ваше замовлення буде відправлено одразу після підтвердження факту оплати.<br><br><a href=kvitan.php target=_blank><b><span>Квитанція для сплати</a></b>');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Наші банківські реквізити:\n\nОтримувач: " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')  . "<br>ЄДРПОУ/ІПН: " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')  . "<br>Назва банку : " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')  . "<br>Номер рахунку/карти: " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')  . "<br>МФО банку: " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')  . "\nПризначення платежу : " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')  . "\n\nПісля оплати замовлення обов'язково повідомте нам по електронній пошті " . STORE_OWNER_EMAIL_ADDRESS . " про факт оплати. Ваше замовлення буде відправлено одразу після підтвердження факту оплати.");
