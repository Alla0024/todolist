<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_PUBLIC_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_DESCRIPTION', '<a href="https://www.authorize.net" target="_blank" style="text-decoration: underline; font-weight: bold;">Відвідайте веб-сайт Authorize.net</a>');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_OWNER', 'Власник кредитної картки:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_NUMBER', 'Номер кредитної картки:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_EXPIRES', 'Дата закінчення терміну дії кредитної картки:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_CVC', 'Код підтвердження картки (CVC):');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_TITLE', 'Під час обробки вашої кредитної картки сталася помилка');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_GENERAL', 'Будь ласка, спробуйте ще раз, і якщо проблеми не зникають, спробуйте інший спосіб оплати.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_DECLINED', 'Ця трансакція з кредитною карткою була відхилена. Будь ласка, спробуйте ще раз, і якщо проблеми не зникають, спробуйте іншу кредитну картку або спосіб оплати.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_INVALID_EXP_DATE', 'Дата закінчення терміну дії кредитної картки недійсна. Перевірте інформацію про карту та спробуйте ще раз.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_EXPIRED', 'Термін дії кредитної картки закінчився. Будь ласка, спробуйте ще раз з іншою карткою або способом оплати.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_CVC', 'Номер чекової картки кредитної картки (CVC) недійсний. Перевірте інформацію про карту та спробуйте ще раз.');
?>
