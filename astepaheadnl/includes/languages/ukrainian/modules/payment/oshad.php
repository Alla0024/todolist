<?php
/*
  $Id: webmoney_merchant.php 1778 2008-01-09 23:37:44Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_OSHAD_TEXT_TITLE', 'Oshad');
define('MODULE_PAYMENT_OSHAD_TEXT_PUBLIC_TITLE', 'Oshadbank Visa/Mastercard');
define('MODULE_PAYMENT_OSHAD_TEXT_DESCRIPTION', 'Після натискання кнопки Підтвердити замовлення Ви перейдете на сайт платіжної системи для оплати замовлення, після оплати Ваше замовлення буде виконано.');
define('MODULE_PAYMENT_OSHAD_TEXT_ADMIN_DESCRIPTION', 'модуль оплати Oshad..');