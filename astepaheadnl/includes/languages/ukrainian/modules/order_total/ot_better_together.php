<?php
//
// +----------------------------------------------------------------------+
// | Better Together discount strings                                     |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 That Software Guy                                 |
// +----------------------------------------------------------------------+
// | Released under the GNU General Public License.                       |
// +----------------------------------------------------------------------+
//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE', 'Супутня знижка');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION', 'Супутня знижка');
define('TWOFER_PROMO_STRING', 'Купуйте цей товар та отримаєте інший безкоштовно!');
define('TWOFER_QUALIFY_STRING', "Ви можете отримати інший %s безкоштовно");
define('OFF_STRING_PCT', '-%s');  // e.g. at 50% off
define('OFF_STRING_CURR', '-%s');  // e.g. $20 off
define('SECOND', ' другий ');  // if both same
define('FREE_STRING', ' безкоштовно');  // i.e. amount off
// Reverse defs
define('REV_GET_ANY', 'Купуйте товар з ');
define('REV_GET_THIS', 'Купуйте ');
define('REV_GET_DISC', ', отримаєте цей товар ');
// No context (off product info page)
define('FREE', " безкоштовно");
define('GET_YOUR_PROD', ", отримаєте ");
define('GET_YOUR_CAT', ", отримаєте ваш вибір з ");
