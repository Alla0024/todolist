<?php
/*
  $Id: ot_coupon.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Купон');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Сертифікати / Купони');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Купон');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Невірний код купона');
define('ERROR_MINIMUM_CART_AMOUNT', 'Мінімальна сума замовлення для цього купона становить: %s');
define('ERROR_INVALID_STARTDATE_COUPON', 'Вказаний купон ще не є дійсним');
define('ERROR_INVALID_FINISDATE_COUPON', 'У даного купона закінчився термін дії');
define('ERROR_INVALID_USES_COUPON', 'Купон вже був використаний ');
define('TIMES', ' раз.');
define('ERROR_INVALID_USES_USER_COUPON', 'Ви використовували купон максимально можливу кількість разів.');
define('TEXT_ENTER_COUPON_CODE', 'Ваш код:&nbsp;&nbsp;');