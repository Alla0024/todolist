<?php
/*
  One Page Checkout, Version: 1.08

  I.T. Web Experts
  http://www.itwebexperts.com

  Copyright (c) 2009 I.T. Web Experts

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Оформлення замовлення');
define('NAVBAR_TITLE_1', 'Оформлення замовлення');
define('HEADING_TITLE', 'Оформлення замовлення');
define('TABLE_HEADING_SHIPPING_ADDRESS', 'Адреса доставки');
define('TABLE_HEADING_BILLING_ADDRESS', 'Адреса оплати');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Найменування');
define('TABLE_HEADING_PRODUCTS_PRICE', 'Ціна за шт.');
define('TABLE_HEADING_PRODUCTS', 'Кошик');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Всього');
define('ENTRY_TELEPHONE', 'Телефон');
define('ENTRY_COMMENT', 'Коментар до замовлення');
define('TABLE_HEADING_SHIPPING_METHOD', 'Спосіб доставки');
define('TABLE_HEADING_PAYMENT_METHOD', 'Спосіб оплати');
define('TEXT_CHOOSE_SHIPPING_METHOD', '');
define('TEXT_SELECT_PAYMENT_METHOD', '');
define('TEXT_ERROR_SHIPPING_METHOD', 'Будь-ласка, введіть вашу адресу доставки, щоб побачити <b>%s</b>');
define('TEXT_ENTER_SHIPPING_INFORMATION', 'В даний час це єдиний спосіб доставки, доступний для використання в цьому замовленні.');
define('TEXT_ENTER_PAYMENT_INFORMATION', '');
define('TABLE_HEADING_COMMENTS', 'Коментарі:');
define('TITLE_CONTINUE_CHECKOUT_PROCEDURE', 'Продовжити оформлення замовлення');
define('EMAIL_SUBJECT', 'Раді вітати Вас в інтернет-магазині ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Поваhжна %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Поважний %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Поважний(а) %s <br /><br/>');
define('EMAIL_WELCOME', 'Раді вітати Вас в інтернет-магазині <b>' . STORE_NAME . '</b>. <br /><br/>');
define('EMAIL_TEXT', 'Тепер Ви маєте доступ до деяких додаткових можливостей, які доступні зареєстрованим користувачам:' . "\n\n" . '<li><b>Кошик</b> - лбудь-які продукти, які додаються в кошик, залишаються там, поки Ви не видалите їх або не оформите замовлення.' . "\n" . '<li><b>Адресна книга</b> - тепер ми можемо відправляти свою продукцію на адресу, яку Ви вказали в пункті "Адреса доставки".' . "\n" . '<li><b>Історія замовлень</b> -у Вас є можливість переглядати історію замовлень в нашому магазині.<br><br>');
define('EMAIL_CONTACT', 'Якщо у Вас виникли будь-які питання, пишіть: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '');
// Start - CREDIT CLASS Gift Voucher Contribution
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'As part of our welcome to new customers, we have sent you an e-Gift Voucher worth %s');
define('EMAIL_GV_REDEEM', 'The redeem code for the e-Gift Voucher is %s, you can enter the redeem code when checking out while making a purchase');
define('EMAIL_GV_LINK', 'or by following this link ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Congratulations, to make your first visit to our online shop a more rewarding experience we are sending you an e-Discount Coupon.' . "\n" .
										' Below are details of the Discount Coupon created just for you' . "\n");
define('EMAIL_COUPON_REDEEM', 'To use the coupon enter the redeem code which is %s during checkout while making a purchase');
// End - CREDIT CLASS Gift Voucher Contribution
define('TEXT_PLEASE_SELECT', 'Виберіть');
define('TEXT_PASSWORD_FORGOTTEN', 'Забули пароль?');
define('IMAGE_LOGIN', 'Увійти');
define('TEXT_HAVE_COUPON_KGT', 'Є купон?');
define('TEXT_DIFFERENT_SHIPPING', 'Адреса оплати відрізняється від адреси доставки?');
define('TEXT_DIFFERENT_BILLING', 'Адреса доставки відрізняється від адреси оплати?');
// Points/Rewards Module V2.1rc2a BOF

define('TEXT_MIN_SUM', 'МІНІМАЛЬНА СУМА ЗАМОВЛЕННЯ');
define('TEXT_NEW_CUSTOMER', 'Я новий покупець');
define('TEXT_LOGIN_SOCIAL', 'Увійти через соц. мережі');
define('TEXT_REGISTRATION_OFF', 'Оформити замовлення без реєстрації');


define('TEXT_EMAIL_EXISTS', 'Покупець з такою адресою ел. пошти вже зареєстрований в нашому магазині!');
define('TEXT_EMAIL_EXISTS2', 'Увійдіть');
define('TEXT_EMAIL_EXISTS3', 'з паролем і ми збережемо це замовлення у Вашому особистому кабінеті, або продовжите без підтвердження цієї адреси ел. пошти');
define('TEXT_EMAIL_WRONG', 'ви ввели невірний e-mail.');
define('TEXT_ORDER_PROCESSING', 'Замовлення обробляється, зачекайте...');
define('TEXT_EMAIL_LOGIN', 'Логін');
define('TEXT_EMAIL_PASS', 'Пароль');

define('TEXT_CHANGE_ADDRESS', 'Змінити адресу');
define('ADDRESS_BOOK', 'Адресна книга');
define('BILLING_ADDRESS_THE_SAME', 'Той самий');

define('CH_JS_REFRESH', 'Оновлення');
define('CH_JS_REFRESH_METHOD', 'Оновлення способа');
define('CH_JS_SETTING_METHOD', 'Встановлення метода');
define('CH_JS_SETTING_ADDRESS', 'Встановлення адреси');
define('CH_JS_SETTING_ADDRESS_BIL', 'Оплати');
define('CH_JS_SETTING_ADDRESS_SHIP', 'Доставки');

define('CH_JS_ERROR_SCART', 'У процесі оновлення кошика виникла помилка, будь ласка, проінформуйте');
define('CH_JS_ERROR_SOME1', 'У процесі оновлення');
define('CH_JS_ERROR_SOME2', 'виникла помилка, будь ласка, проінформуйте');

define('CH_JS_ERROR_SET_SOME1', 'Виникла помилка в процесі установки способу');
define('CH_JS_ERROR_SET_SOME2', ', будь ласка, проінформуйте');
define('CH_JS_ERROR_SET_SOME3', 'про цю помилку.');

define('CH_JS_ERROR_REQ_BIL', 'Заповніть, будь ласка, необхідні поля в розділі "Адреса оплати"');
define('CH_JS_ERROR_ERR_BIL', 'Перевірте, будь ласка, коректність введення даних в розділі "Адреса оплати"');
define('CH_JS_ERROR_REQ_SHIP', 'Заповніть, будь ласка, всі необхідні поля в "Адресі доставки"');
define('CH_JS_ERROR_ERR_SHIP', 'Перевірте, будь ласка, коректність введення даних в розділі "Адреса доставки"');
define('CH_JS_ERROR_ADDRESS', 'Помилка адреси');
define('CH_JS_ERROR_PMETHOD', 'Помилка вибору способу оплати');
define('CH_JS_ERROR_SELECT_PMETHOD', 'Ви повинні обрати спосіб оплати.');
define('CH_JS_CHECK_EMAIL', 'Перевірка E-mail адреси');
define('CH_JS_ERROR_EMAIL', 'У процесі перевірки email адреси виникла помилка, будь ласка, проінформуйте');