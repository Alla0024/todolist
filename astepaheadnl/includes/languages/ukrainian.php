<?php
/*
  $Id: russian.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'en_US.ISO_8859-1');  
@setlocale(LC_TIME, 'ua_UA.UTF-8');
define('OG_LOCALE', 'ua_UA');
define('GO_COMPARE', 'В порівнянні');
define('IN_WHISHLIST', 'В бажаних');
define('COMPARE', 'Порівняти');
define('WHISH', 'Бажані');
// HMCS: Begin Autologon   ******************************************************************
// HMCS: End Autologon     ******************************************************************

//define('DATE_FORMAT_SHORT', '%d/%m/%Y');  // this is used for strftime()
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y г.'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT', 'd.m.Y h:i:s'); // this is used for date()
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


define('TEXT_DAY_1','Понеділок');
define('TEXT_DAY_2','Вівторок');
define('TEXT_DAY_3','Середа');
define('TEXT_DAY_4','Четвер');
define('TEXT_DAY_5','П\'ятниця');
define('TEXT_DAY_6','Субота');
define('TEXT_DAY_7','Неділя');
define('TEXT_DAY_SHORT_1','Пн');
define('TEXT_DAY_SHORT_2','Вт');
define('TEXT_DAY_SHORT_3','Ср');
define('TEXT_DAY_SHORT_4','Чт');
define('TEXT_DAY_SHORT_5','Пт');
define('TEXT_DAY_SHORT_6','Сб');
define('TEXT_DAY_SHORT_7','Нед');
define('TEXT_MONTH_BASE_1','Січень');
define('TEXT_MONTH_BASE_2','Лютий');
define('TEXT_MONTH_BASE_3','Березень');
define('TEXT_MONTH_BASE_4','Квітень');
define('TEXT_MONTH_BASE_5','Травень');
define('TEXT_MONTH_BASE_6','Червень');
define('TEXT_MONTH_BASE_7','Липень');
define('TEXT_MONTH_BASE_8','Серпень');
define('TEXT_MONTH_BASE_9','Вересень');
define('TEXT_MONTH_BASE_10','Жовтень');
define('TEXT_MONTH_BASE_11','Листопад');
define('TEXT_MONTH_BASE_12','Грудень');
define('TEXT_MONTH_1','Січня');
define('TEXT_MONTH_2','Лютого');
define('TEXT_MONTH_3','Березня');
define('TEXT_MONTH_4','Квітня');
define('TEXT_MONTH_5','Травня');
define('TEXT_MONTH_6','Червня');
define('TEXT_MONTH_7','Липня');
define('TEXT_MONTH_8','Серпня');
define('TEXT_MONTH_9','Вересня');
define('TEXT_MONTH_10','Жовтня');
define('TEXT_MONTH_11','Листопада');
define('TEXT_MONTH_12','Грудня');

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'UAH');

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="LTR" lang="uk"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Інтернет магазин');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Реєстрація');
define('HEADER_TITLE_CHECKOUT', 'Оформити замовлення');
define('HEADER_TITLE_TOP', 'Головна');
define('HEADER_TITLE_LOGOFF', 'Вихід');

define('HEAD_TITLE_LOGIN', 'Авторизація');
define('HEAD_TITLE_COMPARE', 'Порівняння');
define('HEAD_TITLE_WISHLIST', 'Товари, що сподобалися');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Забутий пароль');

define('HEAD_TITLE_CHECKOUT', 'Оформлення замовлення');
define('HEAD_TITLE_CHECKOUT_SUCCESS', 'Ваше замовлення успішно оформлено!');
define('HEAD_TITLE_ACCOUNT', 'Ваш кабінет');
define('HEAD_TITLE_ACCOUNT_HISTORY', 'Мої замовлення');
define('HEAD_TITLE_ACCOUNT_EDIT', 'Перегляд і редагування моїх даних');
define('HEAD_TITLE_ADDRESS_BOOK', 'Адресна книга');
define('HEAD_TITLE_ACCOUNT_PASSWORD', 'Змінити пароль');
define('HEAD_TITLE_ALLCOMMENTS', 'Всі коментарі');
define('HEAD_TITLE_CONTACT_US','Контакти магазину '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML карта сайту - '.STORE_NAME);
define('HEAD_TITLE_404','Помилка 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Помилка 403 - '.STORE_NAME);


// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Кошик порожній');


//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Повний список товарів');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Виберіть');
define('PULL_DOWN_COUNTRY', 'Область:');
define('TYPE_BELOW', 'Вибір нижче');

// javascript messages
define('JS_ERROR', 'Помилки при заповненні форми!\n\nВиправте будь ласка:\n\n');


define('JS_FIRST_NAME', '* Поле \'Ім\'я\' має містити не менше ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' символів.\n');
define('JS_LAST_NAME', '* Поле \'Прізвище\' має містити не менше ' . ENTRY_LAST_NAME_MIN_LENGTH . ' символів.\n');


define('JS_ERROR_SUBMITTED', 'Ця форма вже заповнена. натискайте Ok.');


define('CATEGORY_COMPANY', 'Організація');
define('CATEGORY_PERSONAL', 'Ваші персональні дані');
define('CATEGORY_ADDRESS', 'Ваша адреса');
define('CATEGORY_CONTACT', 'Контактна інформація');
define('CATEGORY_OPTIONS', 'Розсилка');

define('ENTRY_COMPANY', 'Назва компанії:');
define('ENTRY_COMPANY_ERROR', 'dsfds');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_FIRST_NAME', 'Ім\'я:');
define('ENTRY_TEXT_FOM_IMAGE', 'Введіть текст з картинки');
define('ENTRY_FIRST_NAME_ERROR', 'Поле Ім\'я повинно містити як мінімум ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' символа.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Прізвище:');
define('ENTRY_LAST_NAME_ERROR', 'Поле Прізвище повинно містити як мінімум ' . ENTRY_LAST_NAME_MIN_LENGTH . ' символа.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Дата народження:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Дату народження необхідно вводити в такому форматі: DD/MM/YYYY (наприклад 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (наприклад 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Поле E-Mail повинно містити як мінімум ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' символів.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Ваша E-Mail адреса вказана невірно, спробуйте ще раз.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Введений Вами E-Mail вже зареєстрований в нашому магазині, спробуйте вказати іншу E-Mail адресу.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Адреса:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Поле Вулиця та номер будинку повинно містити як мінімум ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' символів.');
define('ENTRY_STREET_ADDRESS_TEXT', '* Наприклад: вул. Київська 8, офіс. 2');
define('ENTRY_SUBURB', 'Район:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Почтовий індекс:');
define('ENTRY_POST_CODE_ERROR', 'Поле Поштовий індекс має містити як мінімум ' . ENTRY_POSTCODE_MIN_LENGTH . ' символа.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Місто:');
define('ENTRY_CITY_ERROR', 'Поле Місто повинно містити як мінімум ' . ENTRY_CITY_MIN_LENGTH . ' символа.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Область:');
define('ENTRY_STATE_ERROR', 'Поле Область має містити як мінімум ' . ENTRY_STATE_MIN_LENGTH . ' символа.');
define('ENTRY_STATE_ERROR_SELECT', 'Виберіть область.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Країна:');
define('ENTRY_COUNTRY_ERROR', 'Выберіть країну.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Телефон:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Поле Телефон має містити як мінімум ' . ENTRY_TELEPHONE_MIN_LENGTH . ' символа.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Факс:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Отримувати новини');
define('ENTRY_NEWSLETTER_TEXT', 'Отримувати інформацію про знижки, призи, подарунки');
define('ENTRY_NEWSLETTER_YES', 'Підписатися');
define('ENTRY_NEWSLETTER_NO', 'Відмовитися від підписки');
define('ENTRY_PASSWORD', 'Пароль:');
define('ENTRY_PASSWORD_ERROR', 'Ваш пароль повинен містити як мінімум ' . ENTRY_PASSWORD_MIN_LENGTH . ' символів.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Поле Підтвердіть пароль має збігатися з полем Пароль.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Підтвердіть пароль:');
define('ENTRY_PASSWORD_CURRENT', 'Поточний пароль:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Поле Пароль має містити як мінімум ' . ENTRY_PASSWORD_MIN_LENGTH . ' символів.');
define('ENTRY_PASSWORD_NEW', 'Новий пароль:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Ваш Новий пароль має містити як мінімум ' . ENTRY_PASSWORD_MIN_LENGTH . ' символів.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Поля Підтвердіть пароль і Новий пароль повинні збігатися.');

define('FORM_REQUIRED_INFORMATION', '* Обов\'язкове для заповнення');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Сторінки:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Показано <b>%d</b> - <b>%d</b> з <b>%d</b> позицій');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Показано <b>%d</b> - <b>%d</b> (всього <b>%d</b> заказів)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Показано <b>%d</b> - <b>%d</b> (всього <b>%d</b> спеціальних пропозицій)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Показано <b>%d</b> - <b>%d</b> (всього <b>%d</b> рекомендованих товарів)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'попередня');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Наступна сторінка');
define('PREVNEXT_TITLE_PAGE_NO', 'Сторінка %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Попередніх %d сторінок');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Наступних %d сторінок');
define('PREVNEXT_BUTTON_PREV', '<<');
define('PREVNEXT_BUTTON_NEXT', '>>');

define('PREVNEXT_TITLE_PPAGE', 'Сторінка');

define('ART_XSELL_TITLE', 'З теми');
define('PROD_XSELL_TITLE', 'Супутні товари');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Додати адресу');
define('IMAGE_BUTTON_BACK', 'Назад');
define('IMAGE_BUTTON_CHECKOUT', 'Оформити замовлення');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Підтвердити замовлення');
define('IMAGE_BUTTON_CONTINUE', 'Продовжити');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Продовжити покупки');
define('IMAGE_BUTTON_DELETE', 'Видалити');
define('IMAGE_BUTTON_LOGIN', 'Увійти');
define('IMAGE_BUTTON_IN_CART', 'Товар у кошику');
define('IMAGE_BUTTON_ADDTO_CART', 'Купити');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Купити');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'В кошик');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Спробуйте безкоштовно!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Придбайте цей магазин зараз!');
define('MESSAGE_BUY_TEMPLATE1', 'На жаль ваш тестовий 14-денний період закінчено. Будь ласка, ');
define('MESSAGE_BUY_TEMPLATE2', 'купіть');
define('MESSAGE_BUY_TEMPLATE3', ' даний магазин або створіть ще один тестовий сайт.');

define('CUSTOM_PANEL_EDIT', 'Редагувати ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'товар');
define('CUSTOM_PANEL_EDIT_MANUF', 'виробника');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'статтю');
define('CUSTOM_PANEL_EDIT_SECTION', 'розділ');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'категорію');
define('CUSTOM_PANEL_EDIT_SEO', 'сеотекст');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Адмінпанель');
define('CUSTOM_PANEL_ADD', 'Додати');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Товар');
define('CUSTOM_PANEL_ADD_PAGE', 'Сторінка');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Знижка');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Категорія');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Статистика');
define('CUSTOM_PANEL_ADD_ONLINE', 'На сайті: ');
define('CUSTOM_PANEL_PALETTE', 'Палітра сайту');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Колір тексту');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Фон шапки');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Фон підвалу');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Колір посилань');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Колір кнопок');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Колір фону');
define('CUSTOM_PANEL_ORDERS', 'Замовлення');
define('CUSTOM_PANEL_ORDERS_NEW', 'Нові: ');


define('CUSTOM_PANEL_DATE1', 'день');
define('CUSTOM_PANEL_DATE2', 'дня');
define('CUSTOM_PANEL_DATE3', 'днів');

define('IMAGE_BUTTON_UPDATE', 'Оновити');
define('IMAGE_REDEEM_VOUCHER', 'Застосувати');

define('SMALL_IMAGE_BUTTON_VIEW', 'Дивитись');


define('ICON_ERROR', 'Помилка');
define('ICON_SUCCESS', 'Виконано');
define('ICON_WARNING', 'Увага');

define('TEXT_GREETING_PERSONAL', 'Ласкаво просимо, <span class="greetUser">%s!</span> Ви хочете подивитися які <a href="%s"><u>нові товари</u></a> надійшли в наш магазин?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Ласкаво просимо');
define('TEXT_GREETING_GUEST', 'Ласкаво просимо, <span class="greetUser">ШАНОВНИЙ ГІСТЬ!</span><br> Якщо Ви наш постійний клієнт, <a href="%s"><u>введіть Ваші персональні дані</u></a> для входу. Якщо Ви у нас вперше і хочете зробити покупки, Вам необхідно <a href="%s"><u>зареєструватися</u></a>.');

define('TEXT_SORT_PRODUCTS', 'Сортування:');
define('TEXT_DESCENDINGLY', 'за зменшенням');
define('TEXT_ASCENDINGLY', 'за зростанням');
define('TEXT_BY', ', колонка ');

define('TEXT_UNKNOWN_TAX_RATE', 'Невідома податкова ставка');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'Увага: Магазин закритий з технічних причин до: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'Увага: Магазин закритий з технічних причин');

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Попередження: Не видалена директорія установки магазину: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Будь ласка, видаліть цю директорію для безпеки.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Попередження: Файл конфігурації доступний для запису: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. Це - потенційний ризик безпеки - будь ласка, встановіть необхідні права доступу до цього файлу.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Попередження: директорія сесій не існує: ' . tep_session_save_path() . '. Сесії не працюватимуть поки ця директорія не буде створена.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Попередження: Не вдається скористатися послугами каталогу сесій: ' . tep_session_save_path() . '. Сесії не працюватимуть поки не встановлені необхідні права доступу.');
define('WARNING_SESSION_AUTO_START', 'Попередження: опція session.auto_start включена - будь ласка, вимкніть цю опцію у файлі php.ini і перезапустити веб-сервер.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'Ви вказали невірну дату закінчення терміну дії кредитної картки.<br>Спробуйте ще раз.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Ви вказали невірний номер кредитної картки.<br>Спробуйте ще раз.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Перші цифри Вашої кредитної картки: %s<br>Якщо Ви вказали номер своєї кредитної картки правильно, повідомляємо Вам, що ми не приймаємо до оплати даний тип кредитних карток.<br>Якщо Ви вказали номер кредитної картки невірно, спробуйте ще раз.');


define('BOX_LOGINBOX_HEADING', 'Я постійний кліент');
define('IMAGE_BUTTON_LOGIN', 'Увійти');

define('LOGIN_BOX_MY_CABINET', 'Ваш кабінет');
define('MY_ORDERS_VIEW', 'Мої замовлення');
define('MY_ACCOUNT_PASSWORD', 'Змінити пароль');
define('LOGIN_BOX_ADDRESS_BOOK', 'Адресна книга');
define('LOGIN_BOX_LOGOFF', 'Вихід');

define('LOGIN_FROM_SITE', 'Увійти');
define('RENDER_LOGIN_WITH', 'Увійти за допомогою');

define('LOGO_IMAGE_TITLE','Логотип');
define('SORT_BY','Сортувати за');


// VJ Guestbook for OSC v1.0 begin
define('BOX_INFORMATION_GUESTBOOK', 'Гостьова книга');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('GUESTBOOK_TEXT_MIN_LENGTH', '10'); //[TODO] move to config db table
define('JS_GUESTBOOK_TEXT', '* Поле \'Ваше ровідомлення\' має містити як мінімум ' . GUESTBOOK_TEXT_MIN_LENGTH . ' символів.\n');
define('JS_GUESTBOOK_NAME', '* Ви повинні заповнити поле \'Ваше ім\'я\'.\n');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_DISPLAY_NUMBER_OF_GUESTBOOK_ENTRIES', 'Показано <b>%d</b> - <b>%d</b> (всього <b>%d</b> записів)');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('IMAGE_BUTTON_SIGN_GUESTBOOK', 'Додати запис');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_GUESTBOOK_DATE_ADDED', 'Дата: %s');
define('TEXT_NO_GUESTBOOK_ENTRY', 'Поки немає жодного запису в гостьовій книзі. Будьте першими!');
// VJ Guestbook for OSC v1.0 end


// Article Manager
define('BOX_ALL_ARTICLES', 'Усі статті');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', '<font color="#5a5a5a">Показано <b>%d</b> - <b>%d</b> (всього <b>%d</b> новин)</font>');
define('NAVBAR_TITLE_DEFAULT', 'Статті');
define('TEXT_NAVIGATION_BRANDS','Всі виробники');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT', '-');  //define('PRICES_LOGGED_IN_TEXT', '<b><font style="color:#CE1930">Ціна: </b></font><a href="create_account.tpl.php">тільки опт</a>');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO', 'Мінімум одиниць для замовлення: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART', 'Мінімум одиниць для замовлення: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO', 'Шаг: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART', 'Шаг: '); // order_detail.php
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT', '');
define('ERROR_PRODUCTS_QUANTITY_INVALID', 'Ви намагаєтеся покласти в кошик невірну кількість товару: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT', '');
define('ERROR_PRODUCTS_UNITS_INVALID', 'Ви намагаєтеся покласти в кошик невірну кількість товару ');

// Comments 

define('ADD_COMMENT_HEAD_TITLE', 'Залишити свій відгук');

// Poll Box Text
define('_RESULTS', 'Результати');
define('_VOTE', 'Голосувати');
define('_VOTES', 'Голосів:');

define('PREV_NEXT_PRODUCT', 'Товар ');
define('PREV_NEXT_CAT', ' категорії ');
define('PREV_NEXT_MB', ' виробника ');


// Русские названия боксов 

define('BOX_HEADING_CATEGORIES', 'Розділи');
define('BOX_HEADING_INFORMATION', 'Інформація');
define('BOX_HEADING_MANUFACTURERS', 'Виробники');
define('BOX_HEADING_SPECIALS', 'Знижки');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Знижки %s');
define('BOX_HEADING_SEARCH', 'Пошук');
define('BOX_OPEN_SEARCH_FORM', 'Відкрити пошук');
define('BUTTON_SHOPPING_CART_OPEN', 'Відкрити корзину');
define('BOX_HEADING_WHATS_NEW', 'Новинки');
define('BOX_HEADING_FEATURED', 'Рекомендовані');
define('BOX_HEADING_ARTICLES', 'Статті');
define('BOX_HEADING_LINKS', 'Обмін посиланнями');
define('BOX_HEADING_SHOPPING_ENTER', 'Увійти в кошик');
define('HELP_HEADING', 'Консультант');
define('BOX_HEADING_WISHLIST', 'Відкладені товари');
define('BOX_HEADING_BESTSELLERS', 'Лідери продажу');
define('BOX_HEADING_CURRENCY', 'Валюта');
define('_POLLS', 'Опитування');
define('BOX_HEADING_CALLBACK', 'Передзвоніть мені');
define('BOX_HEADING_CONSULTANT', 'Онлайн-консультант');
define('BOX_HEADING_PRODUCTS', 'товарів');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Додайте категорії товарів.');
define('BOX_HEADING_LASTVIEWED', 'Переглянуті товари');

define('BOX_BESTSELLERS_NO', 'В даному розділі поки немає лідерів продажів');

// Способы и стоимость доставки в корзине
define('SHIPPING_OPTIONS', 'Способи і вартість доставки:');


define('LOW_STOCK_TEXT1', 'Товар на складі закінчується: ');
define('LOW_STOCK_TEXT2', 'Код товару: ');
define('LOW_STOCK_TEXT3', 'Поточна кількість: ');
define('LOW_STOCK_TEXT4', 'Посилання на товар: ');
define('LOW_STOCK_TEXT5', 'Поточне значення змінної Ліміт кількості товару на складі: ');

// wishlist box text in includes/boxes/wishlist.php

define('BOX_TEXT_NO_ITEMS', 'Немає відкладених товарів.');


define('TOTAL_TIME', 'Час виконання: ');
define('TOTAL_CART', 'Сума');

// otf 1.71 defines needed for Product Option Type feature.
define('TEXT_PREFIX', 'txt_');
define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES

//define('NAVBAR_TITLE', 'Кошик');
define('SUB_TITLE_FROM', 'Від:');
define('SUB_TITLE_REVIEW', 'Текст повідомлення:');
define('SUB_TITLE_RATING', 'Рейтинг:');

// Product tabs

define('ALSO_PURCHASED', 'Також з цим товаром замовляли');


// Paginator
define('FORWARD', 'Вперед');

define('COMP_PROD_NAME', 'Назва');
define('COMP_PROD_IMG', 'Картинка');
define('COMP_PROD_PRICE', 'Ціна');
define('COMP_PROD_CLEAR', 'Очистити все');
define('COMP_PROD_BACK', 'Повернутися');
define('COMP_PROD_ADD_TO', 'Додайте товари до порівняння!');
define('TEXT_PASSWORD_FORGOTTEN', 'Забули пароль?');
define('TEXT_PASSWORD_FORGOTTEN_DO', 'Відновити пароль');
define('QUICK_ORDER', 'Швидке замовлення');
define('QUICK_ORDER_SUCCESS', 'Заявку відправлено, незабаром з Вами зв\'яжеться менеджер');
define('QUICK_ORDER_BUTTON', 'Купити в один клік');
define('SEND_MESSAGE', 'Відправити');

define('LABEL_NEW', 'Новинка');
define('LABEL_TOP', 'ТОП');
define('LABEL_SPECIAL', 'Акція');

define('FILTER_BRAND', 'Бренд');
define('FILTER_ALL', 'всі');

define('WISHLIST_PC', 'шт.');

define('FOOTER_INFO', 'Інформація');
define('FOOTER_CATEGORIES', 'Розділи');
define('FOOTER_ARTICLES', 'Статті');
define('FOOTER_CONTACTS', 'Контакти');
define('FOOTER_SITEMAP', 'Карта сайту');
define('FOOTER_DEVELOPED', 'Створення інтернет-магазину');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Показати всі категорії');
define('SHOW_ALL_ARTICLES','Показати всі статті');

define('TEXT_HEADING_PRICE', 'Каталог:');
define('TEXT_HEADING_CATALOG', 'Каталог');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Атрибути');
define('TEXT_PRODUCTS_QTY', 'Кількість товарів');

define('MAIN_NEWS', 'Новини');
define('MAIN_NEWS_ALL', 'Усі новини');
define('MAIN_NEWS_SUBSCRIBE', 'Підписатися <span>на новини</span>');
define('MAIN_NEWS_SUBSCRIBE_BUT', 'Підписатися');
define('MAIN_NEWS_EMAIL', 'Ваш email');

define('MAIN_BESTSELLERS', 'ТОП продажів');
define('MAIN_REVIEWS', 'Відгуки покупців');
define('MAIN_REVIEWS_ALL', 'Всі відгуки');
define('MAIN_MOSTVIEWED', 'ТОП переглядів');

define('LIST_TEMP_INSTOCK', 'В наявності');
define('LIST_TEMP_OUTSTOCK', 'Нема в наявності');

define('HIGHSLIDE_CLOSE', 'Закрити');
define('BUTTON_CANCEL', 'Повернутись');
define('BUTTON_SEND', 'Відправити');

define('LISTING_PER_PAGE', 'На сторінку:');
define('LISTING_SORT_NAME', 'За алфавітом, А-Я');
define('LISTING_SORT_PRICE1', 'дешевші зверху');
define('LISTING_SORT_PRICE2', 'дорожчі зверху');
define('LISTING_SORT_NEW', 'нові зверху');
define('LISTING_SORT_POPULAR', 'популярні');
define('LISTING_SORT_LIST', 'списком');
define('LISTING_SORT_COLUMNS', 'колонками');

define('PROD_DRUGIE', 'Схожі товари');
define('PROD_FILTERS', 'Фільтри');

define('PAGE404_TITLE', '404 Помилка');
define('PAGE404_DESC', 'Сторінка не знайдена');
define('PAGE404_REASONS', 'Може бути декілька причин:');
define('PAGE404_REASON1', 'Сторінка переміщена або перейменована');
define('PAGE404_REASON2', 'Сторінки більше не існує на цьому сайті');
define('PAGE404_REASON3', 'URL не відповідає дійсності');
define('PAGE404_BACK1', 'Повернутися назад');
define('PAGE404_BACK2', 'Повернутися на головну');

// 403
define('PAGE403_DESC', 'У вас немає дозволу переглядати цей ресурс.');
define('PAGE403_TITLE','Помилка 403');
define('PAGE403_TO_HOME_PAGE','Повернутися на головну сторінку');

define('SB_SUBSCRIBER', 'Підписник');
define('SB_EMAIL_NAME', 'Заявка на розсилку');
define('SB_EMAIL_USER', 'Користувач');
define('SB_EMAIL_WAS_SUBSCRIBED', 'підписався на розсилку');
define('SB_EMAIL_ALREADY','вже підписаний на розсилку.');
define("LOAD_MORE_BUTTON", "Показати ще");
define("TIME_LEFT", "До кінця пробної версії");

// CLO CONSTANTS
define('CLO_NEW_PRODUCTS', 'Нещодавно добавлені товари');
define('CLO_FEATURED', 'Ці товари вас зацікавлять');
define('CLO_DESCRIPTION_BESTSELLERS', 'ТОП продажів');
define('CLO_DESCRIPTION_SPECIALS', 'Знижки');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Найбільше переглянуті');

define('SHOPRULES_AGREE', 'Реєструючись, ви погоджуєтесь з');
define('SHOPRULES_AGREE2', 'Правилами Магазину');

define("SHOW_ALL_SUBCATS", "переглянути все");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Безкоштовна доставка");

//home
define('HOME_BOX_HEADING_SEARCH', 'пошук...');
define('HEADER_TITLE_OR', 'або');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'додати');
define('HOME_IMAGE_BUTTON_IN_CART', 'В кошику');
define('HOME_ARTICLE_READ_MORE', 'читати більше');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'підпишись на нову розсилку і <span> отримай знижку 25% </span> на один товар в онлайн-замовленні');
define('HOME_MAIN_NEWS_EMAIL', 'Введіть ваш e-mail');
define('HOME_FOOTER_DEVELOPED', 'створення інтернет магазину');
define('HOME_FOOTER_CATEGORIES', 'Каталог');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Додай коментар до товару');
define('HOME_ENTRY_FIRST_NAME_FORM', 'ім\'я');
define('HOME_ADD_COMMENT_FORM', 'Коментар');
define('HOME_REPLY_TO_COMMENT', 'Відповісти на коментар');
define('HOME_PROD_DRUGIE', 'Переглянуті товари');
define("HOME_LOAD_MORE_INFO", "Детальніше");
define("HOME_LOAD_ROLL_UP", "згорнути");
define("HOME_TITLE_LEFT_CATEGORIES", "товари");
define('HOME_BOX_HEADING_SELL_OUT', 'Розпродаж');
define("HOME_LOAD_MORE_BUTTON", "завантажити ще вироби");
define("HOME_BOX_HEADING_WISHLIST", "Улюблені товари");
define("HOME_PROD_VENDOR_CODE", "артикул ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Акції');
define('HOME_FOOTER_SOCIAL_TITLE', 'ми в соціальних мережах:');
define('HOME_HEADER_SOME_LIST', 'Дивіться також');

define('SEARCH_LANG', 'uk/');


define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'Всі результати');
define('ENTER_KEY', 'Введіть текст');
define('TEXT_LIMIT_REACHED', 'Досягнуто максимальну кількість товарів для порівняння: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Товар успішно доданий у кошик!');
define('CHOOSE_ADDRESS', 'Виберіть адресу');

//default2

define('DEMO2_TOP_PHONES', 'Телефони');
define('DEMO2_TOP_CALLBACK', 'Callback');
define('DEMO2_TOP_ONLINE', 'Онлайн-чат');
define('DEMO2_SHOPPING_CART', 'Кошик');
define('DEMO2_LOGIN_WITH', 'Увійти через ');
define('DEMO2_WISHLIST_LINK', 'Список бажань');
define('DEMO2_FOOTER_CATEGORIES', 'Каталог товарів');
define('DEMO2_MY_INFO', 'Змінити дані');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Змінити адресу доставки');
define('DEMO2_LEFT_CAT_TITLE', 'Категорії');
define('DEMO2_LEFT_ALL_GOODS', 'Усі товари');
define('DEMO2_TITLE_SLIDER_LINK', 'Дивитись все');
define('DEMO2_READ_MORE', 'Читати далі <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Згорнути <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Усі категорії');
define('DEMO2_SHOW_ALL_ARTICLES','Всі статті');
define('DEMO2_BTN_COME_BACK','Повернутися назад');
define('DEMO2_SHARE_TEXT','Поділитися');
define('DEMO2_QUICK_ORDER_BUTTON', 'В один клік');















