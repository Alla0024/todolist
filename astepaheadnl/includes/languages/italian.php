<?php
/*
  $Id: russian.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'en_US.ISO_8859-1');  
@setlocale(LC_TIME, 'it_it.UTF-8');
define('OG_LOCALE', 'it_it');
define('GO_COMPARE', 'a confronto');
define('IN_WHISHLIST', 'nei desideri');
define('COMPARE', 'Confrontare');
define('WHISH', 'Desideri');

// HMCS: Begin Autologon   ******************************************************************
// HMCS: End Autologon     ******************************************************************
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // DATE_FORMAT_SHORTthis is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y г.'); // this is used for strftime()
//define('DATE_FORMAT_SHORT', '%d/%m/%Y');
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
define('DATE_FORMAT', DEFAULT_DATE_FORMAT); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
define('TEXT_DAY_1','Lunedì');
define('TEXT_DAY_2','Martedì');
define('TEXT_DAY_3','Mercoledì');
define('TEXT_DAY_4','Giovedì');
define('TEXT_DAY_5','Venerdì');
define('TEXT_DAY_6','Sabato');
define('TEXT_DAY_7','Domenica');
define('TEXT_DAY_SHORT_1','Lun.');
define('TEXT_DAY_SHORT_2','Mar.');
define('TEXT_DAY_SHORT_3','Mer.');
define('TEXT_DAY_SHORT_4','Gio.');
define('TEXT_DAY_SHORT_5','Ven.');
define('TEXT_DAY_SHORT_6','Sab.');
define('TEXT_DAY_SHORT_7','Dom.');
define('TEXT_MONTH_BASE_1','Gennaio');
define('TEXT_MONTH_BASE_2','Febbraio');
define('TEXT_MONTH_BASE_3','Marzo');
define('TEXT_MONTH_BASE_4','Aprile');
define('TEXT_MONTH_BASE_5','Maggio');
define('TEXT_MONTH_BASE_6','Giugno');
define('TEXT_MONTH_BASE_7','Luglio');
define('TEXT_MONTH_BASE_8','Agosto');
define('TEXT_MONTH_BASE_9','Settembre');
define('TEXT_MONTH_BASE_10','Ottobre');
define('TEXT_MONTH_BASE_11','Novembre');
define('TEXT_MONTH_BASE_12','Dicembre');
define('TEXT_MONTH_1','Gennaio');
define('TEXT_MONTH_2','Febbraio');
define('TEXT_MONTH_3','Marzo');
define('TEXT_MONTH_4','Aprile');
define('TEXT_MONTH_5','Maggio');
define('TEXT_MONTH_6','Giugno');
define('TEXT_MONTH_7','Luglio');
define('TEXT_MONTH_8','Agosto');
define('TEXT_MONTH_9','Settembre');
define('TEXT_MONTH_10','Ottobre');
define('TEXT_MONTH_11','Novembre');
define('TEXT_MONTH_12','Dicembre');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY','EUR');
define('HTML_PARAMS','dir=\"LTR\" lang=\"it\"');
define('CHARSET','utf-8');

// page title
define('TITLE', 'Negozio internet');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Registrati');
define('HEADER_TITLE_CHECKOUT', 'Procedi all’ordine');
define('HEADER_TITLE_TOP', 'Home');
define('HEADER_TITLE_LOGOFF', 'Esci');

define('HEAD_TITLE_LOGIN', 'Accedi');
define('HEAD_TITLE_COMPARE', 'Confronta');
define('HEAD_TITLE_WISHLIST', 'Lista dei desideri');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Password dimenticata');

define('HEAD_TITLE_CHECKOUT', 'Effettua l’ordine');
define('HEAD_TITLE_CHECKOUT_SUCCESS', 'Il tuo ordine è stato effettuato con successo!');
define('HEAD_TITLE_ACCOUNT', 'Il mio account');
define('HEAD_TITLE_ACCOUNT_HISTORY', 'I miei ordini');
define('HEAD_TITLE_ACCOUNT_EDIT', 'Visualizza/modifica i tuoi dati');
define('HEAD_TITLE_ADDRESS_BOOK', 'Indirizzo');
define('HEAD_TITLE_ACCOUNT_PASSWORD', 'Cambiare password');
define('HEAD_TITLE_ALLCOMMENTS', 'Commenti');
define('HEAD_TITLE_CONTACT_US','Contatti del negozio '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML informazione del sito - '.STORE_NAME);
define('HEAD_TITLE_404','Errore 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Errore 403 - '.STORE_NAME);

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Carrello vuoto');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Lista degli ordini');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Scegli');
define('PULL_DOWN_COUNTRY', 'Regione:');
define('TYPE_BELOW', 'Scegli');

// javascript messages
define('JS_ERROR', 'Errore inserimento dati!\n\nRiprova:\n\n\n');


define('JS_FIRST_NAME', '*Vuoto  \'Nome\' Non deve contenere meno di ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' simboli.\n');
define('JS_LAST_NAME', '* Vuoto \'Cognome\' non deve contenere meno di \' . ENTRY_LAST_NAME_MIN_LENGTH . \' simboli.\\n\\n');


define('JS_ERROR_SUBMITTED', 'Questo modulo è già compilato. Clicca OK.');


define('CATEGORY_COMPANY', 'Azienda');
define('CATEGORY_PERSONAL', 'Dati personali');
define('CATEGORY_ADDRESS', 'Indirizzo');
define('CATEGORY_CONTACT', 'Contatti');
define('CATEGORY_OPTIONS', 'Opzioni');

define('ENTRY_COMPANY', 'Nome dell’azienda :');
define('ENTRY_COMPANY_ERROR', 'dsfds');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_FIRST_NAME', 'Nome:');
define('ENTRY_TEXT_FOM_IMAGE', 'Inserisci testo dall’immagine ');
define('ENTRY_FIRST_NAME_ERROR','La casella ‘Nome’ non deve contenere meno di' . ENTRY_FIRST_NAME_MIN_LENGTH . ' simboli.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Cognome:');
define('ENTRY_LAST_NAME_ERROR', 'La casella ‘Cognome’ non deve contenere meno di ' . ENTRY_LAST_NAME_MIN_LENGTH . ' simboli.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Data di nascita:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'La data di nascita deve essere scritta come nell’esempio: DD/MM/YYYY (esempio 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (esempio 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'La casella ‘e-mail’ non deve contenere meno di ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' simboli.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'La tua e-mail non è corretta, riprova.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Questa e-mail è già presente nel nostro sito, riprova con un’ altra e-mail.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Indirizzo:');
define('ENTRY_STREET_ADDRESS_ERROR', 'La casella ‘indirizzo’ non deve contenere meno di ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' simboli.');
define('ENTRY_STREET_ADDRESS_TEXT', '* Esempio: via Pogliano, 16');
define('ENTRY_SUBURB', 'Provincia:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'CAP:');
define('ENTRY_POST_CODE_ERROR', 'La casella ‘CAP’ non deve contenere meno di ' . ENTRY_POSTCODE_MIN_LENGTH . ' simboli.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Città:');
define('ENTRY_CITY_ERROR', 'La casella ‘città’ non deve contenere meno di' . ENTRY_CITY_MIN_LENGTH . ' simboli.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Regione:');
define('ENTRY_STATE_ERROR', 'La casella ‘regione’ non deve contenere meno di ' . ENTRY_STATE_MIN_LENGTH . ' simboli.');
define('ENTRY_STATE_ERROR_SELECT', 'Scegli regione.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Paese:');
define('ENTRY_COUNTRY_ERROR', 'scegli paese');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Telefono :');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'La casella ‘telefono’ non deve contenere meno di ' . ENTRY_TELEPHONE_MIN_LENGTH . ' simboli.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Fax:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Ottieni le novità ');
define('ENTRY_NEWSLETTER_TEXT', 'Ottieni informazione in caso di sconti, premi, regali:');
define('ENTRY_NEWSLETTER_YES', 'Iscriviti');
define('ENTRY_NEWSLETTER_NO', 'Annulla l’iscrizione ');
define('ENTRY_PASSWORD', 'Password:');
define('ENTRY_PASSWORD_ERROR', 'La tua password non deve contenere meno di ' . ENTRY_PASSWORD_MIN_LENGTH . ' simboli.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'La ‘casella conferma’ password deve combaciare con la password.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Conferma password:');
define('ENTRY_PASSWORD_CURRENT', 'Password recente:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'La casella ‘password’ non deve contenere meno di' . ENTRY_PASSWORD_MIN_LENGTH . ' simboli.');
define('ENTRY_PASSWORD_NEW', 'Nuova password:');
define('ENTRY_PASSWORD_NEW_ERROR', 'La tua nuova password non deve contenere meno di ' . ENTRY_PASSWORD_MIN_LENGTH . ' simboli.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Le caselle ‘nuova password’ e ‘conferma password’ devono combaciare.');

define('FORM_REQUIRED_INFORMATION', '* Informazione necessaria');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Pagina:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Disponibili <b>%d</b> - <b>%d</b> tra <b>%d</b> posizioni');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Disponibili <b>%d</b> - <b>%d</b> (in tutto <b>%d</b> acquisti)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Disponibili <b>%d</b> - <b>%d</b> (in tutto <b>%d</b> offerte speciali)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Disponibili <b>%d</b> - <b>%d</b> (in tutto <b>%d</b> prodotti consigliati)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'precedente');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Pagina successiva');
define('PREVNEXT_TITLE_PAGE_NO', 'Pagina %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Pagine precedenti %d');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Pagine successive %d');
define('PREVNEXT_BUTTON_PREV', '<<');
define('PREVNEXT_BUTTON_NEXT', '>>');
define('TEXT_CLOSE_BUTTON', 'Close');

define('PREVNEXT_TITLE_PPAGE', 'Pagina');

define('ART_XSELL_TITLE', 'Guarda anche');
define('PROD_XSELL_TITLE', 'Prodotti attuali');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Aggiungi indirizzo');
define('IMAGE_BUTTON_BACK', 'Indietro');
define('IMAGE_BUTTON_CHECKOUT', 'effettua ordine');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Conferma l’ordine ');
define('IMAGE_BUTTON_CONTINUE', 'Continua');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continua lo shopping');
define('IMAGE_BUTTON_DELETE', 'Elimina');
define('IMAGE_BUTTON_LOGIN', 'Entra');
define('IMAGE_BUTTON_IN_CART', 'Nel carrello');
define('IMAGE_BUTTON_ADDTO_CART', 'Compra');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Aggiungi nel carrello');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'Nel carrello');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Provalo gratis!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Comparare questo negozio!');
define('MESSAGE_BUY_TEMPLATE1', 'Spiacenti, la prova gratuita  di 14 giorni è terminata ');
define('MESSAGE_BUY_TEMPLATE2', 'compra');
define('MESSAGE_BUY_TEMPLATE3', ' negozio attuale oppure aggiungete un’ altra prova.');

define('CUSTOM_PANEL_EDIT', 'Modifica ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'prodotto');
define('CUSTOM_PANEL_EDIT_MANUF', 'produttore');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'articolo ');
define('CUSTOM_PANEL_EDIT_SECTION', 'sezione');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'categoria');
define('CUSTOM_PANEL_EDIT_SEO', 'SEO testo');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Admin login');
define('CUSTOM_PANEL_ADD', 'Aggiungi');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Prodotto');
define('CUSTOM_PANEL_ADD_PAGE', 'pagina');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Sconto');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Categoria');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statistica');
define('CUSTOM_PANEL_ADD_ONLINE', 'Sul sito: ');
define('CUSTOM_PANEL_PALETTE', 'Palette del sito');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Colore del testo');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Sfondo');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Sfondo');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Colore del link');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Colore dei tasti');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Colore dello sfondo');
define('CUSTOM_PANEL_ORDERS', 'ordini');
define('CUSTOM_PANEL_ORDERS_NEW', 'Nuovi: ');


define('CUSTOM_PANEL_DATE1', 'giorno');
define('CUSTOM_PANEL_DATE2', 'dei giorni');
define('CUSTOM_PANEL_DATE3', 'giorni ');


define('IMAGE_BUTTON_UPDATE', 'Aggiorna');
define('IMAGE_REDEEM_VOUCHER', 'Applica');

define('SMALL_IMAGE_BUTTON_VIEW', 'Guada');


define('ICON_ERROR', 'Errore');
define('ICON_SUCCESS', 'Eseguito');
define('ICON_WARNING', 'Attenzione');

define('TEXT_GREETING_PERSONAL', 'Benvenuto, <span class="greetUser">%s!</span> Volete vedere <a href="%s"><u>i nuovi prodotti</u></a> arrivati nel negozio?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Benvenuto');
define('TEXT_GREETING_GUEST', 'Benvenuto, <span class="greetUser">GENTILE OSPITE!</span><br> se sei un nostro cliente , <a href="%s"><u>inserisci i tuoi dati</u></a> per entrare. Se è la prima volta che fate acquisti,  <a href="%s"><u>registrati </u></a>.');

define('TEXT_SORT_PRODUCTS', 'ordine:');
define('TEXT_DESCENDINGLY', 'discendente');
define('TEXT_ASCENDINGLY', 'crescente');
define('TEXT_BY', ', colonna');

define('TEXT_UNKNOWN_TAX_RATE', 'Quota fiscale sconosciuta');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'Attenzione: il negozio è chiuso per problemi tecnici fino al: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'Attenzione: il negozio è chiuso per problemi tecnici');
define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Preavviso: L’ elenco del negozio non è stato eliminato : ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Elimina l’elenco per la tua sicurezza.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Preavviso:il file per la configurazione è disponibile per la registrazione: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. È un potenziale rischio di sicurezza – per favore attiva la configurazione a questo file.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Preavviso: l’ elenco delle sezioni è inesistente : ' . tep_session_save_path() . '. Le sezioni non funzioneranno finché l’ elenco non sarà stato creato.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Preavviso : Nessun accesso al catalogo delle sezioni: ' . tep_session_save_path() . '. Le sezioni non funzioneranno finché non saranno installati i diritti di accesso.');
define('WARNING_SESSION_AUTO_START', 'Preavviso: l’opzione session.auto_start è attivata – per cortesia disattiva questa opzione nel file php.ini e riaggiorna la pagina.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'La data di scadenza della carta è errata.<br>Riprova.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Non hai inserito il numero giusto della carta di credito.<br>Riprova.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Le prime cifre della carta di credito: %s<br>Se hai inserito correttamente il numero della tua carta di credito, ti informiamo , che non accettiamo questo tipo di carta di credito.<br>Se il numero è sbagliato, riprova.');


define('BOX_LOGINBOX_HEADING', 'Esci');
define('IMAGE_BUTTON_LOGIN', 'Entra');
define('RENDER_LOGIN_WITH', 'entra con l’aiuto ');

define('LOGIN_BOX_MY_CABINET', 'Il mio account ');
define('MY_ORDERS_VIEW', 'I miei ordini');
define('MY_ACCOUNT_PASSWORD', 'Cambia password');
define('LOGIN_BOX_ADDRESS_BOOK', 'Rubrica indirizzi ');
define('LOGIN_BOX_LOGOFF', 'Esci');

define('LOGIN_FROM_SITE', 'Entra');

define('SORT_BY','Sorta per');
define('LOGO_IMAGE_TITLE','Logo');

// VJ Guestbook for OSC v1.0 begin
define('BOX_INFORMATION_GUESTBOOK', 'Lista degli ospiti');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('GUESTBOOK_TEXT_MIN_LENGTH', '10'); //[TODO] move to config db table
define('JS_GUESTBOOK_TEXT', '* Casella \'Il tuo messaggio\' non deve contenere più di' . GUESTBOOK_TEXT_MIN_LENGTH . ' simboli.\n');
define('JS_GUESTBOOK_NAME', '* Riempi la casella \'Nome\'.\n');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_DISPLAY_NUMBER_OF_GUESTBOOK_ENTRIES', 'Visualizzato <b>%d</b> - <b>%d</b> (totale <b>%d</b> записей)');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('IMAGE_BUTTON_SIGN_GUESTBOOK', 'Aggiungi registrazione');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_GUESTBOOK_DATE_ADDED', 'Data: %s');
define('TEXT_NO_GUESTBOOK_ENTRY', 'Non ci sono ancora ospiti nell’ ordine. Diventa il primo!');
// VJ Guestbook for OSC v1.0 end


// Article Manager
define('BOX_ALL_ARTICLES', 'Tutti gli articoli');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', '<font color="#5a5a5a">Visualizzato <b>%d</b> - <b>%d</b> (всего <b>%d</b> новостей)</font>');
define('NAVBAR_TITLE_DEFAULT', 'Articoli');
define('TEXT_NAVIGATION_BRANDS','Tutti i produttori');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT', '-'); //define('PRICES_LOGGED_IN_TEXT', '<b><font style="color:#CE1930">Prezzo: </b></font><a href="create_account.tpl.php">solo all’ingrosso </a>');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO', 'Quantità minore dell’ordine : '); // order_detail.php
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART', 'quantità minore dell’ordine : '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO', 'Passo: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART', 'passo: '); // order_detail.php
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT', '');
define('ERROR_PRODUCTS_QUANTITY_INVALID', 'Hai raggiunto la quantità massima del prodotto: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT', '');
define('ERROR_PRODUCTS_UNITS_INVALID', 'Hai raggiunto la quantità massima del prodotto: ');

// Comments 

define('ADD_COMMENT_HEAD_TITLE', 'Lascia una valutazione');

// Poll Box Text
define('_RESULTS', 'Risultati');
define('_VOTE', 'Valutare');
define('_VOTES', 'Voto:');

define('PREV_NEXT_PRODUCT', 'Prodotto ');
define('PREV_NEXT_CAT', ' Categorie ');
define('PREV_NEXT_MB', ' Produttore ');


// Русские названия боксов 

define('BOX_HEADING_CATEGORIES', 'Sezioni');
define('BOX_HEADING_INFORMATION', 'Informazioni');
define('BOX_HEADING_MANUFACTURERS', 'Produttori');
define('BOX_HEADING_SPECIALS', 'Sconti');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Sconti %s');
define('BOX_HEADING_SEARCH', 'Cerca');
define('BOX_OPEN_SEARCH_FORM', 'Apri la ricerca');
define('BOX_HEADING_WHATS_NEW', 'Nuovi');
define('BOX_HEADING_FEATURED', 'Per te');
define('BOX_HEADING_ARTICLES', 'Articoli');
define('BOX_HEADING_LINKS', 'Cambio link');
define('BOX_HEADING_SHOPPING_ENTER', 'Entra nel carrello’');
define('BUTTON_SHOPPING_CART_OPEN', 'Apri il carrello');
define('HELP_HEADING', 'Consulente');
define('BOX_HEADING_WISHLIST', 'Prodotti in sospeso');
define('BOX_HEADING_BESTSELLERS', 'Leader dei prodotti');
define('BOX_HEADING_CURRENCY', 'Valuta');
define('_POLLS', 'Domande');
define('BOX_HEADING_CALLBACK', 'Richiamami');
define('BOX_HEADING_CONSULTANT', 'Consulente online');
define('BOX_HEADING_PRODUCTS', 'dei prodotti');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Aggiungi una categoria dei prodotti .');
define('BOX_HEADING_LASTVIEWED', 'Prodotti già visti');

define('BOX_BESTSELLERS_NO', 'In questa sezione non ci sono i prodotti più venduti');

// I metodi e il prezzo della spedizione sono nel carrello
define('SHIPPING_OPTIONS', 'Metodi e prezzi della spedizione:');


define('LOW_STOCK_TEXT1', 'il prodotto sta per finire: ');
define('LOW_STOCK_TEXT2', 'Codice del prodotto : ');
define('LOW_STOCK_TEXT3', 'Quantità attuale: ');
define('LOW_STOCK_TEXT4', 'Link sul prodotto: ');
define('LOW_STOCK_TEXT5', 'Numero dei prodotti in magazzino: ');

// wishlist box text in includes/boxes/wishlist.php

define('BOX_TEXT_NO_ITEMS', 'Nessun articolo in sospeso.');

define('TOTAL_CART', 'Totale');
define('TOTAL_TIME', 'Tempo di esecuzione: ');

// otf 1.71 defines needed for Product Option Type feature.
define('TEXT_PREFIX', 'txt_');
define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES

//define('NAVBAR_TITLE', 'Carrello');
define('SUB_TITLE_FROM', 'Indietro:');
define('SUB_TITLE_REVIEW', 'Messaggio:');
define('SUB_TITLE_RATING', 'Valutazione:');

// Product tabs

define('ALSO_PURCHASED', 'Con questo prodotto acquistano anche');


// Paginator
define('FORWARD', 'Avanti');
define('COMP_PROD_NAME', 'Nome');
define('COMP_PROD_IMG', 'Immagine');
define('COMP_PROD_PRICE', 'Prezzo');
define('COMP_PROD_CLEAR', 'Elimina tutto ');
define('COMP_PROD_BACK', 'Torna indietro');
define('COMP_PROD_ADD_TO', 'Aggiungi gli articoli!');
define('TEXT_PASSWORD_FORGOTTEN', 'Hai dimenticato la password?');
define('TEXT_PASSWORD_FORGOTTEN_DO', 'Ripristina password');
define('QUICK_ORDER', 'Ordine veloce');
define('QUICK_ORDER_SUCCESS', 'Il messaggio è stato mandato, tra poco il manager ti contatterà');
define('QUICK_ORDER_BUTTON', 'Compra in un click');
define('SEND_MESSAGE', 'Manda');

define('LABEL_NEW', 'Nuovi arrivi');
define('LABEL_TOP', 'HOT');
define('LABEL_SPECIAL', 'Promozione');

define('FILTER_BRAND', 'Brand');
define('FILTER_ALL', 'Tutti');
define('WISHLIST_PC', 'pz.');

define('FOOTER_INFO', 'Informazioni');
define('FOOTER_CATEGORIES', 'Sezioni');
define('FOOTER_ARTICLES', 'Articoli');
define('FOOTER_CONTACTS', 'Contatti');
define('FOOTER_SITEMAP', 'Mappa del sito');
define('FOOTER_DEVELOPED', 'Apertura del negozio internet');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Guarda tutte le categorie');
define('SHOW_ALL_ARTICLES','Guarda tutti gli articoli');

define('TEXT_HEADING_PRICE', 'Catalogo:');
define('TEXT_HEADING_CATALOG', 'Catalogo');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Attributi');
define('TEXT_PRODUCTS_QTY', 'Quantità dei prodotti ');

define('MAIN_NEWS', 'Novità');
define('MAIN_NEWS_ALL', 'tutte le novità ');
define('MAIN_NEWS_SUBSCRIBE', 'Iscriviti <span> alle novità </span>');
define('MAIN_NEWS_SUBSCRIBE_BUT', 'Segui');
define('MAIN_NEWS_EMAIL', 'E-mail');

define('MAIN_BESTSELLERS', 'più venduti');
define('MAIN_REVIEWS', 'Recensioni dei clienti');
define('MAIN_REVIEWS_ALL', 'Recensioni');
define('MAIN_MOSTVIEWED', 'Più visti');

define('LIST_TEMP_INSTOCK', 'Presente');
define('LIST_TEMP_OUTSTOCK', 'Esaurito');

define('HIGHSLIDE_CLOSE', 'Chiudi');
define('BUTTON_CANCEL', 'Torna');
define('BUTTON_SEND', 'Manda');

define('LISTING_PER_PAGE', 'Sulla pagina:');
define('LISTING_SORT_NAME', 'in ordine alfabetico, A-Z');
define('LISTING_SORT_PRICE1', 'più economici');
define('LISTING_SORT_PRICE2', 'più costosi');
define('LISTING_SORT_NEW', 'più nuovi');
define('LISTING_SORT_POPULAR', 'popolari');
define('LISTING_SORT_LIST', 'lista');
define('LISTING_SORT_COLUMNS', 'colonne');

define('PROD_DRUGIE', 'Prodotti simili');
define('PROD_FILTERS', 'Filtri');

define('PAGE404_TITLE', '404 Errore');
define('PAGE404_DESC', 'Pagina non valida');
define('PAGE404_REASONS', 'Le ragioni possono essere le seguenti:');
define('PAGE404_REASON1', 'La pagina ha modificato il nome');
define('PAGE404_REASON2', 'La pagina non esiste più sul sito');
define('PAGE404_REASON3', 'URL non valido');
define('PAGE404_BACK1', 'Tornare indietro');
define('PAGE404_BACK2', 'tornare all’ home');

// 403
define('PAGE403_DESC', 'Non è permessa la visualizzazione di questa pagina.');
define('PAGE403_TITLE','Errore 403');
define('PAGE403_TO_HOME_PAGE','Tornare all’ home');

define('SB_SUBSCRIBER', 'Iscritto');
define('SB_EMAIL_NAME', 'Richiesta al link');
define('SB_EMAIL_USER', 'Utente');
define('SB_EMAIL_WAS_SUBSCRIBED', 'iscriversi al link');
define('SB_EMAIL_ALREADY','già iscritto al link');
define("LOAD_MORE_BUTTON", "Guarda ancora");
define("TIME_LEFT", "Fine della prova gratuita");

define('CLO_NEW_PRODUCTS', 'Aggiunti di recente');
define('CLO_FEATURED', 'Questi prodotti potrebbero interessarti');
define('CLO_DESCRIPTION_BESTSELLERS', 'Più venduti');
define('CLO_DESCRIPTION_SPECIALS', 'Sconti');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Più visti');

define('SHOPRULES_AGREE', 'Con la registrazione accetti i termini');
define('SHOPRULES_AGREE2', 'condizioni d’ uso del negozio');

define("SHOW_ALL_SUBCATS", "guarda tutto");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Spedizione gratuita");


//home
define('HOME_BOX_HEADING_SEARCH', 'cerca...');
define('HEADER_TITLE_OR', 'oppure');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'aggiungi');
define('HOME_IMAGE_BUTTON_IN_CART', 'nel carrello');
define('HOME_ARTICLE_READ_MORE', 'leggi di più');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'iscriviti <span>e ottieni uno sconto di 25%</span> su un prodotto');
define('HOME_MAIN_NEWS_EMAIL', 'E-Mail ');
define('HOME_FOOTER_DEVELOPED', 'crea un negozio internet');
define('HOME_FOOTER_CATEGORIES', 'Catalogo');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Aggiungi un commento al prodotto');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Nome');
define('HOME_ADD_COMMENT_FORM', 'Commento');
define('HOME_REPLY_TO_COMMENT', 'Rispondi al commento');
define('HOME_PROD_DRUGIE', 'Visti recentemente');
define("HOME_LOAD_MORE_INFO", "maggiori dettagli");
define("HOME_LOAD_ROLL_UP", "Indietro");
define("HOME_TITLE_LEFT_CATEGORIES", "prodotti");
define('HOME_BOX_HEADING_SELL_OUT', 'Saldi');
define("HOME_LOAD_MORE_BUTTON", "carica ancora");
define("HOME_BOX_HEADING_WISHLIST", "Prodotti preferiti");
define("HOME_PROD_VENDOR_CODE", "articolo ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Sconti');
define('HOME_FOOTER_SOCIAL_TITLE', 'I nostri social:');
define('HOME_HEADER_SOME_LIST', 'Guarda anche');


define('VK_LOGIN', 'Log-in');
define('SHOW_RESULTS', 'Tutti i risultati');
define('ENTER_KEY', 'Testo');
define('TEXT_LIMIT_REACHED', 'Raggiunto numero massimo di prodotti messi a confronto: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Il prodotto è stato aggiunto al carrello con successo!');
define('CHOOSE_ADDRESS', 'Scegli indirizzo');

//default2

define('DEMO2_TOP_PHONES', 'Telefoni');
define('DEMO2_TOP_CALLBACK', 'Callback');
define('DEMO2_TOP_ONLINE', 'Chat');
define('DEMO2_SHOPPING_CART', 'Carrello');
define('DEMO2_LOGIN_WITH', 'Entra con ');
define('DEMO2_WISHLIST_LINK', 'Lista dei desideri');
define('DEMO2_FOOTER_CATEGORIES', 'Lista dei prodotti');
define('DEMO2_MY_INFO', 'Cambia i dati ');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Cambia indirizzo');
define('DEMO2_TEXT_CART_EMPTY', '<i class="fa fa-shopping-cart" aria-hidden="true"></i>Aggiungi almeno un articolo');
define('DEMO2_LEFT_CAT_TITLE', 'Categorie');
define('DEMO2_LEFT_ALL_GOODS', 'Prodotti');
define('DEMO2_TITLE_SLIDER_LINK', 'Tutti');
define('DEMO2_READ_MORE', 'Continua a leggere <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Indietro <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Categoria');
define('DEMO2_SHOW_ALL_ARTICLES','Tutti gli articoli');
define('DEMO2_BTN_COME_BACK','Torna indietro');
define('DEMO2_SHARE_TEXT','Condividi');
define('DEMO2_QUICK_ORDER_BUTTON', 'Un click!');


define('SEARCH_LANG', 'it/');

?>