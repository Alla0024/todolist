<?php
/*
  $Id: account.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'My account');
define('HEADING_TITLE', 'My account');


define('MY_ACCOUNT_TITLE', 'My account');

define('MY_ACCOUNT_PASSWORD', 'Change Password');

define('MY_ORDERS_VIEW', 'My orders');

define('MY_NAVI', 'Menu');
define('MY_INFO', 'My details');
define('EDIT_ADDRESS_BOOK', 'Address details');
