<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Checkout');
define('NAVBAR_TITLE_2', 'Successful');
define('HEADING_TITLE', 'Your order has been successfully processed!');
define('TEXT_SUCCESS', 'Your order has been successfully processed!');
define('TEXT_THANKS_FOR_SHOPPING', 'Thanks for shopping with us online!');
define('TABLE_HEADING_COMMENTS', 'We shall contact you soon');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Expiry date: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' Downloads remaining');
define('SMS_NEW_ORDER', 'You have new order');
define('TEXT_SUCCESS_INFO', 'All details about the payment have been sent to you by email');