<?php
/*
  $Id $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Log out');
define('NAVBAR_TITLE', 'Log out');
define('TEXT_MAIN', 'You are logged out of your account.<br><br>Your shopping cart has been saved, the items inside it will be restored whenever you log back into your account.');
?>
