<?php
/*
  $Id: account_history.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My account');
define('NAVBAR_TITLE_2', 'My orders');
define('HEADING_TITLE', 'My orders');
define('TEXT_ORDER_STATUS', 'Status');
define('TEXT_ORDER_DATE', 'Date');
define('TEXT_ORDER_SHIPPED_TO', 'Shipped To:');
define('TEXT_ORDER_BILLED_TO', 'Billed To:');
define('TEXT_ORDER_COST', 'Price excl. VAT');
define('TEXT_ORDER_PC', 'Piece(s)');
define('TEXT_NO_PURCHASES', 'You have not yet made any purchases.');
