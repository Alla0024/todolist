<?php

define('HEADING_TITLE', 'Dealer form');
define('HEADING_SUBTITLE', 'Would you like to become a dealer at AStepAhead? Fill in the form below, <br />it does not commit you to anything yet. After registration, AStepAhead will contact you.');
define('NAVBAR_TITLE', 'Dealers');
define('COMPANY_DETAILS_HEADER', 'Company details');
define('COMPANY_NAME','Company Name');
define('KVK_NUMBER','CoC Number');
define('BTW_NUMBER','VAT number');
define('ADDRESS','Address');
define('VENUE','Venue');
define('POSTAL_CODE','Postal Code');
define('COUNTRY','Country');
define('CONTACT_DETAILS','Contact details');
define('FIRST_NAME','First Name');
define('LAST_NAME','Last name');
define('EMAIL', 'E-Mail');
define('PHONE', 'Telephone number');
define('ENQUIRY', 'Message');
define('SUBSCRIBE_NEWSLETTER', 'Subscribe to our newsletter');
define('AGREE_TEXT', 'By registering you agree to the');
define('TERMS_CONDITIONS', 'Terms and Conditions');
define('EMAIL_SUBJECT', 'Dealer from ' . STORE_NAME);
define('TEXT_SUCCESS', 'Your application has been submitted');
