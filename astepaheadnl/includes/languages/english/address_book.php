<?php
/*
  $Id: address_book.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My account');
define('NAVBAR_TITLE_2', 'Address details');

define('HEADING_TITLE', 'Address details');

define('PRIMARY_ADDRESS_TITLE', 'Primary Address');
define('PRIMARY_ADDRESS_DESCRIPTION', 'This address is used as the pre-selected shipping and billing address for orders placed on this store.<br><br>This address is also used as the base for product and service tax calculations.');

define('ADDRESS_BOOK_TITLE', 'Address details');

define('PRIMARY_ADDRESS', '(*)');

define('TEXT_MAXIMUM_ENTRIES', '<span><b>NOTE:</b></span> Maximum amount of addresses <b>%s</b>.');