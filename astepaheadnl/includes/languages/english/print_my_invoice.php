<?php

define('TITLE_PRINT_ORDER', 'ORDER #');

define('TABLE_HEADING_COMMENTS', 'Comments');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Product model');
define('TABLE_HEADING_INVOICE_PRODUCT_PRICE', 'Product price excl. VAT');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_TAX', 'Tax');
define('TABLE_HEADING_TOTAL', 'Total');
define('TABLE_HEADING_REF', 'Ref.');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX', 'Price (ex)');
define('TABLE_HEADING_PRICE_INCLUDING_TAX', 'Price (inc)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX', 'Total excl VAT');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX', 'Total (inc)');

define('ENTRY_SOLD_TO', 'SOLD TO:');
define('ENTRY_SHIP_TO', 'SHIP TO:');
define('ENTRY_PAYMENT_METHOD', 'Payment Method:');
define('ENTRY_SUB_TOTAL', 'Sub-Total:');
define('ENTRY_TAX', 'Tax:');
define('ENTRY_SHIPPING', 'Shipping:');
define('ENTRY_TOTAL', 'Total:');

define('ENTRY_SELLER', 'SELLER:');
define('ENTRY_QTY', 'Amount');
define('ENTRY_QTY_SUM', 'Total qty, pcs.');
define('ENTRY_SUMBIT1', 'I have this received order in full.');
define('ENTRY_SUMBIT2', '');
define('ENTRY_SIGN', 'Customer`s signature');

define('INVOICE_CLOSE_BTN', 'Close');
define('INVOIC_BOX_ORDER_DATE', 'Order date:');
define('DOCUMENT_TITLE', 'Invoice');
define('ADDRESS_TITLE', 'Invoice address');
define('ADDRESS', 'Mens Mentis SRL<br>
Attn : Accounts Payable / t.a.v. de Administratie<br>
Ep.M Pavel street 23/5<br>
410210 Oradea Bihor Romania<br>');
define('TEL_TITLE', 'Telephone');
define('TELEPHONE', '+40 359 444 443');
define('INVOICE_NUMBER_TITLE', 'Invoice number');
define('INVOICE_DEBITEUR_TITLE', 'Debtor number');
define('INVOICE_DATE_TITLE', 'Invoice date');
define('INVOICE_EXPIRATION_DATE', 'Expiration date');
define('INVOICE_VAT_TITLE', 'VAT # customer');
define('INVOICE_VAT', 'RO24914293');
define('INVOICE_SHEET_TITLE', 'Sheet number');
define('INVOICE_SHEET_NUMBER', '1');
define('PRODUCT_LINK', 'Link');
define('SECOND_PART_TITLE', 'Special deal starting order');

define('CREDIT_LIMIT', 'Credit Limit');
define('OTHER', 'Other');
define('SUBTOTAL', 'Subtotal excl. VAT');
define('VAT_SUBTOTAL', 'VAT value');
define('INVOICE_TOTAL', 'Total incl. VAT');

define('PAYMENT_CONDITION_TITLE', 'Payment Condition');
define('PAYMENT_CONDITION', 'Pay In Advance / Prepayment');
define('PAYMENT_TERM_TITLE', 'Payment term');
define('PAYMENT_TERM', 'When paying within 14 days, you can deduct the credit limit.');
define('CREDIT_LIMITATION_TITLE', 'Credit limitation');
define('CREDIT_LIMITATION', '100% payment within 14 days, stating: of the invoice and debtor number');
define('COMPLAINTS', 'Complaints must be in our possession within 8 days of the invoice date.');
define('INVOICE_CREATED', 'Invoice created by : AstepAhead');
define('FOOTER_BLOCK1', '<b>DRAX Europe / ASA B.V</b><br>
Distribution & Service<br>
Ondernemingsweg 45<br>
2404 HN Alpen aan den Rijn<br>
The Netherlands<br>');
define('FOOTER_BLOCK2', '<b>DRAX Europe</b><br>
Showroom & Life Setting<br>
Snijdersbergweg 97<br>
1105 AN Amsterdam<br>
The Netherlands<br>');
define('FOOTER_PHONE', 'Phone : +31 85 7501025');
define('FOOTER_EMAIL', 'Email : sales@draxfit.eu');
define('FOOTER_SITE', 'Website : www.draxfit.eu');
define('FOOTER_CC', 'CC no : 27362484');
define('FOOTER_IBAN', 'IBAN : NL57 RABO 01528857 73');
define('FOOTER_BIC', 'BIC : RABONNL2U');
define('FOOTER_VAT', 'VAT no : NL821969778B01');