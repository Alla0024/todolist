<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Description');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Enable / Disable Module');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Default status');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Merchant account');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Merchant secret');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'The sort order');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Payment State');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'True - enable, False - disable');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'The status that is set to the order when it is created');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Merchant account');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Merchant secret');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Sorting order of the module in the block with payment modules');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'The status that is set to the order upon successful payment');
