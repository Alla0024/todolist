<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Bank Transfer');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', "");
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "");