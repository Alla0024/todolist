<?php
/*
  $Id $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Αποσύνδεση');
define('NAVBAR_TITLE', 'Αποσύνδεση');
define('TEXT_MAIN', 'Έχετε αποσυνδεθεί ο λογαριασμός σας. Τώρα είναι ασφαλές να αφήσετε τον υπολογιστή.<br><br>Το καλάθι αγορών σας έχει αποθηκευτεί, τα στοιχεία μέσα σε αυτό θα αποκατασταθούν κάθε φορά που θα συνδεθείτε ξανά στο λογαριασμό σας.');
?>
