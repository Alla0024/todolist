<?php
/*
  $Id: ot_shipping.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_ORDER_TOTAL_SHIPPING_TITLE', 'Αποστολή');
  define('MODULE_ORDER_TOTAL_SHIPPING_DESCRIPTION', 'Κόστος αποστολής της παραγγελίας');

  define('FREE_SHIPPING_TITLE', 'Δωρεάν αποστολή');
  define('FREE_SHIPPING_DESCRIPTION', 'Δωρεάν αποστολή για παραγγελίες πάνω από% s');
?>