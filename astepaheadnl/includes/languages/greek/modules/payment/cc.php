<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_CC_TEXT_TITLE', 'Πιστωτική κάρτα');
  define('MODULE_PAYMENT_CC_TEXT_DESCRIPTION', 'Πληροφορίες δοκιμής πιστωτικών καρτών:<br><br>CC#: 4111111111111111<br>Λήξη: Οποιαδήποτε');
  define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_OWNER', 'Ιδιοκτήτης πιστωτικής κάρτας:');
  define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_NUMBER', 'Αριθμός πιστωτικής κάρτας:');
  define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_EXPIRES', 'Ημερομηνία λήξης της πιστωτικής κάρτας:');
  define('MODULE_PAYMENT_CC_TEXT_ERROR', 'Σφάλμα πιστωτικής κάρτας!');