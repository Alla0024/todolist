<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Περιγραφή');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Ενεργοποίηση / απενεργοποίηση μονάδας');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Προεπιλεγμένη κατάσταση');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Λογαριασμός εμπόρου');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Εμπορικό μυστικό');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'Η σειρά ταξινόμησης');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Κατάσταση πληρωμής');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'True - ενεργοποίηση, False - απενεργοποίηση');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Η κατάσταση που ορίζεται στη σειρά κατά τη δημιουργία της');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Λογαριασμός εμπόρου');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Εμπορικό μυστικό');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Ταξινόμηση σειράς της μονάδας στο μπλοκ με μονάδες πληρωμής');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'Η κατάσταση που έχει οριστεί στην παραγγελία μετά την επιτυχή πληρωμή');