<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'ΕΠΙΚΟΙΝΩΝΙΑ');
define('HEADING_SUBTITLE', 'Επικοινωνήστε μαζί μας ');
define('NAVBAR_TITLE', 'Επικοινωνήστε μαζί μας');
define('TEXT_SUCCESS', 'Η ερώτησή σας έχει σταλεί με επιτυχία στον υπεύθηνο του καταστήματος.');
define('EMAIL_SUBJECT', 'Enquiry from ' . STORE_NAME);
define('ENTRY_NAME', 'Πλήρες όνομα:');
define('ENTRY_EMAIL', 'Διεύθυνση ηλεκτρονικού ταχυδρομείου:');
define('ENTRY_PHONE', 'Τηλέφωνο:');
define('ENTRY_ENQUIRY', 'Ερευνα:');
define('SEND_TO_TEXT', 'Στέλνω σε:');
define('POP_CONTACT_US','Στείλτε μας email και θα σας καλέσουμε μόλις μπορέσουμε!');