<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Ολοκλήρωση αγοράς');
define('NAVBAR_TITLE_2', 'Επιτυχής');
define('HEADING_TITLE', 'Η παραγγελία σας έχει επεξεργάζετε!');
define('TEXT_SUCCESS', 'Η παραγγελία σας έχει υποβληθεί σε επεξεργασία με επιτυχία! Τα προϊόντα σας θα φτάσουν στον προορισμό τους εντός 2-5 εργάσιμων ημερών.');
define('TEXT_THANKS_FOR_SHOPPING', 'Ευχαριστούμε για την αγορά!');
define('TABLE_HEADING_COMMENTS', 'Θα επικοινωνήσουμε μαζί σας σύντομα');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Ημερομηνία λήξης: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', 'υπόλοιπες λήψεις');
define('SMS_NEW_ORDER', 'Έχετε νέα παραγγελία!!!');
define('TEXT_SUCCESS_INFO', 'Όλες οι περαιτέρω οδηγίες και λεπτομέρειες σχετικά με την πληρωμή σας έχουν σταλεί ταχυδρομικώς');