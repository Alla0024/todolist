<?php
/*
  $Id: create_account.tpl.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// guest_account start
if ($guest_account == false) { // Not a Guest Account
define('NAVBAR_TITLE', 'Δημιουργία λογαριασμού');
} else {
  define('NAVBAR_TITLE', 'Ολοκλήρωση');
}
// guest_account end
define('HEADING_TITLE', 'Πληροφορίες λογαριασμού μου');
// guest_account start
define('CR_ENTER', 'Σύνδεση');
define('CR_THX', 'Ευχαριστούμε! Είστε εγγεγραμμένοι.');
define('CR_IF', 'Αν έχετε ήδη εγγραφεί.');
// guest_account end
define('TEXT_ORIGIN_LOGIN', '<span><small><b>NOTE:</b></span></small> Αν έχετε ήδη λογαριασμό μαζί μας, συνδεθείτε στο <a href="%s"><u>lσελίδα σύνδεσης</u></a>.');
define('EMAIL_SUBJECT', 'Καλωσήρθες στο ' . STORE_NAME);
define('EMAIL_GREET_MR', 'Αγαπητέ κύριε. %s,' . "\n\n");
define('EMAIL_GREET_MS', 'Αγαπητή κυρία. %s,' . "\n\n");
define('EMAIL_GREET_NONE', 'Αγαπητέ %s' . "\n\n");
define('EMAIL_WELCOME', 'Σας καλωσορίζουμε <b>' . STORE_NAME . '</b>.' . "\n\n");
define('EMAIL_TEXT', 'Τώρα μπορείτε να λάβετε μέρος στο <b>διάφορες υπηρεσίες</b> wΠρέπει να σας προσφέρουμε. Ορισμένες από αυτές τις υπηρεσίες περιλαμβάνουν:' . "\n\n" . '<li><b>μόνιμο καλάθι</b> - Όλα τα προϊόντα που προστίθενται στο ηλεκτρονικό σας καλάθι παραμένουν εκεί μέχρι να τα αφαιρέσετε ή να τα ελέγξετε.' . "\n" . '<li><b>Address Book</b> - We can now deliver your products to another address other than yours! This is perfect to send birthday gifts direct to the birthday-person themselves.' . "\n" . '<li><b>Order History</b> - Δείτε το ιστορικό των αγορών που έχετε κάνει μαζί μας.' . "\n" . '<li>' . "\n\n");
define('EMAIL_CONTACT', 'Για βοήθεια με οποιαδήποτε από τις υπηρεσίες μας στο διαδίκτυο, στείλτε email στον υπεύθηνο καταστήματος: ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n\n");
define('EMAIL_WARNING', '<b>Note:</b> Αυτή η διεύθυνση ηλεκτρονικού ταχυδρομείου μας δόθηκε από έναν από τους πελάτες μας. Αν δεν εγγραφείτε ως μέλος, παρακαλώ στείλτε ένα email στο ' . STORE_OWNER_EMAIL_ADDRESS . '.' . "\n");
/* ICW Credit class gift voucher begin */
define('EMAIL_GV_INCENTIVE_HEADER', "\n\n" .'Ως μέρος της υποδοχής μας σε νέους πελάτες, σας έχουμε στείλει ένα δελτίο ηλεκτρονικού δώρου αξίας% s');
define('EMAIL_GV_REDEEM', 'Ο κωδικός εξαργύρωσης του κουπονιού e-Gift είναι% s, μπορείτε να εισάγετε τον κωδικό εξαργύρωσης κατά τον έλεγχο κατά την πραγματοποίηση αγοράς');
define('EMAIL_GV_LINK', 'ή ακολουθώντας αυτόν τον σύνδεσμο ');
define('EMAIL_COUPON_INCENTIVE_HEADER', 'Συγχαρητήρια, για να κάνετε την πρώτη σας επίσκεψη στο ηλεκτρονικό μας κατάστημα μια πιο ικανοποιητική εμπειρία, σας στέλνουμε ένα κουπόνι e-Discount.' . "\n" .
                                        ' Παρακάτω υπάρχουν λεπτομέρειες σχετικά με το κουπόνι έκπτωσης που δημιουργήθηκε μόνο για εσάς' . "\n");
define('EMAIL_COUPON_REDEEM', 'Χρησιμοποιήστε το κουπόνι και εισάγετε τον κωδικό εξαργύρωσης που είναι% s κατά την ολοκλήρωση της αγοράς ενώ κάνετε μια αγορά');
define('CR_LOGIN', 'Η σύνδεσή σας (e-mail)');
define('CR_PASS', 'Ο κωδικός σας');
define('CR_ADD_EMAIL', 'Παρακαλώ εισάγετε το <b>e-mail:</b>');
define('CR_SUBMIT', 'υποβολή');
/* ICW Credit class gift voucher end */