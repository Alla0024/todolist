<?php
/*
  $Id: pollbooth.php,v 1.5 2003/04/06 21:45:33 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Copyright (c) 2000,2001 The Exchange Project

  Released under the GNU General Public License
*/
if (!isset($_GET['op'])) {
	$_GET['op']="list";
	}
if ($_GET['op']=='results') {
  define('HEADING_TITLE', 'Δείτε τι σκέφτονται οι άλλοι');
}
if ($_GET['op']=='list') {
  define('HEADING_TITLE', 'Εκτιμούμε τις σκέψεις σας');
}
if ($_GET['op']=='vote') {
  define('HEADING_TITLE', 'Οι πελάτες μας έχουν σημασία');
}
if ($_GET['op']=='comment') {
  define('HEADING_TITLE', 'Σχολιάστε αυτή τη δημοσκόπηση');
}
define('_WARNING', 'Προειδοποίηση : ');
define('_ALREADY_VOTED', 'Έχετε ψηφιστεί πρόσφατα σε αυτή τη δημοσκόπηση.');
define('_NO_VOTE_SELECTED', 'Δεν επιλέξατε την επιλογή να ψηφίσετε.');
define('_TOTALVOTES', 'Σύνολο ψήφων');
define('_OTHERPOLLS', 'Άλλες Δημοσκοπήσεις');
define('_POLLRESULTS', 'Κάντε κλικ εδώ για αποτελέσματα δημοσκόπησης');
define('_VOTING', 'Ψηφίστε τώρα');
define('_RESULTS', 'Αποτελέσματα');
define('_VOTES', 'Ψηφοφορίες');
define('_VOTE', 'ΨΗΦΟΙ');
define('_COMMENT', 'Σχόλια');
define('_COMMENTS_BY', 'Σχόλιο από ');
define('_COMMENTS_ON', ' επί ');
define('_YOURNAME', 'Το όνομα σας');
define('_OTZYV', 'Σχόλιο:');
define('TEXT_CONTINUE', 'συνεχεια');
define('_PUBLIC','Δημοσίευση');
define('_PRIVATE','Ιδιωτικό');
define('_POLLOPEN','η Δημοσκόπηση ειναι Άνοιχτη');
define('_POLLCLOSED','Η Δημοσκόπηση είναι κλειστή');
define('_POLLPRIVATE','Ιδιωτική δημοσκόπηση, θα πρέπει να συνδεθείτε για να ψηφίσετε');
define('_ADD_COMMENTS', 'Πρόσθεσε σχόλια');
define('TEXT_DISPLAY_NUMBER_OF_COMMENTS', 'Εμφάνιση <b>%d</b> to <b>%d</b> (of <b>%d</b> σχολίων)');
?>
