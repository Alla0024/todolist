<?php
/*
  $Id: article_info.php, v1.0 2003/12/04 12:00:00 ra Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_ARTICLE_NOT_FOUND', 'Article Not Found');
define('TEXT_ARTICLE_NOT_FOUND', 'Sorry, but the article you requested is not available in this site.');
define('TEXT_DATE_ADDED', 'This article was published on %s.');
define('TEXT_DATE_AVAILABLE', 'This article will be published on %s.');
define('TEXT_BY', 'by ');
