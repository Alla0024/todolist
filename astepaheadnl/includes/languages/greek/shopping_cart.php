<?php
/*
  $Id: shopping_cart.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Περιεχόμενο καλαθιού');
define('HEADING_TITLE', 'Καλάθι αγορών');
define('TABLE_HEADING_REMOVE', 'Αφαίρεση');
define('TABLE_HEADING_REMOVE_FROM', 'από το καλάθι');
define('TABLE_HEADING_QUANTITY', 'Ποσότητα.');
define('TABLE_HEADING_IMAGE', 'Εικόνα');
define('TABLE_HEADING_NAME', 'Ονομα');
define('TABLE_HEADING_PRICE', 'Τιμή');
define('TABLE_HEADING_MODEL', 'Μοντέλο');
define('TABLE_HEADING_PRODUCTS', 'Προϊόντα');
define('TABLE_HEADING_TOTAL', 'Σύνολο');
define('TABLE_HEADING_PRICE_TOTAL', 'Σύνολο');
define('TEXT_CART_EMPTY', 'Το καλάθι αγορών σας είναι άδειο!');
define('SUB_TITLE_COUPON', 'Κουπόνι');
define('SUB_TITLE_COUPON_SUBMIT', 'υποβολή');
define('SUB_TITLE_COUPON_VALID', 'Το κουπόνι σας είναι έγκυρο. Έλεγχος έκπτωσης:');
define('SUB_TITLE_COUPON_INVALID', 'Το κουπόνι σας δεν είναι έγκυρο.');

define('OUT_OF_STOCK_CANT_CHECKOUT', 'Προϊόντα που σημειώνονται με ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' Δεν υπάρχουν στην επιθυμητή ποσότητα στο απόθεμά μας.<br>Αλλάξτε την ποσότητα των προϊόντων που σημειώνονται με (' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . '), Ευχαριστούμε πολύ');
define('OUT_OF_STOCK_CAN_CHECKOUT', 'Προϊόντα που σημειώνονται με ' . STOCK_MARK_PRODUCT_OUT_OF_STOCK . ' Δεν υπάρχουν στην επιθυμητή ποσότητα στο απόθεμά μας.<br>Μπορείτε να τα αγοράσετε ούτως ή άλλως και να ελέγξετε την ποσότητα που έχουμε στο απόθεμα για άμεση παράδοση στη διαδικασία πληρωμής.');
?>