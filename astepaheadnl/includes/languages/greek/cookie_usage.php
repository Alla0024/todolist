<?php
/*
  $Id: cookie_usage.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Χρήση cookie');
define('HEADING_TITLE', 'Cookie Usage');
define('TEXT_INFORMATION', 'Εντοπίσαμε ότι το πρόγραμμα περιήγησής σας δεν υποστηρίζει cookies ή έχει ορίσει την απενεργοποίηση των cookies.<br><br>To continue shopping online, we encourage you to enable cookies on your browser.<br><br>For <b>Internet Explorer</b> browsers, please follow these instructions:<br><ol><li>Click on the Tools menubar, and select Internet Options</li><li>Select the Security tab, and reset the security level to Medium</li></ol>We have taken this measurement of security for your benefit, and apologize upfront if any inconveniences are caused.<br><br>Please contact the store owner if you have any questions relating to this requirement, or to continue purchasing products offline.');
define('BOX_INFORMATION', 'Τα cookies πρέπει να είναι ενεργοποιημένα για να αγοράζουν online σε αυτό το κατάστημα για να καλύψουν θέματα σχετικά με την προστασία της ιδιωτικής ζωής και της ασφάλειας σχετικά με την επίσκεψή σας στον ιστότοπο. Με την ενεργοποίηση της υποστήριξης cookie στο πρόγραμμα περιήγησής σας, η επικοινωνία μεταξύ εσάς και αυτού του ιστότοπου ενισχύεται είστε εσείς που πραγματοποιείτε συναλλαγές για λογαριασμό σας και αποτρέψτε τη διαρροή των προσωπικών σας στοιχείων.');
