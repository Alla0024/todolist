<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Το προϊόν δεν βρέθηκε!');
define('TEXT_DATE_ADDED', 'Αυτό το προϊόν προστέθηκε στον κατάλογό μας στο% s.');
define('TEXT_DATE_AVAILABLE', '<span>Αυτό το προϊόν θα είναι διαθέσιμο στο% s.</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Το προϊόν έχει προστεθεί με επιτυχία στη λίστα επιθυμιών σας');
define('TEXT_COLOR', 'Χρώμα');
define('TEXT_SHARE', 'Μοιραστείτε με τους φίλους σας');
define('TEXT_REVIEWSES', 'σχόλια');  
define('TEXT_REVIEWSES2', 'Κριτικές');  
define('TEXT_DESCRIPTION', 'Περιγραφή');
define('TEXT_ATTRIBS', 'Χαρακτηριστικά');
define('TEXT_PAYM_SHIP', 'Λεπτομέρειες αποστολής');
define('SHORT_DESCRIPTION', 'Σύντομη περιγραφή');


//home
define('HOME_TEXT_PAYM_SHIP', 'Εγγύηση');


?>