<?php
/*
  $Id: english.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
@setlocale(LC_TIME, 'en_US.ISO_8859-1');
define('OG_LOCALE', 'en_US');
define('GO_COMPARE', 'Go to compare');
define('IN_WHISHLIST', 'In Wishlist');
define('COMPARE', 'Compare');
define('WHISH', 'To Wishlist');

define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');

define('DATE_FORMAT_SHORT', '%m/%d/%Y');  // this is used for strftime()
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y y.'); // this is used for strftime()
define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT', 'm/d/Y'); // this is used for date()
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');

define('TEXT_DAY_1','Monday');
define('TEXT_DAY_2','Tuesday');
define('TEXT_DAY_3','Environment');
define('TEXT_DAY_4','Thursday');
define('TEXT_DAY_5','Friday');
define('TEXT_DAY_6','Saturday');
define('TEXT_DAY_7','Sunday');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1','January');
define('TEXT_MONTH_BASE_2','February');
define('TEXT_MONTH_BASE_3','March');
define('TEXT_MONTH_BASE_4','April');
define('TEXT_MONTH_BASE_5','May');
define('TEXT_MONTH_BASE_6','June');
define('TEXT_MONTH_BASE_7','July');
define('TEXT_MONTH_BASE_8','August');
define('TEXT_MONTH_BASE_9','September');
define('TEXT_MONTH_BASE_10','October');
define('TEXT_MONTH_BASE_11','November');
define('TEXT_MONTH_BASE_12','December');
define('TEXT_MONTH_1','Jan');
define('TEXT_MONTH_2','Feb');
define('TEXT_MONTH_3','Mar');
define('TEXT_MONTH_4','APR');
define('TEXT_MONTH_5','May');
define('TEXT_MONTH_6','Jun');
define('TEXT_MONTH_7','July');
define('TEXT_MONTH_8','Aug');
define('TEXT_MONTH_9','Sep');
define('TEXT_MONTH_10','Oct');
define('TEXT_MONTH_11','Nov');
define('TEXT_MONTH_12','Dec');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'LEI');

// Global entries for the <html> tag
define('HTML_PARAMS','dir="LTR" lang="en"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Your Store Name');

define('CUSTOM_PANEL_EDIT', 'Editați | ×');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'produs');
define('CUSTOM_PANEL_EDIT_MANUF', 'producător');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'articol');
define('CUSTOM_PANEL_EDIT_SECTION', 'secțiune');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'categorie');
define('CUSTOM_PANEL_EDIT_SEO', 'text seo');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Panoul Administratorului');
define('CUSTOM_PANEL_ADD', 'Adăuga');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Produs');
define('CUSTOM_PANEL_ADD_PAGE', 'Pagină');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Reducere');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Categorie');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statistici');
define('CUSTOM_PANEL_ADD_ONLINE', 'Pe net:');
define('CUSTOM_PANEL_PALETTE', 'Paleta site-ului');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Culoarea textului');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Fundalul antetului');
define('PULL_DOWN_COUNTRY', 'Regiune:');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Piesă de fundal');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Culoarea legăturii');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Culoarea butonului');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Culoare de fundal');
define('CUSTOM_PANEL_ORDERS', 'Comenzi');
define('CUSTOM_PANEL_ORDERS_NEW', 'Nou:');



// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Creaza cont');
define('HEADER_TITLE_CHECKOUT', 'Comanda');
define('HEADER_TITLE_TOP', 'Top');
define('HEADER_TITLE_LOGOFF', 'Log Off');

define('HEAD_TITLE_CHECKOUT','Confirmare');
define('HEAD_TITLE_CHECKOUT_SUCCESS','Comanda a fost transmisa!');
define('HEAD_TITLE_ACCOUNT','Cont');
define('HEAD_TITLE_ACCOUNT_HISTORY','Comenzile mele');
define('HEAD_TITLE_ACCOUNT_EDIT','Edit my data');
define('HEAD_TITLE_ADDRESS_BOOK','Adrese');
define('HEAD_TITLE_ACCOUNT_PASSWORD','Schimba parola');
define('HEAD_TITLE_ALLCOMMENTS','Toate comentariile');
define('HEAD_TITLE_CONTACT_US','Contacts of store '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML sitemap - '.STORE_NAME);
define('HEAD_TITLE_404','Error 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Error 403 - '.STORE_NAME);

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Cos gol');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'View All Items');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Please Select');
define('TYPE_BELOW', 'Completeaza mai jos');

// javascript messages
define('JS_ERROR', 'Errors have occured during the process of your form.\n\nPlease make the following corrections:\n\n');


define('JS_FIRST_NAME', '* The \'First Name\' must contain a minimum of ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters.\n');
define('JS_LAST_NAME', '* The \'Last Name\' must contain a minimum of ' . ENTRY_LAST_NAME_MIN_LENGTH . ' characters.\n');




define('JS_ERROR_SUBMITTED', 'This form has already been submitted. Please press Ok and wait for this process to be completed.');


define('CATEGORY_COMPANY', 'Company Details');
define('CATEGORY_PERSONAL', 'Your Personal Details');
define('CATEGORY_ADDRESS', 'Your Address');
define('CATEGORY_CONTACT', 'Your Contact Information');
define('CATEGORY_OPTIONS', 'Options');

define('ENTRY_COMPANY', 'Denumire firma si cod fiscal (pt. pers. juridice)');
define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_FIRST_NAME', 'Nume');
define('ENTRY_TEXT_FOM_IMAGE', 'Enter text from image');
define('ENTRY_FIRST_NAME_ERROR', 'Your First Name must contain a minimum of ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Prenume');
define('ENTRY_LAST_NAME_ERROR', 'Your Last Name must contain a minimum of ' . ENTRY_LAST_NAME_MIN_LENGTH . ' characters.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Date of Birth:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Your Date of Birth must be in this format: MM/DD/YYYY (eg 05/21/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (eg. 05/21/1970)');
define('ENTRY_EMAIL_ADDRESS', 'Email');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Adresa de email trebuie sa contina minim ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' caractere.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Adresa de email nu pare sa fie valida. ');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Adresa de email exista deja in baza noastra de date. Va rugam sa va logati cu datele existente.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Adresa');
define('ENTRY_STREET_ADDRESS_ERROR', 'Adresa trebuie sa contina minim ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' caractere.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB', 'Judet');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Zip Code:');
define('ENTRY_POST_CODE_ERROR', 'Your Post Code must contain a minimum of ' . ENTRY_POSTCODE_MIN_LENGTH . ' characters.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Localitate');
define('ENTRY_CITY_ERROR', 'Localitatea trebuie sa contina minim ' . ENTRY_CITY_MIN_LENGTH . ' caractere.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Judet');
define('ENTRY_STATE_ERROR', 'Your State must contain a minimum of ' . ENTRY_STATE_MIN_LENGTH . ' characters.');
define('ENTRY_STATE_ERROR_SELECT', 'Please select a state from the States pull down menu.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Country:');
define('ENTRY_COUNTRY_ERROR', 'You must select a country from the Countries pull down menu.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Numar telefon');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Your Telephone Number must contain a minimum of ' . ENTRY_TELEPHONE_MIN_LENGTH . ' characters.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Fax Number:');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Newsletter');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Subscribed');
define('ENTRY_NEWSLETTER_NO', 'Unsubscribed');
define('ENTRY_PASSWORD', 'Parola');
define('ENTRY_PASSWORD_ERROR', 'Parola trebuie sa contina minim ' . ENTRY_PASSWORD_MIN_LENGTH . ' caractere.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Confirmarea parolei trebuie sa coincida.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Confirmare parola');
define('ENTRY_PASSWORD_CURRENT', 'Parola curenta');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Your Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_NEW', 'Parola noua');
define('ENTRY_PASSWORD_NEW_ERROR', 'Noua parola trebuie sa contina minim ' . ENTRY_PASSWORD_MIN_LENGTH . ' caractere.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Confirmarea parolei nu coincide.');

define('FORM_REQUIRED_INFORMATION', '* informatii necesare');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Pagina:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> products)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Afisare de la <b>%d</b> la <b>%d</b> (din <b>%d</b> comenzi)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> specials)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> featured products)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Previous Page');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Next Page');
define('PREVNEXT_TITLE_PAGE_NO', 'Page %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Previous Set of %d Pages');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Next Set of %d Pages');
define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;Prev]');
define('PREVNEXT_BUTTON_NEXT', '[Next&nbsp;&gt;&gt;]');

define('PREVNEXT_TITLE_PPAGE', 'Page');

define('ART_XSELL_TITLE', 'Similar content');
define('PROD_XSELL_TITLE', 'Din aceeasi colectie');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Adauga adresa');
define('IMAGE_BUTTON_BACK', 'Inapoi');
define('IMAGE_BUTTON_CHECKOUT', 'Confirma comanda');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Confirm Order');
define('IMAGE_BUTTON_CONTINUE', 'OK');
define('IMAGE_BUTTON_DELETE', 'Sterge');
define('IMAGE_BUTTON_LOGIN', 'Cont');

define('IMAGE_BUTTON_IN_CART', 'In cos');
define('IMAGE_BUTTON_ADDTO_CART', 'Adauga in cos');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Adauga in cos');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'Add to <b>Cart</b>');

define('IMAGE_BUTTON_BUY_TEMPLATE', 'Buy this template now!');
define('MESSAGE_BUY_TEMPLATE1', 'Unfortunately, your test period of 14 days is over. Please ');
define('MESSAGE_BUY_TEMPLATE2', 'buy');
define('MESSAGE_BUY_TEMPLATE3', ' this store or create another test site.');

define('CUSTOM_PANEL_EDIT', 'Edit ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'product');
define('CUSTOM_PANEL_EDIT_MANUF', 'manufacturer');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'article');
define('CUSTOM_PANEL_EDIT_SECTION', 'section');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'category');
define('CUSTOM_PANEL_EDIT_SEO', 'seo text');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Admin panel');
define('CUSTOM_PANEL_ADD', 'Add');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Product');
define('CUSTOM_PANEL_ADD_PAGE', 'Page');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Discount');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Category');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statistics');
define('CUSTOM_PANEL_ADD_ONLINE', 'Online: ');
define('CUSTOM_PANEL_PALETTE', 'Site palette');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Text color');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Header background');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Background footer');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Link color');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Button color');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Background color');
define('CUSTOM_PANEL_ORDERS', 'Orders');
define('CUSTOM_PANEL_ORDERS_NEW', 'New: ');

define('CUSTOM_PANEL_DATE1', 'day');
define('CUSTOM_PANEL_DATE2', 'days');
define('CUSTOM_PANEL_DATE3', 'days');

define('IMAGE_BUTTON_UPDATE', 'Update');
define('IMAGE_REDEEM_VOUCHER', 'Submit');

define('SMALL_IMAGE_BUTTON_VIEW', 'Vizualizare');


define('ICON_ERROR', 'Eroare');
define('ICON_SUCCESS', 'Success');
define('ICON_WARNING', 'Warning');

define('TEXT_GREETING_PERSONAL', 'Welcome back <span class="greetUser">%s!</span> Would you like to see which <a href="%s"><u>new products</u></a> are available to purchase?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Our Customer Greeting');
define('TEXT_GREETING_GUEST', 'Welcome <span class="greetUser">Guest!</span> Would you like to <a href="%s"><u>log yourself in</u></a>? Or would you prefer to <a href="%s"><u>create an account</u></a>?');

define('TEXT_SORT_PRODUCTS', 'Sortare:');
define('TEXT_DESCENDINGLY', 'descendingly');
define('TEXT_ASCENDINGLY', 'ascendingly');
define('TEXT_BY', ' by ');

define('TEXT_UNKNOWN_TAX_RATE', 'Unknown tax rate');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'NOTICE: This website will be down for maintenance on: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'NOTICE: the website is currently Down For Maintenance to the public');

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Warning: Installation directory exists at: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Please remove this directory for security reasons.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Warning: I am able to write to the configuration file: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. This is a potential security risk - please set the right user permissions on this file.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Warning: The sessions directory does not exist: ' . tep_session_save_path() . '. Sessions will not work until this directory is created.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Warning: I am not able to write to the sessions directory: ' . tep_session_save_path() . '. Sessions will not work until the right user permissions are set.');
define('WARNING_SESSION_AUTO_START', 'Warning: session.auto_start is enabled - please disable this php feature in php.ini and restart the web server.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'The expiry date entered for the credit card is invalid.<br>Please check the date and try again.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'The credit card number entered is invalid.<br>Please check the number and try again.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'The first four digits of the number entered are: %s <br>If that number is correct, we do not accept that type of credit card.<br>If it is wrong, please try again.');






define('BOX_LOGINBOX_HEADING', 'Your account');
define('LOGIN_BOX_MY_CABINET','Cont');
define('LOGIN_BOX_ADDRESS_BOOK','Adrese');
define('LOGIN_BOX_LOGOFF','Log off');

define('LOGIN_FROM_SITE','Login');
define('RENDER_LOGIN_WITH', 'Login with');

define('LOGO_IMAGE_TITLE','Logo');

define('SORT_BY','Sort by');


// Article Manager
define('BOX_ALL_ARTICLES', 'All Articles');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> articles)');
define('NAVBAR_TITLE_DEFAULT', 'Info');
define('TEXT_NAVIGATION_BRANDS','Toți producătorii');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT','-'); // define('PRICES_LOGGED_IN_TEXT','Must be logged in for prices!');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO','Order Minumum is: ');
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART','Order Minimum is: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO','Order in Units of: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART','Order in Units of: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT','');
define('ERROR_PRODUCTS_QUANTITY_INVALID','Invalid Qty: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT','');
define('ERROR_PRODUCTS_UNITS_INVALID','Invalid Units: ');

// Comments
define('ADD_COMMENT_HEAD_TITLE', 'Add your comment');

// Poll Box Text
define('_RESULTS', 'Results');
define('_VOTE', 'Vote');
define('_VOTES', 'Votes:');

define('PREV_NEXT_PRODUCT', 'Product ');
define('PREV_NEXT_CAT', ' of category ');
define('PREV_NEXT_MB', ' of manafacturer ');


// English names of boxes

define('BOX_HEADING_CATEGORIES', 'Categorii');
define('BOX_HEADING_INFORMATION', 'Info');
define('BOX_HEADING_MANUFACTURERS', 'Branduri');
define('BOX_HEADING_SPECIALS', 'Oferte speciale');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Oferte speciale %s');
define('BOX_HEADING_SEARCH', '');
define('BOX_OPEN_SEARCH_FORM', 'Cautare');
define('BOX_HEADING_WHATS_NEW', 'Noutati');
define('BOX_HEADING_FEATURED', 'Recomandari');
define('BOX_HEADING_ARTICLES', 'Info');
define('BOX_HEADING_LINKS', 'Links');
define('HELP_HEADING', 'Help');
define('BOX_HEADING_WISHLIST', 'Wishlist');
define('BOX_HEADING_BESTSELLERS', 'Top vanzari');
define('BOX_HEADING_CURRENCY', 'Currency');
define('_POLLS', 'Polls');
define('BOX_HEADING_CALLBACK', 'Callback');
define('BOX_HEADING_CONSULTANT', 'Consultant online');
define('BOX_HEADING_PRODUCTS', 'items');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Add category of items.');
define('BOX_HEADING_LASTVIEWED', 'Recent vizualizate');

define('BOX_BESTSELLERS_NO', 'There is no bestsellers yet');


  define('LOW_STOCK_TEXT1','Low stock warning: ');
  define('LOW_STOCK_TEXT2','Model No.: ');
  define('LOW_STOCK_TEXT3','Quantity: ');
  define('LOW_STOCK_TEXT4','Product URL: ');
  define('LOW_STOCK_TEXT5','Current Low order limit is ');

// wishlist box text in includes/boxes/wishlist.php

  define('BOX_TEXT_NO_ITEMS', 'No products are in your Wishlist.');
    define('BUTTON_SHOPPING_CART_OPEN', 'Cos cumparaturi');
  define('TOTAL_TIME', 'Execution time: ');
  define('TOTAL_CART', 'Total');
    
// otf 1.71 defines needed for Product Option Type feature.
  define('TEXT_PREFIX', 'txt_');
  define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES
  
  // Product tabs

define('ALSO_PURCHASED', 'Also purchased');

define('FORWARD', 'Forward');

define('COMP_PROD_NAME','Name');
define('COMP_PROD_IMG','Image');
define('COMP_PROD_PRICE','Pret');
define('COMP_PROD_CLEAR','Clear all');
define('COMP_PROD_BACK','Back');
define('COMP_PROD_ADD_TO','Add products to compare!');
define('TEXT_PASSWORD_FORGOTTEN', 'Forgot password?');
define('TEXT_PASSWORD_FORGOTTEN_DO', 'Reset password');
define('QUICK_ORDER','Solicita informatii pentru acest produs');
define('QUICK_ORDER_SUCCESS','Solicitarea a fost transmisa. Veti fi contactat(a) cat mai curand posibil.');
define('QUICK_ORDER_BUTTON','Solicita informatii');
define('SEND_MESSAGE','Send');

define('LABEL_NEW','New');
define('LABEL_TOP','TOP');
define('LABEL_SPECIAL','Special');

define('FILTER_BRAND','Brand');
define('FILTER_ALL','Toate');

define('WISHLIST_PC','pc.');

define('FOOTER_INFO','Info');
define('FOOTER_CATEGORIES','Categorii');
define('FOOTER_ARTICLES','Articole');
define('FOOTER_CONTACTS','');
define('FOOTER_SITEMAP','Sitemap');
define('FOOTER_DEVELOPED','');

define('SHOW_ALL_CATS','Arata toate categoriile');


define('TEXT_HEADING_PRICE', 'Catalog:');
define('TEXT_HEADING_CATALOG', 'Catalog');
define('TEXT_PRODUCTS_ATTRIBUTES', 'info');
define('TEXT_PRODUCTS_QTY', 'Products Quantity');

define('MAIN_NEWS','Info');
define('MAIN_NEWS_ALL','Toate');
define('MAIN_NEWS_SUBSCRIBE','');
define('MAIN_NEWS_SUBSCRIBE_BUT','Abonare newsletter');
define('MAIN_NEWS_EMAIL','adresa email');

define('MAIN_BESTSELLERS','Cele mai vandute');
define('MAIN_REVIEWS','Reviews');
define('MAIN_REVIEWS_ALL','All reviews');
define('MAIN_MOSTVIEWED','Most viewed');

define('LIST_TEMP_INSTOCK','In stock');
define('LIST_TEMP_OUTSTOCK','Livrare pe comanda speciala de la fabrica');

define('HIGHSLIDE_CLOSE','Inchide');
define('BUTTON_CANCEL','Inchide');
define('BUTTON_SEND','Trimite');

define('LISTING_PER_PAGE','Pe pagina');
define('LISTING_SORT_NAME','A-Z');
define('LISTING_SORT_PRICE1','Pret crescator');
define('LISTING_SORT_PRICE2','Pret descrescator');
define('LISTING_SORT_NEW','Cele mai noi');
define('LISTING_SORT_POPULAR','Relevanta');
define('LISTING_SORT_LIST','list');
define('LISTING_SORT_COLUMNS','columns');

define('PROD_DRUGIE','Recomandari');
define('PROD_FILTERS','Filtre');

define('PAGE404_TITLE','Error 404');
define('PAGE404_DESC','Pagina nu a fost gasita');
define('PAGE404_REASONS','There may be several reasons:');
define('PAGE404_REASON1','This page was moved or renamed');
define('PAGE404_REASON2','Pagina inexistenta');
define('PAGE404_REASON3','Your URL is wrong');
define('PAGE404_BACK1','Back to previous page');
define('PAGE404_BACK2','Back to home page');

// page 403
define('PAGE403_DESC', 'You do not have permission to view this page.');
define('PAGE403_TITLE','Error 403');
define('PAGE403_TO_HOME_PAGE','Return to home page');


define('SB_SUBSCRIBER','Subscriber');
define('SB_EMAIL_NAME','Request for newsletters');
define('SB_EMAIL_USER','User');
define('SB_EMAIL_WAS_SUBSCRIBED','a fost inregistrata pentru a primi newsletterul nostru');
define('SB_EMAIL_ALREADY','have already subscribed in our newsletters!');
define("LOAD_MORE_BUTTON", "Mai mult");
define("TIME_LEFT", "Time left: ");

// CLO
define('CLO_NEW_PRODUCTS', 'Recently added products');
define('CLO_FEATURED', 'These products will interest you');
define('CLO_DESCRIPTION_BESTSELLERS', 'Best sellers');
define('CLO_DESCRIPTION_SPECIALS', 'Discounts');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Mostviewed');

define('SHOPRULES_AGREE', 'By signing up you agree to our');
define('SHOPRULES_AGREE2', 'Terms of Service');


define("SHOW_ALL_SUBCATS", "show all");

define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Free shipping");

//home
define('HOME_BOX_HEADING_SEARCH', 'find...');
define('HEADER_TITLE_OR', 'or');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'add');
define('HOME_IMAGE_BUTTON_IN_CART', 'In cart');
define('HOME_ARTICLE_READ_MORE', 'read more');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'subscribe to a new newsletter and <span> get a 25% discount </span> for one product in an online order');
define('HOME_MAIN_NEWS_EMAIL', 'Enter your e-mail');
define('HOME_FOOTER_DEVELOPED', 'online store creation');
define('HOME_FOOTER_CATEGORIES', 'Catalog');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Add a comment to the product');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Name');
define('HOME_ADD_COMMENT_FORM', 'Comment');
define('HOME_REPLY_TO_COMMENT', 'Reply to comment');
define('HOME_PROD_DRUGIE', 'Viewed products');
define("HOME_LOAD_MORE_INFO", "more");
define("HOME_LOAD_ROLL_UP", "roll up");
define("HOME_TITLE_LEFT_CATEGORIES", "products");
define('HOME_BOX_HEADING_SELL_OUT', 'Sell-out');
define("HOME_LOAD_MORE_BUTTON", "download more products");
define("HOME_BOX_HEADING_WISHLIST", "Favorite products");
define("HOME_PROD_VENDOR_CODE", "vendor code ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Stock');
define('HOME_FOOTER_SOCIAL_TITLE', 'we are in social networks:');
define('HOME_HEADER_SOME_LIST', 'See also');


define('SEARCH_LANG', 'en/');

define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'All results');
define('ENTER_KEY', 'Enter keywords');
define('TEXT_LIMIT_REACHED', 'Maximum products in compares reached: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Product was successfully added to your cart!');
define('CHOOSE_ADDRESS', 'Choose address');


//default2

define('DEMO2_TOP_PHONES', 'Telephones');
define('DEMO2_TOP_CALLBACK', 'Callback');
define('DEMO2_TOP_ONLINE', 'Online chat');
define('DEMO2_SHOPPING_CART', 'Basket');
define('DEMO2_LOGIN_WITH', 'Login with ');
define('DEMO2_WISHLIST_LINK', 'A wish list');
define('DEMO2_FOOTER_CATEGORIES', 'Catalog');
define('DEMO2_MY_INFO', 'To change the data');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Change shipping address');
define('DEMO2_LEFT_CAT_TITLE', 'Categories');
define('DEMO2_LEFT_ALL_GOODS', 'All goods');
define('DEMO2_TITLE_SLIDER_LINK', 'See all');
define('DEMO2_READ_MORE', 'Read more <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Roll up <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','All categories');
define('DEMO2_SHOW_ALL_ARTICLES','All articles');
define('DEMO2_BTN_COME_BACK','Come back');
define('DEMO2_SHARE_TEXT','Share');
define('DEMO2_QUICK_ORDER_BUTTON', 'In one click');
 /*
$array_for_js = array('BUTTON_SEND'=>BUTTON_SEND,
                      'VK_LOGIN'=>'Login',
                      'IMAGE_BUTTON_IN_CART'=>IMAGE_BUTTON_IN_CART,
                      'SHOW_RESULTS'=>'Toate rezultatele',
                      'RENDER_TEXT_ADDED_TO_CART'=>'Produs adaugat in cos!',
                      'ENTER_KEY'=>'Enter keywords',
                      'TEXT_LIMIT_REACHED'=>'Maximum products in compares reached: ',
                      'CHOOSE_ADDRESS'=>'Adrese',
                      'RENDER_TEMPLATE' => DIR_WS_TEMPLATES . TEMPLATE_NAME . '/render_template.php',
                      'IMAGE_BUTTON_ADDTO_CART'=>IMAGE_BUTTON_ADDTO_CART);      */
