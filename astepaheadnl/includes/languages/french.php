<?php 
define('OG_LOCALE','fr_FR');
define('GO_COMPARE','En comparaison');
define('IN_WHISHLIST','Dans les désirs');
define('COMPARE','Comparer');
define('WHISH','Le désir');
//define('DATE_FORMAT_SHORT', '%d/%m/%Y');
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG','le %d / %m.%Y');
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT','d.m.Y h:i:s');
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT',DATE_FORMAT_SHORT.' %H:%M:%S');
define('TEXT_DAY_1','Lundi');
define('TEXT_DAY_2','Mardi');
define('TEXT_DAY_3','Mercredi');
define('TEXT_DAY_4','Jeudi');
define('TEXT_DAY_5','Vendredi');
define('TEXT_DAY_6','Samedi');
define('TEXT_DAY_7','Dimanche');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1','Janvier');
define('TEXT_MONTH_BASE_2','Février');
define('TEXT_MONTH_BASE_3','Mars');
define('TEXT_MONTH_BASE_4','Avril');
define('TEXT_MONTH_BASE_5','Mai');
define('TEXT_MONTH_BASE_6','Juin');
define('TEXT_MONTH_BASE_7','Juillet');
define('TEXT_MONTH_BASE_8','Août');
define('TEXT_MONTH_BASE_9','Septembre');
define('TEXT_MONTH_BASE_10','Octobre');
define('TEXT_MONTH_BASE_11','Novembre');
define('TEXT_MONTH_BASE_12','Décembre');
define('TEXT_MONTH_1','Janvier');
define('TEXT_MONTH_2','Février');
define('TEXT_MONTH_3','Mars');
define('TEXT_MONTH_4','Avril');
define('TEXT_MONTH_5','Mai');
define('TEXT_MONTH_6','Juin');
define('TEXT_MONTH_7','Juillet');
define('TEXT_MONTH_8','Août');
define('TEXT_MONTH_9','Septembre');
define('TEXT_MONTH_10','Octobre');
define('TEXT_MONTH_11','Novembre');
define('TEXT_MONTH_12','Décembre');
define('LANGUAGE_CURRENCY','UAH');
define('HTML_PARAMS','dir="LTR" lang="fr"');
define('CHARSET','utf-8');
define('TITLE','Boutique en ligne');
define('HEADER_TITLE_CREATE_ACCOUNT','Et');
define('HEADER_TITLE_CHECKOUT','Commander');
define('HEADER_TITLE_TOP','Accueil');
define('HEADER_TITLE_LOGOFF','Sortie');
define('HEAD_TITLE_CHECKOUT','La prsentation de la commande');

define('HEAD_TITLE_LOGIN', 'Se connecter');
define('HEAD_TITLE_COMPARE', 'Comparaison');
define('HEAD_TITLE_WISHLIST', 'Produits préférés');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Mot de passe oublié');

define('HEAD_TITLE_CHECKOUT_SUCCESS','Votre commande avec succès à la décoration!');
define('HEAD_TITLE_ACCOUNT','Votre cabinet');
define('HEAD_TITLE_ACCOUNT_HISTORY','Mes commandes');
define('HEAD_TITLE_ACCOUNT_EDIT','Afficher et modifier mes données');
define('HEAD_TITLE_ADDRESS_BOOK','Carnet d\'adresses');
define('HEAD_TITLE_ACCOUNT_PASSWORD','Modifier le mot de passe');
define('HEAD_TITLE_ALLCOMMENTS','Tous les commentaires');
define('HEAD_TITLE_CONTACT_US','Les contacts de la boutique '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML plan du site - '.STORE_NAME);
define('HEAD_TITLE_404','Erreur 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Erreur 403 - '.STORE_NAME);
define('BOX_SHOPPING_CART_EMPTY','Votre panier est vide');
define('BOX_INFORMATION_ALLPRODS','La liste complète des produits');
define('PULL_DOWN_DEFAULT','Sélectionnez');
define('PULL_DOWN_COUNTRY','Zone:');
define('TYPE_BELOW','Sélection ci-dessous');
define('JS_ERROR','');
define('JS_FIRST_NAME','');
define('JS_LAST_NAME','* Le champ \'Nom\' doit contenir au moins ' . ENTRY_LAST_NAME_MIN_LENGTH . ' caractères.');
define('JS_ERROR_SUBMITTED','Cette forme est déjà rempli. Appuyez Sur Ok.');
define('CATEGORY_COMPANY','L\'organisation');
define('CATEGORY_PERSONAL','Vos données personnelles');
define('CATEGORY_ADDRESS','Votre adresse');
define('CATEGORY_CONTACT','Coordonnées');
define('CATEGORY_OPTIONS','Newsletter');
define('ENTRY_COMPANY','Nom de l\'entreprise:');
define('ENTRY_COMPANY_ERROR','dsfds');
define('ENTRY_COMPANY_TEXT','');
define('ENTRY_FIRST_NAME','Nom:');
define('ENTRY_TEXT_FOM_IMAGE','Entrez le texte dans l\'image');
define('ENTRY_FIRST_NAME_ERROR','Le champ Nom doit contenir au moins ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' caractère.');
define('ENTRY_FIRST_NAME_TEXT','*');
define('ENTRY_LAST_NAME','Nom de famille:');
define('ENTRY_LAST_NAME_ERROR','Le champ Nom doit contenir au moins '.ENTRY_LAST_NAME_MIN_LENGTH.' caractère.');
define('ENTRY_LAST_NAME_TEXT','*');
define('ENTRY_DATE_OF_BIRTH','Date de naissance:');
define('ENTRY_DATE_OF_BIRTH_ERROR','La date de naissance est nécessaire d\'entrer dans le format suivant: JJ/MM/AAAA (exemple 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT','* (exemple 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS','E-Mail:');
define('ENTRY_EMAIL_ADDRESS_ERROR','Le champ E-Mail doit contenir au moins '.ENTRY_EMAIL_ADDRESS_MIN_LENGTH.' caractères.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR','Votre adresse E-Mail n\'est pas valide, essayez encore une fois.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS','Vous avez entré votre E-Mail est déjà enregistrée dans notre magasin, essayez d\'utiliser une autre adresse E-Mail.');
define('ENTRY_EMAIL_ADDRESS_TEXT','*');
define('ENTRY_STREET_ADDRESS','Adresse:');
define('ENTRY_STREET_ADDRESS_ERROR','Le champ de l\'Adresse et le numéro de la maison doit contenir au moins '.ENTRY_STREET_ADDRESS_MIN_LENGTH.' caractères.');
define('ENTRY_STREET_ADDRESS_TEXT','* Exemple: rue de l\'Hetman 1B, bureau. 2');
define('ENTRY_SUBURB','Secteur:');
define('ENTRY_SUBURB_ERROR','');
define('ENTRY_SUBURB_TEXT','');
define('ENTRY_POST_CODE','Code postal:');
define('ENTRY_POST_CODE_ERROR','Le champ Code postal doit contenir au moins '.ENTRY_POSTCODE_MIN_LENGTH.' caractère.');
define('ENTRY_POST_CODE_TEXT','*');
define('ENTRY_CITY','Ville:');
define('ENTRY_CITY_ERROR','Le champ de la Ville doit contenir au moins '.ENTRY_CITY_MIN_LENGTH.' caractère.');
define('ENTRY_CITY_TEXT','*');
define('ENTRY_STATE','Domaine:');
define('ENTRY_STATE_ERROR','Le champ doit contenir au moins '.ENTRY_STATE_MIN_LENGTH.' caractère.');
define('ENTRY_STATE_ERROR_SELECT','Sélectionnez la zone.');
define('ENTRY_STATE_TEXT','*');
define('ENTRY_COUNTRY','Pays:');
define('ENTRY_COUNTRY_ERROR','Sélectionnez un pays.');
define('ENTRY_COUNTRY_TEXT','*');
define('ENTRY_TELEPHONE_NUMBER','Téléphone:');
define('ENTRY_TELEPHONE_NUMBER_ERROR','Le champ Téléphone doit contenir au moins '.ENTRY_TELEPHONE_MIN_LENGTH.' caractère.');
define('ENTRY_TELEPHONE_NUMBER_TEXT','*');
define('ENTRY_FAX_NUMBER','Fax:');
define('ENTRY_FAX_NUMBER_ERROR','');
define('ENTRY_FAX_NUMBER_TEXT','');
define('ENTRY_NEWSLETTER','Recevoir les nouvelles');
define('ENTRY_NEWSLETTER_TEXT','Recevoir des informations sur les rabais, les prix, les cadeaux:');
define('ENTRY_NEWSLETTER_YES','Abonnez-vous');
define('ENTRY_NEWSLETTER_NO','Renoncer à l\'abonnement');
define('ENTRY_PASSWORD','Mot de passe:');
define('ENTRY_PASSWORD_ERROR','Votre mot de passe doit contenir au moins '.ENTRY_PASSWORD_MIN_LENGTH.' caractères.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING','Le champ Confirmer le mot de passe doit correspondre au champ Mot de passe.');
define('ENTRY_PASSWORD_CONFIRMATION','Confirmez le mot de passe:');
define('ENTRY_PASSWORD_CURRENT','Le mot de passe actuel:');
define('ENTRY_PASSWORD_CURRENT_ERROR','Le champ Mot de passe doit contenir au moins '.ENTRY_PASSWORD_MIN_LENGTH.' caractères.');
define('ENTRY_PASSWORD_NEW','Le nouveau mot de passe:');
define('ENTRY_PASSWORD_NEW_ERROR','Votre Nouveau mot de passe doit contenir au moins '.ENTRY_PASSWORD_MIN_LENGTH.' caractères.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING','Le champ Confirmer le mot de passe et le Nouveau mot de passe doivent être identiques.');
define('FORM_REQUIRED_INFORMATION','* Obligatoire');
define('TEXT_RESULT_PAGE','Page:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS','Montre <b>%d</b> - <b>%d</b> <b>%d</b> positions');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS','Montre <b>%d</b> - <b>%d</b> (<b>%d</b> commandes)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS','Montre <b>%d</b> - <b>%d</b> (<b>%d</b> des offres spéciales)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED','Montre <b>%d</b> - <b>%d</b> (<b>%d</b> produits phares)');
define('PREVNEXT_TITLE_PREVIOUS_PAGE','précédente');
define('PREVNEXT_TITLE_NEXT_PAGE','Page suivante');
define('PREVNEXT_TITLE_PAGE_NO','Page %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE','Précédente %d pages');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE','Les %d pages');
define('PREVNEXT_BUTTON_PREV','<<');
define('PREVNEXT_BUTTON_NEXT','>>');
define('PREVNEXT_TITLE_PPAGE','Page');
define('ART_XSELL_TITLE','Sur le thème de la');
define('PROD_XSELL_TITLE','Produits connexes');
define('IMAGE_BUTTON_ADD_ADDRESS','Ajouter une adresse');
define('IMAGE_BUTTON_BACK','De retour');
define('IMAGE_BUTTON_CHECKOUT','Commander');
define('IMAGE_BUTTON_CONFIRM_ORDER','Confirmer la commande');
define('IMAGE_BUTTON_CONTINUE','Continuez');
define('IMAGE_BUTTON_CONTINUE_SHOPPING','Continuer les achats');
define('IMAGE_BUTTON_DELETE','Supprimer');
define('IMAGE_BUTTON_LOGIN','Entrer');
define('IMAGE_BUTTON_IN_CART','Dans le panier');
define('IMAGE_BUTTON_ADDTO_CART','Acheter');
define('IMAGE_BUTTON_ADDTO_CART_2','Ajouter au panier');
define('IMAGE_BUTTON_ADDTO_CART_CLO','Dans le panier');
define('IMAGE_BUTTON_TEST_TEMPLATE','Essayez ce modèle gratuit!');
define('IMAGE_BUTTON_BUY_TEMPLATE','Achetez ce magasin!');
define('MESSAGE_BUY_TEMPLATE1','Malheureusement, votre test est une période de 14 jours est terminée. S\'il vous plaît ');
define('MESSAGE_BUY_TEMPLATE2','achetez');
define('MESSAGE_BUY_TEMPLATE3',' ce magasin ou en créer un autre site de test.');

define('CUSTOM_PANEL_EDIT', 'Modifier ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'les marchandises');
define('CUSTOM_PANEL_EDIT_MANUF', 'fabricant');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'article');
define('CUSTOM_PANEL_EDIT_SECTION', 'section');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'catégorie');
define('CUSTOM_PANEL_EDIT_SEO', 'seotext');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Panneau d\'administration');
define('CUSTOM_PANEL_ADD', 'Ajouter');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Produit');
define('CUSTOM_PANEL_ADD_PAGE', 'La page');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Remise');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Catégorie');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statistiques');
define('CUSTOM_PANEL_ADD_ONLINE', 'Sur le site: ');
define('CUSTOM_PANEL_PALETTE', 'Palette de sites');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Couleur du texte');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Tête d\'arrière-plan');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Pied de page d\'arrière-plan');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Couleur du lien');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Couleur du bouton');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Couleur de fond');
define('CUSTOM_PANEL_ORDERS', 'Commandes');
define('CUSTOM_PANEL_ORDERS_NEW', 'Nouveau: ');


define('CUSTOM_PANEL_DATE1', 'day');
define('CUSTOM_PANEL_DATE2', 'jours');
define('CUSTOM_PANEL_DATE3', 'jours');

define('IMAGE_BUTTON_UPDATE','Mettre à jour');
define('IMAGE_REDEEM_VOUCHER','Appliquer');
define('SMALL_IMAGE_BUTTON_VIEW','Regarder');
define('ICON_ERROR','Erreur');
define('ICON_SUCCESS','Remplie');
define('ICON_WARNING','Attention');
define('TEXT_GREETING_PERSONAL','Bienvenue, <span class="greetUser">%s!</span> Vous voulez voir les <a href="%s"><u>nouveaux produits</u></a> entrez dans notre magasin?');
define('TEXT_CUSTOMER_GREETING_HEADER','Bienvenue');
define('TEXT_GREETING_GUEST','Bienvenue, <span class="greetUser">CHER client!</span><br> Si Vous êtes un client régulier, <a href="%s"><u>saisissez Vos données personnelles</u></a> pour vous connecter. Si Vous nous avons, pour la première fois et que vous voulez faire un achat, Vous devez <a href="%s"><u>s\'inscrire</u></a>.');
define('TEXT_SORT_PRODUCTS','Tri:');
define('TEXT_DESCENDINGLY','desc');
define('TEXT_ASCENDINGLY','par ordre croissant');
define('TEXT_BY',' colonne ');
define('TEXT_UNKNOWN_TAX_RATE','Le taux d\'imposition est inconnue');
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE','Attention: le Magasin est fermé pour des raisons techniques, à: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE','Attention: le Magasin est fermé pour des raisons techniques');
define('WARNING_INSTALL_DIRECTORY_EXISTS','Avertissement: Ne pas supprimé le répertoire d\'installation de la boutique: ' . dirname($_SERVER['SCRIPT_FILENAME']) . 'install. S\'il vous plaît supprimer ce répertoire pour la sécurité.');
define('WARNING_CONFIG_FILE_WRITEABLE','Avertissement: le Fichier de configuration est disponible pour l\'enregistrement: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. C\'est un risque potentiel pour la sécurité - s\'il vous plaît installer des droits nécessaires pour accéder à ce fichier.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT','Avertissement: le répertoire des sessions n\'existe pas: '.tep_session_save_path().'. La session ne fonctionnera pas jusqu\'à ce que ce dossier ne sera pas créé.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE','Avertissement: Pas d\'accès au répertoire de sessions: '.tep_session_save_path().'. La session ne fonctionneront pas encore installé les droits d\'accès nécessaires.');
define('WARNING_SESSION_AUTO_START','Avertissement: l\'option de session.auto_start inclus - s\'il vous plaît désactiver cette option dans le fichier php.ini et redémarrez le serveur web.');
define('TEXT_CCVAL_ERROR_INVALID_DATE','Vous avez indiqué une date d\'expiration de la carte de crédit.<br>Essayez de nouveau.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER','Vous choisissez le mauvais numéro de carte de crédit.<br>Essayez de nouveau.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD','Les premiers chiffres de Votre carte de crédit: %s<br>Si Vous avez indiqué votre numéro de carte de crédit correctement, nous Vous informons que nous n\'acceptons pas de ce type de cartes de crédit.<br>Si Vous avez indiqué un numéro de carte de crédit mal, essayez encore une fois.');
define('BOX_LOGINBOX_HEADING','Entrée');
define('RENDER_LOGIN_WITH','Se connecter à l\'aide');
define('LOGIN_BOX_MY_CABINET','Votre cabinet');
define('MY_ORDERS_VIEW','Mes commandes');
define('MY_ACCOUNT_PASSWORD','Modifier le mot de passe');
define('LOGIN_BOX_ADDRESS_BOOK','Carnet d\'adresses');
define('LOGIN_BOX_LOGOFF','Sortie');
define('LOGIN_FROM_SITE','Entrer');
define('SORT_BY','Trier par');
define('LOGO_IMAGE_TITLE','Le logo');
define('BOX_INFORMATION_GUESTBOOK','Livre d\'or');
define('GUESTBOOK_TEXT_MIN_LENGTH','Dix');
define('JS_GUESTBOOK_TEXT','');
define('JS_GUESTBOOK_NAME','');
define('TEXT_DISPLAY_NUMBER_OF_GUESTBOOK_ENTRIES','Montre <b>%d</b> - <b>%d</b> (<b>%d</b> entrées)');
define('IMAGE_BUTTON_SIGN_GUESTBOOK','Ajouter un enregistrement');
define('TEXT_GUESTBOOK_DATE_ADDED','Date: %s');
define('TEXT_NO_GUESTBOOK_ENTRY','Jusqu\'à ce que il n\'ya aucune trace dans le livre d\'or. Soyez les premiers!');
define('BOX_ALL_ARTICLES','Tous les articles');
define('TEXT_NAVIGATION_BRANDS','Tous les fabricants');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES','<font color="#5a5a5a">Montre <b>%d</b> - <b>%d</b> (<b>%d</b> de nouvelles)</font>');
define('NAVBAR_TITLE_DEFAULT','Article');
define('PRICES_LOGGED_IN_TEXT','-');
define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO','Un minimum d\'unités de commande: ');
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART','Un minimum d\'unités de commande: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO','Étape: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART','Étape: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT','');
define('ERROR_PRODUCTS_QUANTITY_INVALID','Vous essayez de le mettre dans votre panier un nombre incorrect de la marchandise: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT','');
define('ERROR_PRODUCTS_UNITS_INVALID','Vous essayez de le mettre dans votre panier un nombre incorrect de la marchandise: ');
define('ADD_COMMENT_HEAD_TITLE','Laissez vos commentaires');
define('_RESULTS','Les résultats');
define('_VOTE','Voter');
define('_VOTES','Voix:');
define('PREV_NEXT_PRODUCT','Marchandises ');
define('PREV_NEXT_CAT',' catégorie ');
define('PREV_NEXT_MB',' le fabricant ');
define('BOX_HEADING_CATEGORIES','Sections');
define('BOX_HEADING_INFORMATION','Informations');
define('BOX_HEADING_MANUFACTURERS','Les fabricants');
define('BOX_HEADING_SPECIALS','Rabais');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Rabais %s');
define('BOX_HEADING_SEARCH','Recherche');
define('BOX_OPEN_SEARCH_FORM','Ouvrir la recherche');
define('BOX_HEADING_WHATS_NEW','Nouveautés');
define('BOX_HEADING_FEATURED','Recommandés');
define('BOX_HEADING_ARTICLES','Article');
define('BOX_HEADING_LINKS','L\'échange de liens');
define('BOX_HEADING_SHOPPING_ENTER','Entrer dans le panier');
define('BUTTON_SHOPPING_CART_OPEN','Ouvrir le panier');
define('HELP_HEADING','Consultant');
define('BOX_HEADING_WISHLIST','Produits préférés');
define('BOX_HEADING_BESTSELLERS','Les leaders des ventes');
define('BOX_HEADING_CURRENCY','La monnaie');
define('_POLLS','Les sondages');
define('BOX_HEADING_CALLBACK','Rappelez-moi');
define('BOX_HEADING_CONSULTANT','Consultant en ligne');
define('BOX_HEADING_PRODUCTS','marchandises');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS',' Ajoutez les catégories de produits.');
define('BOX_HEADING_LASTVIEWED','Vus les marchandises');
define('BOX_BESTSELLERS_NO','Dans cette section, jusqu\'à présent, aucun des leaders des ventes');
define('SHIPPING_OPTIONS','Les moyens et le coût de la livraison:');
define('LOW_STOCK_TEXT1','Les marchandises se termine: ');
define('LOW_STOCK_TEXT2','Code de l\'article: ');
define('LOW_STOCK_TEXT3','Nombre actuel: ');
define('LOW_STOCK_TEXT4','Lien vers le produit: ');
define('LOW_STOCK_TEXT5','La valeur actuelle de la variable de la Limite de la quantité de la marchandise dans l\'entrepôt: ');
define('BOX_TEXT_NO_ITEMS','Pas de produits différés.');
define('TOTAL_CART','Total');
define('TOTAL_TIME','Temps d\'exécution: ');
define('TEXT_PREFIX','txt_');
define('PRODUCTS_OPTIONS_VALUE_TEXT_ID','Zero');
define('SUB_TITLE_FROM','De:');
define('SUB_TITLE_REVIEW','Texte du message:');
define('SUB_TITLE_RATING','Note:');
define('ALSO_PURCHASED','Également avec ce produit commandé');
define('FORWARD','L\'avant');
define('COMP_PROD_NAME','Le nom de');
define('COMP_PROD_IMG','Image');
define('COMP_PROD_PRICE','Prix');
define('COMP_PROD_CLEAR','Effacer tous les');
define('COMP_PROD_BACK','Retour');
define('COMP_PROD_ADD_TO','Ajoutez des produits à la comparaison!');
define('TEXT_PASSWORD_FORGOTTEN','Mot de passe oublié?');
define('TEXT_PASSWORD_FORGOTTEN_DO','Récupérer le mot de passe');
define('QUICK_ORDER','Commande rapide');
define('QUICK_ORDER_SUCCESS','La demande est envoyée, dans un court laps de temps, Vous serez contacté par un gestionnaire');
define('QUICK_ORDER_BUTTON','Acheter en un seul clic');
define('SEND_MESSAGE','Envoyer');
define('LABEL_NEW','Nouveauté');
define('LABEL_TOP','TOP');
define('LABEL_SPECIAL','L\'action');
define('FILTER_BRAND','Marque');
define('FILTER_ALL','tous les');
define('WISHLIST_PC','pièces');
define('FOOTER_INFO','Informations');
define('FOOTER_CATEGORIES','Sections');
define('FOOTER_ARTICLES','Article');
define('FOOTER_CONTACTS','Contacts');
define('FOOTER_SITEMAP','Plan du site');
define('FOOTER_DEVELOPED','Le développement de la boutique');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Afficher toutes les catégories');
define('SHOW_ALL_ARTICLES','Afficher tous les articles');
define('TEXT_HEADING_PRICE','Répertoire:');
define('TEXT_HEADING_CATALOG','Catalogue');
define('TEXT_PRODUCTS_ATTRIBUTES','Les attributs');
define('TEXT_PRODUCTS_QTY','Le nombre de produits');
define('MAIN_NEWS','Nouvelles');
define('MAIN_NEWS_ALL','Toutes les nouvelles');
define('MAIN_NEWS_SUBSCRIBE','Abonnez-vous <span>nouvelles</span>');
define('MAIN_NEWS_SUBSCRIBE_BUT','Abonnez-vous');
define('MAIN_NEWS_EMAIL','Votre email');
define('MAIN_BESTSELLERS','TOP des ventes');
define('MAIN_REVIEWS','Commentaires des internautes');
define('MAIN_REVIEWS_ALL','Tous les commentaires');
define('MAIN_MOSTVIEWED','Vues de HAUT');
define('LIST_TEMP_INSTOCK','En présence de');
define('LIST_TEMP_OUTSTOCK','Non disponible');
define('HIGHSLIDE_CLOSE','Fermer');
define('BUTTON_CANCEL','Retour');
define('BUTTON_SEND','Envoyer');
define('LISTING_PER_PAGE','Sur la page:');
define('LISTING_SORT_NAME','Alphabétique, PUIS-JE');
define('LISTING_SORT_PRICE1','moins cher d\'en haut');
define('LISTING_SORT_PRICE2','plus cher d\'en haut');
define('LISTING_SORT_NEW','nouvelles d\'en haut');
define('LISTING_SORT_POPULAR','populaires');
define('LISTING_SORT_LIST','liste');
define('LISTING_SORT_COLUMNS','les colonnes');
define('PROD_DRUGIE','Produits similaires');
define('PROD_FILTERS','Les filtres');
define('PAGE404_TITLE','L\'Erreur 404');
define('PAGE404_DESC','La page n\'est pas trouvée');
define('PAGE404_REASONS','Peuvent être quelques raisons:');
define('PAGE404_REASON1','La page est déplacée ou renommée');
define('PAGE404_REASON2','La page n\'existe plus sur ce site');
define('PAGE404_REASON3','L\'URL ne correspond pas à la réalité');
define('PAGE404_BACK1','Revenir en arrière');
define('PAGE404_BACK2','Retour à l\'accueil');
define('PAGE403_DESC','Vous n\'êtes pas autorisé à afficher cette page.');
define('PAGE403_TITLE','Erreur 403');
define('PAGE403_TO_HOME_PAGE','Revenir à la page d\'accueil');
define('SB_SUBSCRIBER','L\'abonné');
define('SB_EMAIL_NAME','Candidature à la newsletter');
define('SB_EMAIL_USER','L\'utilisateur');
define('SB_EMAIL_WAS_SUBSCRIBED','me suis inscrit à la newsletter');
define('SB_EMAIL_ALREADY','déjà souscrit à la newsletter');
define('LOAD_MORE_BUTTON','Afficher plus');
define('TIME_LEFT','Jusqu\'à la fin de la version d\'essai');

define('CLO_NEW_PRODUCTS','Récemment ajouté des produits');
define('CLO_FEATURED','Ces produits vous intéressent');
define('CLO_DESCRIPTION_BESTSELLERS','TOP des ventes');
define('CLO_DESCRIPTION_SPECIALS','Rabais');
define('CLO_DESCRIPTION_MOSTVIEWED','Les plus populaires');
define('SHOPRULES_AGREE','En vous inscrivant, Vous acceptez');
define('SHOPRULES_AGREE2','conditions d\'utilisation de la boutique');
define('SHOW_ALL_SUBCATS','regarder tous les');
define('TEXT_PRODUCT_INFO_FREE_SHIPPING','Livraison gratuite');

//home
define('HOME_BOX_HEADING_SEARCH','la recherche de la...');
define('HEADER_TITLE_OR','ou');
define('HOME_IMAGE_BUTTON_ADDTO_CART','ajouter');
define('HOME_IMAGE_BUTTON_IN_CART','dans le panier');
define('HOME_ARTICLE_READ_MORE','lire plus');
define('HOME_MAIN_NEWS_SUBSCRIBE','abonnez-vous à la nouvelle newsletter et <span>obtenez un rabais de 25%</span> sur un produit en ligne de commande');
define('HOME_MAIN_NEWS_EMAIL','Entrez votre e-mail');
define('HOME_FOOTER_DEVELOPED','création de boutique en ligne');
define('HOME_FOOTER_CATEGORIES','Catalogue');
define('HOME_ADD_COMMENT_HEAD_TITLE','Ajouter un commentaire à l\'article');
define('HOME_ENTRY_FIRST_NAME_FORM','Le nom de');
define('HOME_ADD_COMMENT_FORM','Commentaire');
define('HOME_REPLY_TO_COMMENT','Répondre à ce commentaire');
define('HOME_PROD_DRUGIE','Vus les marchandises');
define('HOME_LOAD_MORE_INFO','lire la suite');
define('HOME_LOAD_ROLL_UP','réduire');
define('HOME_TITLE_LEFT_CATEGORIES','marchandises');
define('HOME_BOX_HEADING_SELL_OUT','Vente');
define('HOME_LOAD_MORE_BUTTON','téléchargement du produit');
define('HOME_BOX_HEADING_WISHLIST','Produits préférés');
define('HOME_PROD_VENDOR_CODE','modèle ');
define('HOME_BOX_HEADING_WHATS_STOCK','Les actions de la');
define('HOME_FOOTER_SOCIAL_TITLE', 'nous sommes dans les réseaux sociaux:');
define('HOME_HEADER_SOME_LIST', 'Voir aussi');

define('VK_LOGIN','Connexion');
define('SHOW_RESULTS','Tous les résultats');
define('ENTER_KEY','Entrez le texte');
define('TEXT_LIMIT_REACHED','Le nombre maximal de produits pour la comparaison: ');
define('RENDER_TEXT_ADDED_TO_CART','La marchandise a bien été ajouté au panier!');
define('CHOOSE_ADDRESS','Sélectionnez l\'adresse');
define('SEARCH_LANG','fr/');


//default2

define('DEMO2_TOP_PHONES', 'Téléphones');
define('DEMO2_TOP_CALLBACK', 'Rappel');
define('DEMO2_TOP_ONLINE', 'Chat en ligne');
define('DEMO2_SHOPPING_CART', 'Panier');
define('DEMO2_LOGIN_WITH', 'Se connecter avec ');
define('DEMO2_WISHLIST_LINK', 'Liste de souhaits');
define('DEMO2_FOOTER_CATEGORIES', 'Catalogue de produits');
define('DEMO2_MY_INFO', 'Editer les données');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Changer l\'adresse de livraison');
define('DEMO2_LEFT_CAT_TITLE', 'Les catégories');
define('DEMO2_LEFT_ALL_GOODS', 'Tous les produits');
define('DEMO2_TITLE_SLIDER_LINK', 'Voir tout');
define('DEMO2_READ_MORE', 'Lire plus <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Réduire <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Toutes les catégories');
define('DEMO2_SHOW_ALL_ARTICLES','Tous les articles');
define('DEMO2_BTN_COME_BACK','Retour');
define('DEMO2_SHARE_TEXT','Partagez ceci');
define('DEMO2_QUICK_ORDER_BUTTON', 'En un clic');

















