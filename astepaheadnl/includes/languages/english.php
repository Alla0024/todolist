<?php
/*
  $Id: english.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
@setlocale(LC_TIME, 'en_US.ISO_8859-1');
define('OG_LOCALE', 'en_US');
define('GO_COMPARE', 'Compare');
define('IN_WHISHLIST', 'Save');
define('COMPARE', 'Compare');
define('WHISH', 'Save');

//define('DATE_FORMAT_SHORT', '%m/%d/%Y');  // this is used for strftime()
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y y.'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT', 'm/d/Y'); // this is used for date()
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');

////
// Return date in raw format
// $date should be in format mm/dd/yyyy
// raw date is in format YYYYMMDD, or DDMMYYYY
define('TEXT_DAY_1','Monday');
define('TEXT_DAY_2','Tuesday');
define('TEXT_DAY_3','Environment');
define('TEXT_DAY_4','Thursday');
define('TEXT_DAY_5','Friday');
define('TEXT_DAY_6','Saturday');
define('TEXT_DAY_7','Sunday');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1','January');
define('TEXT_MONTH_BASE_2','February');
define('TEXT_MONTH_BASE_3','March');
define('TEXT_MONTH_BASE_4','April');
define('TEXT_MONTH_BASE_5','May');
define('TEXT_MONTH_BASE_6','June');
define('TEXT_MONTH_BASE_7','July');
define('TEXT_MONTH_BASE_8','August');
define('TEXT_MONTH_BASE_9','September');
define('TEXT_MONTH_BASE_10','October');
define('TEXT_MONTH_BASE_11','November');
define('TEXT_MONTH_BASE_12','December');
define('TEXT_MONTH_1','Jan');
define('TEXT_MONTH_2','Feb');
define('TEXT_MONTH_3','Mar');
define('TEXT_MONTH_4','APR');
define('TEXT_MONTH_5','May');
define('TEXT_MONTH_6','Jun');
define('TEXT_MONTH_7','July');
define('TEXT_MONTH_8','Aug');
define('TEXT_MONTH_9','Sep');
define('TEXT_MONTH_10','Oct');
define('TEXT_MONTH_11','Nov');
define('TEXT_MONTH_12','Dec');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'USD');

// Global entries for the <html> tag
define('HTML_PARAMS','dir="LTR" lang="en"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Your Store Name');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Create an Account');
define('HEADER_TITLE_CHECKOUT', 'Checkout');
define('HEADER_TITLE_TOP', 'Top');
define('HEADER_TITLE_LOGOFF', 'Log out');

define('HEAD_TITLE_LOGIN', 'Authorization');
define('HEAD_TITLE_COMPARE', 'Compare');
define('HEAD_TITLE_WISHLIST', 'Favorite products');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Forgotten password');

define('HEAD_TITLE_CHECKOUT','Checkout');
define('HEAD_TITLE_CHECKOUT_SUCCESS','Your order has been successfully processed!');
define('HEAD_TITLE_ACCOUNT','My account');
define('HEAD_TITLE_ACCOUNT_HISTORY','My orders');
define('HEAD_TITLE_ACCOUNT_EDIT','Edit my data');
define('HEAD_TITLE_ADDRESS_BOOK','Address book');
define('HEAD_TITLE_ACCOUNT_PASSWORD','Change password');
define('HEAD_TITLE_ALLCOMMENTS','All comments');
define('HEAD_TITLE_CONTACT_US','AStepAhead - Contact');
define('HEAD_TITLE_PRICE','HTML sitemap - '.STORE_NAME);
define('HEAD_TITLE_404','Error 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Error 403 - '.STORE_NAME);

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Cart is empty');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'View All Items');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Please Select');
define('PULL_DOWN_COUNTRY', 'Region:');
define('TYPE_BELOW', 'Type Below');

// javascript messages
define('JS_ERROR', 'Errors have occured during the process of your form.\n\nPlease make the following corrections:\n\n');


define('JS_FIRST_NAME', '* The \'First Name\' must contain a minimum of ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' characters.\n');
define('JS_LAST_NAME', '* The \'Last Name\' must contain a minimum of ' . ENTRY_LAST_NAME_MIN_LENGTH . ' characters.\n');




define('JS_ERROR_SUBMITTED', 'This form has already been submitted. Please press Ok and wait for this process to be completed.');


define('CATEGORY_COMPANY', 'Company Details');
define('CATEGORY_PERSONAL', 'Your Personal Details');
define('CATEGORY_ADDRESS', 'Your Address');
define('CATEGORY_CONTACT', 'Your Contact Information');
define('CATEGORY_OPTIONS', 'Options');

define('ENTRY_COMPANY', 'Company Name:');
define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_FIRST_NAME', 'First Name:');
define('ENTRY_TEXT_FOM_IMAGE', 'Enter text from image');
define('ENTRY_FIRST_NAME_ERROR', 'Required field');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Last Name:');
define('ENTRY_LAST_NAME_ERROR', 'Required field');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Date of Birth:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Your Date of Birth must be in this format: MM/DD/YYYY (eg 05/21/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (eg. 05/21/1970)');
define('ENTRY_EMAIL_ADDRESS', 'Email');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'enter a valid email address (e.g. johndoe@domain.com).');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Your E-Mail Address does not appear to be valid - please make any necessary corrections.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Your E-Mail Address already exists in our records - please log in with the e-mail address or create an account with a different address.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Street Address:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Enter a valid address (e.g. street name 1).');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB', 'Suburb:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Postal Code:');
define('ENTRY_POST_CODE_ERROR', 'enter a valid zip code (eg 1234 AB).');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Place:');
define('ENTRY_CITY_ERROR', 'Required field');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'State/Province:');
define('ENTRY_STATE_ERROR', 'Required field');
define('ENTRY_STATE_ERROR_SELECT', 'State:');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Country:');
define('ENTRY_COUNTRY_ERROR', 'You must select a country from the Countries pull down menu.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Telephone number:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'enter a valid phone number (minimum three digits).');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Fax Number:');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Subscribe to our newsletter');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Subscribed');
define('ENTRY_NEWSLETTER_NO', 'Unsubscribed');
define('ENTRY_PASSWORD', 'Password:');
define('ENTRY_PASSWORD_ERROR', 'Your Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'The Password Confirmation must match your Password.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Password Confirmation:');
define('ENTRY_PASSWORD_CURRENT', 'Current Password:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Your Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_NEW', 'New Password:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Your new Password must contain a minimum of ' . ENTRY_PASSWORD_MIN_LENGTH . ' characters.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'The Password Confirmation must match your new Password.');

define('FORM_REQUIRED_INFORMATION', '* Required information');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Result Pages:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> products)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> orders)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> specials)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> featured products)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Previous Page');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Next Page');
define('PREVNEXT_TITLE_PAGE_NO', 'Page %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Previous Set of %d Pages');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Next Set of %d Pages');
define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;Prev]');
define('PREVNEXT_BUTTON_NEXT', '[Next&nbsp;&gt;&gt;]');

define('PREVNEXT_TITLE_PPAGE', 'Page');

define('ART_XSELL_TITLE', 'Similar content');
define('PROD_XSELL_TITLE', 'Similar products');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Add Address');
define('IMAGE_BUTTON_BACK', 'Back');
define('IMAGE_BUTTON_CHECKOUT', 'Checkout');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Confirm Order');
define('IMAGE_BUTTON_CONTINUE', 'Continue');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continue Shopping');
define('IMAGE_BUTTON_DELETE', 'Delete');
define('IMAGE_BUTTON_LOGIN', 'Login');

define('IMAGE_BUTTON_IN_CART', 'Added');
define('IMAGE_BUTTON_ADDTO_CART', 'Add to cart');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Add to Cart');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'Add to <b>Cart</b>');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Try this template for FREE!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Buy this template now!');
define('MESSAGE_BUY_TEMPLATE1', 'Unfortunately, your test period of 14 days is over. Please ');
define('MESSAGE_BUY_TEMPLATE2', 'buy');
define('MESSAGE_BUY_TEMPLATE3', ' this store or create another test site.');

define('CUSTOM_PANEL_EDIT', 'Edit ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'product');
define('CUSTOM_PANEL_EDIT_MANUF', 'manufacturer');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'article');
define('CUSTOM_PANEL_EDIT_SECTION', 'section');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'category');
define('CUSTOM_PANEL_EDIT_SEO', 'seo text');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Admin panel');
define('CUSTOM_PANEL_ADD', 'Add');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Product');
define('CUSTOM_PANEL_ADD_PAGE', 'Page');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Discount');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Category');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statistics');
define('CUSTOM_PANEL_ADD_ONLINE', 'Online: ');
define('CUSTOM_PANEL_PALETTE', 'Site palette');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Text color');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Header background');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Background footer');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Link color');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Button color');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Background color');
define('CUSTOM_PANEL_ORDERS', 'Orders');
define('CUSTOM_PANEL_ORDERS_NEW', 'New: ');



define('CUSTOM_PANEL_DATE1', 'day');
define('CUSTOM_PANEL_DATE2', 'days');
define('CUSTOM_PANEL_DATE3', 'days');


define('IMAGE_BUTTON_UPDATE', 'Update');
define('IMAGE_REDEEM_VOUCHER', 'Submit');

define('SMALL_IMAGE_BUTTON_VIEW', 'View');


define('ICON_ERROR', 'Error');
define('ICON_SUCCESS', 'Success');
define('ICON_WARNING', 'Warning');

define('TEXT_GREETING_PERSONAL', 'Welcome back <span class="greetUser">%s!</span> Would you like to see which <a href="%s"><u>new products</u></a> are available to purchase?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Our Customer Greeting');
define('TEXT_GREETING_GUEST', 'Welcome <span class="greetUser">Guest!</span> Would you like to <a href="%s"><u>log yourself in</u></a>? Or would you prefer to <a href="%s"><u>create an account</u></a>?');

define('TEXT_SORT_PRODUCTS', 'Sort products:');
define('TEXT_DESCENDINGLY', 'descendingly');
define('TEXT_ASCENDINGLY', 'ascendingly');
define('TEXT_BY', ' by ');

define('TEXT_UNKNOWN_TAX_RATE', 'Unknown tax rate');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'NOTICE: This website will be down for maintenance on: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'NOTICE: the website is currently Down For Maintenance to the public');

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Warning: Installation directory exists at: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Please remove this directory for security reasons.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Warning: I am able to write to the configuration file: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. This is a potential security risk - please set the right user permissions on this file.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Warning: The sessions directory does not exist: ' . tep_session_save_path() . '. Sessions will not work until this directory is created.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Warning: I am not able to write to the sessions directory: ' . tep_session_save_path() . '. Sessions will not work until the right user permissions are set.');
define('WARNING_SESSION_AUTO_START', 'Warning: session.auto_start is enabled - please disable this php feature in php.ini and restart the web server.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'The expiry date entered for the credit card is invalid.<br>Please check the date and try again.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'The credit card number entered is invalid.<br>Please check the number and try again.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'The first four digits of the number entered are: %s <br>If that number is correct, we do not accept that type of credit card.<br>If it is wrong, please try again.');






define('BOX_LOGINBOX_HEADING', 'Your account');
define('LOGIN_BOX_MY_CABINET','My account');
define('LOGIN_BOX_ADDRESS_BOOK','Address book');
define('LOGIN_BOX_LOGOFF','Log out');

define('LOGIN_FROM_SITE','My account');
define('RENDER_LOGIN_WITH', 'Login with');
define('HEADER_TITLE_OR','or');

define('LOGO_IMAGE_TITLE','Logo');

define('SORT_BY','Sort by');


// Article Manager
define('BOX_ALL_ARTICLES', 'All Articles');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> articles)');
define('NAVBAR_TITLE_DEFAULT', 'Articles');


//TotalB2B start
define('PRICES_LOGGED_IN_TEXT','-'); // define('PRICES_LOGGED_IN_TEXT','Must be logged in for prices!');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO','Order Minumum is: ');
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART','Order Minimum is: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO','Order in Units of: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART','Order in Units of: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT','');
define('ERROR_PRODUCTS_QUANTITY_INVALID','Invalid Qty: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT','');
define('ERROR_PRODUCTS_UNITS_INVALID','Invalid Units: ');

// Comments
define('ADD_COMMENT_HEAD_TITLE', 'Add your comment');

// Poll Box Text
define('_RESULTS', 'Results');
define('_VOTE', 'Vote');
define('_VOTES', 'Votes:');

define('PREV_NEXT_PRODUCT', 'Product ');
define('PREV_NEXT_CAT', ' of category ');
define('PREV_NEXT_MB', ' of manafacturer ');


// English names of boxes

define('BOX_HEADING_CATEGORIES', 'Categories');
define('BOX_HEADING_INFORMATION', 'Information');
define('BOX_HEADING_MANUFACTURERS', 'Brands');
define('BOX_HEADING_SPECIALS', 'Specials');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Specials %s');
define('BOX_HEADING_SEARCH', 'Search for...');
define('BOX_OPEN_SEARCH_FORM', 'Open Search Form');
define('BOX_HEADING_WHATS_NEW', 'Latest products');
define('BOX_HEADING_FEATURED', 'Eye-catchers');
define('BOX_HEADING_ARTICLES', 'Articles');
define('BOX_HEADING_LINKS', 'Links');
define('HELP_HEADING', 'Help');
define('BOX_HEADING_WISHLIST', 'Your wishlist');
define('BOX_HEADING_BESTSELLERS', 'Best sellers');
define('BOX_HEADING_CURRENCY', 'Currency');
define('_POLLS', 'Polls');
define('BOX_HEADING_CALLBACK', '+31 172 233 223');
define('BOX_HEADING_CONSULTANT', 'Online chat');
define('BOX_HEADING_PRODUCTS', 'Item(s)');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Add category of items.');
define('BOX_HEADING_LASTVIEWED', 'You previously viewed');

define('BOX_BESTSELLERS_NO', 'There is no bestsellers yet');


  define('LOW_STOCK_TEXT1','Low stock warning: ');
  define('LOW_STOCK_TEXT2','Model No.: ');
  define('LOW_STOCK_TEXT3','Quantity: ');
  define('LOW_STOCK_TEXT4','Product URL: ');
  define('LOW_STOCK_TEXT5','Current Low order limit is ');

// wishlist box text in includes/boxes/wishlist.php

  define('BOX_TEXT_NO_ITEMS', 'No items in your wishlist.');
    define('BUTTON_SHOPPING_CART_OPEN', 'Open Shopping Cart');
  define('TOTAL_TIME', 'Execution time: ');
  define('TOTAL_CART', 'Total incl. VAT');
    
// otf 1.71 defines needed for Product Option Type feature.
  define('TEXT_PREFIX', 'txt_');
  define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES
  
  // Product tabs

define('ALSO_PURCHASED', 'Also purchased');

define('FORWARD', 'Forward');

define('COMP_PROD_NAME','Name');
define('COMP_PROD_IMG','Image');
define('COMP_PROD_PRICE','Price');
define('COMP_PROD_CLEAR','Clear all');
define('COMP_PROD_BACK','Back');
define('COMP_PROD_ADD_TO','Add products to compare!');
define('TEXT_PASSWORD_FORGOTTEN', 'Forgot password?');
define('TEXT_PASSWORD_FORGOTTEN_DO', 'Reset password');
define('QUICK_ORDER','Get a call from AStepAhead');
define('QUICK_ORDER_SUCCESS','Your application is submitted, we will contact you soon');
define('QUICK_ORDER_BUTTON','Quick Buy');
define('SEND_MESSAGE','Send');

define('LABEL_NEW','New');
define('LABEL_TOP','TOP');
define('LABEL_SPECIAL','Used');

define('FILTER_BRAND','Brand');
define('FILTER_ALL','all');

define('WISHLIST_PC','piece(s)');

define('FOOTER_INFO','Customer service');
define('FOOTER_CATEGORIES','Categories');
define('FOOTER_ARTICLES','Articles');
define('FOOTER_CONTACTS','Contact');
define('FOOTER_SITEMAP','Sitemap');
define('FOOTER_DEVELOPED','');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');

define('SHOW_ALL_CATS','Show all categories');
define('SHOW_ALL_ARTICLES','Show all articles');
define('TEXT_NAVIGATION_BRANDS','All brands');

define('TEXT_HEADING_PRICE', 'Catalog:');
define('TEXT_HEADING_CATALOG', 'Categories');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Attributes');
define('TEXT_PRODUCTS_QTY', 'Products Quantity');

define('MAIN_NEWS','News');
define('MAIN_NEWS_ALL','All news');
define('MAIN_NEWS_SUBSCRIBE','Subscribe <span>for news</span>');
define('MAIN_NEWS_SUBSCRIBE_BUT','Subscribe');
define('MAIN_NEWS_EMAIL','Your email');

define('MAIN_BESTSELLERS','Best sellers');
define('MAIN_REVIEWS','Reviews');
define('MAIN_REVIEWS_ALL','All reviews');
define('MAIN_MOSTVIEWED','Most viewed');

define('LIST_TEMP_INSTOCK','In stock');
define('LIST_TEMP_OUTSTOCK','Out of stock');

define('HIGHSLIDE_CLOSE','Close');
define('BUTTON_CANCEL','Close');
define('BUTTON_SEND','Send');

define('LISTING_PER_PAGE','Per page:');
define('LISTING_SORT_NAME','Alphabetically, A-Z');
define('LISTING_SORT_PRICE1','Price: Low-High');
define('LISTING_SORT_PRICE2','Price: High-Low');
define('LISTING_SORT_NEW','Newest');
define('LISTING_SORT_POPULAR','Popularity');
define('LISTING_SORT_LIST','list');
define('LISTING_SORT_COLUMNS','columns');

define('PROD_DRUGIE','Similar products');
define('PROD_FILTERS','Filters');

define('PAGE404_TITLE','Error 404');
define('PAGE404_DESC','Page not found');
define('PAGE404_REASONS','There may be several reasons:');
define('PAGE404_REASON1','This page was moved or renamed');
define('PAGE404_REASON2','This page is no longer exists on this site');
define('PAGE404_REASON3','Your URL is wrong');
define('PAGE404_BACK1','Back to previous page');
define('PAGE404_BACK2','Back to Home Page');

// page 403
define('PAGE403_DESC', 'You do not have permission to view this page.');
define('PAGE403_TITLE','Error 403');
define('PAGE403_TO_HOME_PAGE','Return to home page');


define('SB_SUBSCRIBER','Subscriber');
define('SB_EMAIL_NAME','Request for newsletters');
define('SB_EMAIL_USER','User');
define('SB_EMAIL_WAS_SUBSCRIBED','was subscribed for newsletters');
define('SB_EMAIL_ALREADY','have already subscribed in our newsletters!');
define("LOAD_MORE_BUTTON", "Load more");
define("TIME_LEFT", "Until the end of the trial version");

// CLO
define('CLO_NEW_PRODUCTS', 'Recently added products');
define('CLO_FEATURED', 'Eye-catchers');
define('CLO_DESCRIPTION_BESTSELLERS', 'Best sellers');
define('CLO_DESCRIPTION_SPECIALS', 'Discounts');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Mostviewed');

define('SHOPRULES_AGREE', 'By signing up you agree to our  ');
define('SHOPRULES_AGREE2', '&nbsp<a href=\'en/terms-and-conditions/a-131.html\' target=\'_blank\'>Terms and Conditions</a>');

define("SHOW_ALL_SUBCATS", "show all");

define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Free shipping");

//home
define('HOME_BOX_HEADING_SEARCH', 'find...');
define('HEADER_TITLE_OR', 'or');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'add');
define('HOME_IMAGE_BUTTON_IN_CART', 'Added');
define('HOME_ARTICLE_READ_MORE', 'read more');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'subscribe to a new newsletter and <span> get a 25% discount </span> for one product in an online order');
define('HOME_MAIN_NEWS_EMAIL', 'Enter your e-mail');
define('HOME_FOOTER_DEVELOPED', 'online store creation');
define('HOME_FOOTER_CATEGORIES', 'Categories');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Add a comment to the product');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Name');
define('HOME_ADD_COMMENT_FORM', 'Comment');
define('HOME_REPLY_TO_COMMENT', 'Reply to comment');
define('HOME_PROD_DRUGIE', 'You previously viewed');
define("HOME_LOAD_MORE_INFO", "more");
define("HOME_LOAD_ROLL_UP", "roll up");
define("HOME_TITLE_LEFT_CATEGORIES", "products");
define('HOME_BOX_HEADING_SELL_OUT', 'Sell-out');
define("HOME_LOAD_MORE_BUTTON", "download more products");
define("HOME_BOX_HEADING_WISHLIST", "Favorite products");
define("HOME_PROD_VENDOR_CODE", "vendor code ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Stock');
define('HOME_FOOTER_SOCIAL_TITLE', 'we are in social networks:');
define('HOME_HEADER_SOME_LIST', 'See also');


define('SEARCH_LANG', 'en/');

define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'All results');
define('ENTER_KEY', 'Enter keywords');
define('TEXT_LIMIT_REACHED', 'Maximum products in compares reached: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Product was successfully added to your cart!');
define('CHOOSE_ADDRESS', 'Choose address');


//default2

define('DEMO2_TOP_PHONES', 'Telephones');
define('DEMO2_TOP_CALLBACK', 'Callback');
define('DEMO2_TOP_ONLINE', 'Online chat');
define('DEMO2_SHOPPING_CART', 'Basket');
define('DEMO2_LOGIN_WITH', 'Login with ');
define('DEMO2_WISHLIST_LINK', 'A wish list');
define('DEMO2_FOOTER_CATEGORIES', 'Catalog');
define('DEMO2_MY_INFO', 'To change the data');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Change shipping address');
define('DEMO2_LEFT_CAT_TITLE', 'Categories');
define('DEMO2_LEFT_ALL_GOODS', 'All goods');
define('DEMO2_TITLE_SLIDER_LINK', 'See all');
define('DEMO2_READ_MORE', 'Read more <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Roll up <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','All categories');
define('DEMO2_SHOW_ALL_ARTICLES','All articles');
define('DEMO2_BTN_COME_BACK','Come back');
define('DEMO2_SHARE_TEXT','Share');
define('DEMO2_QUICK_ORDER_BUTTON', 'In one click');
define('MENU_ACCOUNT', 'My account');

define('TABLE_HEADING_TOTAL_EXCLUDING_TAX','Total (excl. btw)');




