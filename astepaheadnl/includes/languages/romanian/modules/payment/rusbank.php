<?php
/*
  $Id: rusbank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_RUS_BANK_TEXT_TITLE', 'Transfer bancar');
define('MODULE_PAYMENT_RUS_BANK_TEXT_DESCRIPTION', 'Transfer Bancar');
define('MODULE_PAYMENT_RUS_BANK_TEXT_EMAIL_FOOTER', "Our bank details:<br>Bank name: " . MODULE_PAYMENT_RUS_BANK_1 . "<br>Account number: " . MODULE_PAYMENT_RUS_BANK_2 . "<br>BIC: " . MODULE_PAYMENT_RUS_BANK_3 . "<br>Account number 2: " . MODULE_PAYMENT_RUS_BANK_4 . "<br>TIN: " . MODULE_PAYMENT_RUS_BANK_5 . "<br>Recipient: " . MODULE_PAYMENT_RUS_BANK_6 . "<br>Recipient 2: " . MODULE_PAYMENT_RUS_BANK_7 . "<br>Purpose: " . MODULE_PAYMENT_RUS_BANK_8 . "<br><br>After payment of the order, please inform us by e-mail " . STORE_OWNER_EMAIL_ADDRESS . " about your payment. Your order will be sent immediately after confirming the fact of payment.");