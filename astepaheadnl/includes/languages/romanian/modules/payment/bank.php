<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Bank Transfer');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', "Our bank details:<br>Bank name: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'') . "<br>Account number: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'') . "<br>BIC: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'') . "<br>Account number 2: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'') . "<br>TIN: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'') . "<br>Recipient: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'') . "<br>Recipient 2: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'') . "<br>Purpose: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'') . "<br><br>After payment of the order, please inform us by e-mail " . STORE_OWNER_EMAIL_ADDRESS . " about your payment. Your order will be sent immediately after confirming the fact of payment.<br><br><a href=kvitan.php target=_blank><b><span>Receipt for payment</a></b>");
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Our bank details:<br>Bank name: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'') . "<br>Account number: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'') . "<br>BIC: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'') . "<br>Account number 2: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'') . "<br>TIN: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'') . "<br>Recipient: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'') . "<br>Recipient 2: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'') . "<br>Purpose: " . (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'') . "<br><br>After payment of the order, please inform us by e-mail " . STORE_OWNER_EMAIL_ADDRESS . " about your payment. Your order will be sent immediately after confirming the fact of payment.");