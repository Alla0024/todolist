<?php
/*
  $Id: password_forgotten.php,v 1.8 2003/06/09 22:46:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Login');
define('NAVBAR_TITLE_2', 'Parola uitata');
define('HEADING_TITLE', 'Am uitat parola!');
define('TEXT_MAIN', 'Daca ati uitat parola introduceti adresa de email mai jos si va vom trimite o alta parola.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND', 'Adresa de email nu exista in baza noastra de date.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT', STORE_NAME . ' - Parola noua');
define('EMAIL_PASSWORD_REMINDER_BODY', 'O noua parola a fost solicitata de ' . $REMOTE_ADDR . '.' . "\n\n" . 'Parola noua pentru \'' . STORE_NAME . '\' este:' . "\n\n" . '   %s' . "\n\n");
define('SUCCESS_PASSWORD_SENT', 'O noua parola a fost trimisa pe adresa de email.');