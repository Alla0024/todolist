<?php
/*
  $Id: contact_us.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'CONTACTS');
define('HEADING_SUBTITLE', 'Contact us now and get your own Internet-shop &amp; easy-to-use cms for the online store!');
define('NAVBAR_TITLE', 'Contact Us');
define('TEXT_SUCCESS', 'Your enquiry has been successfully sent to the Store Owner.');
define('EMAIL_SUBJECT', 'Enquiry from ' . STORE_NAME);
define('ENTRY_NAME', 'Full Name:');
define('ENTRY_EMAIL', 'E-Mail Address:');
define('ENTRY_PHONE', 'Phone number:');
define('ENTRY_ENQUIRY', 'Enquiry:');
define('SEND_TO_TEXT', 'Send to:');
define('POP_CONTACT_US','Email us and we`ll call you!');