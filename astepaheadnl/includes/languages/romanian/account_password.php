<?php
/*
  $Id: account_password.php,v 1.1.1.1 2003/09/18 19:04:31 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'My Account');
define('NAVBAR_TITLE_2', 'Change Password');

define('HEADING_TITLE', 'Parola cont');

define('MY_PASSWORD_TITLE', 'Parola cont');

define('SUCCESS_PASSWORD_UPDATED', 'Your password has been successfully updated.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING', 'Parola nu corepsunde. Incercati din nou.
');
?>
