<?php
/*
  $Id: index.php,v 1.1 2003/06/11 17:38:00 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '');
define('TABLE_HEADING_DATE_EXPECTED', 'Date Expected');
define('TABLE_HEADING_DEFAULT_SPECIALS', 'Oferte speciale pentru %s');

if ( ($category_depth == 'products') || (isset($_GET['manufacturers_id'])) ) {
  define('HEADING_TITLE', 'Let\'s See What We Have Here');
  define('TABLE_HEADING_IMAGE', '');
  define('TABLE_HEADING_MODEL', 'Model');
  define('TABLE_HEADING_PRODUCTS', 'Denumire');
  define('TABLE_HEADING_MANUFACTURER', 'Brand');
  define('TABLE_HEADING_QUANTITY', 'Cantitate');
  define('TABLE_HEADING_PRICE', 'Pret');
  define('TABLE_HEADING_WEIGHT', 'Greutate');
  define('TEXT_ALL_MANUFACTURERS', 'Toate brandurile');
} elseif ($category_depth == 'top') {
  define('HEADING_TITLE', 'What\'s New Here?');
} elseif ($category_depth == 'nested') {
  define('HEADING_TITLE', 'Categorii');
}
 	// BOF Wolfen featured sets
  define('TABLE_HEADING_PRICE', 'Pret');
  define('TEXT_NO_FEATURED_PRODUCTS', '');
 	// EOF Wolfen featured sets
?>