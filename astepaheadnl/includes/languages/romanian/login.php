<?php
/*
  $Id: login.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Login');
define('HEADING_TITLE', 'Bine ati venit. Va invitam sa va logati.');
define('TEXT_NEW_CUSTOMER', 'Sunt client nou');
define('TEXT_NEW_CUSTOMER_INTRODUCTION', 'Prin crearea unui cont pe ' . STORE_NAME . ' veti putea vizualiza istoricul comenzilor si sa efectuati mai usor comenzile ulterioare.
');
define('TEXT_RETURNING_CUSTOMER', 'Sunt client inregistrat');
define('TEXT_PASSWORD_FORGOTTEN', 'Password forgotten? Click here.');
define('TEXT_LOGIN_ERROR', 'Eroare: adresa de email sau parola nu se potrivesc');
// guest_account start
define('NO_CUSTOMER', 'Ceva nu se potriveste. Nu avem acest user in baza de date');
define('NO_PASS', 'Parola gresita');
define('SOC_VK', 'VK');
define('SOC_SITE', 'site');
define('SOC_ENTER_FROM_OTHER', 'Login from another social network or enter your login/password.');
define('SOC_NOW_FROM', 'You are trying to login from');
define('SOC_NEED_FROM', 'You was registered via');
define('SOC_LOGIN_THX', 'You was successfully login');