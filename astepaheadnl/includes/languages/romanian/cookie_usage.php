<?php
/*
  $Id: cookie_usage.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Cookie Usage');
define('HEADING_TITLE', 'Cookie Usage');
define('TEXT_INFORMATION', 'Browserul dumneavoastra nu suporta fisierele tip cookie sau a fost setat pe dezactivare fisiere cookies. Pentru realizarea comenzilor online este necesara activarea acestora. 

');
define('BOX_INFORMATION', 'Fisierele tip cookie sunt necesare pentru utilizarea acestui website. Acestea ajuta la buna functionare a website-ului atunci cand efectuati comenzile.

');
