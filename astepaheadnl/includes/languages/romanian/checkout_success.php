<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Checkout');
define('NAVBAR_TITLE_2', 'Success');
define('HEADING_TITLE', 'Comanda a fost transmisa.');
define('TEXT_SUCCESS', 'Your order has been successfully processed! Your products will arrive at their destination within 2-5 working days.');
define('TEXT_THANKS_FOR_SHOPPING', 'Cerere comanda transmisa. <br />Mutumim!');
define('TABLE_HEADING_COMMENTS', 'Enter a comment for the order processed');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Expiry date: ');
define('TABLE_HEADING_DOWNLOAD_COUNT', ' downloads remaining');
define('SMS_NEW_ORDER', 'You have new order');
define('TEXT_SUCCESS_INFO', 'Toate instrucțiunile și detaliile suplimentare pentru plată v-au fost trimise prin poștă');