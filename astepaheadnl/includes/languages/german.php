<?php
/*
  $Id: russian.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
//@setlocale(LC_TIME, 'en_US.ISO_8859-1');  
@setlocale(LC_TIME, 'de_DE.UTF-8');
define('OG_LOCALE', 'de_DE');
define('GO_COMPARE', 'Im Vergleich');
define('IN_WHISHLIST', 'In Wünschen');
define('COMPARE', 'Vergleichen');
define('WHISH', 'Wunsch');

// HMCS: Begin Autologon   ******************************************************************
// HMCS: End Autologon     ******************************************************************
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // DATE_FORMAT_SHORTthis is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y y.'); // this is used for strftime()
//define('DATE_FORMAT_SHORT', '%d/%m/%Y');
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT', 'd.m.Y h:i:s'); // this is used for date()
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


define('TEXT_DAY_1','Montag');
define('TEXT_DAY_2','Dienstag');
define('TEXT_DAY_3','Mittwoch');
define('TEXT_DAY_4','Donnerstag');
define('TEXT_DAY_5','Freitag');
define('TEXT_DAY_6','Samstag');
define('TEXT_DAY_7','Sonntag');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1','Januar');
define('TEXT_MONTH_BASE_2','Februar');
define('TEXT_MONTH_BASE_3','März');
define('TEXT_MONTH_BASE_4','April');
define('TEXT_MONTH_BASE_5','Mai');
define('TEXT_MONTH_BASE_6','Juni');
define('TEXT_MONTH_BASE_7','Juli');
define('TEXT_MONTH_BASE_8','August');
define('TEXT_MONTH_BASE_9','September');
define('TEXT_MONTH_BASE_10','Oktober');
define('TEXT_MONTH_BASE_11','November');
define('TEXT_MONTH_BASE_12','Dezember');
define('TEXT_MONTH_1','Januar');
define('TEXT_MONTH_2','Februar');
define('TEXT_MONTH_3','März');
define('TEXT_MONTH_4','April');
define('TEXT_MONTH_5','Mai');
define('TEXT_MONTH_6','Juni');
define('TEXT_MONTH_7','Juli');
define('TEXT_MONTH_8','August');
define('TEXT_MONTH_9','September');
define('TEXT_MONTH_10','Oktober');
define('TEXT_MONTH_11','November');
define('TEXT_MONTH_12','Dezember');

// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'EUR');

// Global entries for the <html> tag
define('HTML_PARAMS', 'dir="LTR" lang="de"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Online-Shop');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Registrierung');
define('HEADER_TITLE_CHECKOUT', 'Eine Bestellung aufgeben');
define('HEADER_TITLE_TOP', 'Zuhause');
define('HEADER_TITLE_LOGOFF', 'Beenden');

define('HEAD_TITLE_LOGIN', 'Login');
define('HEAD_TITLE_COMPARE', 'Vergleich');
define('HEAD_TITLE_WISHLIST', 'Lieblingsprodukte');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Passwort vergessen');

define('HEAD_TITLE_CHECKOUT', 'Bestellreihenfolge');
define('HEAD_TITLE_CHECKOUT_SUCCESS', 'Ihre Bestellung wurde erfolgreich abgeschlossen!');
define('HEAD_TITLE_ACCOUNT', 'Ihr Büro');
define('HEAD_TITLE_ACCOUNT_HISTORY', 'Meine Befehle');
define('HEAD_TITLE_ACCOUNT_EDIT', 'Meine Daten anzeigen und bearbeiten');
define('HEAD_TITLE_ADDRESS_BOOK', 'Adressbuch');
define('HEAD_TITLE_ACCOUNT_PASSWORD', 'Passwort ändern');
define('HEAD_TITLE_ALLCOMMENTS', 'Alle Kommentare');
define('HEAD_TITLE_CONTACT_US','Kontakte des Ladens '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML sitemap - '.STORE_NAME);
define('HEAD_TITLE_404','Error 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Error 403 - '.STORE_NAME);

// text for gender
define('MALE', 'Männlich');
define('FEMALE', 'Weiblich');


// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Der Korb ist leer');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Vollständige Liste der Produkte');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Auswählen');
define('PULL_DOWN_COUNTRY', 'Bereich:');
define('TYPE_BELOW', 'Wahl unten');

// javascript messages
define('JS_ERROR', 'Fehler beim Ausfüllen des Formulars!\n\nKorrigiere bitte:\n\n');


define('JS_FIRST_NAME', '* Feld \'Name\' muss mindestens enthalten ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' Symbole.\n');
define('JS_LAST_NAME', '* Feld \'Nachname\' muss mindestens enthalten ' . ENTRY_LAST_NAME_MIN_LENGTH . ' Symbole.\n');


define('JS_ERROR_SUBMITTED', 'Diese Form ist bereits voll. Drücken Sie auf OK.');


define('CATEGORY_COMPANY', 'Organisation');
define('CATEGORY_PERSONAL', 'Ihre persönlichen Informationen');
define('CATEGORY_ADDRESS', 'Ihre Adresse');
define('CATEGORY_CONTACT', 'Kontakte');
define('CATEGORY_OPTIONS', 'Newsletter');

define('ENTRY_COMPANY', 'Name des Unternehmens:');
define('ENTRY_COMPANY_ERROR', 'dsfds');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_GENDER', 'Geschlecht:');
define('ENTRY_GENDER_ERROR', 'Sie müssen Ihr Geschlecht angeben.');
define('ENTRY_GENDER_TEXT', '*');
define('ENTRY_FIRST_NAME', 'Name:');
define('ENTRY_TEXT_FOM_IMAGE', 'Geben Sie Text aus dem Bild ein');
define('ENTRY_FIRST_NAME_ERROR', 'Das Feld Name muss mindestens enthalten ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' Symbol.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Nachname:');
define('ENTRY_LAST_NAME_ERROR','Das Feld Nachname muss mindestens enthalten ' . ENTRY_LAST_NAME_MIN_LENGTH . ' Symbol.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'Geburtsdatum:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Das Geburtsdatum muss im folgenden Format eingegeben werden: DD/MM/YYYY (Beispiel 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (Beispiel 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'E-Mail:');
define('ENTRY_EMAIL_ADDRESS_ERROR', ' Feld E-Mail muss mindestens enthalten ' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' Symbole.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Ihre E-Mail-Adresse ist nicht korrekt, versuchen Sie es erneut.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Die von Ihnen angegebene E-Mail-Adresse ist bereits in unserem Geschäft registriert. Versuchen Sie, eine andere E-Mail-Adresse anzugeben.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Adresse:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Das Adressfeld und die Hausnummer müssen mindestens enthalten sein ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' Symbole.');
define('ENTRY_STREET_ADDRESS_TEXT', '* Beispiel: st. Getman 1B, Büro. 2');
define('ENTRY_SUBURB', 'Bereich:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Postleitzahl:');
define('ENTRY_POST_CODE_ERROR', 'Das Postleitzahlenfeld sollte mindestens enthalten ' . ENTRY_POSTCODE_MIN_LENGTH . ' Symbol.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'Stadt:');
define('ENTRY_CITY_ERROR', 'Das Feld der Stadt muss mindestens enthalten ' . ENTRY_CITY_MIN_LENGTH . ' Symbol.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Region:');
define('ENTRY_STATE_ERROR', 'Das Feld muss mindestens enthalten sein ' . ENTRY_STATE_MIN_LENGTH . ' Symbol.');
define('ENTRY_STATE_ERROR_SELECT', 'Wählen Sie einen Bereich.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Land:');
define('ENTRY_COUNTRY_ERROR', 'Wählen Sie das Land aus.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Telefon:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Telefonfeld muss mindestens enthalten sein ' . ENTRY_TELEPHONE_MIN_LENGTH . ' Symbol.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Fax:');
define('ENTRY_FAX_NUMBER_ERROR', '');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Um Informationen über Rabatte, Preise, Geschenke zu erhalten:');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Jetzt abonnieren');
define('ENTRY_NEWSLETTER_NO', 'Abbestellen');
define('ENTRY_PASSWORD', 'Passwort:');
define('ENTRY_PASSWORD_ERROR', 'Ihr Passwort muss mindestens enthalten sein ' . ENTRY_PASSWORD_MIN_LENGTH . ' Symbolе.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Das Feld Passwort bestätigen muss mit dem Feld Passwort übereinstimmen.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Bestätigen Sie das Passwort:');
define('ENTRY_PASSWORD_CURRENT', 'Aktuelles Passwort:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Passwortfeld muss mindestens enthalten ' . ENTRY_PASSWORD_MIN_LENGTH . ' Symbolе.');
define('ENTRY_PASSWORD_NEW', 'Neues Passwort:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Ihr neues Passwort muss mindestens enthalten sein ' . ENTRY_PASSWORD_MIN_LENGTH . ' Symbolе.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Passwort bestätigen und Neues Passwort muss übereinstimmen.');

define('FORM_REQUIRED_INFORMATION', '* Muss ausgefüllt werden');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Seiten:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Zeige <b>%d</b> - <b>%d</b> von der <b>%d</b> Positionen ');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Zeige <b>%d</b> - <b>%d</b> (insgesamt <b>%d</b> Bestellungen)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Zeige <b>%d</b> - <b>%d</b> (insgesamt <b>%d</b> spezielle Angebote)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Zeige <b>%d</b> - <b>%d</b> (insgesamt <b>%d</b> empfohlene Produkte)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'vorherige');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Nächste Seite');
define('PREVNEXT_TITLE_PAGE_NO', 'Seite %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Vorherige %d Seiten ');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Nächste %d Seiten ');
define('PREVNEXT_BUTTON_PREV', '<<');
define('PREVNEXT_BUTTON_NEXT', '>>');

define('PREVNEXT_TITLE_PPAGE', ' Seite ');

define('ART_XSELL_TITLE', 'Über das Thema');
define('PROD_XSELL_TITLE', 'Verwandte Produkte');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Adresse hinzufügen');
define('IMAGE_BUTTON_BACK', 'Zurück');
define('IMAGE_BUTTON_CHECKOUT', 'Eine Bestellung aufgeben');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Bestellung bestätigen');
define('IMAGE_BUTTON_CONTINUE', 'Weiter');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continue Shopping');
define('IMAGE_BUTTON_DELETE', 'Löschen');
define('IMAGE_BUTTON_LOGIN', 'Anmelden');
define('IMAGE_BUTTON_IN_CART', 'In den Korb');
define('IMAGE_BUTTON_ADDTO_CART', 'Kaufen');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Kaufen');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'In den Korb ');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Kostenlos testen!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Kaufe diesen Laden jetzt!');
define('MESSAGE_BUY_TEMPLATE1', 'Leider ist Ihre Testphase von 14 Tagen vorbei. Bitte, ');
define('MESSAGE_BUY_TEMPLATE2', 'kaufen');
define('MESSAGE_BUY_TEMPLATE3', ' Laden oder erstellen Sie eine andere Testseite.');


define ('CUSTOM_PANEL_EDIT', 'Bearbeiten ');
define ('CUSTOM_PANEL_EDIT_PRODUCT', 'die ware');
define ('CUSTOM_PANEL_EDIT_MANUF', 'hersteller');
define ('CUSTOM_PANEL_EDIT_ARTICLE', 'artikel');
define ('CUSTOM_PANEL_EDIT_SECTION', 'abschnitt');
define ('CUSTOM_PANEL_EDIT_CATEGORY', 'kategorie');
define ('CUSTOM_PANEL_EDIT_SEO', 'seotext');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Admin-Bereich');
define('CUSTOM_PANEL_ADD', 'Hinzufügen');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Produkt');
define('CUSTOM_PANEL_ADD_PAGE', 'Seite');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Rabatt');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Kategorie');
define('CUSTOM_PANEL_ADD_STATISTICS', 'Statistiken');
define('CUSTOM_PANEL_ADD_ONLINE', 'Auf der Seite: ');
define('CUSTOM_PANEL_PALETTE', 'Site-Palette');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Textfarbe');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Header Hintergrund');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Hintergrundfußzeile');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Linkfarbe');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Tastenfarbe');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Hintergrundfarbe');
define('CUSTOM_PANEL_ORDERS', 'Bestellungen');
define('CUSTOM_PANEL_ORDERS_NEW', 'Neu: ');


define('CUSTOM_PANEL_DATE1', 'Tag');
define('CUSTOM_PANEL_DATE2', 'Tage');
define('CUSTOM_PANEL_DATE3', 'Tage');


define('IMAGE_BUTTON_UPDATE', 'Aktualisieren');
define('IMAGE_REDEEM_VOUCHER', 'Übernehmen');

define('SMALL_IMAGE_BUTTON_VIEW', 'Zusehen');


define('ICON_ERROR', 'Fehler');
define('ICON_SUCCESS', ' Fertig ');
define('ICON_WARNING', 'Achtung');

define('TEXT_GREETING_PERSONAL', 'Willkommen zurück, <span class="greetUser">%s!</span> du willst was sehen <a href="%s"><u> neue Produkte </u></a> trat in unseren Laden ein?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Willkommen zurück ');
define('TEXT_GREETING_GUEST', 'Willkommen zurück, <span class="greetUser"> LIEBE GÄSTE!</span><br> Wenn Sie unser Stammkunde sind, <a href="%s"><u> geben Sie Ihre persönlichen Daten ein </u></a> um einzutreten. Wenn Sie zum ersten Mal bei uns sind und Einkäufe tätigen möchten, brauchen Sie <a href="%s"><u> registrieren </u></a>.');

define('TEXT_SORT_PRODUCTS', 'Sortierung:');
define('TEXT_DESCENDINGLY', 'absteigend');
define('TEXT_ASCENDINGLY', 'aufsteigende Reihenfolge');
define('TEXT_BY', ', Spalte ');

define('TEXT_UNKNOWN_TAX_RATE', 'Unbekannter Steuersatz');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'Warnung: Der Laden ist aus technischen Gründen vorher geschlossen: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'Warnung: Der Laden ist aus technischen Gründen vorher geschlossen ');

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Warnung: Das Verzeichnis für die Installation des Geschäfts wurde nicht gelöscht: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/install. Bitte löschen Sie dieses Verzeichnis aus Sicherheitsgründen.');
define('WARNING_CONFIG_FILE_WRITEABLE', ' Warnung: Die Konfigurationsdatei ist beschreibbar: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/includes/configure.php. Dies ist ein potenzielles Sicherheitsrisiko - stellen Sie bitte die erforderlichen Berechtigungen für diese Datei ein.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Warnung: Sitzungsverzeichnis existiert nicht: ' . tep_session_save_path() . '. Sitzungen funktionieren erst, wenn dieses Verzeichnis erstellt wurde.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Warnung: Zugriff auf das Sitzungsverzeichnis nicht möglich: ' . tep_session_save_path() . '. Sitzungen funktionieren erst, wenn die erforderlichen Zugriffsrechte festgelegt sind.');
define('WARNING_SESSION_AUTO_START', 'Warnung: Option session.auto_start aktiviert - bitte deaktivieren Sie diese Option in der Datei php.ini und starten Sie den Webserver neu.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'Sie haben ein ungültiges Ablaufdatum der Kreditkarte angegeben. <br> Versuchen Sie es erneut.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Sie haben die falsche Kreditkartennummer eingegeben. <br> Versuchen Sie es erneut.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Die ersten Ziffern Ihrer Kreditkarte: %s<br> Wenn Sie Ihre Kreditkartennummer richtig angegeben haben, informieren wir Sie, dass wir diese Art von Kreditkarte nicht akzeptieren. <br> Wenn Sie eine Kreditkartennummer falsch angegeben haben, versuchen Sie es erneut. ');

define('BOX_LOGINBOX_HEADING', 'Eingabe');
define('IMAGE_BUTTON_LOGIN', 'Einloggen');
define('RENDER_LOGIN_WITH', 'Login with');

define('LOGIN_BOX_MY_CABINET', 'Ihr Büro');
define('MY_ORDERS_VIEW', 'Meine Befehle');
define('MY_ACCOUNT_PASSWORD', 'Passwort ändern ');
define('LOGIN_BOX_ADDRESS_BOOK', 'Adressbuch ');
define('LOGIN_BOX_LOGOFF', 'Beenden');

define('LOGIN_FROM_SITE', 'Einloggen');


define('LOGO_IMAGE_TITLE','"Logo"');

// VJ Guestbook for OSC v1.0 begin
define('BOX_INFORMATION_GUESTBOOK', 'Gästebuch ');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('GUESTBOOK_TEXT_MIN_LENGTH', '10'); //[TODO] move to config db table
define('JS_GUESTBOOK_TEXT', '* Das Feld \'Ihre Nachricht\' muss mindestens enthalten ' . GUESTBOOK_TEXT_MIN_LENGTH . ' Symbole.\n');
define('JS_GUESTBOOK_NAME', '* Sie müssen das Feld \'Ihr Name ausfüllen\'.\n');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_DISPLAY_NUMBER_OF_GUESTBOOK_ENTRIES', 'Zeige <b>%d</b> - <b>%d</b> (insgesamt <b>%d</b> Aufzeichnungen)');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('IMAGE_BUTTON_SIGN_GUESTBOOK', 'Eintrag hinzufügen');
// VJ Guestbook for OSC v1.0 end

// VJ Guestbook for OSC v1.0 begin
define('TEXT_GUESTBOOK_DATE_ADDED', ' Datum: %s');
define('TEXT_NO_GUESTBOOK_ENTRY', 'Es sind noch keine Einträge im Gästebuch vorhanden. Sei der Erste!');
// VJ Guestbook for OSC v1.0 end


// Article Manager
define('BOX_ALL_ARTICLES', 'Alle Artikel');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', '<font color="#5a5a5a">Zeige <b>%d</b> - <b>%d</b> (insgesamt<b>%d</b> Nachrichten)</font>');
define('NAVBAR_TITLE_DEFAULT', 'Artikel');
define('TEXT_NAVIGATION_BRANDS','Alle Hersteller');

//TotalB2B start
define('PRICES_LOGGED_IN_TEXT', ''); //define('PRICES_LOGGED_IN_TEXT', '<b><font style="color:#CE1930">Preis: </b></font><a href="create_account.tpl.php">nur Großhandel</a>');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO', 'Mindestbestellmengeneinheiten: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART', 'Mindestbestellmengeneinheiten: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO', 'Schritt: '); // order_detail.php
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART', 'Schritt: '); // order_detail.php
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT', '');
define('ERROR_PRODUCTS_QUANTITY_INVALID', 'Sie versuchen, falsche Warenmengen in den Einkaufswagen zu legen: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT', '');
define('ERROR_PRODUCTS_UNITS_INVALID', 'Sie versuchen, falsche Warenmengen in den Einkaufswagen zu legen: ');

// Comments 

define('ADD_COMMENT_HEAD_TITLE', 'Schreiben Sie Ihre eigene Rezension ');

// Poll Box Text
define('_RESULTS', 'Ergebnisse');
define('_VOTE', 'Stimme');
define('_VOTES', 'Stimmen:');

define('PREV_NEXT_PRODUCT', 'Produkt ');
define('PREV_NEXT_CAT', ' Kategorien');
define('PREV_NEXT_MB', ' Hersteller');


// Russische Namen von Boxen

define('BOX_HEADING_CATEGORIES', 'Abschnitte');
define('BOX_HEADING_INFORMATION', 'Informationen');
define('BOX_HEADING_MANUFACTURERS', 'Hersteller');
define('BOX_HEADING_SPECIALS', 'Rabatte');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Rabatte %s');
define('BUTTON_SHOPPING_CART_OPEN', 'Open Shopping Cart');
define('BOX_HEADING_SEARCH', 'Suche');
define('BOX_OPEN_SEARCH_FORM', 'Open Search Form');
define('BOX_HEADING_WHATS_NEW', ' Neuheiten ');
define('BOX_HEADING_FEATURED', ' Empfehlungen ');
define('BOX_HEADING_ARTICLES', ' Artikel ');
define('BOX_HEADING_LINKS', 'Linkaustausch');
define('BOX_HEADING_SHOPPING_ENTER', 'In den Warenkorb einloggen');
define('HELP_HEADING', 'Consultant');
define('BOX_HEADING_WISHLIST', 'Aufgeschobene Waren');
define('BOX_HEADING_BESTSELLERS', 'Führer der Verkäufe');
define('BOX_HEADING_CURRENCY', 'Währung');
define('_POLLS', 'Umfragen');
define('BOX_HEADING_CALLBACK', 'Ruf mich zurück');
define('BOX_HEADING_CONSULTANT', ' Online - Consultant ');
define('BOX_HEADING_PRODUCTS', 'Waren');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Fügen Sie Produktkategorien hinzu.');
define('BOX_HEADING_LASTVIEWED', 'Angesehene Produkte');

define('BOX_BESTSELLERS_NO', 'In diesem Bereich gibt es noch keine Verkaufsleiter');

// Wege und Kosten der Lieferung in den Warenkorb
define('SHIPPING_OPTIONS', 'Wege und Kosten der Lieferung:');


define('LOW_STOCK_TEXT1', 'Das Produkt ist nicht auf Lager: ');
define('LOW_STOCK_TEXT2', 'Artikelcode: ');
define('LOW_STOCK_TEXT3', 'Aktuelle Menge: ');
define('LOW_STOCK_TEXT4', 'Link zum Produkt: ');
define('LOW_STOCK_TEXT5', 'Aktueller Wert der Variablen Das Mengenlimit im Lager: ');

// wishlist box text in includes/boxes/wishlist.php

define('BOX_TEXT_NO_ITEMS', 'Es gibt keine zurückgestellten Waren.');


define('TOTAL_TIME', 'Ausführungszeit: ');
define('TOTAL_CART', 'Summe');


// otf 1.71 defines needed for Product Option Type feature.
define('TEXT_PREFIX', 'txt_');
define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES

//define('NAVBAR_TITLE', 'Einkaufswagen');
define('SUB_TITLE_FROM', 'Von:');
define('SUB_TITLE_REVIEW', 'Nachrichtentext:');
define('SUB_TITLE_RATING', 'Bewertung:');

// Product tabs

define('ALSO_PURCHASED', 'Auch mit diesem Produkt bestellt');


// Paginator
define('FORWARD', 'Vorwärts');
define('COMP_PROD_NAME', 'Titel');
define('COMP_PROD_IMG', 'Bild');
define('COMP_PROD_PRICE', 'Preis');
define('COMP_PROD_CLEAR', 'Klar alles');
define('COMP_PROD_BACK', 'Zurück');
define('COMP_PROD_ADD_TO', 'Fügen Sie Produkte zum Vergleichen hinzu!');
define('TEXT_PASSWORD_FORGOTTEN', 'Passwort vergessen?');
define('QUICK_ORDER', 'Schnelle Bestellung');
define('QUICK_ORDER_SUCCESS', 'Die Anwendung wurde gesendet, der Manager wird Sie in Kürze kontaktieren. ');
define('QUICK_ORDER_BUTTON', 'Kaufen mit einem Klick');
define('SEND_MESSAGE', 'Senden');

define('LABEL_NEW', 'Neu');
define('LABEL_TOP', ' TOP ');
define('LABEL_SPECIAL', 'Teilen');

define('FILTER_BRAND', 'Marke');
define('FILTER_ALL', 'alles');
define('WISHLIST_PC', 'Stück');

define('FOOTER_INFO', 'Informationen');
define('FOOTER_CATEGORIES', 'Abschnitte');
define('FOOTER_ARTICLES', 'Artikel');
define('FOOTER_CONTACTS', 'Kontakte');
define('FOOTER_SITEMAP', 'Sitema');
define('FOOTER_DEVELOPED', 'Entwicklung des Ladens');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Zeige alle Kategorien');
define('SHOW_ALL_ARTICLES','Zeige alle Artikel');

define('TEXT_HEADING_PRICE', 'Katalog:');
define('TEXT_HEADING_CATALOG', 'Katalog');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Attributes');
define('TEXT_PRODUCTS_QTY', 'Products Quantity');

define('MAIN_NEWS', 'Nachrichten');
define('MAIN_NEWS_ALL', 'Alle Nachrichten');
define('MAIN_NEWS_SUBSCRIBE', 'NEWS ABONNIEREN');
define('MAIN_NEWS_SUBSCRIBE_BUT', 'Abonnieren');
define('MAIN_NEWS_EMAIL', 'Ihre E-Mail-Adresse');

define('MAIN_BESTSELLERS', 'Top-Verkäufe ');
define('MAIN_REVIEWS', 'Kundenbewertungen');
define('MAIN_REVIEWS_ALL', 'Alle Bewertungen');
define('MAIN_MOSTVIEWED', 'TOP Ansichten ');

define('LIST_TEMP_INSTOCK', 'Auf Lager');
define('LIST_TEMP_OUTSTOCK', 'Nicht verfügbar');

define('HIGHSLIDE_CLOSE', 'Schließen');
define('BUTTON_CANCEL', 'Abbrechen');
define('BUTTON_SEND', 'Senden');

define('LISTING_PER_PAGE', 'Gehe zur Seite:');
define('LISTING_SORT_NAME', 'Alphabetisch, A-J');
define('LISTING_SORT_PRICE1', 'billiger von oben');
define('LISTING_SORT_PRICE2', 'teurer von oben');
define('LISTING_SORT_NEW', 'neu von oben');
define('LISTING_SORT_POPULAR', 'beliebt von oben');
define('LISTING_SORT_LIST', 'Liste');
define('LISTING_SORT_COLUMNS', 'Spalten');

define('PROD_DRUGIE', 'Verwandte Produkte');
define('PROD_FILTERS', 'Filter');

define('PAGE404_TITLE', '404 Error');
define('PAGE404_DESC', 'Seite nicht gefunden');
define('PAGE404_REASONS', 'Es kann mehrere Gründe geben:');
define('PAGE404_REASON1', 'Die Seite wurde verschoben oder umbenannt');
define('PAGE404_REASON2', 'Seiten existieren nicht mehr auf dieser Seite');
define('PAGE404_REASON3', 'URL ist nicht wahr');
define('PAGE404_BACK1', 'Zurück');
define('PAGE404_BACK2', 'Zurück zum Home');
// 403
define('PAGE403_DESC', 'Sie sind nicht berechtigt, diese Seite anzuzeigen.');
define('PAGE403_TITLE', 'Error 403');
define('PAGE403_TO_HOME_PAGE', 'Zurück zur Hauptseite');

define('SB_SUBSCRIBER', 'Subscriber');
define('SB_EMAIL_NAME', 'Anfrage zur Verteilung');
define('SB_EMAIL_USER', 'Benutzer');
define('SB_EMAIL_WAS_SUBSCRIBED', 'abonniert den Newsletter');
define('SB_EMAIL_ALREADY','haben bereits in unseren Newslettern abonniert!');
define("LOAD_MORE_BUTTON", "Zeige mehr");
define("TIME_LEFT", "Bis zum ende der testversion");

define('CLO_NEW_PRODUCTS', 'Kürzlich hinzugefügte Produkte');
define('CLO_FEATURED', 'Diese Produkte werden dich interessieren');
define('CLO_DESCRIPTION_BESTSELLERS', 'TOP sales');
define('CLO_DESCRIPTION_SPECIALS', 'Rabatte');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Meist gesehen');

define('SHOPRULES_AGREE', 'Ich bin einverstanden mit');
define('SHOPRULES_AGREE2', 'geschäftsregeln');

define("SHOW_ALL_SUBCATS", "beobachte alles");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Kostenloser Versand");

//home
define('HOME_BOX_HEADING_SEARCH', 'suchen...');
define('HEADER_TITLE_OR', 'oder');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'addieren');
define('HOME_IMAGE_BUTTON_IN_CART', 'in den Korb');
define('HOME_ARTICLE_READ_MORE', 'Lesen Sie mehr');
define('HOME_MAIN_NEWS_SUBSCRIBE', 'Abonnieren Sie einen neuen Newsletter und <span> erhalten Sie 25% Rabatt </span> für ein Produkt in einer Online-Bestellung');
define('HOME_MAIN_NEWS_EMAIL', 'Geben Sie Ihre E-Mail-Adresse ein');
define('HOME_FOOTER_DEVELOPED', 'Online-Shop-Erstellung');
define('HOME_FOOTER_CATEGORIES', 'Katalog');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Fügen Sie dem Produkt einen Kommentar hinzu');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Vorname');
define('HOME_ADD_COMMENT_FORM', 'Kommentar');
define('HOME_REPLY_TO_COMMENT', 'Antwort auf Kommentar');
define('HOME_PROD_DRUGIE', 'Gesehene Produkte');
define("HOME_LOAD_MORE_INFO", "mehr");
define("HOME_LOAD_ROLL_UP", "aufrollen");
define("HOME_TITLE_LEFT_CATEGORIES", "Waren");
define('HOME_BOX_HEADING_SELL_OUT', 'Ausverkauf');
define("HOME_LOAD_MORE_BUTTON", "weitere Produkte herunterladen");
define("HOME_BOX_HEADING_WISHLIST", "Lieblingsprodukte");
define("HOME_PROD_VENDOR_CODE", "artikelnummer ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Aktie');
define('HOME_FOOTER_SOCIAL_TITLE', 'wir sind in sozialen Netzwerken:');
define('HOME_HEADER_SOME_LIST', 'Siehe auch');

define('SEARCH_LANG', 'de/');

define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'Alle Ergebnisse');
define('ENTER_KEY', 'Enter keywords');
define('TEXT_LIMIT_REACHED', 'Maximum products in compares reached: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Product was successfully added to your cart!');
define('CHOOSE_ADDRESS', 'Wählen Sie die Adresse');

//default2

define('DEMO2_TOP_PHONES', 'Telefone');
define('DEMO2_TOP_CALLBACK', 'Rückruf');
define('DEMO2_TOP_ONLINE', 'Online-Chat');
define('DEMO2_SHOPPING_CART', 'Korb');
define('DEMO2_LOGIN_WITH', 'Einloggen mit ');
define('DEMO2_WISHLIST_LINK', 'Wunschliste');
define('DEMO2_FOOTER_CATEGORIES', 'Produktkatalog');
define('DEMO2_MY_INFO', 'Daten bearbeiten');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Lieferadresse ändern');
define('DEMO2_LEFT_CAT_TITLE', 'Kategorien');
define('DEMO2_LEFT_ALL_GOODS', 'Alle Waren');
define('DEMO2_TITLE_SLIDER_LINK', 'Alles sehen');
define('DEMO2_READ_MORE', 'Weiterlesen <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Aufrollen <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Alle Kategorien');
define('DEMO2_SHOW_ALL_ARTICLES','Alle Artikel');
define('DEMO2_BTN_COME_BACK','Zurückkommen');
define('DEMO2_SHARE_TEXT','Teile dies');
define('DEMO2_QUICK_ORDER_BUTTON', 'Mit einem klick');
















