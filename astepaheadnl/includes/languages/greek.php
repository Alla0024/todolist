<?php
/*
  $Id: english.php,v 1.1.1.1 2003/09/18 19:04:27 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

// look in your $PATH_LOCALE/locale directory for available locales
// or type locale -a on the server.
// Examples:
// on RedHat try 'en_US'
// on FreeBSD try 'en_US.ISO_8859-1'
// on Windows try 'en', or 'English'
@setlocale(LC_TIME, 'el_GR.UTF-8');
define('OG_LOCALE', 'el_GR');
define('GO_COMPARE', 'Πηγαίνετε να συγκρίνετε');
define('IN_WHISHLIST', 'Σε λίστα επιθυμιών');
define('COMPARE', 'Συγκρίνετε');
define('WHISH', 'Στη λίστα επιθυμιών');

//define('DATE_FORMAT_SHORT', '%d/%m/%Y');  // this is used for strftime()
define('DATE_FORMAT_SHORT', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT_LONG', '%A %d %B, %Y'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d %B %Y г.'); // this is used for strftime()
//define('DATE_FORMAT_LONG', '%d.%m.%Y'); // this is used for strftime()
define('DATE_FORMAT_LONG', DEFAULT_FORMATED_DATE_FORMAT);
//define('DATE_FORMAT', 'd/m/Y'); // this is used for date()
define('DATE_FORMAT', DEFAULT_DATE_FORMAT);
define('DATE_TIME_FORMAT', DATE_FORMAT_SHORT . ' %H:%M:%S');


define('TEXT_DAY_1', 'Δευτέρα');
define('TEXT_DAY_2', 'Τρίτη');
define('TEXT_DAY_3', 'Τετάρτη');
define('TEXT_DAY_4', 'Πέμπτη');
define('TEXT_DAY_5', 'Παρασκευή');
define('TEXT_DAY_6', 'Σάββατο');
define('TEXT_DAY_7', 'Κυριακή');
define('TEXT_DAY_SHORT_1','MON');
define('TEXT_DAY_SHORT_2','TUE');
define('TEXT_DAY_SHORT_3','WED');
define('TEXT_DAY_SHORT_4','THU');
define('TEXT_DAY_SHORT_5','FRI');
define('TEXT_DAY_SHORT_6','SAT');
define('TEXT_DAY_SHORT_7','SUN');
define('TEXT_MONTH_BASE_1', 'Ιανουάριος');
define('TEXT_MONTH_BASE_2', 'Φεβρουάριος');
define('TEXT_MONTH_BASE_3', 'Μάρτιος');
define('TEXT_MONTH_BASE_4', 'Απρίλιος');
define('TEXT_MONTH_BASE_5', 'Μάιος');
define('TEXT_MONTH_BASE_6', 'Ιούνιος');
define('TEXT_MONTH_BASE_7', 'Ιούλιος');
define('TEXT_MONTH_BASE_8', 'Αύγουστος');
define('TEXT_MONTH_BASE_9', 'Σεπτέμβριος');
define('TEXT_MONTH_BASE_10', 'Οκτώβριος');
define('TEXT_MONTH_BASE_11', 'Νοέμβριος');
define('TEXT_MONTH_BASE_12', 'Δεκέμβριος');
define('TEXT_MONTH_1', 'Ιανουάριος');
define('TEXT_MONTH_2', 'Φεβρουάριος');
define('TEXT_MONTH_3', 'Martha');
define('TEXT_MONTH_4', 'Απρίλιος');
define('TEXT_MONTH_5', 'Μάιος');
define('TEXT_MONTH_6', 'Ιούνιος');
define('TEXT_MONTH_7', 'Ιούλιος');
define('TEXT_MONTH_8', 'Αύγουστος');
define('TEXT_MONTH_9', 'Σεπτέμβριος');
define('TEXT_MONTH_10', 'Οκτώβριος');
define('TEXT_MONTH_11', 'Νοέμβριος');
define('TEXT_MONTH_12', 'Δεκέμβριος');
// if USE_DEFAULT_LANGUAGE_CURRENCY is true, use the following currency, instead of the applications default currency (used when changing language)
define('LANGUAGE_CURRENCY', 'EUR');

// Global entries for the <html> tag
define('HTML_PARAMS','dir="LTR" lang="el"');

// charset for web pages and emails
define('CHARSET', 'utf-8');

// page title
define('TITLE', 'Ηλεκτρονικό κατάστημα');

// header text in includes/header.php
define('HEADER_TITLE_CREATE_ACCOUNT', 'Δημιουργία λογαριασμούt');
define('HEADER_TITLE_CHECKOUT', 'Ολοκλήρωση');
define('HEADER_TITLE_TOP', 'Ανω');
define('HEADER_TITLE_LOGOFF', 'Αποσύνδεση');

define('HEAD_TITLE_LOGIN', 'Συνδεθείτε');
define('HEAD_TITLE_COMPARE', 'Σύγκριση');
define('HEAD_TITLE_WISHLIST', 'Αγαπημένα προϊόντα');
define('HEAD_TITLE_ACCOUNT_PASSWORD_FORGOTTEN', 'Ξεχάσατε τον κωδικό πρόσβασης');

define('HEAD_TITLE_CHECKOUT','Ολοκλήρωση');
define('HEAD_TITLE_CHECKOUT_SUCCESS','Ετοιμο!');
define('HEAD_TITLE_ACCOUNT','Ο λογαριασμός μου');
define('HEAD_TITLE_ACCOUNT_HISTORY','Οι παραγγελίες μου');
define('HEAD_TITLE_ACCOUNT_EDIT','Επεξεργαστείτε τα δεδομένα μου');
define('HEAD_TITLE_ADDRESS_BOOK','Βιβλίο διευθύνσεων');
define('HEAD_TITLE_ACCOUNT_PASSWORD','Αλλαξτε κωδικό');
define('HEAD_TITLE_ALLCOMMENTS','Σχόλια?');
define('HEAD_TITLE_CONTACT_US','Επαφές του καταστήματος '.STORE_NAME);
define('HEAD_TITLE_PRICE','HTML sitemap - '.STORE_NAME);
define('HEAD_TITLE_404','Error 404 - '.STORE_NAME);
define('HEAD_TITLE_403','Error 403 - '.STORE_NAME);

// shopping_cart box text in includes/boxes/shopping_cart.php
define('BOX_SHOPPING_CART_EMPTY', 'Το καλάθι σας είναι άδειο');

//BEGIN allprods modification
define('BOX_INFORMATION_ALLPRODS', 'Προβολή όλων των αντικειμένων');
//END allprods modification


// checkout procedure text

// pull down default text
define('PULL_DOWN_DEFAULT', 'Παρακαλώ επιλέξτε');
define('PULL_DOWN_COUNTRY', 'Περιοχή:');
define('TYPE_BELOW', 'Παρακαλώ πληκτρολογήστε παρακάτω');

// javascript messages
define('JS_ERROR', 'Παρουσιάστηκαν σφάλματα κατά τη διαδικασία της φόρμας.\n\nΚάντε τις παρακάτω διορθώσεις:\n\n');


define('JS_FIRST_NAME', '* The \'First Name\' πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_FIRST_NAME_MIN_LENGTH . ' χαρακτήρες.\n');
define('JS_LAST_NAME', '* The \'Last Name\' πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_LAST_NAME_MIN_LENGTH . ' χαρακτήρες.\n');




define('JS_ERROR_SUBMITTED', 'Αυτή η φόρμα έχει ήδη υποβληθεί. Πατήστε OK και περιμένετε να ολοκληρωθεί αυτή η διαδικασία.');


define('CATEGORY_COMPANY', 'ΣΤΟΙΧΕΙΑ ΕΤΑΙΡΕΙΑΣ');
define('CATEGORY_PERSONAL', 'Τα προσωπικά σας στοιχεία');
define('CATEGORY_ADDRESS', 'Η διεύθυνση σας');
define('CATEGORY_CONTACT', 'Οι πληροφορίες επικοινωνίας σας');
define('CATEGORY_OPTIONS', 'Επιλογές');

define('ENTRY_COMPANY', 'Company Name:');
define('ENTRY_COMPANY_ERROR', '');
define('ENTRY_COMPANY_TEXT', '');
define('ENTRY_FIRST_NAME', 'Ονομα:');
define('ENTRY_FIRST_NAME_ERROR', 'Το Όνομα σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_FIRST_NAME_MIN_LENGTH . 'χαρακτήρα.');
define('ENTRY_FIRST_NAME_TEXT', '*');
define('ENTRY_LAST_NAME', 'Last Name:');
define('ENTRY_TEXT_FOM_IMAGE', 'Εισαγάγετε κείμενο από την εικόνα');
define('ENTRY_LAST_NAME_ERROR', 'Το επίθετο σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_LAST_NAME_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_LAST_NAME_TEXT', '*');
define('ENTRY_DATE_OF_BIRTH', 'ΗΜΕΡΟΜΗΝΙΑ ΓΕΝΝΗΣΗΣ:');
define('ENTRY_DATE_OF_BIRTH_ERROR', 'Η ημερομηνία γέννησής σας πρέπει να είναι σε αυτή τη μορφή: DD/MM/YYYY (eg 21/05/1970)');
define('ENTRY_DATE_OF_BIRTH_TEXT', '* (eg. 21/05/1970)');
define('ENTRY_EMAIL_ADDRESS', 'Διεύθυνση ηλεκτρονικού ταχυδρομείου:');
define('ENTRY_EMAIL_ADDRESS_ERROR', 'Η διεύθυνση ηλεκτρονικού ταχυδρομείου σας πρέπει να περιέχει τουλάχιστον ένα' . ENTRY_EMAIL_ADDRESS_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_EMAIL_ADDRESS_CHECK_ERROR', 'Η διεύθυνση ηλεκτρονικού ταχυδρομείου σας δεν φαίνεται να είναι έγκυρη - εκτελέστε τις απαραίτητες διορθώσεις.');
define('ENTRY_EMAIL_ADDRESS_ERROR_EXISTS', 'Η ηλεκτρονική σας διεύθυνση υπάρχει ήδη στα αρχεία μας - συνδεθείτε με τη διεύθυνση ηλεκτρονικού ταχυδρομείου ή δημιουργήστε έναν λογαριασμό με διαφορετική διεύθυνση.');
define('ENTRY_EMAIL_ADDRESS_TEXT', '*');
define('ENTRY_STREET_ADDRESS', 'Διεύθυνση:');
define('ENTRY_STREET_ADDRESS_ERROR', 'Η διεύθυνσή σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_STREET_ADDRESS_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_STREET_ADDRESS_TEXT', '*');
define('ENTRY_SUBURB', 'Περιοχή:');
define('ENTRY_SUBURB_ERROR', '');
define('ENTRY_SUBURB_TEXT', '');
define('ENTRY_POST_CODE', 'Ταχυδρομικός Κωδικος:');
define('ENTRY_POST_CODE_ERROR', 'Ο ταχυδρομικός σας κώδικας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_POSTCODE_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_POST_CODE_TEXT', '*');
define('ENTRY_CITY', 'City');
define('ENTRY_CITY_ERROR', 'Η πόλη σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_CITY_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_CITY_TEXT', '*');
define('ENTRY_STATE', 'Πόλη:');
define('ENTRY_STATE_ERROR', 'Η πόλη σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_STATE_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_STATE_ERROR_SELECT', 'Επιλέξτε μια κατάσταση από το μενού κράτησης.');
define('ENTRY_STATE_TEXT', '*');
define('ENTRY_COUNTRY', 'Χώρα:');
define('ENTRY_COUNTRY_ERROR', 'Πρέπει να επιλέξετε μια χώρα από το κάτωθι αναπτυσσόμενο μενού Χώρες.');
define('ENTRY_COUNTRY_TEXT', '*');
define('ENTRY_TELEPHONE_NUMBER', 'Αριθμός τηλεφώνου:');
define('ENTRY_TELEPHONE_NUMBER_ERROR', 'Ο αριθμός τηλεφώνου σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_TELEPHONE_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_TELEPHONE_NUMBER_TEXT', '*');
define('ENTRY_FAX_NUMBER', 'Fax :');
define('ENTRY_FAX_NUMBER_TEXT', '');
define('ENTRY_NEWSLETTER', 'Ο αριθμός τηλεφώνου σας πρέπει να περιέχει τουλάχιστον ένα:');
define('ENTRY_NEWSLETTER_TEXT', '');
define('ENTRY_NEWSLETTER_YES', 'Ναι');
define('ENTRY_NEWSLETTER_NO', 'Οχι');
define('ENTRY_PASSWORD', 'Κωδικός πρόσβασης:');
define('ENTRY_PASSWORD_ERROR', 'Ο κωδικός σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_PASSWORD_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_PASSWORD_ERROR_NOT_MATCHING', 'Η επιβεβαίωση κωδικού πρόσβασης πρέπει να αντιστοιχεί στον κωδικό πρόσβασής σας.');
define('ENTRY_PASSWORD_CONFIRMATION', 'Επιβεβαίωση κωδικού πρόσβασης:');
define('ENTRY_PASSWORD_CURRENT', 'Ο τρέχων κωδικός πρόσβασης:');
define('ENTRY_PASSWORD_CURRENT_ERROR', 'Ο κωδικός σας πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_PASSWORD_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_PASSWORD_NEW', 'New Password:');
define('ENTRY_PASSWORD_NEW_ERROR', 'Ο νέος σας κωδικός πρόσβασης πρέπει να περιέχει τουλάχιστον ένα ' . ENTRY_PASSWORD_MIN_LENGTH . ' χαρακτήρα.');
define('ENTRY_PASSWORD_NEW_ERROR_NOT_MATCHING', 'Η επιβεβαίωση κωδικού πρόσβασης πρέπει να ταιριάζει με τον νέο σας κωδικό πρόσβασης.');

define('FORM_REQUIRED_INFORMATION', '* Απαιτούμενες πληροφορίες');

// constants for use in tep_prev_next_display function
define('TEXT_RESULT_PAGE', 'Σελίδες αποτελεσμάτων:');
define('TEXT_DISPLAY_NUMBER_OF_PRODUCTS', 'Εμφάνιση <b>%d</b> to <b>%d</b> (of <b>%d</b> προϊόντων)');
define('TEXT_DISPLAY_NUMBER_OF_ORDERS', 'Εμφάνιση <b>%d</b> to <b>%d</b> (of <b>%d</b> παραγγελιών)');
define('TEXT_DISPLAY_NUMBER_OF_SPECIALS', 'Εμφάνιση <b>%d</b> to <b>%d</b> (of <b>%d</b> εκπτωτικών)');
define('TEXT_DISPLAY_NUMBER_OF_FEATURED', 'Εμφάνιση <b>%d</b> to <b>%d</b> (of <b>%d</b> προιόντων που αναμενεται η παραλαβή)');

define('PREVNEXT_TITLE_PREVIOUS_PAGE', 'Προηγούμενη σελίδα');
define('PREVNEXT_TITLE_NEXT_PAGE', 'Επόμενη σελίδα');
define('PREVNEXT_TITLE_PAGE_NO', 'Σελίδα %d');
define('PREVNEXT_TITLE_PREV_SET_OF_NO_PAGE', 'Προηγούμενο σύνολο από %d σελίδες');
define('PREVNEXT_TITLE_NEXT_SET_OF_NO_PAGE', 'Επόμενο σύνολο από %d σελίδες');
define('PREVNEXT_BUTTON_PREV', '[&lt;&lt;&nbsp;προηγούμενο]');
define('PREVNEXT_BUTTON_NEXT', '[Επόμενο&nbsp;&gt;&gt;]');

define('PREVNEXT_TITLE_PPAGE', 'Σελίδα');

define('ART_XSELL_TITLE', 'Παρόμοιο περιεχόμενο');
define('PROD_XSELL_TITLE', 'Παρόμοια προϊόντα');

define('IMAGE_BUTTON_ADD_ADDRESS', 'Προσθήκη διεύθυνσης');
define('IMAGE_BUTTON_BACK', 'Πίσω');
define('IMAGE_BUTTON_CHECKOUT', 'Ολοκλήρωση αγοράς');
define('IMAGE_BUTTON_CONFIRM_ORDER', 'Επιβεβαίωση παραγγελίας');
define('IMAGE_BUTTON_CONTINUE', 'Συνέχεια');
define('IMAGE_BUTTON_CONTINUE_SHOPPING', 'Continue Shopping');
define('IMAGE_BUTTON_DELETE', 'Διαγραφή');
define('IMAGE_BUTTON_LOGIN', 'Συνδεθείτε');

define('IMAGE_BUTTON_IN_CART', 'Στο καλάθι');
define('IMAGE_BUTTON_ADDTO_CART', 'Αγορά');
define('IMAGE_BUTTON_ADDTO_CART_2', 'Αγορά');
define('IMAGE_BUTTON_ADDTO_CART_CLO', 'Προσθήκη στο<b>καλάθι</b>');

define('IMAGE_BUTTON_TEST_TEMPLATE', 'Δοκίμασε δωρεάν!');
define('IMAGE_BUTTON_BUY_TEMPLATE', 'Αγοράστε αυτό το πρότυπο τώρα?');
define('MESSAGE_BUY_TEMPLATE1', 'Δυστυχώς, η δοκιμαστική περίοδος των 14 ημερών έχει λήξει παει. ');
define('MESSAGE_BUY_TEMPLATE2', 'Αγορά');
define('MESSAGE_BUY_TEMPLATE3', ' αυτό το χώρο αποθήκευσης ή να δημιουργήσετε έναν άλλο ιστότοπο δοκιμών.');

define('CUSTOM_PANEL_EDIT', 'Επεξεργασία ');
define('CUSTOM_PANEL_EDIT_PRODUCT', 'τα εμπορεύματα');
define('CUSTOM_PANEL_EDIT_MANUF', 'κατασκευαστή');
define('CUSTOM_PANEL_EDIT_ARTICLE', 'άρθρο');
define('CUSTOM_PANEL_EDIT_SECTION', 'ενότητα');
define('CUSTOM_PANEL_EDIT_CATEGORY', 'κατηγορία');
define('CUSTOM_PANEL_EDIT_SEO', 'seotext');
define('CUSTOM_PANEL_ADMIN_LOGIN', 'Πίνακας διαχειριστών');
define('CUSTOM_PANEL_ADD', 'Προσθέστε');
define('CUSTOM_PANEL_ADD_PRODUCT', 'Προϊόν');
define('CUSTOM_PANEL_ADD_PAGE', 'σελίδα');
define('CUSTOM_PANEL_ADD_DISCOUNT', 'Έκπτωση');
define('CUSTOM_PANEL_ADD_CATEGORY', 'Κατηγορία');
define('CUSTOM_PANEL_ADD_STATISTICS', 'στατιστική');
define('CUSTOM_PANEL_ADD_ONLINE', 'Στο site: ');
define('CUSTOM_PANEL_PALETTE', 'παλέτα ιστοσελίδα');
define('CUSTOM_PANEL_PALETTE_TEXT_COLOR', 'Χρώμα κειμένου');
define('CUSTOM_PANEL_PALETTE_HEADER_BG', 'Κεφάλαιο φόντου');
define('CUSTOM_PANEL_PALETTE_FOOTER_BG', 'Υποσέλιδο υποβάθρου');
define('CUSTOM_PANEL_PALETTE_LINK_COLOR', 'Χρώμα συνδέσμου');
define('CUSTOM_PANEL_PALETTE_BTN_COLOR', 'Χρώμα κουμπιού');
define('CUSTOM_PANEL_PALETTE_BG_COLOR', 'Χρώμα φόντουa');
define('CUSTOM_PANEL_ORDERS', 'Παραγγελίες');
define('CUSTOM_PANEL_ORDERS_NEW', 'Νέα: ');


define('CUSTOM_PANEL_DATE1', 'ημέρα');
define('CUSTOM_PANEL_DATE2', 'ημέρες');
define('CUSTOM_PANEL_DATE3', 'ημέρες');


define('IMAGE_BUTTON_UPDATE', 'Εκσυγχρονίστε');
define('IMAGE_REDEEM_VOUCHER', 'Εφαρμόστε');

define('SMALL_IMAGE_BUTTON_VIEW', 'Οψη');


define('ICON_ERROR', 'Λάθος');
define('ICON_SUCCESS', 'Επιτυχής');
define('ICON_WARNING', 'Προειδοποίηση');

define('TEXT_GREETING_PERSONAL', 'Καλώς ήρθατε πίσω <span class="greetUser">%s!</span> Θα θέλατε να δείτε ποια <a href="%s"><u>new products</u></a> are available to purchase?');
define('TEXT_CUSTOMER_GREETING_HEADER', 'Σας Χαιρετιζουμε ');
define('TEXT_GREETING_GUEST', 'Καλώς ήρθατε <span class="greetUser">Επισκέπτης!</span> Θα θελατε <a href="%s"><u>lΕγγραφείτε</u></a>? Ή θα προτιμούσατε να <a href="%s"><u>δημιουργία λογαριασμού</u></a>?');

define('TEXT_SORT_PRODUCTS', 'Ταξινόμηση προϊόντων:');
define('TEXT_DESCENDINGLY', 'Κατά φθίνουσα σειρά');
define('TEXT_ASCENDINGLY', 'Αύξουσα');
define('TEXT_BY', ' με');

define('TEXT_UNKNOWN_TAX_RATE', 'Άγνωστος συντελεστής φόρου');


// Down For Maintenance
define('TEXT_BEFORE_DOWN_FOR_MAINTENANCE', 'NOTICE: Αυτός ο ιστότοπος θα είναι εκτός λειτουργίας για συντήρηση σχετικά με: ');
define('TEXT_ADMIN_DOWN_FOR_MAINTENANCE', 'NOTICE: Ο ιστότοπος είναι προς το παρόν εκτός για συντήρηση στο κοινό');

define('WARNING_INSTALL_DIRECTORY_EXISTS', 'Προειδοποίηση: Ο κατάλογος εγκατάστασης υπάρχει στη διεύθυνση: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/εγκαθιστώ. Καταργήστε αυτόν τον κατάλογο για λόγους ασφαλείας.');
define('WARNING_CONFIG_FILE_WRITEABLE', 'Προειδοποίηση: Μπορώ να γράψω στο αρχείο ρυθμίσεων: ' . dirname($_SERVER['SCRIPT_FILENAME']) . '/περιλαμβάνει / configure.php. Πρόκειται για δυνητικό κίνδυνο ασφάλειας - ορίστε τα σωστά δικαιώματα χρήστη σε αυτό το αρχείο.');
define('WARNING_SESSION_DIRECTORY_NON_EXISTENT', 'Προειδοποίηση: Ο κατάλογος συνεδριών δεν υπάρχει: ' . tep_session_save_path() . '. Οι περίοδοι σύνδεσης δεν θα λειτουργήσουν μέχρι να δημιουργηθεί αυτός ο κατάλογος.');
define('WARNING_SESSION_DIRECTORY_NOT_WRITEABLE', 'Προειδοποίηση: Δεν είμαι σε θέση να γράψω στον κατάλογο συνεδριών: ' . tep_session_save_path() . '. Οι περίοδοι σύνδεσης δεν θα λειτουργήσουν μέχρι να ρυθμιστούν τα σωστά δικαιώματα χρήστη.');
define('WARNING_SESSION_AUTO_START', 'Προειδοποίηση: το session.auto_start είναι ενεργοποιημένο - απενεργοποιήστε αυτή τη λειτουργία php στο php.ini και κάντε επανεκκίνηση του διακομιστή ιστού.');


define('TEXT_CCVAL_ERROR_INVALID_DATE', 'Η ημερομηνία λήξης που έχει εισαχθεί για την πιστωτική κάρτα δεν είναι έγκυρη.<br>Ελέγξτε την ημερομηνία και δοκιμάστε ξανά.');
define('TEXT_CCVAL_ERROR_INVALID_NUMBER', 'Ο αριθμός πιστωτικής κάρτας που καταχωρίσατε είναι άκυρος. <br> Ελέγξτε τον αριθμό και δοκιμάστε ξανά.');
define('TEXT_CCVAL_ERROR_UNKNOWN_CARD', 'Τα πρώτα τέσσερα ψηφία του καταχωρημένου αριθμού είναι: %s <br>Εάν ο αριθμός αυτός είναι σωστός, δεχόμαστε αυτόν τον τύπο πιστωτικής κάρτας.<br>Αν είναι λάθος, δοκιμάστε ξανά.');






define('BOX_LOGINBOX_HEADING', 'Του λογαριασμού σας');
define('LOGIN_BOX_MY_CABINET','Ο λογαριασμός μου');
define('RENDER_LOGIN_WITH', 'Login with');
define('LOGIN_BOX_ADDRESS_BOOK','Βιβλίο διευθύνσεων');
define('LOGIN_BOX_LOGOFF','Αποσύνδεση');

define('LOGIN_FROM_SITE','Σύνδεση');

define('LOGO_IMAGE_TITLE','Λογότυπο');



// Article Manager
define('BOX_ALL_ARTICLES', 'Όλα τα άρθρα');
define('TEXT_DISPLAY_NUMBER_OF_ARTICLES', 'Εμφάνιση <b>%d</b> to <b>%d</b> (of <b>%d</b> άρθρα)');
define('NAVBAR_TITLE_DEFAULT', 'Άρθρα');


//TotalB2B start
define('ΤΙΜΕΣ_ΕΓΓΡΑΦΗ_ΣΕ_ΚΕΙΜΕΝΟ',''); // define('PRICES_LOGGED_IN_TEXT','Must be logged in for prices!');
//TotalB2B end

define('PRODUCTS_ORDER_QTY_MIN_TEXT_INFO','Η εντολή Ελάχιστο είναι: ');
define('PRODUCTS_ORDER_QTY_MIN_TEXT_CART','Η εντολή Ελάχιστο είναι: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_INFO','Παραγγελία σε μονάδες του: ');
define('PRODUCTS_ORDER_QTY_UNIT_TEXT_CART','Παραγγελία σε μονάδες του: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_MIN_TEXT','');
define('ERROR_PRODUCTS_QUANTITY_INVALID','Μη έγκυρη ποσότητα: ');
define('ERROR_PRODUCTS_QUANTITY_ORDER_UNITS_TEXT','');
define('ERROR_PRODUCTS_UNITS_INVALID','Μη έγκυρες μονάδες: ');

// Comments
define('ADD_COMMENT_HEAD_TITLE', 'Προσθέστε το σχόλιό σας');

// Poll Box Text
define('_RESULTS', 'Αποτελέσματα');
define('_VOTE', 'Ψήφοφορία');
define('_VOTES', 'Ψηφοφορίες:');

define('PREV_NEXT_PRODUCT', 'Προϊόν ');
define('PREV_NEXT_CAT', ' Της κατηγορίας ');
define('PREV_NEXT_MB', ' Του κατασκευαστή');


// English names of boxes

define('BOX_HEADING_CATEGORIES', 'Κατηγορίες');
define('BOX_HEADING_INFORMATION', 'Πληροφορίες');
define('BOX_HEADING_MANUFACTURERS', 'Κατασκευαστές');
define('BOX_HEADING_SPECIALS', 'Προσφορές');
define('BOX_HEADING_DEFAULT_SPECIALS', 'Προσφορές %s');
define('BOX_HEADING_SEARCH', 'Αναζήτηση');
define('BOX_OPEN_SEARCH_FORM', 'Open Search Form');
define('BOX_HEADING_WHATS_NEW', 'Νεα προϊόντα');
define('BOX_HEADING_FEATURED', 'Προτεινόμενα Προϊόντα');
define('BOX_HEADING_ARTICLES', 'Άρθρα');
define('BOX_HEADING_LINKS', 'Συνδέσεις');
define('HELP_HEADING', 'Βοήθεια');
define('BOX_HEADING_WISHLIST', 'Επιθυμητάt');
define('BOX_HEADING_BESTSELLERS', 'Best sellers');
define('BOX_HEADING_CURRENCY', 'Νόμισμα');
define('_POLLS', 'Polls');
define('BOX_HEADING_CALLBACK', 'Callback');
define('BOX_HEADING_CONSULTANT', 'Online Σύμβουλος');
define('BOX_HEADING_PRODUCTS', 'Αντικειμένων');
define('BOX_HEADING_NO_CATEGORY_OF_PRODUCTS', ' Προσθήκη κατηγορίας στοιχείων.');
define('BOX_HEADING_LASTVIEWED', 'Προϊόντα που είδατε πρόσφατα');

define('BOX_BESTSELLERS_NO', 'Δεν υπάρχουν ακόμη bestsellers');


  define('LOW_STOCK_TEXT1','Προειδοποίηση χαμηλού αποθέματος: ');
  define('LOW_STOCK_TEXT2','Αριθμός Μοντέλου: ');
  define('LOW_STOCK_TEXT3','Ποσότητα: ');
  define('LOW_STOCK_TEXT4','Διεύθυνση URL προϊόντος: ');
  define('LOW_STOCK_TEXT5','Το τρέχον όριο χαμηλής παραγγελίας είναι ');

// wishlist box text in includes/boxes/wishlist.php

  define('BOX_TEXT_NO_ITEMS', 'Δεν υπάρχουν προϊόντα στην Λίστα επιθυμιών σας.');

  define('TOTAL_TIME', 'Χρόνος εκτέλεσης: ');
define('BUTTON_SHOPPING_CART_OPEN', 'Open Shopping Cart');

define('TOTAL_CART', 'Σύνολο');
    
// otf 1.71 defines needed for Product Option Type feature.
  define('TEXT_PREFIX', 'txt_');
  define('PRODUCTS_OPTIONS_VALUE_TEXT_ID', 0);  //Must match id for user defined "TEXT" value in db table TABLE_PRODUCTS_OPTIONS_VALUES
  
  // Product tabs

define('ALSO_PURCHASED', 'Επίσης αγοράστηκε');

define('FORWARD', 'Προς τα εμπρός');

define('COMP_PROD_NAME','Ονομα');
define('COMP_PROD_IMG','Εικόνα');
define('COMP_PROD_PRICE','Τιμή');
define('COMP_PROD_CLEAR','Καθαρίσμος όλων');
define('COMP_PROD_BACK','Πίσω');
define('COMP_PROD_ADD_TO','Προσθήκη προϊόντων για σύγκριση!');
define('TEXT_PASSWORD_FORGOTTEN', 'Ξεχάσατε τον κωδικό?');
define('QUICK_ORDER','Τηλεφωνική παραγγελία !');
define('QUICK_ORDER_SUCCESS','Η αίτησή σας υποβλήθηκε, θα επικοινωνήσουμε μαζί σας σύντομα');
define('QUICK_ORDER_BUTTON','Τηλεφωνική παραγγελία !');
define('SEND_MESSAGE','Αποστολή');

define('LABEL_NEW','Νέο');
define('LABEL_TOP','TOP');
define('LABEL_SPECIAL','Ειδικός');

define('FILTER_BRAND','Μάρκα');
define('FILTER_ALL','όλα');

define('WISHLIST_PC','pc.');

define('FOOTER_INFO','Πληροφορίες');
define('FOOTER_CATEGORIES','Κατηγορίες');
define('FOOTER_ARTICLES','Άρθρα');
define('FOOTER_CONTACTS','Επαφές');
define('FOOTER_SITEMAP','Sitemap');
define('FOOTER_DEVELOPED','Developed by');
define('FOOTER_COPYRIGHT','Copyright');
define('FOOTER_ALLRIGHTS','All Rights Reserved');
define('SHOW_ALL_CATS','Εμφάνιση όλων των κατηγοριών');
define('SHOW_ALL_ARTICLES','Εμφάνιση όλων των άρθρων');
define('TEXT_NAVIGATION_BRANDS','Όλοι οι κατασκευαστές');

define('TEXT_HEADING_PRICE', 'Κατάλογος:');
define('TEXT_HEADING_CATALOG', 'Κατάλογος');
define('TEXT_PRODUCTS_ATTRIBUTES', 'Attributes');
define('TEXT_PRODUCTS_QTY', 'Products Quantity');

define('MAIN_NEWS','Νέα');
define('MAIN_NEWS_ALL','Όλα τα νέα');
define('MAIN_NEWS_SUBSCRIBE','Εγγραφείτε <span>για ειδήσεις</span>');
define('MAIN_NEWS_SUBSCRIBE_BUT','Εγγραφείτε');
define('MAIN_NEWS_EMAIL','Η διεύθυνση του ηλεκτρονικού σου ταχυδρομείου');

define('MAIN_BESTSELLERS','Best sellers');
define('MAIN_REVIEWS','Κριτικές');
define('MAIN_REVIEWS_ALL','Όλες οι κριτικές');
define('MAIN_MOSTVIEWED','Περισσότερες εμφανίσεις');

define('LIST_TEMP_INSTOCK','Σε απόθεμα');
define('LIST_TEMP_OUTSTOCK','Αναμένεται');

define('HIGHSLIDE_CLOSE','Κοντά');
define('BUTTON_CANCEL','Συνεχίστε τις αγορές');
define('BUTTON_SEND','Στείλετε');

define('LISTING_PER_PAGE','Ανά σελίδα:');
define('LISTING_SORT_NAME','Αλφαβητικά, Α-Ω');
define('LISTING_SORT_PRICE1','Τιμή: Χαμηλή έως υψηλή');
define('LISTING_SORT_PRICE2','Τιμή: Υψηλή προς χαμηλή');
define('LISTING_SORT_NEW','Με το νεότερο');
define('LISTING_SORT_POPULAR','Καλύτερη βαθμολογία');
define('LISTING_SORT_LIST','Λίστα');
define('LISTING_SORT_COLUMNS','Στήλη');

define('PROD_DRUGIE','Παρόμοια προϊόντα');
define('PROD_FILTERS','Φίλτρα');

define('PAGE404_TITLE','Error 404');
define('PAGE404_DESC','Η σελίδα δεν βρέθηκε');
define('PAGE404_REASONS','Μπορεί να υπάρχουν πολλοί λόγοι:');
define('PAGE404_REASON1','Αυτή η σελίδα μετακινήθηκε ή μετονομάστηκε');
define('PAGE404_REASON2','Αυτή η σελίδα δεν υπάρχει πλέον σε αυτόν τον ιστότοπο');
define('PAGE404_REASON3','Η διεύθυνση URL σας είναι λάθος');
define('PAGE404_BACK1','Επιστροφή στην προηγούμενη σελίδα');
define('PAGE404_BACK2','Επιστροφή στην αρχική σελίδα');

// page 403
define('PAGE403_DESC', 'Δεν έχετε άδεια για να δείτε αυτή τη σελίδα.');
define('PAGE403_TITLE','Error 403');
define('PAGE403_TO_HOME_PAGE','Επιστροφή στην αρχική σελίδα');


define('SB_SUBSCRIBER','Συνδρομητής');
define('SB_EMAIL_NAME','Αίτηση για ενημερωτικά δελτία');
define('SB_EMAIL_USER','Χρήστης');
define('SB_EMAIL_WAS_SUBSCRIBED','ήταν συνδρομητής για ενημερωτικά δελτία');
define('SB_EMAIL_ALREADY','έχουν ήδη εγγραφεί στα ενημερωτικά μας δελτία');
define("LOAD_MORE_BUTTON", "Φόρτωσε περισσότερα");
define("TIME_LEFT", "Μέχρι το τέλος της δοκιμαστικής έκδοσης");

// CLO
define('CLO_NEW_PRODUCTS', 'Πρόσφατα προστιθέμενα προϊόντα');
define('CLO_FEATURED', 'Αυτά τα προϊόντα θα σας ενδιαφέρουν');
define('CLO_DESCRIPTION_BESTSELLERS', 'Κορυφαιές Πωλήσεις');
define('CLO_DESCRIPTION_SPECIALS', 'Εκπτώσεις');
define('CLO_DESCRIPTION_MOSTVIEWED', 'Περισσότερες εμφανίσεις');

define('SHOPRULES_AGREE', 'συμφωνώ με');
define('SHOPRULES_AGREE2', 'κανόνες αποθήκευσης');

define("SHOW_ALL_SUBCATS", "παρακολουθήστε όλα");
define("TEXT_PRODUCT_INFO_FREE_SHIPPING", "Δωρεάν αποστολή");

//home
define('HOME_BOX_HEADING_SEARCH', 'αναζήτηση...');
define('HEADER_TITLE_OR', 'ή');
define('HOME_IMAGE_BUTTON_ADDTO_CART', 'προσθέστε');
define('HOME_IMAGE_BUTTON_IN_CART', 'στο καλάθι');
define('HOME_ARTICLE_READ_MORE', 'διαβάστε περισσότερα');
define('HOME_MAIN_NEWS_SUBSCRIBE', '<span> να πάρει 25% </span> σε ένα φορτίο στο online-κακά');
define('HOME_MAIN_NEWS_EMAIL', 'Εισάγετε το e-mail σας');
define('HOME_FOOTER_DEVELOPED', 'δημιουργία ηλεκτρονικού καταστήματος');
define('HOME_FOOTER_CATEGORIES', 'Κατάλογος');
define('HOME_ADD_COMMENT_HEAD_TITLE', 'Προσθέστε ένα σχόλιο στο προϊόν');
define('HOME_ENTRY_FIRST_NAME_FORM', 'Όνομα');
define('HOME_ADD_COMMENT_FORM', 'Σχόλιο');
define('HOME_REPLY_TO_COMMENT', 'Απάντηση στο σχόλιο');
define('HOME_PROD_DRUGIE', 'Προϊόντα που βλέπετε');
define("HOME_LOAD_MORE_INFO", "περισσότερα");
define("HOME_LOAD_ROLL_UP", "ανεβείτε");
define("HOME_TITLE_LEFT_CATEGORIES", "εμπορεύματα");
define('HOME_BOX_HEADING_SELL_OUT', 'Εκκαθάριση πώλησης');
define("HOME_LOAD_MORE_BUTTON", "κατεβάστε περισσότερα προϊόντα");
define("HOME_BOX_HEADING_WISHLIST", "Αγαπημένα προϊόντα");
define("HOME_PROD_VENDOR_CODE", "αριθμός άρθρου ");
define('HOME_BOX_HEADING_WHATS_STOCK', 'Προσφορές');
define('HOME_FOOTER_SOCIAL_TITLE', 'είμαστε σε κοινωνικά δίκτυα:');
define('HOME_HEADER_SOME_LIST', 'Δείτε επίσης');


define('VK_LOGIN', 'Login');
define('SHOW_RESULTS', 'Όλα τα αποτελέσματα');
define('ENTER_KEY', 'Enter keywords');
define('TEXT_LIMIT_REACHED', 'Maximum products in compares reached: ');
define('RENDER_TEXT_ADDED_TO_CART', 'Product was successfully added to your cart!');
define('CHOOSE_ADDRESS', 'Επιλέξτε διεύθυνση');


define('SEARCH_LANG', 'el/');


//default2

define('DEMO2_TOP_PHONES', 'Τηλέφωνα');
define('DEMO2_TOP_CALLBACK', 'Επανάκληση');
define('DEMO2_TOP_ONLINE', 'Online chat');
define('DEMO2_SHOPPING_CART', 'Καλάθι');
define('DEMO2_LOGIN_WITH', 'Σύνδεση με το ');
define('DEMO2_WISHLIST_LINK', 'Λίστα επιθυμιών');
define('DEMO2_FOOTER_CATEGORIES', 'Κατάλογος προϊόντων');
define('DEMO2_MY_INFO', 'Επεξεργασία δεδομένων');
define('DEMO2_EDIT_ADDRESS_BOOK', 'Αλλάξτε τη διεύθυνση αποστολής');
define('DEMO2_LEFT_CAT_TITLE', 'Κατηγορίες');
define('DEMO2_LEFT_ALL_GOODS', 'Όλα τα αγαθά');
define('DEMO2_TITLE_SLIDER_LINK', 'Δείτε όλους');
define('DEMO2_READ_MORE', 'Διαβάστε περισσότερα <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M20.485 372.485l99.029 99.03c4.686 4.686 12.284 4.686 16.971 0l99.029-99.03c7.56-7.56 2.206-20.485-8.485-20.485H156V44c0-6.627-5.373-12-12-12h-32c-6.627 0-12 5.373-12 12v308H28.97c-10.69 0-16.044 12.926-8.485 20.485z"></path></svg>');
define("DEMO2_READ_MORE_UP", 'Ελαχιστοποίηση <svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512"><path d="M235.515 139.515l-99.029-99.03c-4.686-4.686-12.284-4.686-16.971 0l-99.029 99.03C12.926 147.074 18.28 160 28.97 160H100v308c0 6.627 5.373 12 12 12h32c6.627 0 12-5.373 12-12V160h71.03c10.69 0 16.044-12.926 8.485-20.485z"></path></svg>');
define('DEMO2_SHOW_ALL_CATS','Όλες οι κατηγορίες');
define('DEMO2_SHOW_ALL_ARTICLES','Όλα τα άρθρα');
define('DEMO2_BTN_COME_BACK','Πηγαίνετε πίσω');
define('DEMO2_SHARE_TEXT','Μοιραστείτε αυτό');
define('DEMO2_QUICK_ORDER_BUTTON', 'Με ένα κλικ');
















