<?php 
define('NAVBAR_TITLE_1','Mes données');
define('NAVBAR_TITLE_2','Modifier le mot de passe');
define('HEADING_TITLE','Mon mot de passe');
define('MY_PASSWORD_TITLE','Mon mot de passe');
define('SUCCESS_PASSWORD_UPDATED','Vous avez réussi à changer votre mot de passe.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING','Vous choisissez le mauvais mot de passe, indiquez le mot de passe que Vous avez entré lors de l\'enregistrement.');
