<?php 
define('NAVBAR_TITLE_1','Mes données');
define('NAVBAR_TITLE_2','Historique des commandes');
define('HEADING_TITLE','Historique des commandes');
define('TEXT_ORDER_STATUS','Le statut de la commande');
define('TEXT_ORDER_DATE','Date de commande');
define('TEXT_ORDER_SHIPPED_TO','Adresse de livraison:');
define('TEXT_ORDER_BILLED_TO','L\'acheteur:');
define('TEXT_ORDER_COST','Le montant');
define('TEXT_ORDER_PC','pièces');
define('TEXT_NO_PURCHASES','Malheureusement, Vous n\'avez encore rien acheté dans notre magasin.');
