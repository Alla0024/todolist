<?php 
define('NAVBAR_TITLE','Le mot de passe');
define('HEADING_TITLE','Laissez entrer!');
define('TEXT_NEW_CUSTOMER','Pour vous inscrire!');
define('TEXT_NEW_CUSTOMER_INTRODUCTION','En vous inscrivant dans notre magasin, Vous serez en mesure de faire des achats beaucoup <b>plus rapide et plus pratique</b> en outre, Vous serez en mesure de suivre l\'exécution des commandes, de regarder l\'historique de ses commandes.');
define('TEXT_RETURNING_CUSTOMER','J\'ai déjà enregistré!');
define('TEXT_LOGIN_ERROR','<p><b>ERREUR:</b></span> Incorrect \'E-Mail\' et/ou \'Mot de passe\'.');
define('NO_CUSTOMER','Cet utilisateur n\'est pas dans la base');
define('NO_PASS','Mot de passe incorrect');
define('SOC_VK','Facebook');
define('SOC_SITE','site');
define('SOC_ENTER_FROM_OTHER','Rendez-vous à partir d\'un autre sots. réseau ou sous son nom d\'utilisateur/mot de passe.');
define('SOC_NOW_FROM','Maintenant, vous essayez d\'aller de');
define('SOC_NEED_FROM','Vous vous êtes inscrit des');
define('SOC_LOGIN_THX','Merci, vous êtes connecté avec succès!');
