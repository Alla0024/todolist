<?php 
define('MODULE_ORDER_TOTAL_COUPON_TITLE','Coupon');
define('MODULE_ORDER_TOTAL_COUPON_HEADER','Certificats/Coupons');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION','Coupon');
define('ERROR_NO_INVALID_REDEEM_COUPON','Mauvais code promo');
define('ERROR_MINIMUM_CART_AMOUNT', 'Le montant minimum du panier pour ce coupon est: %s');
define('ERROR_INVALID_STARTDATE_COUPON','Spécifié encore coupon n\'est pas valide');
define('ERROR_INVALID_FINISDATE_COUPON','Ce coupon a expiré');
define('ERROR_INVALID_USES_COUPON','Le coupon a déjà été utilisé ');
define('TIMES',' fois.');
define('ERROR_INVALID_USES_USER_COUPON','Vous avez utilisé un coupon, le nombre maximum de fois.');
define('TEXT_ENTER_COUPON_CODE','Votre code:&nbsp;&nbsp;');
