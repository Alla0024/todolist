<?php 
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE','En marge de réduction');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION','En marge de réduction');
define('TWOFER_PROMO_STRING','Achetez ce produit et obtenez un autre gratuitement!');
define('TWOFER_QUALIFY_STRING','Vous pouvez obtenir un autre point de %s gratuit');
define('OFF_STRING_PCT','-%s');
define('OFF_STRING_CURR','-%s');
define('SECOND',' le deuxième ');
define('FREE_STRING',' gratuit');
define('REV_GET_ANY','Achetez de la marchandise ');
define('REV_GET_THIS','Achetez ');
define('REV_GET_DISC',', recevez ce produit ');
define('FREE',' gratuit');
define('GET_YOUR_PROD',', obtenez ');
define('GET_YOUR_CAT',' votre choix de ');
