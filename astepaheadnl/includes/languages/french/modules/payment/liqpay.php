<?php 
define('MODULE_PAYMENT_LIQPAY_TEXT_TITLE','LiqPAY');
define('MODULE_PAYMENT_LIQPAY_TEXT_PUBLIC_TITLE','Visa/Mastercard LiqPay');
define('MODULE_PAYMENT_LIQPAY_TEXT_DESCRIPTION','Après avoir cliqué sur le bouton valider la commande, Vous accédez à un site de paiement pour le paiement de la commande, après le paiement de Votre commande.');
define('MODULE_PAYMENT_LIQPAY_TEXT_ADMIN_DESCRIPTION','Module de paiement LiqPAY..');
