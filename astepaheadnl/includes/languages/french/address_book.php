<?php 
define('NAVBAR_TITLE_1','Mes adresses');
define('NAVBAR_TITLE_2','Carnet d\'adresses');
define('HEADING_TITLE','Mon carnet d\'adresses');
define('PRIMARY_ADDRESS_TITLE','Votre adresse');
define('PRIMARY_ADDRESS_DESCRIPTION','Cette adresse est sélectionnée par défaut.<br>Il sera utilisé lors de l\'achat et de toute autre action dans notre magasin.');
define('ADDRESS_BOOK_TITLE','Entrées de carnet d\'adresses');
define('PRIMARY_ADDRESS','(*)');
define('TEXT_MAXIMUM_ENTRIES','<b>REMARQUE:</b> la quantité Maximale de carnet d\'adresses <b>%s</b> ');
