<?php 
define('NAVBAR_TITLE_1','Mes données');
define('NAVBAR_TITLE_2','Carnet d\'adresses');
define('NAVBAR_TITLE_ADD_ENTRY','Nouvelle entrée');
define('NAVBAR_TITLE_MODIFY_ENTRY','Modifier une entrée');
define('NAVBAR_TITLE_DELETE_ENTRY','Supprimer un enregistrement');
define('HEADING_TITLE_ADD_ENTRY','Nouvelle entrée');
define('HEADING_TITLE_MODIFY_ENTRY','Modifier une entrée');
define('HEADING_TITLE_DELETE_ENTRY','Supprimer un enregistrement');
define('DELETE_ADDRESS_TITLE','Supprimer l\'adresse');
define('DELETE_ADDRESS_DESCRIPTION','Voulez-vous vraiment supprimer les adresses du carnet d\'adresses?');
define('NEW_ADDRESS_TITLE','Nouvelle entrée');
define('SELECTED_ADDRESS','L\'adresse sélectionnée');
define('SET_AS_PRIMARY','Faire par défaut.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_DELETED','L\'adresse sélectionnée est supprimé à partir du carnet d\'adresses.');
define('SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED','Votre carnet d\'adresses à jour.');
define('WARNING_PRIMARY_ADDRESS_DELETION','L\'adresse par défaut ne peut pas être supprimé. Définissez l\'état par défaut à une autre adresse et essayez encore une fois.');
define('ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY','Le carnet d\'adresses n\'est pas trouvée.');
define('ERROR_ADDRESS_BOOK_FULL','Votre carnet d\'adresses est complètement rempli. Supprimez inutile Vous adresse, et seulement après cela, Vous pouvez ajouter une nouvelle adresse.');
