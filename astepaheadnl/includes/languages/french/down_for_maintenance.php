<?php 
define('NAVBAR_TITLE','Le magasin est fermé pour maintenance.');
define('HEADING_TITLE','La boutique est temporairement fermée ...');
define('DOWN_FOR_MAINTENANCE_TEXT_INFORMATION','Organise des techniques de travail sur le serveur, rendez-vous plus tard.');
define('TEXT_MAINTENANCE_ON_AT_TIME','Date de clôture: ');
define('TEXT_MAINTENANCE_PERIOD','Cadeaux de reprendre le travail à travers: ');
define('DOWN_FOR_MAINTENANCE_STATUS_TEXT','Cliquez sur le bouton "Continuer", peut-être, la boutique fonctionne déjà:');
