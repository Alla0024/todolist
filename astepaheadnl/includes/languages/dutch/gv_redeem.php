<?php 
define('NAVBAR_TITLE','Activeren van het certificaat');
define('HEADING_TITLE','Activeren van het certificaat');
define('TEXT_INFORMATION','');
define('TEXT_INVALID_GV','De opgegeven certificaat bestaat niet of het werd al gebruikt. Als U vragen heeft, neem dan contact met ons op.');
define('TEXT_VALID_GV','U hebt geactiveerd uw certificaat voor %s! Nu kunt U uw certificaat gebruiken om aankopen te doen in onze online store.');
