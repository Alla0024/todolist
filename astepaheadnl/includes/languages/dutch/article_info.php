<?php 
define('HEADING_ARTICLE_NOT_FOUND','Artikel niet gevonden');
define('TEXT_ARTICLE_NOT_FOUND','Sorry, maar de pagina die U heeft opgevraagd is niet beschikbaar!');
define('TEXT_DATE_ADDED','Dit artikel werd gepubliceerd %s.');
define('TEXT_DATE_AVAILABLE','Dit artikel zal beschikbaar zijn %s.');
define('TEXT_BY','door ');
