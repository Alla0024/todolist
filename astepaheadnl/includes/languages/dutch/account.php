<?php 
define('NAVBAR_TITLE','Mijn account');
define('HEADING_TITLE','Mijn account informatie');
define('MY_ACCOUNT_TITLE','Mijn gegevens');
define('MY_ACCOUNT_PASSWORD','Wachtwoord wijzigen.');
define('MY_ORDERS_VIEW','Mijn bestellingen');
define('MY_NAVI','Menu');
define('MY_INFO','Mijn gegevens');
define('EDIT_ADDRESS_BOOK','Adresgegevens');
