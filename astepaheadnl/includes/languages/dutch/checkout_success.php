<?php 
define('NAVBAR_TITLE_1','Afrekenen');
define('NAVBAR_TITLE_2','Succesvol');
define('HEADING_TITLE','Uw bestelling is succesvol verwerkt!');
define('TEXT_SUCCESS','Uw bestelling is succesvol verwerkt!');
define('TEXT_THANKS_FOR_SHOPPING','Bedankt voor het online winkelen bij ons!');
define('TABLE_HEADING_COMMENTS','We nemen spoedig contact met u op');
define('TABLE_HEADING_DOWNLOAD_DATE','Geldig tot en met: ');
define('TABLE_HEADING_DOWNLOAD_COUNT','Resterende downloads');
define('SMS_NEW_ORDER','U heeft een nieuwe bestelling');
define('TEXT_SUCCESS_INFO', 'Alle details over de betaling zijn per mail naar u verzonden');
