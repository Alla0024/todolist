<?php 
define('NAVBAR_TITLE_1','Mijn account');
define('NAVBAR_TITLE_2','Adresgegevens');
define('HEADING_TITLE','Adresgegevens');
define('PRIMARY_ADDRESS_TITLE','Uw adres');
define('PRIMARY_ADDRESS_DESCRIPTION','Dit is het primaire adres is.<br>Het zal worden gebruikt bij aankopen en elke andere handeling in onze winkel.');
define('ADDRESS_BOOK_TITLE','Adresgegevens');
define('PRIMARY_ADDRESS','(*)');
define('TEXT_MAXIMUM_ENTRIES','<b>OPMERKING:</b> Maximale hoeveelheid adressen <b>%s</b>.');
