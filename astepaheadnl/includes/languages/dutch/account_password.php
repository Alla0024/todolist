<?php 
define('NAVBAR_TITLE_1','Mijn account');
define('NAVBAR_TITLE_2','Wachtwoord wijzigen');
define('HEADING_TITLE','Wachtwoord wijzigen');
define('MY_PASSWORD_TITLE','Wachtwoord wijzigen');
define('SUCCESS_PASSWORD_UPDATED','U heeft uw uw wachtwoord gewijzigd.');
define('ERROR_CURRENT_PASSWORD_NOT_MATCHING','Uw huidige wachtwoord kwam niet overeen met het wachtwoord in onze administratie. Probeer het alstublieft opnieuw.');
