<?php 
define('NAVBAR_TITLE','De winkel is gesloten voor onderhoud.');
define('HEADING_TITLE','De winkel is tijdelijk gesloten ...');
define('DOWN_FOR_MAINTENANCE_TEXT_INFORMATION','Het onderhoud van de server, controleer later nog op terug.');
define('TEXT_MAINTENANCE_ON_AT_TIME','Sluitingsdatum: ');
define('TEXT_MAINTENANCE_PERIOD','De winkel zal hervatten via: ');
define('DOWN_FOR_MAINTENANCE_STATUS_TEXT','Klik op "Doorgaan", misschien is de winkel al werkt:');
