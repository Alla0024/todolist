<?php 
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE','Verwante korting');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_DESCRIPTION','Verwante korting');
define('TWOFER_PROMO_STRING','Koop dit artikel en krijg nog eens gratis!');
define('TWOFER_QUALIFY_STRING','U kunt voor verschillende %s gratis');
define('OFF_STRING_PCT','-%s');
define('OFF_STRING_CURR','-%s');
define('SECOND',' tweede ');
define('FREE_STRING',' gratis');
define('REV_GET_ANY','Kopen van goederen ');
define('REV_GET_THIS','Kopen ');
define('REV_GET_DISC',' ontvang het item ');
define('FREE',' gratis');
define('GET_YOUR_PROD',' krijgt ');
define('GET_YOUR_CAT',' uw keuze ');
