<?php 
define('MODULE_ORDER_TOTAL_COUPON_TITLE','Coupon');
define('MODULE_ORDER_TOTAL_COUPON_HEADER','Certificaten/Coupons');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION','Coupon');
define('ERROR_NO_INVALID_REDEEM_COUPON','Ongeldige coupon code');
define('ERROR_MINIMUM_CART_AMOUNT', 'Minimum winkelwagenbedrag voor deze coupon is: %s');
define('ERROR_INVALID_STARTDATE_COUPON','De gespecificeerde coupon is nog steeds ongeldig');
define('ERROR_INVALID_FINISDATE_COUPON','Deze coupon is verlopen');
define('ERROR_INVALID_USES_COUPON','De coupon werd al gebruikt ');
define('TIMES',' tijd.');
define('ERROR_INVALID_USES_USER_COUPON','U gebruikt de coupon zo vaak als mogelijk.');
define('TEXT_ENTER_COUPON_CODE','Uw code:&nbsp;&nbsp;');
