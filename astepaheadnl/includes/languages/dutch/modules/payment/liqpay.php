<?php 
define('MODULE_PAYMENT_LIQPAY_TEXT_TITLE','LiqPAY');
define('MODULE_PAYMENT_LIQPAY_TEXT_PUBLIC_TITLE','Visa/Mastercard LiqPay');
define('MODULE_PAYMENT_LIQPAY_TEXT_DESCRIPTION','Nadat u op Bevestig bestelling zal U worden omgeleid naar de betalingssysteem website om de bestelling te betalen, na betaling zal Uw bestelling worden ingevuld.');
define('MODULE_PAYMENT_LIQPAY_TEXT_ADMIN_DESCRIPTION','Betaling module LiqPAY..');
