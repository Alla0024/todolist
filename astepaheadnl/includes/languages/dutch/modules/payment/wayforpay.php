<?php

define('MODULE_PAYMENT_WAYFORPAY_TEXT_TITLE', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_PUBLIC_TITLE', 'Visa / Mastercard WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_DESCRIPTION', 'Omschrijving');
define('MODULE_PAYMENT_WAYFORPAY_TEXT_ADMIN_DESCRIPTION', 'WayForPay');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_TITLE', 'Module in- / uitschakelen');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Standaardstatus');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_TITLE', 'Handelaarsaccount');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_TITLE', 'Koopmansgeheim');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_TITLE', 'De sorteervolgorde');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_TITLE', 'Betalingsstaat');
define('MODULE_PAYMENT_WAYFORPAY_STATUS_DESC', 'Waar - inschakelen, Onwaar - uitschakelen');
define('MODULE_PAYMENT_WAYFORPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'De status die is ingesteld op de volgorde wanneer deze wordt gemaakt');
define('MODULE_PAYMENT_WAYFORPAY_MERCHANT_ACCOUNT_DESC', 'Handelaarsaccount');
define('MODULE_PAYMENT_WAYFORPAY_PASSWORD_DESC', 'Koopmansgeheim');
define('MODULE_PAYMENT_WAYFORPAY_SORT_ORDER_DESC', 'Sorteervolgorde van de module in het blok met betalingsmodules');
define('MODULE_PAYMENT_WAYFORPAY_ORDER_STATUS_ID_DESC', 'De status die bij succesvolle betaling aan de bestelling is toegewezen');
