<?php 
define('MODULE_PAYMENT_STRIPE_TEXT_TITLE','Streep');
define('MODULE_PAYMENT_STRIPE_TEXT_DESCRIPTION','Streep - online betalen');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_OWNER','De houder van de creditcard:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_NUMBER','Credit card nummer:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_MM','na de MM:');
define('MODULE_PAYMENT_STRIPE_TEXT_CREDIT_CARD_EXPIRES_YY','het verstrijken YY:');
define('MODULE_PAYMENT_STRIPE_TEXT_ERROR','Fout credit card!');
