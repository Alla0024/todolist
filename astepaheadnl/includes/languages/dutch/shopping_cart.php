<?php 
define('NAVBAR_TITLE','De inhoud van de winkelwagen');
define('HEADING_TITLE','Mijn winkelwagen');
define('TABLE_HEADING_REMOVE','Verwijderen');
define('TABLE_HEADING_REMOVE_FROM','uit de winkelwagen');
define('TABLE_HEADING_QUANTITY','Aantal');
define('TABLE_HEADING_IMAGE','Afbeelding');
define('TABLE_HEADING_NAME','Naam');
define('TABLE_HEADING_PRICE','Stuk prijs');
define('TABLE_HEADING_MODEL','Product code');
define('TABLE_HEADING_PRICE_TOTAL','Subtotaal');
define('TABLE_HEADING_PRODUCTS','Producten');
define('TABLE_HEADING_TOTAL','Totaal');
define('TEXT_CART_EMPTY','Uw winkelwagen is leeg!');
define('SUB_TITLE_COUPON','Kortingscode');
define('SUB_TITLE_COUPON_SUBMIT','Toepassen');
define('SUB_TITLE_COUPON_VALID','U heeft de juiste kortingscode!');
define('SUB_TITLE_COUPON_INVALID','U heeft een ongeldige kortingscode');
define('OUT_OF_STOCK_CANT_CHECKOUT','Helaas is de gekozen hoeveelheid niet op voorraad.');
define('OUT_OF_STOCK_CAN_CHECKOUT','Helaas is de gekozen hoeveelheid niet op voorraad.');
