<?php 
define('NAVBAR_TITLE_1','Inloggen');
define('NAVBAR_TITLE_2','Wachtwoord vergeten');
define('HEADING_TITLE','Ik ben mijn wachtwoord vergeten!');
define('TEXT_MAIN','Als u uw wachtwoord bent vergeten, vul dan hieronder uw e-mailadres in.<br />Wij sturen u een e-mail met het nieuwe wachtwoord.');
define('TEXT_NO_EMAIL_ADDRESS_FOUND','<span><b>Fout:</b></span> het e-mailadres is niet gevonden in onze administratie, probeer het opnieuw.');
define('EMAIL_PASSWORD_REMINDER_SUBJECT',STORE_NAME.' - Uw wachtwoord');
define('EMAIL_PASSWORD_REMINDER_BODY','Verzoek een nieuw wachtwoord ontvangen .Uw nieuwe wachtwoord in in '.STORE_NAME.' :%s');
define('SUCCESS_PASSWORD_SENT','Voltooid: uw nieuwe wachtwoord is verzonden naar het e-mailadres.');
