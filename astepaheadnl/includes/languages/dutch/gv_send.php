<?php 
define('HEADING_TITLE','Stuur certificaat');
define('NAVBAR_TITLE','Stuur certificaat');
define('EMAIL_SUBJECT','Een bericht van de online store');
define('HEADING_TEXT','<br / >uploaden van het certificaat, moet U invullen van de volgende vorm.<br>');
define('ENTRY_NAME','De naam van de ontvanger:');
define('ENTRY_EMAIL','E-Mail adres van de ontvanger:');
define('ENTRY_MESSAGE','Bericht:');
define('ENTRY_AMOUNT','De som van het certificaat:');
define('ERROR_ENTRY_AMOUNT_CHECK','&nbsp;&nbsp;<span class="errorText">Ongeldig bedrag</span>');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK','&nbsp;&nbsp;<span class="errorText">Ongeldig E-mailadres</span>');
define('MAIN_MESSAGE','U kiest voor het verzenden van het certificaat voor %s naar uw vriend %s, zijn e-mail adres: %s<br><br>certificaat Ontvanger ontvangt het volgende bericht:<br><br>Lieve %s<br><br>  U verstuurd het certificaat voor %s, afzender: %s');
define('PERSONAL_MESSAGE','%s schreef:');
define('TEXT_SUCCESS','Gefeliciteerd, Uw certificaat is succesvol verzonden');
define('EMAIL_SEPARATOR','----------------------------------------------------------------------------------------');
define('EMAIL_GV_TEXT_HEADER','Gefeliciteerd, Je hebt een certificaat voor %s');
define('EMAIL_GV_TEXT_SUBJECT','Een geschenk van %s');
define('EMAIL_GV_FROM','De afzender van dit certificaat is %s');
define('EMAIL_GV_MESSAGE','Het bericht van de afzender: ');
define('EMAIL_GV_SEND_TO','Hallo, %s');
define('EMAIL_GV_REDEEM','Activeren van het certificaat, klik op de link die eronder zit. Certificaat-code: %s');
define('EMAIL_GV_LINK','Klik hier om te activeren het certificaat ');
define('EMAIL_GV_FIXED_FOOTER','Als U problemen ondervindt bij het activeren van het certificaat via de links hierboven Wij raden u aan het invoeren certificaat code bij het afrekenen, en niet via bovenstaande link.');
define('EMAIL_GV_SHOP_FOOTER','');
