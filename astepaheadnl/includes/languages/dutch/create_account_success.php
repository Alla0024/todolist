<?php 
define('NAVBAR_TITLE_1','Account aanmaken');
define('NAVBAR_TITLE_2','Registratie voltooid');
define('HEADING_TITLE','Uw account is succesvol aangemaakt!');
define('TEXT_ACCOUNT_CREATED','U ontvangt de privileges van een geregistreerde gebruiker.  Een bevestiging van de registratie is verzonden naar het door u ingevulde e-mailadres. <br />Mocht u deze niet binnen het uur hebben ontvangen, neem dan contact met ons op.');
