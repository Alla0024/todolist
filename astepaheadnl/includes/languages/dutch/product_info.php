<?php 
define('TEXT_PRODUCT_NOT_FOUND','Product niet gevonden!');
define('TEXT_DATE_ADDED','Dit product is toegevoegd aan onze catalogus op %s');
define('TEXT_DATE_AVAILABLE','<span>Goederen beschikbaar zal zijn %s</span>');
define('PRODUCT_ADDED_TO_WISHLIST','Product is uitgesteld!');
define('TEXT_COLOR','Kleur');
define('TEXT_SHARE','');
define('TEXT_REVIEWSES','beoordelingen');
define('TEXT_REVIEWSES2','Beoordelingen');
define('TEXT_DESCRIPTION','Beschrijving');
define('TEXT_ATTRIBS','Kenmerken');
define('TEXT_PAYM_SHIP','Betaling en verzending');
define('SHORT_DESCRIPTION','Korte beschrijving');
define('HOME_TEXT_PAYM_SHIP','Garantie');
define('TEXT_PRICE_WITHOUT_VAT', 'Excl. BTW');
