<?php
/*
  $Id: bank.php,v 1.2 2002/11/22

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_TITLE', 'Bargeldlose Abrechnung');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_DESCRIPTION', 'Unsere Bank:<br><br>Bankname: &nbsp;&nbsp;&nbsp;' .   (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')  . '<br> Kontokorrent: &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')  . '<br> BIC: &nbsp;&nbsp;&nbsp; ' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')  . '<br> Cor./account: &nbsp;&nbsp;&nbsp; ' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')  . ' <br> INN: &nbsp;&nbsp;&nbsp; ' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')  . ' <br>Empfänger: &nbsp;&nbsp;&nbsp; ' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')  . 'Gearbox: &nbsp;&nbsp;&nbsp; ' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')  . '<br>Zahlung: &nbsp;&nbsp;&nbsp;' .  (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')  . '<br> Nach Zahlungsauftrag müssen uns informieren per E-Mail <a href="mailto:' . STORE_OWNER_EMAIL_ADDRESS . '">' . STORE_OWNER_EMAIL_ADDRESS . '</a> Zahlung Tatsache. Ihre Bestellung wird sofort versendet werden, nachdem Zahlung Tatsache bestätigt.<br><br><a href=kvitan.php target=_blank><b><span>Die Quittung für die Zahlung</a></b>');
define('MODULE_PAYMENT_BANK_TRANSFER_TEXT_EMAIL_FOOTER', "Unsere Bankverbindung: \n\nBankname: " .  (defined('MODULE_PAYMENT_BANK_TRANSFER_1')?MODULE_PAYMENT_BANK_TRANSFER_1:'')  . "\nRaschetny Konto:" .
   (defined('MODULE_PAYMENT_BANK_TRANSFER_2')?MODULE_PAYMENT_BANK_TRANSFER_2:'')  . "\nBIK:" . 
   (defined('MODULE_PAYMENT_BANK_TRANSFER_3')?MODULE_PAYMENT_BANK_TRANSFER_3:'')  . "\nKor / Kosten: " . 
   (defined('MODULE_PAYMENT_BANK_TRANSFER_4')?MODULE_PAYMENT_BANK_TRANSFER_4:'')  . "\ninn: " . 
   (defined('MODULE_PAYMENT_BANK_TRANSFER_5')?MODULE_PAYMENT_BANK_TRANSFER_5:'')  . "\nPoluchatel: " .
   (defined('MODULE_PAYMENT_BANK_TRANSFER_6')?MODULE_PAYMENT_BANK_TRANSFER_6:'')  . "\nKPP: " .
   (defined('MODULE_PAYMENT_BANK_TRANSFER_7')?MODULE_PAYMENT_BANK_TRANSFER_7:'')  . "\nNaznachenie Zahlung: " . 
   (defined('MODULE_PAYMENT_BANK_TRANSFER_8')?MODULE_PAYMENT_BANK_TRANSFER_8:'')  . "\n\nNach die Reihenfolge der Zahlung sollten Sie uns per E-Mail wissen lassen, " . STORE_OWNER_EMAIL_ADDRESS . " die Tatsache der Zahlung. Wird Ihre Bestellung sofort nach Zahlungsbestätigung der Tatsache gesendet wird.");