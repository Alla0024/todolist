<?php
/*
  WebMakers.com Added: Free Payments and Shipping
  Written by Linda McGrath osCOMMERCE@WebMakers.com
  http://www.thewebmakerscorner.com

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_FREECHARGER_TEXT_TITLE', 'Kostenloser Download');
define('MODULE_PAYMENT_FREECHARGER_TEXT_DESCRIPTION', 'Verwendet für freie virtuelle Güter');
define('MODULE_PAYMENT_FREECHARGER_TEXT_EMAIL_FOOTER', 'Kostenloser Download');