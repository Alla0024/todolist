<?php
/*
  $Id: webmoney_merchant.php 1778 2008-01-09 23:37:44Z hpdl $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2008 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_LIQPAY_TEXT_TITLE', 'LiqPAY');
define('MODULE_PAYMENT_LIQPAY_TEXT_PUBLIC_TITLE', 'Visa/Mastercard LiqPay');
define('MODULE_PAYMENT_LIQPAY_TEXT_DESCRIPTION', 'Nachdem Sie auf die Schaltfläche Bestellung bestätigen geklickt haben, werden Sie zur Bezahlung der Bestellung auf die Website des Zahlungssystems weitergeleitet. Nach der Zahlung wird Ihre Bestellung ausgeführt.');
define('MODULE_PAYMENT_LIQPAY_TEXT_ADMIN_DESCRIPTION', 'Zahlungsmodul LiqPAY ..');