<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2007 osCommerce

  Released under the GNU General Public License
*/

  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_PUBLIC_TITLE', 'Authorize.net');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_TEXT_DESCRIPTION', '<a href="https://www.authorize.net" target="_blank" style="text-decoration: underline; font-weight: bold;">Besuchen Sie Authorize.net Website</a>');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_OWNER', 'Kreditkarteninhaber:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_NUMBER', 'Kreditkartennummer:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_EXPIRES', 'Ablaufdatum der Kreditkarte:');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_CREDIT_CARD_CVC', 'Kartenprüfcode (CVC):');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_TITLE', 'Bei der Verarbeitung Ihrer Kreditkarte ist ein Fehler aufgetreten');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_GENERAL', 'Bitte versuchen Sie es erneut und falls die Probleme weiterhin bestehen, versuchen Sie es mit einer anderen Zahlungsmethode.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_DECLINED', 'Diese Kreditkartentransaktion wurde abgelehnt. Bitte versuchen Sie es erneut. Wenn die Probleme weiterhin bestehen, versuchen Sie es mit einer anderen Kreditkarte oder Zahlungsmethode.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_INVALID_EXP_DATE', 'Das Ablaufdatum der Kreditkarte ist ungültig. Bitte überprüfen Sie die Karteninformationen und versuchen Sie es erneut.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_EXPIRED', 'Die Kreditkarte ist abgelaufen. Bitte versuchen Sie es mit einer anderen Karte oder Zahlungsmethode erneut.');
  define('MODULE_PAYMENT_AUTHORIZENET_CC_AIM_ERROR_CVC', 'Die Kreditkartenprüfnummer (CVC) ist ungültig. Bitte überprüfen Sie die Karteninformationen und versuchen Sie es erneut.');
?>
