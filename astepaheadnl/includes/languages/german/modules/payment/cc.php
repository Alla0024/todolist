<?php
/*
  $Id: cc.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_PAYMENT_CC_TEXT_TITLE', 'Kreditkarte');
define('MODULE_PAYMENT_CC_TEXT_DESCRIPTION', 'Kreditkarteninformationen für den Test: <br><br> Kartennummer: 411111111111111111- Gültig bis: Beliebiges Datum');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_OWNER', 'Kreditkarteninhaber:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_NUMBER', 'Kreditkartennummer:');
define('MODULE_PAYMENT_CC_TEXT_CREDIT_CARD_EXPIRES', 'Gültig bis:');
define('MODULE_PAYMENT_CC_TEXT_ERROR', 'Daten falsch eingegeben!');