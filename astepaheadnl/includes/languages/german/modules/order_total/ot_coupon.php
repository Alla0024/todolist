<?php
/*
  $Id: ot_coupon.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_COUPON_TITLE', 'Coupon');
define('MODULE_ORDER_TOTAL_COUPON_HEADER', 'Zertifikate / Coupons');
define('MODULE_ORDER_TOTAL_COUPON_DESCRIPTION', 'Coupon');
define('ERROR_NO_INVALID_REDEEM_COUPON', 'Ungültiger Gutscheincode');
define('ERROR_MINIMUM_CART_AMOUNT', 'Der Mindestbestellwert für diesen Gutschein beträgt: %s');
define('ERROR_INVALID_STARTDATE_COUPON', 'Der angegebene Coupon existiert nicht');
define('ERROR_INVALID_FINISDATE_COUPON', 'Dieser Coupon ist abgelaufen');
define('ERROR_INVALID_USES_COUPON', 'Coupon wurde bereits verwendet');
define('ZEITEN', 'Zeiten.');
define('ERROR_INVALID_USES_USER_COUPON', 'Du hast den Gutschein so oft wie möglich benutzt.');
define('TEXT_ENTER_COUPON_CODE', 'Dein Code:&nbsp;&nbsp;');