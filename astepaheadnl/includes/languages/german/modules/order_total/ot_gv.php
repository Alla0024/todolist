<?php
/*
  $Id: ot_gv.php,v 1.1.1.1 2003/09/18 19:04:32 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('MODULE_ORDER_TOTAL_GV_TITLE', 'Zertifikat');
define('MODULE_ORDER_TOTAL_GV_HEADER', 'Zertifikate / Coupons');
define('MODULE_ORDER_TOTAL_GV_DESCRIPTION', 'Geschenkgutscheine');
define('MODULE_ORDER_TOTAL_GV_USER_PROMPT', 'Sie können Ihr Zertifikat verwenden, aktivieren Sie dieses Kontrollkästchen und klicken Sie auf "Übernehmen":&nbsp;');
define('TEXT_ENTER_GV_CODE', 'Zertifikatscode&nbsp;&nbsp;');