<?php
/*
  $Id: index.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_MAIN', '&nbsp;');
define('TABLE_HEADING_DATE_EXPECTED', 'Datum der Ankunft');
define('TABLE_HEADING_DEFAULT_SPECIALS', 'Rabatte %s');

if (($category_depth == 'produkte') || (isset ($_GET ['manufacturers_id'])) ) {
  define('HEADING_TITLE', 'Produktliste');

  define('TABLE_HEADING_IMAGE', '<span class="sort"> Sortieren nach:</span>');
  define('TABLE_HEADING_MODEL', 'Artikelcode');
  define('TABLE_HEADING_PRODUCTS', '<span class="cena">Name</span>');
  define('TABLE_HEADING_MANUFACTURER', 'Hersteller');
  define('TABLE_HEADING_QUANTITY', 'Menge');
  define('TABLE_HEADING_PRICE', '<span class="cena">Preis</span>');
  define('TABLE_HEADING_WEIGHT', 'Gewicht');
  define('TABLE_HEADING_PRODUCT_SORT', 'Bestellung');
  define('TEXT_ALL_MANUFACTURERS', 'Alle Produzenten');
} elseif ($category_depth == 'top') {
  define('HEADING_TITLE', 'Willkommen');
} elseif ($category_depth == 'verschachtelt') {
  define('HEADING_TITLE', 'Abschnitte');
}