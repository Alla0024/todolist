<?php
/*
  $Id: create_account_success.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Registrierung');
define('NAVBAR_TITLE_2', 'Erfolgreich');
define('HEADING_TITLE', 'Dein Account wurde erfolgreich erstellt!');
define('TEXT_ACCOUNT_CREATED', 'Herzlichen Glückwunsch, Ihre Registrierung wurde erfolgreich abgeschlossen! Sie erhalten die Privilegien des registrierten Benutzers. <br><br> Die Bestätigung der vorläufigen Registrierung wurde per E-Mail an die in der Registrierung angegebene Adresse gesendet.');