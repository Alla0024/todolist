<?php
/*
  $Id: checkout_success.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE_1', 'Bestellung');
define('NAVBAR_TITLE_2', 'Erfolgreich');
define('HEADING_TITLE', 'Deine Bestellung ist dekoriert!');
define('TEXT_SUCCESS', 'Ihre Bestellung wurde erfolgreich ausgestellt!');
define('TEXT_THANKS_FOR_SHOPPING', 'Ihre Bestellung wurde erfolgreich ausgestellt!');
define('TABLE_HEADING_COMMENTS', 'Wir werden uns bald mit Ihnen in Verbindung setzen');
define('TABLE_HEADING_DOWNLOAD_DATE', 'Der Link ist gültig bis:');
define('TABLE_HEADING_DOWNLOAD_COUNT', 'mal kannst du die Datei hochladen.');
define('SMS_NEW_ORDER', 'Sie haben eine neue Bestellung');
define('TEXT_SUCCESS_INFO', 'Alle weiteren Zahlungsanweisungen und -details wurden Ihnen per Post zugesandt');