<?php
/*
  $Id: cookie_usage.php,v 1.1.1.1 2003/09/18 19:04:30 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Cookies verwenden');
define('HEADING_TITLE', 'Cookies verwenden');
define('TEXT_INFORMATION', 'Ihr Browser unterstützt keine Cookies oder Support ist deaktiviert. <br><br>Um mit unserem Shop arbeiten zu können, müssen Sie Cookies in Ihrem Browser aktivieren. <br><br> Browser-Handbuch <b>Internet Explorer</b>:<br><ol><li>Extras, dann Internetoptionen </li><li> Wählen Sie die Registerkarte Sicherheit, legen Sie die Sicherheitsstufe auf Mittel</li></ol><br><br>fest If Sie haben Probleme beim Einkaufen in unserem Geschäft, wenden Sie sich an den Filialadministrator, um das Problem zu beheben.');
define('BOX_INFORMATION', 'Um mit unserem Shop zu arbeiten, müssen Sie Cookies in Ihrem Browser aktivieren.<br><br> Wenn Sie beim Einkauf in unserem Geschäft Probleme haben, wenden Sie sich an den Geschäftsadministrator, um die Probleme zu beheben. ');