<?php
/*
  $Id: gv_redeem.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Zertifikat aktivieren');
define('HEADING_TITLE', 'Zertifikat aktivieren');
define('TEXT_INFORMATION', '');
define('TEXT_INVALID_GV', 'Das angegebene Zertifikat existiert entweder nicht oder es wurde bereits verwendet.) Wenn Sie Fragen haben, kontaktieren Sie uns bitte.');
define('TEXT_VALID_GV', 'Sie haben Ihr Zertifikat erfolgreich für den Betrag von %s aktiviert! Jetzt können Sie Ihr Zertifikat verwenden, um Einkäufe in unserem Online-Shop zu tätigen.');
?>
