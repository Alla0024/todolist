<?php
/*
  $Id: product_info.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TEXT_PRODUCT_NOT_FOUND', 'Produkt nicht gefunden!');
define('TEXT_DATE_ADDED', 'Das Produkt wurde zu unserem Verzeichnis hinzugefügt %s');
define('TEXT_DATE_AVAILABLE', '<span>Artikel ist im Lager %s</span>');
define('PRODUCT_ADDED_TO_WISHLIST', 'Artikel erfolgreich verzögert!');
define('TEXT_COLOR', 'Farbe');
define('TEXT_SHARE', 'Teilen mit Freunden');
define('TEXT_REVIEWSES', 'Bewertungen');
define('TEXT_REVIEWSES2', 'Reviews');
define('TEXT_DESCRIPTION', 'Beschreibung');
define('TEXT_ATTRIBS', 'Eigenschaften');
define('TEXT_PAYM_SHIP', 'Zahlung und Lieferung');
define('SHORT_DESCRIPTION', 'Kurzbeschreibung');

//home
define('HOME_TEXT_PAYM_SHIP', 'Garantie');