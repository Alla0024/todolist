<?php
/*
  $Id: gv_send.php,v 1.1.1.1 2003/09/18 19:04:28 wilt Exp $

  The Exchange Project - Community Made Shopping!
  http://www.theexchangeproject.org

  Gift Voucher System v1.0
  Copyright (c) 2001,2002 Ian C Wilson
  http://www.phesis.org

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Zertifikat senden');
define('NAVBAR_TITLE', 'Zertifikat senden');
define('EMAIL_SUBJECT', 'Nachricht aus dem Online-Shop');
define('HEADING_TEXT', '<br>Um ein Zertifikat zu senden, müssen Sie das folgende Formular ausfüllen.<br>');
define('ENTRY_NAME', 'Empfängername:');
define('ENTRY_EMAIL', 'E-Mail-Adresse des Empfängers:');
define('ENTRY_MESSAGE', 'Nachricht:');
define('ENTRY_AMOUNT', 'Zertifikatsumme:');
define('ERROR_ENTRY_AMOUNT_CHECK', '&nbsp;&nbsp;<span class="errorText">Ungültiger Betrag</span>');
define('ERROR_ENTRY_EMAIL_ADDRESS_CHECK', '&nbsp;&nbsp;<span class="errorText">Ungültige E-Mail-Adresse</span>');
define('MAIN_MESSAGE', 'Sie haben sich entschieden, ein Zertifikat zu senden %s seinem Freund %s, seine E-Mail-Adresse: %s<br><br>Der Empfänger des Zertifikats erhält die folgende Nachricht:<br><br>Geachtet %s<br><br>
                        Sie haben ein Zertifikat für erhalten %s, Absender: %s');
define('PERSONAL_MESSAGE', ' %s schreibt:');
define('TEXT_SUCCESS', 'Glückwünsche, wird das Zertifikat erfolgreich gesendet');
define('EMAIL_SEPARATOR', '------------------------------------------- --------------------------------------------- ');
define('EMAIL_GV_TEXT_HEADER', 'Glückwunsch, Sie haben das Zertifikat für die Menge von %s empfangen');
define('EMAIL_GV_TEXT_SUBJECT', 'Gift von %s');
define('EMAIL_GV_FROM', 'Sender dieses Zertifikat - %s');
define('EMAIL_GV_MESSAGE', 'Nachricht des Absenders:');
define('EMAIL_GV_SEND_TO', 'Hallo, %s');
define('EMAIL_GV_REDEEM', 'Um das Zertifikat zu aktivieren, öffnen Sie den folgenden Link: Zertifikats-ID: %s');
define('EMAIL_GV_LINK', 'Klicken Sie hier, um das Zertifikat zu aktivieren');
define('EMAIL_GV_FIXED_FOOTER', 'Wenn Sie Probleme haben, wenn Sie das Zertifikat über den obigen Link aktivieren,' . "\n" .
                                'Wir empfehlen, den Zertifikatscode bei der Bestellung einzugeben und nicht über den obigen Link.' . "\n\n");
define('EMAIL_GV_SHOP_FOOTER', '');