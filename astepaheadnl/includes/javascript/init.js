"use strict";
var initSlider;
jQuery(document).ready(function() {
    $(document).on('input','form[name="create_account"] input[type="text"]',function(){
        $(this).val($(this).val().replace(/(<([^>]+)>)/ig,"")); //striptags
    });
    //lazyload
    $(".lazyload").lazyload();//.addClass('anim');
    //$(".product img").unveil();
    //$(".product_list img").unveil();
    $(".row_catalog_products img").unveil(700, addAnimClassToImg)//.addClass('anim');
    $("#sidebar-left .row_catalog_products img").unveil(0, addAnimClassToImg); // show images in left colunm
    $(".logo img").unveil(0, addAnimClassToImg);
    $('#loadMoreProducts').click(function () {
        loadMoreProducts();
    });
    $(document).on('click', '.ch_link', function(event) {
        hs.expand(this);
        return false;
    });
    $('.buy_one_click').click(function(event) {
        var $modal = null;
        $.post('./ajax.php', {
            request: 'getBuyOnClickForm'
        }, function(data) {
            modal({
                id: 'QuickBuy',
                body: data.html,
                render: true,
                after: function(modal) {
                    $modal = modal;

                }
            });

            $('#QuickBuyForm').on('submit', function(event) {
                event.preventDefault();

                var $data = $('#QuickBuyForm,#products_id').serialize();
                $data += '&prod_price_uah=' + $('.productSpecialPrice').text();
                $data += '&model=' + $('.art_card_product').text();
                $.post('./ajax.php', $data, function(data, textStatus, xhr) {

                    if (data.success) {
                        $('#QuickBuyForm').html(data.message);

                    } else {
                        alert('Send error!');
                    }
                }, 'json');
                if(GOOGLE_GOALS_CLICK_ON_PHONE_DESKTOP && IS_MOBILE == "0") {
                    gtag('event', 'quick_buy_submit_desktop');
                }else if(GOOGLE_GOALS_CLICK_ON_PHONE_MOBILE && IS_MOBILE == "1"){
                    gtag('event', 'quick_buy_submit_mobile');
                }
            });

        }, 'json');
        return false;
    });

    $(document).on('click','.sidebar-toggle-back',function(event) {
        event.stopPropagation();
        var $button = jQuery('.sidebar-toggle-back');
        jQuery('#sidebar-left').toggleClass('opened');
        $button.removeClass('visible-xs').removeClass('visible-sm');
        $button.addClass('hidden-xs').addClass('hidden-sm');
        jQuery('#sidebar-left').unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd");

    });
    $(document).on('click', '.sidebar-toggle-up', function(event) {
        event.stopPropagation();
        var $button = $('.sidebar-toggle-back');
        jQuery('#sidebar-left').toggleClass('opened');
        jQuery('#sidebar-left').on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function() {
            $button.removeClass('hidden-xs').removeClass('hidden-sm');
            $button.addClass('visible-xs').addClass('visible-sm');
        });

    });

    // when click on "reviews" it opens for us "reviews" tab
    $(document).on('click','.quantity_rating', function(e) {
        e.preventDefault();
        $('.nav-tabs a[href="#tab-comments"]').tab('show');
    });

    // show share popup windows
    $('.share_with_friends a, .social_group_footer a, .social_buttons a').click(function(e) {
        e.preventDefault();
        showLoginvk($(this).attr('href'));
    });

    $('.nav-tabs a').click(function(e) {
        e.preventDefault();
        $(this).tab('show')
    });
    

    $('#cat_accordion').cutomAccordion({
        classExpand : "custom_id"+$('input[name=current_hidden_cat_id]').val(),
        menuClose: false,
        autoClose: true,
        saveState: false,
        disableLink: false,
        autoExpand: true
    });
    $('#left_cat_accordion').cutomAccordion({
        classExpand : "custom_id"+$('input[name=current_hidden_cat_id]').val(),
        menuClose: false,
        autoClose: false,
        saveState: false,
        disableLink: false,
        autoExpand: true
    });

    /*
     $(document).on("shown.bs.collapse shown.bs.tab", ".panel-collapse, a[data-toggle='tab']", function(e) {
     var $elOffset_top = $(e.target).parent('.panel').offset().top;
     jQuery('body,html').stop(false, false).animate({
     scrollTop: $elOffset_top
     }, {
     duration: 600,
     easing: 'swing'
     }, 800);
     });
     */

    if($(window).width()>'768') $('.categories_menu ul').superfish({
        autoArrows:false,
        onInit:function(){
            $('img').addClass('anim');
        }
    });

    jQuery('#slider_product a').click(function(e) {
        if (jQuery(this).is('.active')) {
            e.preventDefault();
            return false;
        } else {
            jQuery('#slider_product a').removeClass('active');
            jQuery(this).addClass('active');
        }
    });

    $("#sync1").owlCarousel({
        items: 1,
        loop: true,
        dots: true,
        slideSpeed: 200,
        nav: true,
        navText:['<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>','<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>'],
        dotsContainer: '#sync2',
        responsiveRefreshRate: 200,
        onTranslated: function(){
            var images = this.$element.find('.lazyload');
            if (images.length){
                this.$element.find('.lazyload').lazyload();
            }
        }
    });

    $("#owl-frontslider").owlCarousel({
        items: 1,
        nav: true,
        lazyLoad: true,
        loop:true,
        video:true,
        navText:['<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>','<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>'],
        autoplay:true,
        autoplayTimeout:5000,
        dotsContainer: '#carousel-custom-dots',
        autoplayHoverPause:true,
        smartSpeed: 2000,
        onInitialized:function () {$(".owl-carousel, .single_slide").removeAttr('style');$(".active .owl-video-play-icon").trigger("click");}, // autoplay video on slider load
        onTranslated:function () {$(".active .owl-video-play-icon").trigger("click");} // autoplay video on change slide

    });
    // custom dots:
    $('.owl-dot').click(function () {
        //  $(this).html();
        $("#owl-frontslider").trigger('to.owl.carousel', [$(this).index(), 300]);
    });

    $("#manufacturers > div").owlCarousel({
        items: 4,
        responsive:{0:{items:2},600:{items:5}},
        nav: true,
        dots: false,
        loop:true,
        navText:['<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>','<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>'],
        slideSpeed: 200
    });
    $("#manufacturers > div img").unveil(700, addAnimClassToImg);
    $('.owl-carousel img').lazyload();
    /* ---------  END Additional images --------- */
    ///-------------------test-----------------------
    if (typeof $.fn.owlCarousel != 'undefined') {

        var sliderObject = document.getElementById("news");
        if (sliderObject != null) {
            var cols = $('#news').attr('data-carousel-cols');
            cols = cols ? cols : '2;3;3;5;5';
            var array_cols = cols.split(";");
            var itemsCount = sliderObject.querySelectorAll('a').length;

            var it0 = array_cols[0];
            var it600 = array_cols[1];
            var it992 = array_cols[2];
            var it1200 = array_cols[3];
            var it1600 = array_cols[4];

            var navValue0 = it0 < itemsCount;
            var navValue600 = it600 < itemsCount;
            var navValue992 = it992 < itemsCount;
            var navValue1200 = it1200 < itemsCount;
            var navValue1600 = it1600 < itemsCount;
            $("#news > div").owlCarousel({
                items: array_cols[3],
                responsive: {
                    0: {items: it0, nav: navValue0},
                    600: {items: it600, nav: navValue600},
                    992: {items: it992, nav: navValue992, loop: true},
                    1200: {items: it1200, nav: navValue1200, loop: true},
                    1600: {items: it1600, nav: navValue1600, loop: true}
                },
                nav: true,
                dots: false,
                loop: true,
                navText: ['<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>', '<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>'],
                slideSpeed: 200,
            });
        }
    }
    if (typeof $.fn.unveil != 'undefined') {
        $("#news > div img").unveil(100, addAnimClassToImg);
    }
    /////------------------test----------------------

    /* ---------  Shopping cart --------- */
    jQuery('body').on('submit', 'form[name="cart_quantity"]', function(event) {
        event.preventDefault();
        doAddProduct(this);
    });

    // add to cart in product listing
    jQuery('body').on('click', '.add2cart', function(event) {
        doAddProductList($(this));
    });

    jQuery('body').on('click', '#checkoutButton', function(event) {
        event.preventDefault();
        var href = jQuery(this).attr('href');
        jQuery("#popup_cart_form").ajaxSubmit({
            target: '#modal_cart_popup .modal-body',
            success: function() {
                showPopupResponse();
                window.location.href = href;
            }
        });
        /*var options={target:'#modal_cart_popup .modal-body', detailhref:href};
         ajaxSubmitSerializePopup (options);*/
    });
    jQuery('body').on('focus', 'input[name="cart_quantity[]"]', function(event) {
        $(this).parent().next('.ok').fadeIn();
    });
    jQuery('body').on('click', '#popup_cart_form .ok', function(event) {
        jQuery("#popup_cart_form").ajaxSubmit({
            target: '#modal_cart_popup .modal-body',
            success: showPopupResponse
        });
        //var options={target:'#modal_cart_popup .modal-body'};
        //ajaxSubmitSerializePopup (options);
    });

    //by Demo2
    jQuery(document).on('shown.bs.dropdown','#divShoppingCard', function(event) {
        event.preventDefault();
        $.get('./popup_cart.php',null,function(response) {
            $('#new_basked_block').html(response);
        });
    });
    //by Demo2
    jQuery(document).on('change', '#new_basked_block input[name="cart_quantity[]"]', function(event) {
        jQuery("#popup_cart_form").ajaxSubmit({
            target: '#new_basked_block',
            success: function(){
                $.post('./ajax_update_cart.php','', function(data) {
                    $('.quantity_basket,.mobile_cart_count').html(data.cart_count);
                    $.get('./popup_cart.php',null,function(response) {
                        $('#new_basked_block').html(response);
                    });
                },'json');
            }
        });
    });

    jQuery('body').on('click', '#popup_cart_form .delete', function(event) {
        //	jQuery("#cart_delete" + jQuery(this).val()).attr('checked', 'checked');
        $(this).parent().find($('input[name="cart_delete[]"]')).attr('checked', 'checked');
        if (jQuery("#popup_cart_form .delete").length == 1) { // if there is only one element, hide other element in shopping cart
            jQuery("#popup_cart_form").animate({
                opacity: 0
            }, 200, function() { // fadeout of deleted element
                jQuery("#modal_cart_popup .modal-body").animate({
                    height: jQuery("#modal_cart_popup .modal-body").height() - 163
                }, 200, function() { // fadeout of deleted element
                    jQuery("#popup_cart_form").ajaxSubmit({
                        target: '#modal_cart_popup .modal-body',
                        success: showPopupResponse
                    }); // submit form
                    //var options={target:'#modal_cart_popup .modal-body'};
                    //ajaxSubmitSerializePopup (options);
                });
            });
        } else {
            jQuery(this).parent().parent().slideUp(200, function() { // fadeout of deleted element
                jQuery("#popup_cart_form").ajaxSubmit({
                    target: '#modal_cart_popup .modal-body',
                    success: showPopupResponse
                }); // submit form
                //var options={target:'#modal_cart_popup .modal-body'};
                //ajaxSubmitSerializePopup (options);
            });
        }
        var _vall = $(this).attr('data-clearpid');

        $('.added2cart[data-id='+_vall+']').replaceWith(sprintf(RTPL_ADD_TO_CART_BUTTON, _vall));
        $("#r_buy_intovar[data-id="+_vall+"]").html(RTPL_ADD_TO_CART_BUTTON_PRODUCT_PAGE); // replace button in product info page.

    });

    /* ---------  Shopping cart --------- */

    /* ---------  Subscribe --------- */
    jQuery(document).on('submit', '[class="form_subscribe_news"]', function(event) {
        event.preventDefault();
        var this_form = $(this);

        $.post($(this).attr('action'), $(this).serialize(), function(response) {
            this_form.trigger("reset"); // обнуляем поля
            modal({
                id: 'subscribe',
                body: response.message,
                render: true,
                after: function(modal) {
                    setTimeout(function() {
                        modal.modal('hide');
                    }, 3500);
                }
            });
        },'json');
        /* Act on the event */
    });

    /* ---------  Popups --------- */
    jQuery(document).on('submit', '[name="contact_us"], [name="dealer"]', function(event) {
        event.preventDefault();
        var this_form = $(this);
        // Sending email
        $.post($(this).attr('action'), $(this).serialize(), function(response) {
            if(response.status!='fail') {
                this_form.trigger("reset");
                if (typeof grecaptcha === 'object'){
                    grecaptcha.reset();
                }
            } // reset all fields
            modal({
                id: 'ContactUs',
                body: response.msg,
                render: true,
                after: function(modal) {
                    /*   setTimeout(function() {
                           modal.modal('hide');
                       }, 3500);  */
                }
            });
        },'json');
        /* Act on the event */
    });


    jQuery('body').on('click touchstart', '.popup_cart', function(event) {
        event.preventDefault();
        showCartpopup();
    });

    jQuery('.call_us_btn').click(function(e) {
        e.preventDefault();
        pop_contact_us();
    });

    $('#user-login-dropdown .dropdown-menu').on('click', function(event){
        event.stopPropagation();
    });

    /* --------- End Popups --------- */

    $(document).ready(function() {

        if ($(window).width() > 768) {
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
        }

        jQuery('body').on('click', '.show_search_form', function () {
            openSearchForm();
        });

        $(document).on('click', '#search-form-button-close, #search-form-button-close1', function () {
            closeSearchForm();
        });

        $(document).on('click', '#search-form-fader', function () {
            closeSearchForm();
        });


        $(window).on("scroll", function () {
            // adding shadow to header when scrolling
            var stickyPosition = Math.abs(parseInt($('header').css('top'), 10));

            if ($(this).scrollTop() > stickyPosition) {
                $('header').addClass('header_shadow');

            } else {
                $('header').removeClass('header_shadow');
            }
        });

        $('.hover .dropdown-menu').on("click", function (e) {
            e.stopPropagation();
            // e.preventDefault();
        });

        if ($(window).width() >= 992) {
            $("#user-login-dropdown")
            // on #user-login-dropdown hovered
                .mouseenter(function () {
                    // opens dropdown on hover
                    // $(this).addClass('open');
                })
                // on #user-login-dropdown unhovered
                .mouseleave(function () {
                    // closes dropdown on hover
                    // $(this).removeClass('open');
                });


            $(".dropdown-hover")
            // on #header-megamenu hovered
                .mouseenter(function () {
                    // opens dropdown on hover
                    $(this).addClass('open');
                })
                // on #header-megamenu unhovered
                .mouseleave(function () {
                    // closes dropdown on hover
                    $(this).removeClass('open');
                });
        }

        $(".dropdown-submenu a.submenu")
        // hover on button to open submenu
            .mouseenter(function() {
                var headerHeight = Math.abs(parseInt($('header').css('height'), 10));
                var menuHeight = Math.abs(parseInt($('.add_nav').css('height'), 10));

                // if submenu exists add class .hover for visible effect
                $(this).parent('li').addClass('hover');

                // when scrolling with opened .submenu
                $(window).on("scroll", function() {
                    var windowsScrolled = $(this).scrollTop();

                    // if scrolled less than 164px from top, then change top: value for .dropdown-menu inside .submenu
                    if ($(this).scrollTop() < 164) {
                        $('li.hover .dropdown-menu').css({
                            'top': headerHeight-windowsScrolled+'px'
                        });
                        // else set top: value 51px for .dropdown-menu inside .submenu
                    } else {
                        $('li.hover .dropdown-menu').css({
                            'top': '51px'
                        });
                    }
                });
            })

            // leave mouse from button (that opens submenu) to any direction
            .mouseleave(function() {
                $('.dropdown-submenu')
                // hover mouse on .submenu
                    .mouseenter(function() {
                        // removes class .hover when left not to parent button
                        $(this).siblings('li.hover').removeClass('hover');
                    });
            });



        // delete in two weeks (22.02.19)
        // $(".dropdown-submenu a.submenu")
        // // hover on button to open submenu
        //     .mouseenter(function() {
        //         var headerHeight = Math.abs(parseInt($('header').css('height'), 10));
        //         var menuHeight = Math.abs(parseInt($('.add_nav').css('height'), 10));
        //
        //         // if submenu exists add class .hover for visible effect
        //         // $(this).parent('li').addClass('hover');
        //
        //         // getting padding-left: of bootstrap class .container
        //         var pageContainerPaddingLeft =  Math.abs(parseInt($('.container').css('padding-left'), 10));
        //
        //         // getting margin-left: of bootstrap class .container
        //         var pageContainerMarginLeft =  Math.abs(parseInt($('.container').css('margin-left'), 10));
        //
        //         // getting width: of .dropdown-menu when opened
        //         var menuWidth = Math.abs(parseInt($('.dropdown .dropdown-menu').css('width'), 10));
        //
        //         // summing padding-left: (.container), margin left (.container) and .dropdown-menu width
        //         var dropDownMenuLeft = menuWidth+pageContainerMarginLeft+pageContainerPaddingLeft-1;
        //
        //         $(this).siblings('.dropdown-menu').css('left', dropDownMenuLeft+'px');
        //
        //         if ($(window).scrollTop() >= 164) {
        //             $(this).siblings('.dropdown-menu').css('top', '51px');
        //         } else {
        //             $(this).siblings('.dropdown-menu').css('top', headerHeight+'px');
        //         }
        //
        //         // when scrolling with opened .submenu
        //         $(window).on("scroll", function() {
        //             var windowsScrolled = $(this).scrollTop();
        //
        //             // if scrolled less than 164px from top, then change top: value for .dropdown-menu inside .submenu
        //             if ($(this).scrollTop() < 164) {
        //                 $('li.hover .dropdown-menu').css({
        //                     'top': headerHeight-windowsScrolled+'px'
        //                 });
        //                 // else set top: value 51px for .dropdown-menu inside .submenu
        //             } else {
        //                 $('li.hover .dropdown-menu').css({
        //                     'top': '51px'
        //                 });
        //             }
        //         });
        //     })
        //
        //     // leave mouse from button (that opens submenu) to any direction
        //     .mouseleave(function() {
        //         $('.dropdown-submenu')
        //         // hover mouse on .submenu
        //             .mouseenter(function() {
        //                 // removes class .hover when left not to parent button
        //                 $(this).siblings('li.hover').removeClass('hover');
        //             });
        //     });

    });

    $('#pdf_block span.btn-link').click(function(){
        window.open($(this).data('href'));
        return false;
    });


    /* ---------   attr_select --------- */


    $(function() {

        //  calculate_sum($('.select_id_option:first :selected'));
        calculate_sum($('.select_id_option:first'));

        $(document).on('change', '.select_id_option', function(e) {
            if ($(this).hasClass('select_attr_img')){
                var selected_op = this.options[e.target.selectedIndex];
            }else {
                var selectizeLoc = this.selectize;
                var selected_op = selectizeLoc.$input[0].selectedOptions[0];
                selected_op.setAttribute("data-prefix", selectizeLoc.options[selected_op.value].prefix);
            }
            calculate_sum($(selected_op));
        });

        $(document).on('click', '.prod_options_radio label', function (e) {
            var $option = $('#'+$(this).attr('for')).val();
            var $name = $('#'+$(this).attr('for')).attr('name');
            // $('#select_id_'+$name).val($option).change();
            $('#select_id_'+$name)[0].selectize.setValue($option);
        });

    });

    /* --------- end  attr_select --------- */

    /* --------- DEBUG --------- */
    if (jQuery('pre.debug').length) {
        jQuery('pre.debug').remove().insertBefore('body');
        jQuery('pre.debug').fadeIn(300).animate({
            top: '100px'
        }, "slow");
        jQuery('pre.debug').append('<span class="close">X</span>');
        jQuery('pre.debug .close').click(function() {
            jQuery(this).parent('.debug').remove();
        });
    }
    /* --------- /DEBUG --------- */



    /* --------- SEARCH --------- */
    /*  jQuery('#searchpr').on({
          focus: function() {
              jQuery(this).val() == this.defaultValue && jQuery(this).val('');
          },
          blur: function() {
              !jQuery(this).val() && jQuery(this).val(this.defaultValue);
          }
      });  */
    var id_search = $("#searchpr").attr('id');
    $("#searchpr").autocomplete("ajax_search.php", {
        resultsClass: "ac_results " +id_search,
        delay: 200,
        minChars: 2,
        matchSubset: 1,
        autoFill: false,
        matchContains: 1,
        cacheLength: 10,
        selectFirst: true,
        formatItem: liFormat,
        maxItemsToShow: 8,
        onItemSelect: selectItem
    });
    $("#searchpr1").autocomplete("ajax_search.php", {
        delay: 200,
        minChars: 2,
        matchSubset: 1,
        autoFill: false,
        matchContains: 1,
        cacheLength: 10,
        selectFirst: true,
        formatItem: liFormat,
        maxItemsToShow: 8,
        onItemSelect: selectItem
    });
    /* --------- END SEARCH --------- */

    // ACCORDION
    $('.accordion').accordion({
        heightStyle: "content",
        header: "h3",
        active:false,
        collapsible:true,
        autoHeight:false
    });

    $(document).on('click', '.ajax_modal_article', function(e) {
        e.preventDefault();
        $.post('./ajax_modal_article.php',{ article_id:$(this).attr('data-id')},function(data) {
            modal({
                id: 'ajax_modal_article',
                title: data.articles_name,
                body: data.articles_description
            });
        },'json');
    });

    // making "Create Account" button active if "store rules" checked
    $('#shoprules').click( function(e) {
        var el = 'form[name=create_account] input[type=submit]';
        if($(el).hasClass('active_submit')) $(el).removeClass('active_submit');
        else $(el).addClass('active_submit');
    });

    // POPUP LOGIN WINDOW
    $('.enter_link').click(
        function(e){
            e.preventDefault();
            $('.enter_window').fadeIn();
        }
    );

    $('.close_window').click(
        function(e){
            e.preventDefault();
            $('.enter_window').fadeOut(100);
        }
    );

    // SHOW/HIDE NAVIGATION WHEN WIDTH <= 768 PX
    $('.main_nav > ul').prepend('<li class="mobile"></li>');

    $('.toggle_nav').click(function(e) {
        if( $('.main_nav li').hasClass('mobile') ) {
            e.preventDefault();
            $('.main_nav').toggleClass('expand');
        }
    });

    $('.main_nav li > a').click(function(e) {
        $('.main_nav li').removeClass('active_page');
        $(this).closest('li').addClass('active_page');
    });

    // TABS
    $('.tabs').tabs();

    $('.nav-tabs').children('li:first-child').addClass('active');
    $('.tab-content').children('.tab-pane.fade:first-child').addClass('in active');

    /* --------- SELECTIZE --------- */

    $("select").not("select[name=billing_country], select[name=shipping_country], select[name=selectCountry], select[name=country], .select_attr_img").selectize({
        hideSelected:true,
        maxItems:1
    });

    $('select[name="currency"], select[name="row_by_page"], select#pl_sort, select.select_attr_select').selectize({
        hideSelected:true,
        maxItems:1
        // plugins: ['hidden_textfield']
    });
    $('select[name="selectCountry"], select[name=country], select[name="shipping_zone_id"]').selectize({
        hideSelected:true,
        maxItems:1,
        highlight:true,
        onFocus: function(){
            this.clear();
        }

    });
    $('select[name="shipping_country"], select[name="billing_country"]').selectize({
        hideSelected:true,
        maxItems:1,
        highlight:true,
        onDropdownOpen:function() {
            this.$input.closest('.form-group').find('.error_icon').css('display','none');
        },
        onFocus: function(){
            this.clear();
        }
    });
    /* --------- END SELECTIZE --------- */
    // Select option region
    $("select[name=selectCountry], select[name=country]").on('change', function() {

        var country = $(this);
        var default_region = country.attr('data-zone');
        var zone_field_name = '';
        var fieldForInsert = '';
        country.attr('data-zone',''); // erase default zone for all other countries except default.

        if(country.attr('name')=='selectCountry') {
            zone_field_name = 'selectRegion';
            fieldForInsert = $('[name=city]');
        } else if(country.attr('name')=='country') {
            zone_field_name = 'zone_id';
            fieldForInsert = $('[name=city]');
        }

        $("select[name="+country.attr('name')+"]+button span:last-of-type").html(country.children(':selected').text()); // change country selectize
        // console.log(11);

        //  $("*[name="+zone_field_name+"]").remove();

        $.get( "./ajax_select_region.php", { country_id:country.val(),'default_region':default_region,'zone_field_name':zone_field_name}, function(data) {

            if($("*[name="+zone_field_name+"]").length!=0) $("*[name="+zone_field_name+"]").parent().replaceWith(data);
            else if (fieldForInsert.length!=0) fieldForInsert.parent().after(data);
            else country.parent().after(data);

            if(typeof checkout !== 'undefined') checkout.checkFields();
            $("select[name="+zone_field_name+"]").selectize({
                hideSelected:true,
                maxItems:1,
                // placeholder:ENTER_KEY,
                highlight:true,
                onDropdownOpen:function() {
                    this.$input.closest('.form-group').find('.error_icon').css('display','none');
                },
                onFocus: function(){
                    this.clear();
                }
            });
        });
    });

    $('select[name=country]').change();
    $('select[name=selectCountry]').change();

    var options = {target: '#block',beforeSubmit: showRequest,success:showResponse};

    $('.vivod_columns button, .vivod_list button').on('click', function() {
        if($("#"+$(this).attr("name")+"2").length!=0){$("#"+$(this).attr("name")+"2").val($(this).val());} // if element ...2 already exists - delete it
        else{$("#m_srch").append("<input type=hidden name="+$(this).attr("name")+" id="+$(this).attr("name")+"2 value="+$(this).val()+" />");} // create hidden element in form
        //$('#m_srch').ajaxSubmit(options);// submit form
        ajaxSubmitSerialize(options);

        // delete class "hover" for all buttons and add this class only for current element
        $(this).parent().parent().find('button').each(function() {
            $(this).removeClass('hover');
        });
        $(this).addClass('hover');
    });

    $( "#pl_onpage" ).change(function() {if($("#"+$(this).attr("name")+"2").length!=0){$("#"+$(this).attr("name")+"2").remove();}$("#m_srch").append("<input type=hidden name="+$(this).attr("name")+" id="+$(this).attr("name")+"2 value="+$(this).val()+" />");
        //$('#m_srch').ajaxSubmit(options);
        ajaxSubmitSerialize(options);
    });
    $( "#pl_sort" ).change(function() {if($("#"+$(this).attr("name")+"2").length!=0){$("#"+$(this).attr("name")+"2").remove();}$("#m_srch").append("<input type=hidden name="+$(this).attr("name")+" id="+$(this).attr("name")+"2 value="+$(this).val()+" />");
        //$('#m_srch').ajaxSubmit(options);
        ajaxSubmitSerialize(options);
    });
    $( "input:radio[name=sort]" ).change(function() {if($("#"+$(this).attr("name")+"2").length!=0){$("#"+$(this).attr("name")+"2").remove();}$("#m_srch").append("<input type=hidden name="+$(this).attr("name")+" id="+$(this).attr("name")+"2 value="+$(this).val()+" />");
        //$('#m_srch').ajaxSubmit(options);
        ajaxSubmitSerialize(options);
    });

    function changeColor(picker_field, color) {
        if(picker_field.data('configuration-key')) {
            var configurationCode = picker_field.data('configuration-key');
            var colorCode = color.toHexString();

            $.ajax({
                url: "/includes/modules/CustomizationPanel/index.php?action=changeColor",
                type: "POST",
                data: {colorCode: colorCode, configurationCode: configurationCode},
                dataType: 'json'
            });

        }
    }

	/*
	 if($('input[name=displayajax]').val()=='true') {
	 var options = {target: "#block",beforeSubmit: showRequest,success:showResponse};
	 ajaxSubmitSerialize(options);
	 }
	 */

    // colorpicker on main page:

    // jQuery('body').on('click', '.custom_panel_block.visible .change_color', function(event) {
    //     event.preventDefault();
    //     var picker_field = $(this).find('.change_color-input');
    //
    //     $(this).ColorPicker({
    //         color: picker_field.val(),
    //         onChange: function (hsb, hex, rgb) {
    //             $(picker_field).val('#'+hex);
    //             $("body").get(0).style.setProperty("--"+picker_field.attr('data-color'), '#'+hex);
    //         }
    //     });
    //     $(this).click();
    // });
    if (IS_MOBILE == '1') {
        $(document).on('click', '.change_color', function () {
            var picker_field = $(this).find('.change_color-input'),
                spectrum_block = $(this).find('.spectrum_block');
            $(this).closest('.palette_li').find('.close_palette').addClass('visible');

            spectrum_block.spectrum({
                color: picker_field.val(),
                type: "color",
                showPalette: "false",
                showPaletteOnly: "true",
                hideAfterPaletteSelect: "true",
                showAlpha: "false",
                palette: [
                    ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
                    ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
                    ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
                    ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
                    ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
                    ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
                    ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"]
                ],
                change: function (color) {
                    $("body").get(0).style.setProperty("--" + picker_field.attr('data-color'), color.toHexString());
                    picker_field.attr('value', color.toHexString());
                    changeColor(picker_field, color);
                },
                hide: function(color) {
                    $('.close_palette').removeClass('visible');
                }
            }).spectrum("show");
        });
    } else {
        $(document).on('click', '.change_color', function () {
            var picker_field = $(this).find('.change_color-input'),
                spectrum_block = $(this).find('.spectrum_block');

            spectrum_block.spectrum({
                color: picker_field.val(),
                type: "color",
                showPalette: "false",
                togglePaletteOnly: "true",
                hideAfterPaletteSelect: "true",
                showAlpha: "false",
                change: function (color) {
                    $("body").get(0).style.setProperty("--" + picker_field.attr('data-color'), color.toHexString());
                    picker_field.attr('value', color.toHexString());
                    changeColor(picker_field, color);
                },
                move: function (color) {
                    $("body").get(0).style.setProperty("--" + picker_field.attr('data-color'), color.toHexString());
                    picker_field.attr('value', color.toHexString());
                }
            }).spectrum("show");
        });
    }

    $('.close_palette').on('click', function (e) {
        e.stopPropagation();
        $('.spectrum_block').spectrum("hide");
        $(this).removeClass('visible');
    });

    //custom panel
    $(document).mouseup(function (e){
        var collapse_block = $('.custom_panel_block .collapse_li');
        // console.log(collapse_block)
        if (!collapse_block.is(e.target) && collapse_block.has(e.target).length === 0) {
            collapse_block.find('.drop_menu.in').collapse('hide');
            collapse_block.find('.collapse_btn').addClass('collapsed');
        }
    });

    $(document).on('click', '.open_custom_panel_btn', function () {
        if($('.custom_panel_block').hasClass('visible')) {
            $('.custom_panel_fader').removeClass('visible');
            $('.custom_panel_block .collapse_btn').addClass('collapsed');
            $('.custom_panel_block .drop_menu.in').collapse('hide');
            $('.custom_panel_block').removeClass('visible');
            $('.open_custom_panel_btn').addClass('anim');
        } else {
            $('.custom_panel_block').addClass('visible');
            $('.open_custom_panel_btn').removeClass('anim');
        }
    });
    $(document).on('click', '.custom_panel_close', function () {
        $('.custom_panel_block').removeClass('visible');
        $('.custom_panel_fader').removeClass('visible');
        $('.custom_panel_block .collapse_btn').addClass('collapsed');
        $('.custom_panel_block .drop_menu.in').collapse('hide');
        $('.open_custom_panel_btn').addClass('anim');
    });

    // if ($(window).width()>'991') {
    //     setTimeout(function() {
    //         $('.custom_panel_block').addClass('visible');
    //         $('.open_custom_panel_btn').removeClass('anim');
    //     }, 1500);
    // } else {
    //     $('.open_custom_panel_btn').addClass('anim');
    // }
    //end custom panel


    $(document).on('click', '.popupPrintReceipt', function(e) {
        e.preventDefault();
        window.open($(this).attr('href'),'popupPrintReceipt','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=750');
    });

    var firstImage, srcImage;

    $(document).on({
        mouseenter: function () {
            srcImage = $(this).find('.p_img_href img');
            if(srcImage.is("[data-hover]")) {
                firstImage = srcImage.attr('src');

                srcImage.attr('src',srcImage.attr("data-hover"));
            } else {
                firstImage = '';
            }
        },
        mouseleave: function () {
            if(typeof srcImage === 'object' && srcImage.is("[data-hover]") && firstImage!='') srcImage.attr('src',firstImage);
        }
    }, '.product');

    $('.checkout_authorization').click(function(event) {
        $.post('./ajax.php', {
            request: 'getLoginForm'
        }, function(data) {
            modal({
                id: 'getLoginForm',
                body: data.html,
                render: true,
                width: '320px',
            });

        }, 'json');
        return false;
    });
});
