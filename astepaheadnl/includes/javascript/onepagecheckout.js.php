<script> var addressTypeConfiguration = <?=json_encode($addressTypeIdentificators)?>; </script>
<script language="javascript" src="includes/javascript/onepage/jquery.ajaxq-0.0.1.js"></script>
<script language="javascript" src="includes/checkout/checkout.js"></script>

<script language="javascript">

  var onePage = checkout;
  onePage.initializing = true;
  onePage.ajaxCharset = 'utf-8';
  onePage.storeName = '<?php echo addslashes(STORE_NAME); ?>';
  onePage.loggedIn = <?php echo (tep_session_is_registered('customer_id') ? 'true' : 'false');?>;
  onePage.autoshow = <?php echo ((ONEPAGE_AUTO_SHOW_BILLING_SHIPPING == 'False') ? 'false' : 'true');?>;
  onePage.stateEnabled = <?php echo (ACCOUNT_STATE == 'true' ? 'true' : 'false');?>;
  onePage.showAddressInFields = <?php echo ((ONEPAGE_CHECKOUT_SHOW_ADDRESS_INPUT_FIELDS == 'False') ? 'false' : 'true');?>;
  onePage.showMessagesPopUp = <?php echo ((ONEPAGE_CHECKOUT_LOADER_POPUP == 'True') ? 'true' : 'false');?>;
  onePage.ccgvInstalled = <?php echo (MODULE_ORDER_TOTAL_COUPON_STATUS == 'true' ? 'true' : 'false');?>;
  
  onePage.refresh = '<?php echo addslashes(CH_JS_REFRESH); ?>';
  onePage.refresh_method = '<?php echo addslashes(CH_JS_REFRESH_METHOD); ?>';
  onePage.setting_method = '<?php echo addslashes(CH_JS_SETTING_METHOD); ?>';
  onePage.setting_address = '<?php echo addslashes(CH_JS_SETTING_ADDRESS); ?>';
  onePage.setting_address_ship = '<?php echo addslashes(CH_JS_SETTING_ADDRESS_SHIP); ?>';
  onePage.setting_address_bil = '<?php echo addslashes(CH_JS_SETTING_ADDRESS_BIL); ?>';
  
  onePage.error_scart = '<?php echo addslashes(CH_JS_ERROR_SCART); ?>';
  onePage.error_some1 = '<?php echo addslashes(CH_JS_ERROR_SOME1); ?>';
  onePage.error_some2 = '<?php echo addslashes(CH_JS_ERROR_SOME2); ?>';
  onePage.error_set_some1 = '<?php echo addslashes(CH_JS_ERROR_SET_SOME1); ?>';
  onePage.error_set_some2 = '<?php echo addslashes(CH_JS_ERROR_SET_SOME2); ?>';
  onePage.error_set_some3 = '<?php echo addslashes(CH_JS_ERROR_SET_SOME3); ?>';
  onePage.error_req_bil = '<?php echo addslashes(CH_JS_ERROR_REQ_BIL); ?>';
  onePage.error_err_bil = '<?php echo addslashes(CH_JS_ERROR_ERR_BIL); ?>';
  onePage.error_req_ship = '<?php echo addslashes(CH_JS_ERROR_REQ_SHIP); ?>';
  onePage.error_err_ship = '<?php echo addslashes(CH_JS_ERROR_ERR_SHIP); ?>';
  onePage.error_address = '<?php echo addslashes(CH_JS_ERROR_ADDRESS); ?>';
  onePage.error_pmethod = '<?php echo addslashes(CH_JS_ERROR_PMETHOD); ?>';
  onePage.error_select_pmethod = '<?php echo addslashes(CH_JS_ERROR_SELECT_PMETHOD); ?>';
  onePage.check_email = '<?php echo addslashes(CH_JS_CHECK_EMAIL); ?>';
  onePage.error_email = '<?php echo addslashes(CH_JS_ERROR_EMAIL); ?>';

  onePage.shippingEnabled = <?php echo ($onepage['shippingEnabled'] === true ? 'true' : 'false');?>;
  onePage.pageLinks = {

  }

  function getFieldErrorCheck($element){
	  var rObj = {};
	  switch($element.attr('name')){  
		  case 'billing_firstname':
		  case 'shipping_firstname':
			  rObj.minLength = <?php echo addslashes(ENTRY_FIRST_NAME_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_FIRST_NAME_ERROR);?>';
		  break;
		  case 'billing_lastname':
		  case 'shipping_lastname':
			  rObj.minLength = <?php echo addslashes(ENTRY_LAST_NAME_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_LAST_NAME_ERROR);?>';
		  break;
		  case 'billing_email_address':
			  rObj.minLength = <?php echo addslashes(ENTRY_EMAIL_ADDRESS_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_EMAIL_ADDRESS_ERROR);?>';
		  break;
		  case 'billing_street_address':
		  case 'shipping_street_address':
			  rObj.minLength = <?php echo addslashes(ENTRY_STREET_ADDRESS_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_STREET_ADDRESS_ERROR);?>';
		  break;
		  case 'billing_zipcode':
		  case 'shipping_zipcode':
			  rObj.minLength = <?php echo addslashes(ENTRY_POSTCODE_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_POST_CODE_ERROR);?>';
		  break;
		  case 'shipping_suburb':
		  case 'billing_suburb':
			  rObj.minLength = <?php echo addslashes(ENTRY_STATE_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_SUBURB_ERROR);?>';
		  break;
		  case 'billing_city':
		  case 'shipping_city':
			  rObj.minLength = <?php echo addslashes(ENTRY_CITY_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_CITY_ERROR);?>';
		  break;
		  case 'billing_telephone':
			  rObj.minLength = <?php echo addslashes(ENTRY_TELEPHONE_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_TELEPHONE_NUMBER_ERROR);?>';
		  break;
		  case 'billing_country':
		  case 'shipping_country':
			  rObj.errMsg = '<?php echo addslashes(ENTRY_COUNTRY_ERROR);?>';
		  break;
		  case 'billing_state':
		  case 'delivery_state':
			  rObj.minLength = <?php echo addslashes(ENTRY_STATE_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_STATE_ERROR);?>';
		  break;
		  case 'shipping_zone_id':
		  case 'billing_zone_id':
			  rObj.minLength = <?php echo addslashes(ENTRY_STATE_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_STATE_ERROR);?>';
		  break;
		  case 'password':
		  case 'confirmation':
			  rObj.minLength = <?php echo addslashes(ENTRY_PASSWORD_MIN_LENGTH);?>;
			  rObj.errMsg = '<?php echo addslashes(ENTRY_PASSWORD_ERROR);?>';
		  break;
	  }
	return rObj;
  }

$(document).ready(function (){
	$('#pageContentContainer').show();
	onePage.initCheckout();
});

function clearRadeos(){
	 return true;
}

</script>