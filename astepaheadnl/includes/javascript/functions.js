"use strict";
/* --------- POPUPS --------- */


function modal(options){
  var settings = {
    id: Math.floor(Math.random() * (1000 - 1 + 1)) + 1,
    after : function(){},
    width: 0,
    before : function(){}
  }

  $.extend(true, settings, options);
  var width = '';
  if(settings.width!=0) width = 'style="width:auto;max-width:'+settings.width+'"';

    if(jQuery('.modal').length == 0){
      var $html = '<div class="modal fade" id="modal_'+settings.id+'" tabindex="-1" role="dialog" aria-labelledby="'+settings.id+'_label" aria-hidden="true">';
      $html +='<div class="modal-dialog" '+width+'><div class="modal-content">';
      $html +='<div class="modal-header">';
        $html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"></path></svg></button>';
          if(settings.title){
              $html += '<h4 class="modal-title" id="modal_'+settings.id+'_label">'+settings.title+'</h4>';
          }
          $html +='</div>';
          $html +='<div class="modal-body">'+settings.body+'</div>\
        </div>\
      </div>\
      </div>';
      jQuery('body').append($html).promise().done(function(){
        var $modal = jQuery('#modal_'+settings.id);
        var $before = settings.before($modal);
        if($before !== false){
            if(settings.classes){
            $modal.addClass(settings.classes);
          }

          if(settings.title == null){
              $modal.addClass('no-title');
          }

          if($modal.hasClass('valign-false') == false){
            centerModal($modal);
          }
          $modal.on('hidden.bs.modal', function (e) {
             $modal.remove();
          });
          $modal.on('shown.bs.modal', function (e) {
            if(settings.after){
              settings.after($modal);
            }
          });
          $modal.modal();

        }else{
          $modal.remove();
        }

    });
  }

}

function centerModal(el) {
    $(el).css('display', 'block');
    var $dialog = $(el).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    var bottomMargin = $dialog.css('marginBottom');
    bottomMargin = parseInt(bottomMargin);
    if(offset < bottomMargin) offset = bottomMargin;
    $dialog.css("margin-top", offset);
}

function pop_contact_us(){
  $.post('./pop_contact_us.php','',function(data) {
    modal({
      id: 'pop_contact_us',
      title: data.title,
      body: data.html
      // classes: 'valign-false'
    });
  },'json');
}
function showCartpopup(){
  $.get('./popup_cart.php',null,function(response) {
    modal({
      id: 'cart_popup',
      body: response,
      classes: 'valign-false',
      after: function(){
     //   scrollToTop();
      }
    });
  });
}
// call after get response
function showPopupResponse(res)  {
    updateCart();     
    // reset checkout
    if(typeof checkout != 'undefined') {
        checkout.updateShippingMethods();
        checkout.updateOrderTotals();
    }
}
// call after get response
function showPopupResponsev2(res)  {
    updateCart();     
    // reset checkout
    if(typeof checkout != 'undefined') {
        checkout.updateShippingMethods();
        checkout.updateOrderTotals();
    }
}
/* --------- /POPUPS --------- */

/* --------- AJAX-add to cart --------- */

function doAddProduct(form) {
  var fe = form.elements;
  var senddata = new Object();
  for(var i=0 ; i<fe.length ; i++) {
    if ( fe[i].type=="radio" || fe[i].type=="checkbox" ) {
      if ( fe[i].checked ) senddata[fe[i].name] = fe[i].value;
    } else {
      senddata[fe[i].name] = fe[i].value;
    }
  }
  var url = form.action;
  $.post(url,senddata, function(data) {                              
  //  $('.add2cart[data-id='+senddata["products_id"]+']').replaceWith(sprintf(RTPL_CART_BUTTON, senddata["products_id"]));
    $('#r_buy_intovar[data-id='+senddata["products_id"]+']').html(RTPL_CART_BUTTON_PRODUCT_PAGE); // product page
    if ($('.fade.tooltip.top.in').length) $('.fade.tooltip.top.in').remove();
      if ($(window).width() > 768) {
          $('[data-toggle="tooltip"]').tooltip({
              container: 'body'
          });
      }
    showCartpopup();
    updateCart('add_product');
  });
}
                
function doAddProductList(button) {

  $.post('?action=add_product',{'products_id':button.attr('data-id'),'cart_quantity':button.attr('data-qty')}, function(data) {                              
    $('.add2cart[data-id='+button.attr('data-id')+']').replaceWith(sprintf(RTPL_CART_BUTTON, button.attr('data-id')));
    if ($('.fade.tooltip.top.in').length) $('.fade.tooltip.top.in').remove();
      if ($(window).width() > 768) {
          $('[data-toggle="tooltip"]').tooltip({container: 'body'});
      }

      if(FACEBOOK_PIXEL_MODULE_ENABLED && typeof fbq === "function")
      {
          fbq('track', 'AddToCart', {
              content_type: 'product',
              content_ids: [data.id],
              content_name: data.name,
              content_category: data.category,
              value: data.price,
              currency: CURRENCY_CODE
          });
      }

      showCartpopup();
      updateCart('add_product');
  }, 'json');
}

function showAlert (text, parent, type, removeClass) {
    // if removeClass isn't empty - show close button
    if (removeClass !== '') var close = '<button class="close" data-dismiss="alert" aria-label="close"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"></path></svg></button>';

    // adds alert box to parent with next properties
    parent.after("<div class='alert " + type + " apple-tree fade in "+removeClass+"' data-alert='alert'>"+close+"<span>"+text+"</span></div>");

    setTimeout(function(){
        parent.next().remove();
    },5000);
}

// call before send data
function showRequest(formData, jqForm, options) {
    var queryString = $.param(formData);
  //  $('#block').animate({opacity: 0.6}, 50);
    $('.refresh_icon .fa-spin').fadeIn(20);
    queryString='?'+queryString;
    history.replaceState({ foo: "bar" },"Jesus saves",document.location.pathname.toString()+queryString);
     return true;
}
// call after get answer
function showResponse(responseText, statusText)  {
 //   console.log(responseText);
  //  $('#block').animate({opacity: 1}, 50);
  $('.refresh_icon .fa-spin').fadeOut(20);
}

/* --------- END AJAX-add to cart --------- */



function updateCart(action){ // function updateCart(action = ''){
  $.post('./ajax_update_cart.php','', function(data) {
    $('#divShoppingCard').replaceWith(data.cart_html); // $('#divShoppingCard').html(data.cart_html);
    $('.mobile_cart_count').html(data.cart_count);
    // if ($('.xs-menu-actions .popup_cart .counter').length) $('.xs-menu-actions .popup_cart .counter').text(data.count)
      if (action == 'add_product') {
        $('.alert').remove();
        showAlert(RENDER_TEXT_ADDED_TO_CART, $('#divShoppingCard'), 'alert-success', 'alert-dismissible');
      }
  },'json');
}

/* --------- ETC --------- */
function setLastcols(context, element){
  var listing_item = jQuery(context+' '+element);
  var cols = Math.floor(jQuery('#center').width() / listing_item.width());
  jQuery(context+' '+element+':nth-of-type('+cols+'n+'+cols+')').addClass('last-col');
}
//Scroll to some element on page
function scrollToEl(element){
  var $el = jQuery(element);
  var $elOffset_top = $el.offset().top;
  jQuery('body,html').stop(false, false).animate({scrollTop: $elOffset_top},{duration:600, easing: 'swing'}, 800);
}
function scrollToTop(){
  jQuery('body,html').stop(false, false).animate({scrollTop: 0},{duration:600, easing: 'swing'}, 800);
}
// Item list
function themeItems(){
  jQuery('.item-list li').each(function(e){
    jQuery(this).addClass('item_'+e);
  });
}

function showLoginvk(url) {
// set window width and height
     var w = 635;
     var h = 250;
// calculate window position according to screen resolution
     var lPos = (screen.width) ? (screen.width-w)/2 : 0;
     var tPos = (screen.height) ? (screen.height-h)/2 : 0;
// newWindow = window.open("","","height="+h+",width="+w+",top="+tPos+",left="+lPos);
  var params = "menubar=no,location=yes,resizable=yes,scrollbars=no,status=no,height="+h+",width="+w+",top="+tPos+",left="+lPos;
  window.open(url, VK_LOGIN, params);
}

 function checkLoginvk(id_var,first_name_var,last_name_var,photo_var,email_var,city_var,pass_var){ // detailed popup
   $.get('./ext/auth/login_process.php',{id:id_var, first_name:first_name_var,last_name:last_name_var,photo:photo_var,email:email_var,city:city_var,password:pass_var},function(data) {

      modal({
        id: 'check_login',
        body: data
      });

      $.get('./ext/auth/ajax_login_top.php',{ajaxloading:true},function(data) {
        $('#kabinet').html(data);
      });
  });
 }
 /*
 * Function for fixing dropdown submenu
 * last points by right side, if it`s width more than wrapper`s width
 * Example : fixMenu('.nav','li.parent','.dropdown_div');
 */
 function fixMenu(menu_selector,parent,dropdown){
  var $menu = $(menu_selector);
  var $windowWidth = jQuery(window).width();
  var $wrapperWidth = $menu.width();
  var $outsideWidth = ($windowWidth - $wrapperWidth) / 2;
  $(menu_selector+''+parent).each(function(index, el) {
    var $dropdown = $(el).find(dropdown);
    var $curTotalWidth = ($(el).offset().left + $dropdown.outerWidth(true))-$outsideWidth;
    var $differenceWidth = ($(el).offset().left-$outsideWidth)/2;
    if ($curTotalWidth > $wrapperWidth) {
      $dropdown.css({marginLeft:'-'+$differenceWidth+"px"});
    }
  });
}

/* --------- /ETC --------- */


/* --------- SEARCH --------- */
 function liFormat (row, i, num) {
    var st='';
    var result ='<div class="search_image_wrap"><img src="'+row[2]+'" class="picsearch"></div>'+'<p class=qnt1>' + row[0] + ' </p><span><p class=qntp>'+row[4]+'</p>' + '<p class=qnt>' + row[1] + ' </p></span>';
    return result;
  }
  function selectItem(li) {
    if( li == null ) {var sValue = '!';window.location = "/search="+li.extra[2];}
    if( !!li.extra ) var sValue = li.extra[2];
    else var sValue = li.selectValue;
    var tbox = document.getElementById('searchpr') || document.getElementById('searchpr1');
    if (tbox) {
      window.location = SEARCH_LANG+"product_info.php?products_id="+li.extra[2];
    }
  }
/* --------- SEARCH --------- */

function calculate_sum(selected_op) {
  var summ, sel_ops;

     if($('input[name=prod_price]').val()!='-') {
       if (selected_op.attr('data-prefix')=='=' && $('#id_option_other'+selected_op.val()).val()!='=0') { // if "=" and NOT "=0" then immediately set price
         summ = ($('#id_option_other'+selected_op.val()).val()).substring(1);

       } else {
         $('.select_id_option').each(function() { // search first attribute with "=" sign
           sel_ops = $(this).parent().find($("select option:selected"));
           if (sel_ops.attr('data-prefix')=='=' && $('#id_option_other'+sel_ops.val()).val()!='=0') {
             summ = ($('#id_option_other'+sel_ops.val()).val()).substring(1);
             return false;
           }
         });
         if(!summ) summ = parseFloat($('input[name=prod_price]').val()); // current product price
       }


       $('select.select_id_option').each(function() {   // adding all prices with "+"
         if ($(this).hasClass('selectized')) {
           sel_ops = selectizeGetSelectedItem(selectizeWrapper($(this)));
           if (sel_ops.prefix) {
             if (sel_ops.prefix == '+' || (sel_ops.prefix == '=' && $('#id_option_other' + sel_ops.value).val() == '=0')) {
               summ = parseFloat(summ) + parseFloat(($('#id_option_other' + sel_ops.value).val()).substring(1));
             } else if (sel_ops.prefix == '-') {
               summ = summ - parseFloat(($('#id_option_other' + sel_ops.value).val()).substring(1));
             }
           }
         }else{
           sel_ops = $(this).parent().find($("select option:selected"));
           if(sel_ops.attr('data-prefix')=='+' || (sel_ops.attr('data-prefix')=='=' && $('#id_option_other'+sel_ops.val()).val()=='=0')) {
             summ =  parseFloat(summ) + parseFloat(($('#id_option_other'+sel_ops.val()).val()).substring(1));

           } else if(sel_ops.attr('data-prefix')=='-') {
             summ = summ - parseFloat(($('#id_option_other'+sel_ops.val()).val()).substring(1));
           }
         }
      });





       // ------------------format summ!!------------------

       if($('input[name=prod_dec_places]').val()!='') {
         summ = parseFloat(summ).toFixed($('input[name=prod_dec_places]').val()); // add decimal places after decimal point
         if($('input[name=prod_dec_point]').val()!='') summ = summ.replace('.', $('input[name=prod_dec_point]').val()); // replace dot(.) to store decimal point
       } else {
         summ = parseInt(summ); // if dec_places empty, make it INT
       }

			 if($('input[name=prod_thousands_point]').val()!='') summ = numberWithCommas(summ,$('input[name=prod_thousands_point]').val()); // add thousands point
			 if($('input[name=prod_currency_left]').val()!='') summ = '<span class="ccp">'+$('input[name=prod_currency_left]').val()+'</span>'+summ; // left currency symbol
       if($('input[name=prod_currency_right]').val()!='') summ = summ+' <span class="ccp">'+$('input[name=prod_currency_right]').val()+'</span>'; // right currency symbol

			 $(".new_price_card_product").html(summ);
     }
}

function ajaxSubmitSerialize (options){
    $('#attribs').addClass('pointer_events_none'); // Block filters for processing time
    $('.refresh_icon .fa-spin').fadeIn(20);
    if(options.target) {
        $('#m_srch input[name="page"]').prop("disabled", true);
    }
    var pathSerialize =$('#m_srch').attr('action')+ '?' + $('#m_srch').serialize();
    var queryString='?' + $('#m_srch').serialize();
    $('#m_srch input[name="page"]').prop("disabled", false);
    if (!(typeof SEO_FILTER == "string" && SEO_FILTER == 1)) {
            history.replaceState({foo: "bar"}, "Jesus saves", document.location.pathname.toString() + queryString);
    }
    var findWidthToUnveil;

   // if($('#m_srch input[name="loadajax"]').val()=='true') {
        if (options.target){
          //  $(options.target).html('<div id="r_spisok" class="row row_catalog_products "></div>');
            $.post(pathSerialize,function(data) { // get array
                $('#attribs').removeClass('pointer_events_none'); // unblock filters
                if(data != null && data['globals'] != undefined) {
                  var ajax_globals = data['globals']; // array with global variables and languages constants

                  $(options.target).html(ajax_globals['listing_header']);

                  if(typeof RTPL_DEFAULT_COLS !== 'undefined') {
                    var default_cols = JSON.parse(RTPL_DEFAULT_COLS);
                    var default_cols_str = 'cc-xs-'+default_cols[0]+' cc-sm-'+default_cols[1]+' cc-md-'+default_cols[2]+' cc-lg-'+default_cols[3]+' cc-lg-'+default_cols[4];
                    $('#r_spisok').addClass(default_cols_str);
                  }

                  $.each(data, function(k,v) { // add array directly to html

                      //find width to activate unveil in chrome
                      if (v['product_image'] && !findWidthToUnveil){
                          findWidthToUnveil=v['product_image'].split('&')[1].split('=')[1];
                      }
                      //
                      if(k!='globals') $('#r_spisok').append(draw_product_block(v,ajax_globals));
                  });
                  showResponse();
                  if (findWidthToUnveil){
              //        $("#r_spisok img").width(findWidthToUnveil);
                      findWidthToUnveil=0;
                  }
                  $("#r_spisok img").unveil(500, addAnimClassToImg);
              //    $("#r_spisok img").width('100%'); // to make lazyload work properlty we need to set dimensions in pixels, and then return "100%" width as in css

                  // add "show more" and pagination
               //   $('#r_spisok').after(ajax_globals['load_more']+ajax_globals['pages_html']+ajax_globals['number_of_rows']);
                  $('#r_spisok').after(ajax_globals['listing_footer']);
                  $('#loadMoreProducts').click(function () {
                      loadMoreProducts();
                  });

                  if(RTPL_PDF_ENABLED) {$(RTPL_PDF_BLOCK_SELECTOR).data("href", ajax_globals['pdf_link']) }
                  // if(RTPL_PDF_ENABLED) $('#block').prepend(sprintf(RTPL_LISTING_HEADER, ajax_globals['pdf_link']));

                } else {
                    $('#block').html('');
                    $('#attribs').removeClass('pointer_events_none'); // unblock filters
                    $('.refresh_icon .fa-spin').fadeOut(20);
                }
                if (typeof SEO_FILTER == "string" && SEO_FILTER == 1) {
                    if (typeof ajax_globals !== 'undefined') {
                        if ($('#filters_box').length) $('#filters_box').replaceWith(ajax_globals['filtersBlock'])
                        if (ajax_globals['currentHref']) {
                            window.history.pushState('data', 'Title', ajax_globals['currentHref']);
                        } else {
                            history.replaceState({foo : "bar"}, "Jesus saves", document.location.pathname.toString() + queryString);
                        }
                    } else {
                        history.replaceState({foo : "bar"}, "Jesus saves", document.location.pathname.toString() + queryString);
                    }
                    initSlider();
                }
            },'json');
        }
        else if (options="loadMore"){
            var lastMsg = $('#r_spisok .col_product:last');
            var curOffset = lastMsg.offset().top - $(document).scrollTop();
            $('#attribs').removeClass('pointer_events_none'); // unblock filters
            $.get(pathSerialize, function (data) {
                //from all html get only list of products to add and append them to #r_spisok
                var ajax_globals = data['globals']; // array with global variables and languages constants
                $.each(data, function(k,v) { // add array directly to html
                    //find width to activate unveil in chrome
                    if (v['product_image'] && !findWidthToUnveil){
                        findWidthToUnveil=v['product_image'].split('&')[1].split('=')[1];
                    }
                    if(k!='globals') $('#r_spisok').append(draw_product_block(v,ajax_globals));
                });
                $(document).scrollTop(lastMsg.offset().top-curOffset);
                $($.find(".pagination .active")).next().attr("class", "active"); //mark next page as active
                showResponse();
                if (findWidthToUnveil){
                 //   $("#r_spisok img").width(findWidthToUnveil);
                    findWidthToUnveil=0;
                }
                $("#r_spisok img").unveil(500, addAnimClassToImg);
             //   $("#r_spisok img").width('100%'); //to make lazyload work properlty we need to set dimensions in pixels, and then return "100%" width as in css

                $('#loadMoreI').removeClass('fa-spin'); //stop font-awesome animation
            }, 'json');

        }

}
function loadMoreProducts () {
    var totalProducts = $('input[name=number_of_rows]').val();
  //  var productsPerPage=$('select[name=row_by_page] option:selected').text(); 
    var productsPerPage=$('*[name=row_by_page]').val(); //get value from row_by_page
    var lastRedElement;
    //find last red page(even it has not li .active)
    var obj = $('#m_srch').serializeArray();
    obj.forEach(function(item){
        if (item['name']=="row_by_page"){
            productsPerPage=item['value'];
        }
    });

    var foundpage = false;
    $.each(obj, function (i, field) {
        if (obj[i].name == "page") {
            lastRedElement=parseInt(obj[i].value);
            foundpage = true;

        }
    });
    var totalPagesAdjusted;
    if (~~(totalProducts/productsPerPage)===(totalProducts/productsPerPage)) {
        totalPagesAdjusted=(totalProducts/productsPerPage);
    } else {
        totalPagesAdjusted=~~(totalProducts/productsPerPage)+1;
    }
    if (foundpage == false) {
        lastRedElement=1;
    }

    if (lastRedElement>=totalPagesAdjusted) {
    } else {
      $('#loadMoreI').addClass('fa-spin');//start font-awesome animation
      $('.refresh_icon .fa-spin').fadeIn(20);

      //serialize path with attributes
      var obj = $('#m_srch').serializeArray();
      var foundpage = false;
      $.each(obj, function (i, field) {
          if (obj[i].name == "page") {
              lastRedElement=obj[i].value;
              obj[i].value = parseInt(obj[i].value) + 1;
              $('#m_srch').prop("page").value = obj[i].value;
              foundpage = true;

          }
      });
      if (foundpage == false) {
         $('#m_srch').append('<input id="page1" type="hidden" name="page" value="2">');
			   lastRedElement=1;
      }

      ajaxSubmitSerialize("loadMore");
    }
}

// new draw product block by templates. 03.04.18 by raid
function draw_product_block($listing, glob) {

  var $id = $listing['p_id'];
  var $product_name = $listing['p_name'];
  var $show_button = $listing['show_button'];
  var $product_info = $listing['p_info'];
  var $cat_name = $listing['cat_name'];
  var $product_href = glob['promUrls']?glob['catalog_path']+'p'+$id+'-'+$listing['p_href']+'.html':glob['catalog_path']+$listing['p_href']+'/p-'+$id+'.html';
  var $product_image = sprintf(RTPL_PRODUCTS_IMAGE, glob['img_end'], ($listing['p_img']!=''?$listing['p_img']:glob['img_default']), $product_name, $product_name, ($listing['p_img2']?'data-hover="getimage/'+glob['img_end']+'/products/'+$listing['p_img2']+'"':''));
  var $pmodel = ($listing['p_model']!=''?sprintf(RTPL_PRODUCTS_MODEL, glob['pmodel_class'], $listing['p_model']):'');
  var $stock = ($listing['p_qty']>0?sprintf(RTPL_PRODUCTS_STOCK, glob['stock_pull']):sprintf(RTPL_PRODUCTS_OUTSTOCK, glob['stock_pull']));
  var $label = $listing['lbl']?sprintf(RTPL_LABEL, $listing['l_class'], $listing['lbl']):'';
  var $add_classes = glob['add_classes']?glob['add_classes']:''; // separated old price
  var $not_available = $listing['p_qty']==0?' not_available':'';
  var $p_wishlist, $cart_button;

  var $spec_price = $listing['p_specprice']?$listing['p_specprice']:'';
  var $old_price = $listing['p_price'];
  var $final_price = ($spec_price!=''?sprintf(RTPL_PRODUCTS_SPEC_PRICE, $spec_price, $old_price):sprintf(RTPL_PRODUCTS_PRICE, $old_price));

  var $new_price = sprintf(RTPL_PRODUCTS_PRICE, ($spec_price?$spec_price:$old_price)); // separated new price
  var $old_price = $spec_price?sprintf(RTPL_PRODUCTS_OLD_PRICE, $old_price):''; // separated old price

    $cart_button =  '<input type="hidden" name="cart_quantity" value="1"><input type="hidden" name="products_id" value="'+$id+'">';
    if($listing['p_qty'] <= 0) {
        if($show_button == "true") {
            $cart_button += ($listing['cart_incart']?sprintf(RTPL_CART_BUTTON, $id):sprintf(RTPL_ADD_TO_CART_BUTTON, $id, 1));
        }
    } else {
        $cart_button += ($listing['cart_incart']?sprintf(RTPL_CART_BUTTON, $id):sprintf(RTPL_ADD_TO_CART_BUTTON, $id, 1));
    }

  $listing['compare'] = $listing['compare']?$listing['compare']:'';
  $listing['wish'] = $listing['wish']?$listing['wish']:'';

  // Compare & Wishlist
  var $compare_output = '';
    $p_wishlist = '';
  if (glob['compa_enabled'] == true || glob['wish_enabled'] == true) {

    if (glob['compa_enabled'] == true) {
        if (checkTemplate('default')){
            $compare_output += sprintf(RTPL_PRODUCTS_COMPARE, $id, $id, $id, $listing['compare'], $listing['compare_link_before'] , $id, ($listing['compare']?glob['compa_text_in']:glob['compa_text']), $listing['compare_link_after']);
        }else{
            $compare_output += sprintf(RTPL_PRODUCTS_COMPARE, $id, $id, $id, $listing['compare'] , $id, ($listing['compare']?glob['compa_text_in']:glob['compa_text']));

        }
    }
    if (glob['wish_enabled'] == true)  $p_wishlist = sprintf(RTPL_PRODUCTS_WISHLIST, $id, $id, $id, $listing['wish'], $id, ($listing['wish']?glob['wish_text_in']:glob['wish_text']));
  }

  $compare_output = ($compare_output!=''?sprintf(RTPL_COMPARE_OUTPUT, glob['compare_class'], $compare_output):'');

  // Attributes:
  if ($listing['p_attr']){
      var $product_attributes = '';
      $.each($listing['p_attr'], function($ana_name, $ana_vals){
         $product_attributes += sprintf(glob['attr_listing'], glob['all_attribs'][$ana_name], $ana_vals);
      });
      $product_attributes = $product_attributes?sprintf(glob['attr_body'], $product_attributes):'';
  }

  var $array_from_to = {'blocks_num_lg' :       glob['blocks_num']['lg'],
                        'blocks_num_md' :       glob['blocks_num']['md'],
                        'blocks_num_sm' :       glob['blocks_num']['sm'],
                        'blocks_num_xs' :       glob['blocks_num']['xs'],
                        'blocks_num_xl' :       glob['blocks_num']['xl'],
                        'product_href' :        $product_href,
                        'label' :               $label,
                        'product_image' :       $product_image,
                        'products_model' :      $pmodel,
                        'stock' :               $stock,
                        'final_price' :         $final_price,
                        'new_price' :           $new_price,
                        'old_price' :           $old_price,
                        'id' :                  $id,
                        'category_name' :       $cat_name,
                        'product_name' :        $product_name,
                        'product_info' :        $product_info,
                        'product_attributes' :  $product_attributes,
                        'compare_output' :      $compare_output,
                        'wishlist_output' :     $p_wishlist,
                        'cart_button' :         $cart_button,
                        'add_classes' :         $add_classes,
                        'not_available' :       $not_available
                        };

  return glob['listing_layout'].formatUnicorn($array_from_to);  // replace template by variables from $array_from_to
}

function numberWithCommas(x,comma) {
    if(typeof x!= 'undefined') return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, comma);
}

// scroll to selected element:
function multiselectscroll() {
    $(document).on('click', '.ui-multiselect', function() {
 //   $(".ui-multiselect").on('click', function() {
        if($('.ui-multiselect-menu:visible').length!=0) { // if its opened
          $('.ui-multiselect-menu:visible .ui-multiselect-checkboxes').animate({scrollTop: 0}, 0); // go to top first;
          $('.ui-multiselect-menu:visible .ui-multiselect-checkboxes').animate({ // go to current element
             scrollTop: $(".ui-multiselect-menu:visible .ui-multiselect-checkboxes .ui-state-active").position().top
          }, 0);
        }
    }); 
}

// NOT WORKING for IE:
/* function sprintf(format, ...args) {   
    let i = 0;
    return format.replace(/%s/g, () => args[i++]);
} */

function sprintf() {
    if (arguments.length < 2) {
        return arguments[0];
    }
    var args = arguments;
    var index = 1;
    var result = (args[0] + '').replace(/%((\d)\$)?([sd%])/g, function (match, group, pos, format) {
        if (match === '%%') {
            return '%';
        }
        if (typeof pos === 'undefined') {
            pos = index++;
        }
        if (pos in args && pos > 0) {
            return args[pos];
        } else {
            return match;
        }
    });
    return result;
}

// replace function:
String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
function () {
    "use strict";
    var str = this.toString();
    if (arguments.length) {
        var t = typeof arguments[0];
        var key;
        var args = ("string" === t || "number" === t) ?
            Array.prototype.slice.call(arguments)
            : arguments[0];

        for (key in args) {
            str = str.replace(new RegExp("\\{{" + key + "\\}}", "gi"), args[key]);
        }
    }

    return str;
};
function selectizeWrapper($selector){
  return $selector[0].selectize;
}
function selectizeGetSelectedItem(wrapper){
  return wrapper.options[wrapper.items[0]];
}

function checkTemplate(templateName){
    return typeof TEMPLATE_NAME === 'string' && TEMPLATE_NAME === templateName;
}
function renderSlider(mainPageModules) {
    if(mainPageModules.length == 0) {
        return false;
    }
    var box = mainPageModules.shift();
    $.ajax({
        url: './routes.php?get-module=1',
        type: 'POST',
        data: {render: box},
        success: function (response) {
            var el = $('.ajax-module-box[data-module-id="' + box + '"]');
            el.append((response));
            el.find('img').unveil(100, addAnimClassToImg);
            el.find('.lazy-data-loader').remove();
            var block_id = el.find('.product_slider').attr('id');
            if (typeof block_id != 'undefined') {
                eval('make_' + block_id + '_slider();');
            }
            renderSlider(mainPageModules);
        }
    });
}

//the slider block is loaded when scroll top position near the our block
function blockUnveil(elements, th) {
    for (let index in elements) {
        var element = $('[data-module-id="' + elements[index] + '"]');
        var scrollFlag = (typeof (element.attr('data-scroll')) != 'undefined') ? true : false;
        if (element.length > 0 && !scrollFlag) {
            var wt = $(window).scrollTop(),
                wb = wt + $(window).height(),
                et = element.offset().top,
                eb = et + element.height();
            var ress = eb >= wt - th && et <= wb + th;
            if (ress) {
                scrollFlag = true;
                element.attr('data-scroll', true);
                renderSlider([elements[index]]);
            }
        }
    }
}

function renderCustomizationPanel() {
    $.ajax({
        url: '/customization_panel.php',
        success: function (response) {
            $('body').append(response);
        }
    });
}
function addAnimClassToImg(){
  $(this).addClass('anim');
}