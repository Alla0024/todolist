<?php
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Exception/ExceptionInterface.php';
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Exception/InvalidCallbackException.php';
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Exception/InvalidFileException.php';
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Exception/InvalidPathException.php';
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Exception/ValidationException.php';

require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Validator.php';
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Loader.php';
require_once __DIR__ . '/classes/vlucas/phpdotenv/src/Dotenv.php';
require_once __DIR__ . '/classes/Kernel/App.php';

require_once __DIR__ . '/classes/pimple/pimple/src/Pimple/Container.php';


use Dotenv\Dotenv as DotEnv;
use Pimple\Container;

/**
 * Container for ready objects
 */
$app_container = new Container();
/**
 * Class for adding data from config .env file
 */
$dotEnv = new DotEnv(dirname(__DIR__));
$dotEnv->load();
$app = App::getInstance();

/**
 * Function - facade to get environment variables
 * @param $key
 * @param null $default
 * @return string
 */
function env($key, $default = null)
{
    $value = getenv($key);
    if ($value === false && $default) {
        return $default;
    }
    return $value;

//    return call_user_func('getenv', func_get_args());
}

/**
 * Dump and DIE, tool for debug variables
 * @param $data
 */
function dd($data)
{
    var_dump($data);
    die;
}


/**
 * Set environment
 */
App::setEnv(getenv('APP_ENV'));

?>