<?php

spl_autoload_register(function ($className) {
    $prefix = "SoloMono\\Chronicler\\";
    if (strpos($className, $prefix) === 0) {
        $className     = substr($className, strlen($prefix));
        $classFileName = implode([
            __DIR__,
            DIRECTORY_SEPARATOR,
            "src",
            DIRECTORY_SEPARATOR,
            $className,
            ".php",
        ]);
        if (file_exists($classFileName)) {
            require_once $classFileName;
        }
    }
});