<?php

namespace SoloMono\Chronicler;

use Exception;

/**
 * Class LoggerFactory
 *
 * @package SoloMono\Chronicler
 */
class LoggerFactory
{
    /**
     * @var static
     */
    public static $instance;

    /**
     * @var array
     */
    private $loggers = [];

    /**
     * LoggerFactory constructor.
     */
    private function __construct()
    {
        try {
            $this->createLogDirectory(DIR_FS_LOGS);
            $this->createHtaccessProtectionFile();
        } catch (Exception $e) {

        }
    }

    /**
     * @param string $path
     * @throws Exception
     */
    private function createLogDirectory($path)
    {
        if (!is_dir($path) && !mkdir($path, 0755, true)) {
            throw new Exception('Cant create log directory ' . $path);
        }
    }

    /**
     * @throws Exception
     */
    private function createHtaccessProtectionFile()
    {
        $status = file_put_contents(DIR_FS_LOGS . DIRECTORY_SEPARATOR . ".htaccess", "Deny from all");
        if (!file_exists(DIR_FS_LOGS . DIRECTORY_SEPARATOR . ".htaccess") && $status === false) {
            throw new Exception('Cant create .htaccess file');
        }
    }

    /**
     * @param string $group
     * @param string $name
     * @return LoggerInterface
     */
    public function createLogger($group, $name)
    {
        $group = trim($group, " \t\n\r\0\x0B\\/");
        $name  = trim($name, " \t\n\r\0\x0B\\/");

        if (!isset($this->loggers[$group][$name])) {
            try {
                $this->createLogDirectory(DIR_FS_LOGS . DIRECTORY_SEPARATOR . $group);
                $this->loggers[$group][$name] = new FileLogger(
                    DIR_FS_LOGS . DIRECTORY_SEPARATOR . $group,
                    $name
                );
            } catch (Exception $exception) {
                $this->loggers[$group][$name] = new NullLogger(null, null);
            }
        }

        return $this->loggers[$group][$name];
    }

    /**
     * Prevent cloning object
     */
    private function __clone()
    {
    }

    /**
     * @return static
     */
    public static function create()
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }
        return static::$instance;
    }
}