<?php

namespace SoloMono\Chronicler;

/**
 * Class LogLevels
 *
 * @package SoloMono\Chronicler
 */
abstract class LogLevels
{
    /**
     *
     */
    const CRITICAL  = 'critical';

    /**
     *
     */
    const EMERGENCY = 'emergency';

    /**
     *
     */
    const ALERT   = 'alert';

    /**
     *
     */
    const ERROR   = 'error';

    /**
     *
     */
    const WARNING = 'warning';

    /**
     *
     */
    const DEBUG = 'debug';

    /**
     *
     */
    const INFO = 'info';
}