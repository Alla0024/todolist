<?php

namespace SoloMono\Chronicler;

/**
 * Interface LoggerInterface
 */
interface LoggerInterface
{
    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function critical($message, $context = []);

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function emergency($message, $context = []);

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function alert($message, $context = []);

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function error($message, $context = []);

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function warning($message, $context = []);

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function debug($message, $context = []);

    /**
     * @param string $message
     * @param array $context
     * @return void
     */
    public function info($message, $context = []);

    /**
     * @param string $lvl
     * @param string $message
     * @param array $context
     * @return void
     */
    public function log($lvl, $message, $context = []);
}