<?php
/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 26.10.2020
 * Time: 14:31
 *
 * be sure to add to htaccess
 * php_value auto_prepend_file path_to_project\includes\classes\errors_catcher\ErrorsCatcher.php
 * php_value auto_append_file path_to_project\includes\classes\errors_catcher\ErrorsCatcherFinish.php
 */
ini_set('error_reporting', E_WARNING );
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
class ErrorsCatcher
{
    const ERROR_SUPERVISOR = __DIR__ . '/ErrorSupervisor.php';

    protected $errorSupervisor;

    public static function app () {
        return new self();
    }

    protected function __construct()
    {

        if ($this->checkErrorSupervisor()) {

            $this->loadFile(self::ERROR_SUPERVISOR);
            $this->errorSupervisor = new ErrorSupervisor();
        }
    }

    public function sendMail()
    {
        $this->errorSupervisor->sendMail();
    }

    /**
     * @return bool
     */
    private function checkErrorSupervisor()
    {
        $res = false;
        if (is_file(self::ERROR_SUPERVISOR)) {
            $res = true;
        }

        return $res;
    }

    /**
     * @param string $path
     */
    private function loadFile($path)
    {
        require_once $path;
    }
}

$errorsCatcher = ErrorsCatcher::app();