var submitter = null;
var paymentVals = new Array();

function echeck(str) {

        var at="@"
        var dot="."
        var lat=str.indexOf(at)
        var lstr=str.length        
        var ldot=str.indexOf(dot)
        if (str.indexOf(at)==-1){
           return false
        }

        if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
           return false
        }

        if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
            return false
        }

         if (str.indexOf(at,(lat+1))!=-1){
            return false
         }

         if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
            return false
         }

         if (str.indexOf(dot,(lat+2))==-1){
            return false
         }
        
         if (str.indexOf(" ")!=-1){
            return false
         }
     
  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
if (!(filter.test(str))) {return false}

          return true                    
    }

function submitFunction() {
    submitter = 1;
}

var errCSS = {
    'border-color': 'red',
    'border-style': 'solid'
};

function bindAutoFill($el){
    if ($el.attr('type') == 'select-one'){
        var method = 'change';
    }else{
        var method = 'blur';
    }
    
    $el.blur(unsetFocus).focus(setFocus);
    
    if (document.attachEvent){
        $el.get(0).attachEvent('onpropertychange', function (){
            if ($(event.srcElement).data('hasFocus') && $(event.srcElement).data('hasFocus') == 'true') return;
            if ($(event.srcElement).val() != '' && $(event.srcElement).hasClass('required')){
                $(event.srcElement).trigger(method);
            }
        });
    }else{
        $el.get(0).addEventListener('onattrmodified', function (e){
            if ($(e.currentTarget).data('hasFocus') && $(e.currentTarget).data('hasFocus') == 'true') return;
            if ($(e.currentTarget).val() != '' && $(e.currentTarget).hasClass('required')){
                $(e.currentTarget).trigger(method);
            }
        }, false);
    }
}

function isValidCC(ccNumber){
    var regList = [
        '^(?:3[47][0-9]{13})$', //american express
        '(?:4[0-9]{12}(?:[0-9]{3})?)$', //visa
        '^(?:5[1-5][0-9]{14})$', // master card
        '^(?:6(?:011|5[0-9][0-9])[0-9]{12})$', //discover
        '^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$', //dinner club
        '^(?:2131|1800|35[0-9]{3})[0-9]{11}$'    // jcb
    ]
    regList = regList.map(function(x){return "("+x+")";}).join('|');
    var regExp = new RegExp(regList);
    return regExp.test(ccNumber);
}

function setFocus(){
    $(this).data('hasFocus', 'true');
}

function unsetFocus(){
    var elementValue = $(this).val();
    $(this).val(elementValue.trim());
    $(this).data('hasFocus', 'false');
}


var checkout = {
    charset: 'utf8',
    pageLinks: {},
    errors:true,
    checkoutClicked:false,
    checkoutSubmitted:false,
    checkoutBeforeSubmitted:false,
    amountRemaininginTotal:true,
    billingInfoChanged: false,
    shippingInfoChanged: false,
    fieldSuccessHTML: '<div class="success_icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z"></path></svg></div>',
    fieldErrorHTML: '<div class="error_icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg></div>',
    fieldRequiredHTML: '<div class="required_icon"><svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M478.21 334.093L336 256l142.21-78.093c11.795-6.477 15.961-21.384 9.232-33.037l-19.48-33.741c-6.728-11.653-21.72-15.499-33.227-8.523L296 186.718l3.475-162.204C299.763 11.061 288.937 0 275.48 0h-38.96c-13.456 0-24.283 11.061-23.994 24.514L216 186.718 77.265 102.607c-11.506-6.976-26.499-3.13-33.227 8.523l-19.48 33.741c-6.728 11.653-2.562 26.56 9.233 33.037L176 256 33.79 334.093c-11.795 6.477-15.961 21.384-9.232 33.037l19.48 33.741c6.728 11.653 21.721 15.499 33.227 8.523L216 325.282l-3.475 162.204C212.237 500.939 223.064 512 236.52 512h38.961c13.456 0 24.283-11.061 23.995-24.514L296 325.282l138.735 84.111c11.506 6.976 26.499 3.13 33.227-8.523l19.48-33.741c6.728-11.653 2.563-26.559-9.232-33.036z"></path></svg></div>',  // <i class="fa fa-warning"></i>
    showAjaxLoader: function (){

    },
    hideAjaxLoader: function (){

    },
    showAjaxMessage: function (message){
   
            // $('#checkoutButtonContainer').hide();
        $('#checkoutButtonContainer').find('.btn').addClass('unactive');   

        $('#ajaxMessages').show().html('<span><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><br>' + message + '</span>'); // <img src="/includes/javascript/onepage/ajax_load.gif">

    },
	hideAjaxMessage: function (){

	// raid ------ minimum order!!!---------------- //		 
	if($('#minsum').length) {
	   $('#minimal_sum').html($('#minsum').val());
       $('#checkoutButtonContainer_minimal').css('display','block');
   } 
	// raid ------END minimum order!!!---------------- //	
   else {
     // $('#checkoutButtonContainer').show();
     $('#checkoutButtonContainer').find('.btn').removeClass('unactive');
     $('#checkoutButtonContainer_minimal').css('display','none');
   }  
	 //$('#checkoutButtonContainer').show();
	
   $('#ajaxMessages').hide();
		
	},
    fieldErrorCheck: function ($element, forceCheck, hideIcon){
        
        forceCheck = forceCheck || false;
        hideIcon = hideIcon || false;
        var errMsg = this.checkFieldForErrors($element, forceCheck);
        if (hideIcon == false){
            if (errMsg != false){
                this.addIcon($element, 'error', errMsg);
                return true;
            }else{
                this.addIcon($element, 'success', errMsg);
            }
        }else{
            if (errMsg != false){
                return true;
            }
        }
        return false;
    },
    checkFieldForErrors: function ($element, forceCheck){
        var hasError = false;
     //   if ($element.is(':visible') && ($element.hasClass('required') || forceCheck == true)){
        if ($element.hasClass('required') && ($element.is(':visible') || forceCheck == true || $element.attr('name') == 'shipping_country' /*|| $element.attr('name') == 'shipping_zone_id'*/)){
            var errCheck = getFieldErrorCheck($element);   
            
             // for selectboxes:
            if($element.val()=='') {
              hasError = true;
            } else {
              if (!errCheck.errMsg){
                  return false;
              }
            } 
            
            switch($element.attr('type')){
                case 'password':
                if ($element.attr('name') == 'password'){
                    if ($element.val().length < errCheck.minLength){
                        hasError = true;
                    }
                }else{
                    if ($element.val() != $(':password[name="password"]', $('#shippingAddress')).val() || $element.val().length <= 0){
                        hasError = true;
                    }
                }
                break;
                case 'radio':
                if ($(':radio[name="' + $element.attr('name') + '"]:checked').length <= 0){
                    hasError = true;
                }
                break;
                case 'checkbox':
                if ($(':checkbox[name="' + $element.attr('name') + '"]:checked').length <= 0){
                    hasError = true;
                }
                break;
                case 'select-one':
                if ($element.val() == ''){
                    hasError = true;
                }
                break;
                default:
                if ($element.val().length < errCheck.minLength){
                    hasError = true;
                } else
        if (($element.attr('name') == 'billing_email_address') && ((!(echeck($element.val()))) || $element.attr('data-check') == 'false')) {
            hasError = true;
        
        }
                   
        
                break;
            }

            if (hasError == true){  
             //   console.log(errCheck.errMsg);
                return errCheck.errMsg;
            }
        }
        return hasError;
    },
    addIcon: function ($curField, iconType, title){
        title = title || false;
        $('.success_icon, .error_icon, .required_icon', $curField.parent()).hide();
        $('.checkout_inputs', $curField.parent()).css('border','1px solid #ccc');
        switch(iconType){
            case 'error':
            if (this.initializing == true){
                this.addRequiredIcon($curField, 'Required');
            }else{
                this.addErrorIcon($curField, title);
            }
            break;
            case 'success':
            this.addSuccessIcon($curField, title);
            break;
            case 'required':
            this.addRequiredIcon($curField, 'Required');
            break;
        }
    },
    addErrorIcon: function ($curField, title){
        if ($('.error_icon', $curField.parent()).length <= 0){
            $curField.parent().append(this.fieldErrorHTML);
        }
        // $('.error_icon', $curField.parent()).attr('title', title).show();
        $('.error_icon', $curField.parent()).html(title+'<svg style="margin-left: 5px;" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M569.517 440.013C587.975 472.007 564.806 512 527.94 512H48.054c-36.937 0-59.999-40.055-41.577-71.987L246.423 23.985c18.467-32.009 64.72-31.951 83.154 0l239.94 416.028zM288 354c-25.405 0-46 20.595-46 46s20.595 46 46 46 46-20.595 46-46-20.595-46-46-46zm-43.673-165.346l7.418 136c.347 6.364 5.609 11.346 11.982 11.346h48.546c6.373 0 11.635-4.982 11.982-11.346l7.418-136c.375-6.874-5.098-12.654-11.982-12.654h-63.383c-6.884 0-12.356 5.78-11.981 12.654z"></path></svg>').show();
        $('.checkout_inputs', $curField.parent()).css('border','1px solid #f00');
    },
    addSuccessIcon: function ($curField, title){
        if ($('.success_icon', $curField.parent()).length <= 0){
            $curField.parent().append(this.fieldSuccessHTML);
        }
        $('.success_icon', $curField.parent()).attr('title', title).show();
    },
    addRequiredIcon: function ($curField, title){
        if ($curField.hasClass('required')){
            if ($('.required_icon', $curField.parent()).length <= 0){
                $curField.parent().append(this.fieldRequiredHTML);
            }
            $('.required_icon', $curField.parent()).attr('title', title).show();
        }
    },
    clickButton: function (elementName){
        if ($(':radio[name="' + elementName + '"]').length <= 0){
            $('input[name="' + elementName + '"]').trigger('click', true);
        }else{
            $(':radio[name="' + elementName + '"]:checked').trigger('click', true);
         //   console.log(111);
        }
    
    },
    addRowMethods: function($row){
        var checkoutClass = this;

        $row.click(function (){ 
            if (!$(this).hasClass('moduleRowSelected')) {
             
              // delete all classes .moduleRowSelected
              var selector = ($(this).hasClass('shippingRow') ? '.shippingRow' : '.paymentRow') + '.moduleRowSelected';
              $(selector).find(':radio').removeAttr('checked');  
              $(selector).removeClass('moduleRowSelected');
              
              // add class and "checked" to selected module
              $(this).addClass('moduleRowSelected');    
			        $(this).find(':radio').prop('checked', true).click();
                checkoutClass.updateShippingMethods();
            }
        });
    }, 
    queueAjaxRequest: function (options){
        var checkoutClass = this;  
        var o = {
            url: options.url,
            cache: options.cache || false,
            dataType: options.dataType || 'html',
            type: options.type || 'GET',
            contentType: options.contentType || 'application/x-www-form-urlencoded; charset=' + this.ajaxCharset,
            data: options.data || false,
            beforeSend: options.beforeSend || function (){
                checkoutClass.showAjaxMessage(options.beforeSendMsg || 'Ajax Operation, Please Wait...');
            //    checkoutClass.showAjaxLoader();
            },
            complete: function (){
                    checkoutClass.hideAjaxMessage();
                    // raid!!!---------------------------
                    //if(checkoutClass.errors != true) $('#onePageCheckoutForm').submit();     
                    // raid!!!---------------------------
                    
                    if (document.ajaxq.q['orderUpdate'].length <= 0){
                        //alert(checkoutClass.errors);  alert(checkoutClass.checkoutClicked);
                        if(checkoutClass.errors != true && checkoutClass.checkoutClicked == true){   
                            var buttonConfirmOrder = $('.ui-dialog-buttonpane button:first');
                            buttonConfirmOrder.removeClass('ui-state-disabled');
                            $('#imgDlgLgr').hide();
                        }
                    
                    }
            },
            success: options.success
						//,
           // error: function (XMLHttpRequest, textStatus, errorThrown){
           //     if (XMLHttpRequest.responseText == 'session_expired') document.location = this.pageLinks.shoppingCart;
           //     alert(options.errorMsg || 'There was an ajax error, please contact ' + checkoutClass.storeName + ' for support.');
                //alert(textStatus +'\n'+ errorThrown+'\n'+options.data+'\n'+options.url);
           // }
        };
        if (checkoutClass.checkoutSubmitted === false) {
            $.ajaxq('orderUpdate', o);
            if (checkoutClass.checkoutBeforeSubmitted === true){
                checkoutClass.checkoutSubmitted === false;
            }
        }
    },  
    updateOrderTotals: function (){
        var checkoutClass = this;
        this.queueAjaxRequest({
            url: this.pageLinks.checkout,
            cache: false,
            data: 'action=getOrderTotals&randomNumber='+Math.random(),
            type: 'post',
            beforeSendMsg: checkoutClass.refresh,
            success: function (data){
                $('.orderTotals').html(data);
                //checkoutClass.updateRadiosforTotal();
            },
            errorMsg: checkoutClass.error_scart+' ' + checkoutClass.storeName
        });
    },
    updateModuleMethods: function (action, noOrdertotalUpdate){
        var checkoutClass = this;    
        var descText = (action == 'shipping' ? 'Shipping' : 'Payment');
        if (action == 'shipping'){
          var setMethod = checkoutClass.setShippingMethod;
        } else {
          var setMethod = checkoutClass.setPaymentMethod;
        }

        this.queueAjaxRequest({
            url: this.pageLinks.checkout,
            data: 'action=update' + descText + 'Methods',
            type: 'post',
            beforeSendMsg: checkoutClass.refresh_method+' ' + descText,
            success: function (data){
                $('#no' + descText + 'Address').hide();
                $('#' + action + 'Methods').html(data).show();
                $('.' + action + 'Row').each(function (){

                    checkoutClass.addRowMethods($(this));
                    $('input[name="' + action + '"]', $(this)).each(function (){

                        $(this).click(function (e, noOrdertotalUpdate){
                            setMethod.call(checkoutClass, $(this));
                        });
                    });

                });
                // if its shipping or if its FIRST time selected payment:          
                if(action == 'shipping' || $('#paymentMethods').find('.moduleRowSelected').length==0) checkoutClass.clickButton(descText.toLowerCase());
                checkoutClass.updateOrderTotals();

                if($("input[name=payment][value="+$("input[name=current_code]").val()+"]").prop("checked")!=true) { //if not checked payment with hidden field with model code - remove additional button
                    $("#checkoutButton").show();
                    $("#additional-button-container").remove();
                }

                // raid  - show additional fields if we select some shipping methods
                //  console.log($('.shippingRow.moduleRowSelected input[type=radio]').val());
                /*
                 var curr_sposob = $('.shippingRow.moduleRowSelected input[type=radio]').val();
                 var suburbblock = $('input[name=billing_suburb]').parent().parent();
                 var streetblock = $('input[name=billing_street_address]').parent().parent();

                 if(curr_sposob=='flat_flat') { 
                 suburbblock.fadeOut(0);
                 streetblock.fadeIn(100);
                 } else if(curr_sposob=='nwpochta_nwpochta'  ) {    
                 streetblock.fadeOut(0);
                 suburbblock.fadeIn(100);
                 } else { 
                 streetblock.fadeOut(0);
                 suburbblock.fadeOut(100);
                 }   */

                $(function () {
                    $('.checkout [data-toggle="tooltip"]').tooltip()
                });

                // raid end

            },
            errorMsg:  checkoutClass.error_some1+' ' + action + ' '+checkoutClass.error_some2+' ' + checkoutClass.storeName
        });
    },                               
    updateShippingMethods: function (noOrdertotalUpdate){
        if (this.shippingEnabled == false){
            return false;
        }        
                
        this.updateModuleMethods('shipping', noOrdertotalUpdate);
        
    },
    updatePaymentMethods: function (noOrdertotalUpdate){
        this.updateModuleMethods('payment', noOrdertotalUpdate);
    },
    setModuleMethod: function (type, method, successFunction){
        var checkoutClass = this;  
        this.queueAjaxRequest({
            url: this.pageLinks.checkout,
            data: 'action=set' + (type == 'shipping' ? 'Shipping' : 'Payment') + 'Method&method=' + method,
            type: 'post',
            beforeSendMsg: checkoutClass.setting_method+' ' + (type == 'shipping' ? 'Shipping' : 'Payment'),
            dataType: 'json',
            success: successFunction,
            errorMsg: checkoutClass.error_set_some1+' ' + type + ' '+checkoutClass.error_set_some2+' ' + checkoutClass.storeName + ' '+checkoutClass.error_set_some2
        }); 
        
        checkoutClass.updatePaymentMethods(true);

    },
    setShippingMethod: function ($button){
        if (this.shippingEnabled == false){
            return false;
        }   

        var checkoutClass = this;
        this.setModuleMethod('shipping', $button.val(), function (data){
          // for PHP 7

        });
    },
    setPaymentMethod: function ($button){

        var checkoutClass = this;
        this.setModuleMethod('payment', $button.val(), function (data){
          // for PHP 7 
        });  
    },

    processBillingAddress: function (skipUpdateTotals){   
        var hasError = false;
        var checkoutClass = this; 
        $('select[name="billing_country"], input[name="billing_street_address"], input[name="billing_zipcode"], input[name="billing_city"]', $('#billingAddress')).each(function (){
          if (checkoutClass.fieldErrorCheck($(this), false, true) == true) {
                  hasError = true;
          }   
        });
        if (hasError == true){ 
            return;        
        }   
    
        this.setBillTo();

        if(skipUpdateTotals == true)
        {
    //        this.updatePaymentMethods(true);
    //        this.updateShippingMethods(true);
    //        this.updateOrderTotals();
        }
        
    },
    processShippingAddress: function (skipUpdateTotals){
        var hasError = false;
        var checkoutClass = this;
        $('select[name="shipping_country"], input[name="shipping_street_address"], input[name="shipping_zipcode"], input[name="shipping_city"], *[name="shipping_state"]', $('#shippingAddress')).each(function (){ // input[id="checkoutButton"],
            if (checkoutClass.fieldErrorCheck($(this), false, true) == true){
                hasError = true;
            }
        });
        if (hasError == true){
            return;
        }
    
        this.setSendTo(true);
        // if (this.shippingEnabled == true && skipUpdateTotals != true){
        //     this.updateShippingMethods(true);
        // }
        if(skipUpdateTotals == true)
        {
    //        this.updatePaymentMethods(true);
    //        this.updateShippingMethods(true);
    //        this.updateOrderTotals();
        }
    },
    setCheckoutAddress: function (type, useShipping){
        var checkoutClass = this;    
        var selector = '#' + type + 'Address';
        var sendMsg = checkoutClass.setting_address+' ' + (type == 'shipping' ? checkoutClass.setting_address_ship : checkoutClass.setting_address_bil);
        var errMsg = type + ' address';
        if (type == 'shipping' && useShipping == false){
         //   selector = '#billingAddress';
         //   sendMsg = 'Setting shipping address';
         //   errMsg = 'payment address';
        }

        action = 'setBillTo';
        if (type == 'shipping'){
            action = 'setSendTo';
        }
        checkoutClass.checkoutBeforeSubmitted = true;
        this.queueAjaxRequest({
            url: this.pageLinks.checkout,
            cache: false,
            beforeSendMsg: sendMsg,
            dataType: 'json',
            data: 'action=' + action + '&' + $('*', $(selector)).serialize(),
            type: 'post',
            success: function (data){
              // raid!!!---------------------------
              if(checkoutClass.errors != true){
                  checkoutClass.checkoutSubmitted = true;
                  $('#onePageCheckoutForm').submit();
              }
              // raid!!!---------------------------
            }
				//		,
        //    errorMsg: 'There was an error updating your ' + errMsg + ', please inform ' + checkoutClass.storeName + ' about this error.'
        });
    },
    setBillTo: function (){
        this.setCheckoutAddress('billing', false);
    },
    setSendTo: function (useShipping){
        this.setCheckoutAddress('shipping', useShipping);
    }, 
    checkFields: function (){
        var checkoutClass = this;
        $('input, password, select', $('#' + addressTypeConfiguration.first.id)).each(function (){
            if ($(this).attr('name') != undefined && $(this).attr('type') != 'checkbox' && $(this).attr('type') != 'radio'){
                if ($(this).attr('type') == 'password'){
                    $(this).blur(function (){
                        if ($(this).hasClass('required')){
                            checkoutClass.fieldErrorCheck($(this));
                        }
                    });
                    /* Used to combat firefox 3 and it's auto-populate junk */
                    $(this).val('');

                }else{ 
                           
            //      $(this).keyup(function (){
                    $(this).blur(function (){ 
                          
                        checkoutClass[addressTypeConfiguration.first.infoChanged] = true;
                        if ($(this).hasClass('required')){
                            checkoutClass.fieldErrorCheck($(this));
                        }
                    });
                    if($(this).attr('name')!='billing_email_address') {
	                    $(this).keyup(function (){  
	                        checkoutClass[addressTypeConfiguration.first.infoChanged] = true;
	                        if ($(this).hasClass('required')){
	                            checkoutClass.fieldErrorCheck($(this));
	                        }
	                    });
                    }
                    // for selectboxes!!
                    $(this).change(function (){
	                        if ($(this).hasClass('required')){
	                            checkoutClass.fieldErrorCheck($(this), true);
	                        }  
                    }); 
                    bindAutoFill($(this));
                }

                if ($(this).hasClass('required')){
                    checkoutClass[addressTypeConfiguration.first.infoChanged] = true;
                    if (checkoutClass.fieldErrorCheck($(this), true, true) == false){
                        checkoutClass.addIcon($(this), 'success');
                    }else{
                        checkoutClass.addIcon($(this), 'required');
                    }
                }
            }
        });
    }, 
       
     checkAllErrors: function(){
        var checkoutClass = this;
            var errMsg = '';
            if ($('.required_icon:visible', $('#' + addressTypeConfiguration.first.id)).length > 0){
                errMsg += checkoutClass.error_err_ship+"\n"+ "<br />";
                $('.required_icon:visible', $('#' + addressTypeConfiguration.first.id)).each(function () {
                    errMsg += $(this).parents('.form-group').find('input').attr('placeholder') + "\n"+ "<br />"
                })
            }

            if ($('.error_icon:visible', $('#' + addressTypeConfiguration.first.id)).length > 0){
                errMsg += checkoutClass.error_err_ship+ "\n+ \"<br />\"";
                $('.error_icon:visible', $('#' + addressTypeConfiguration.first.id)).each(function () {
                    errMsg += $(this).text() + "\n"+ "<br />"
                })
            }

            if($('#diffShipping').prop('checked')==true) {
         //   if ($('#diffShipping:checked').length > 0){
                if ($('.required_icon:visible', $('#' + addressTypeConfiguration.second.id)).length > 0){
                    errMsg += checkoutClass.error_req_bil+ "\n" + "<br />";
                    $('.required_icon:visible', $('#' + addressTypeConfiguration.second.id)).each(function () {
                        errMsg += $(this).parents('.form-group').find('input').attr('placeholder') + "\n" + "<br />"
                    })
                }

                if ($('.error_icon:visible', $('#' + addressTypeConfiguration.second.id)).length > 0){
                    errMsg += checkoutClass.error_req_bil + "\n" + "<br />";
                    $('.error_icon:visible', $('#' + addressTypeConfiguration.second.id)).each(function () {
                        errMsg += $(this).attr('title') + "\n" + "<br />"
                    })
                }
            }

            if (errMsg != ''){
                errMsg = '<p><b>'+checkoutClass.error_address+':</b> ' +
                errMsg + "</p>";
            }

            if(checkoutClass.amountRemaininginTotal == true) {
                if ($(':radio[name="payment"]:checked').length <= 0){
                    if ($('input[name="payment"]:hidden').length <= 0){
                        errMsg += '<p><b>'+checkoutClass.error_pmethod+':</b> ' +
                        checkoutClass.error_select_pmethod + "</p>";
                    }
                }
            }

            if (checkoutClass.shippingEnabled === true){
                if ($(':radio[name="shipping"]:checked').length <= 0){
                    if ($('input[name="shipping"]:hidden').length <= 0) {
                        errMsg += '<p><b>'+checkoutClass.error_pmethod+':</b> ' +
                        checkoutClass.error_select_pmethod + "</p>";
                    }
                }
            }
            if(checkoutClass.ccgvInstalled == true) {
                if($('input[name="gv_redeem_code"]').val() == 'redeem code') {
                    $('input[name="gv_redeem_code"]').val('');
                }
            }

            if(checkoutClass.kgtInstalled == true) {
                if($('input[name="coupon"]').val() == 'redeem code') {
                    $('input[name="coupon"]').val('');
                }
            }


            if (errMsg.length > 0){
                checkoutClass.errors = true;
                // alert(errMsg);
                // showAlert(errMsg, $('#divShoppingCard'), 'alert-danger', 'alert-dismissible');
                return false;
            }else{
                checkoutClass.errors = false;
                //  if (checkoutClass.billingInfoChanged == true && $('.required_icon:visible', $('#billingAddress')).length <= 0 && checkoutClass.loggedIn != true){
                if(GOOGLE_GOALS_CHECKOUT_PROCESS) {
                    gtag('event', 'checkout_progress');
                }
                if(checkoutClass[addressTypeConfiguration.first.infoChanged] == true && $('.required_icon:visible', $('#shippingAddress')).length <= 0) {
                    //errMsg += 'You tried to checkout without first clicking update. We have updated for you. Please review your order to make sure it is correct and click checkout again.' + "\n";
                    checkoutClass[addressTypeConfiguration.first.jsProcessCallback]();
                    checkoutClass[addressTypeConfiguration.first.infoChanged] = false;
                }
                return true;
            }
        },
    initCheckout: function (){
        var checkoutClass = this;
        $('#onePageCheckoutForm input[type="text"]').on('input',function(){
            $(this).val($(this).val().replace(/(<([^>]+)>)/ig,"")); //striptags
        })
        /*var billingInfoChanged = false;
        if ($('#diffShipping').checked && this.loggedIn != true){
        var shippingInfoChanged = false;
        }*/


    		$('#diffShipping').click(function (){
    		  if($(this).prop('checked')==true) {
        //	if (this.checked){
    				$('#' + addressTypeConfiguration.second.id).slideDown();
    	//			$('#shippingMethods').html('');
    				$('#noShippingAddress').show();
    				$('select[name="'+addressTypeConfiguration.second.fullPrefix+'_country"]').trigger('change');
    			}else{
    				$('#' + addressTypeConfiguration.second.id).slideUp();
    				// var errCheck = checkoutClass.processShippingAddress();
    				// if (errCheck == ''){
    				// 	$('#noShippingAddress').hide();
    				// }else{
    				// 	$('#noShippingAddress').show();
    				// }
    			}
    		});
        
        $('#' + addressTypeConfiguration.second.id).hide();
        $('#checkoutYesScript').show();

        this.updateShippingMethods(true);    
        
        // store selected data to session
        $(document).on('change', 'select[name='+addressTypeConfiguration.first.fullPrefix+'_country]', function(event) {
            var $thisField = $(this);
      		// $.post(checkoutClass.pageLinks.checkout, { action:'setCheckoutAddressField',addresstype:'shipping','field':'country_id','value':$(this).val()}, function(data) {
      		// 	checkoutClass.updateShippingMethods(true);
      		// });
            checkoutClass.queueAjaxRequest({
                url: checkoutClass.pageLinks.checkout,
                data: 'action=setCheckoutAddressField&addresstype='+addressTypeConfiguration.first.fullPrefix+'&field=country_id&value=' + $thisField.val(),
                type: 'post',
                beforeSendMsg: checkoutClass.refresh_method,
                dataType: 'json',
                success: function (data) {
                    checkoutClass.updateShippingMethods(true);
                },
            });
        }); 
        
        // store selected data to session
        $(document).on('change', 'select[name='+addressTypeConfiguration.first.fullPrefix+'_zone_id]', function(event) {
            var $thisField = $(this);
      		// $.post(checkoutClass.pageLinks.checkout, { action:'setCheckoutAddressField',addresstype:'shipping','field':'zone_id','value':$(this).val()}, function(data) {
      		// 	checkoutClass.updateShippingMethods(true);
      		// });
            checkoutClass.queueAjaxRequest({
                url: checkoutClass.pageLinks.checkout,
                data: 'action=setCheckoutAddressField&addresstype='+addressTypeConfiguration.first.fullPrefix+'&field=zone_id&value=' + $thisField.val(),
                type: 'post',
                beforeSendMsg: checkoutClass.refresh_method,
                dataType: 'json',
                success: function (data) {
                    checkoutClass.updateShippingMethods(true);
                },
            });
        });
        // store selected data to session
        $(document).on('focusout', 'input[name='+addressTypeConfiguration.first.fullPrefix+'_zipcode]', function(event) {
            var $thisField = $(this);
      		// $.post(checkoutClass.pageLinks.checkout, { action:'setCheckoutAddressField',addresstype:'shipping','field':'zipcode','value':$(this).val()}, function(data) {
      		// 	checkoutClass.updateShippingMethods(true);
      		// });
            checkoutClass.queueAjaxRequest({
                url: checkoutClass.pageLinks.checkout,
                data: 'action=setCheckoutAddressField&addresstype='+addressTypeConfiguration.first.fullPrefix+'&field=zipcode&value=' + $thisField.val(),
                type: 'post',
                beforeSendMsg: checkoutClass.refresh_method,
                dataType: 'json',
                success: function (data) {
                    checkoutClass.updateShippingMethods(true);
                },
            });
        });

        // -------------------CHANGE SHIPPING ADDRESS!!--------------------------
        $(document).on('click', '.choose_modal', function(event) {
          event.preventDefault();
          var addressType = $(this).attr('data-addresstype');
            checkoutClass.queueAjaxRequest({
                url: "./ajax_choose_modal.php",
                data: 'addresstype=' + addressType,
                type: 'post',
                beforeSendMsg: checkoutClass.refresh_method,
                dataType: 'html',
                success: function (data) {
                    modal({
                        id: 'pop_choose',
                        title: CHOOSE_ADDRESS,
                        width:'470px',
                        body: data
                    });
                },
            });
          // $.post( "./ajax_choose_modal.php", {'addresstype':$(this).attr('data-addresstype')}, function(data) {
          //     modal({
          //         id: 'pop_choose',
          //         title: CHOOSE_ADDRESS,
          //         width:'470px',
          //         body: data
          //     });
          // });
        });
        
        $(document).on('click', '.addresses_block', function(event) {
          $(this).closest('.modal-body').find('.addresses_block').removeClass('ab_checked');  
          $(this).addClass('ab_checked');           
          $(this).find('input[type=radio]').prop('checked', true);
        });
        
        $(document).on('click', '#confirm_change_address', function(event) {
          event.preventDefault();
          $.get( "./ajax_select_region.php", { 'name':$(this).closest('.modal-body').find(':radio:checked').attr('name'),'val':$(this).closest('.modal-body').find(':radio:checked').val()}, function(data) {
            location.reload();  
          });
        });
        
         // -------------------CHANGE SHIPPING ADDRESS!!-----END---------------------
        $(document).on('change',"select[name=billing_country], select[name=shipping_country]", function() {

            var country = $(this);
            var default_region = country.attr('data-zone');
            var zone_field_name = '';
            var fieldForInsert = '';

            country.attr('data-zone',''); // erase default zone for all other countries except default.

            if(country.attr('name')=='billing_country') {
              zone_field_name = 'billing_zone_id';
              fieldForInsert = $('[name=billing_city]');
            }
            else if(country.attr('name')=='shipping_country'){
              zone_field_name = 'shipping_zone_id';
              fieldForInsert = $('[name=shipping_city]');
            }


            $("select[name="+country.attr('name')+"]+button span:last-of-type").html(country.children(':selected').text()); // change country selectize

            //  $("*[name="+zone_field_name+"]").remove();
            checkoutClass.queueAjaxRequest({
                url: "./ajax_select_region.php",
                data: 'country_id='+country.val()+'&default_region='+default_region+'&zone_field_name=' + zone_field_name,
                type: 'get',
                beforeSendMsg: checkoutClass.refresh_method,
                success: function (data) {
                    if($("*[name="+zone_field_name+"]").length!=0) $("*[name="+zone_field_name+"]").parent().replaceWith(data);
                    else fieldForInsert.parent().after(data);

                    if(typeof checkout !== 'undefined') checkout.checkFields();
                    $("select[name="+zone_field_name+"]").selectize({
                      hideSelected:true,
                      maxItems:1,
                      // placeholder:ENTER_KEY,
                      highlight:true,
                      onDropdownOpen:function() {
                        this.$input.closest('.form-group').find('.error_icon').css('display','none');
                      },
                      onFocus: function(){
                        this.clear();
                      }
                    });
                },
            });

        });

        $('select[name=billing_country]').change();
        $('select[name=shipping_country]').change();
        $('input[name=billing_email_address]').val('');

      	$(document).on('click', '#voucherRedeem', function(event) {
      		// $.post('./includes/checkout/checkout_cart.php', {
      		// 	gv_redeem_code: $('input[name=gv_redeem_code]').val()
      		// }, function(data) {
          //    $('#checkout_cart').html(data);
          //    checkout.updateOrderTotals();
          // });
      		var gvCode = $('input[name=gv_redeem_code]').val();
            checkoutClass.queueAjaxRequest({
                url: './includes/checkout/checkout_cart.php',
                data: 'gv_redeem_code=' + gvCode,
                type: 'post',
                beforeSendMsg: checkoutClass.refresh_method,
                dataType: 'html',
                success: function (data) {
                    $('#checkout_cart').html(data);
                    checkout.updateOrderTotals();
                },
            });
      	});
        
        $(document).on('click', '.delete', function(event) {
          $.post('./popup_cart.php?action=update_product', {'cart_delete[]':$(this).val(),'products_id[]':$(this).val()}, function(response) {
             $.post('./includes/checkout/checkout_cart.php', '',function(data) {
               if(data!='empty') {
                 $('#checkout_cart').html(data);

                 checkout.updateShippingMethods();
                 checkout.updateOrderTotals();
                 updateCart();
               } else {
                 location.reload();
               }
             });
          });
        });

    checkout.checkFields();

    $('body').on('change', '#registration-off', function () {
        $('input[name="billing_email_address"]').change();
    });
   
		$('input[name="billing_email_address"]').each(function (){
			$(this).unbind('blur').change(function (){
				var $thisField = $(this);
                var withOutRegistration = $('#registration-off').prop('checked');
                $thisField.attr('data-check','progress');

				checkoutClass.shippingInfoChanged = true;
				if (checkoutClass.initializing == true){
					checkoutClass.addIcon($thisField, 'required');
				}else{
					//if (this.changed == false) return;
					if (checkoutClass.fieldErrorCheck($thisField, true, true) == false){
						this.changed = false;
						if($thisField.val() == '') {
							checkoutClass.addIcon($thisField, 'error', data.errMsg.replace('/n', "\n"));
						}
            if(!withOutRegistration) {
                checkoutClass.queueAjaxRequest({
                    url: checkoutClass.pageLinks.checkout,
                    data: 'action=checkEmailAddress&emailAddress=' + $thisField.val(),
                    type: 'post',
                    beforeSendMsg: checkoutClass.check_email,
                    dataType: 'json',
                    success: function (data) {
                        $('.success, .error', $thisField.parent()).hide();
                        if (data.success == 'false') {
                            $thisField.attr('data-check',false);
                            checkoutClass.addIcon($thisField, 'error', data.errMsg.replace('/n', "\n"));
                            //		alert(data.errMsg.replace('/n', "\n").replace('/n', "\n").replace('/n', "\n"));
                            $("#email_error").html(data.errMsg.replace('/n', "\n").replace('/n', "\n").replace('/n', "\n"));
                        } else {
                            $thisField.attr('data-check',true);
                            $("#email_error").html('');
                            checkoutClass.addIcon($thisField, 'success');
                        }
                    },
                    errorMsg: checkoutClass.error_email + ' ' + checkoutClass.storeName + ' ' + checkoutClass.error_set_some3
                });
            } else {
                $("#email_error").html('');
                checkoutClass.addIcon($thisField, 'success');
            }
					}
				}
			}).keyup(function (){
				this.changed = true;
			});
			bindAutoFill($(this));
		});
    
		$('input,select[name="'+addressTypeConfiguration.second.fullPrefix+'_country"]', $('#' + addressTypeConfiguration.second.id)).each(function (){
			if ($(this).attr('name') != undefined && $(this).attr('type') != 'checkbox'){
				var processAddressFunction = function (){
					if ($(this).hasClass('required')){
						checkoutClass.fieldErrorCheck($(this));
					}
				};
			
				$(this).blur(processAddressFunction);
				bindAutoFill($(this));

				if ($(this).hasClass('required')){
					var icon = 'required';
					if ($(this).val() != '' && checkoutClass.fieldErrorCheck($(this), true, true) == false){
						icon = 'success';
					}
					checkoutClass.addIcon($(this), icon);
				}
			}
		});
		
		$('input,select[name="'+addressTypeConfiguration.first.fullPrefix+'_country"]', $('#' + addressTypeConfiguration.first.id)).each(function (){
			if ($(this).attr('name') != undefined && $(this).attr('type') != 'checkbox'){
				var processAddressFunction = function (){

					if ($(this).hasClass('required')){
						if (checkoutClass.fieldErrorCheck($(this)) == false){
					//		$('#noShippingAddress').hide();
					//		$('#shippingMethods').show();
							checkoutClass[addressTypeConfiguration.first.jsProcessCallback]();  // only if checkout button is not clicked.
						}else{
					//		$('#noShippingAddress').show();
					//		$('#shippingMethods').hide();
						}
					}
				};
			
				$(this).blur(processAddressFunction);
				bindAutoFill($(this));

			}
		});
    
		if(checkoutClass.stateEnabled == true)
		{

      /*
			$('select[name="shipping_country"], select[name="billing_country"]').each(function (){
				var $thisName = $(this).attr('name');
				var fieldType = 'billing';
				if ($thisName == 'shipping_country'){
					fieldType = 'delivery';
				}
	//			checkoutClass.addCountryAjax($(this), fieldType + '_state', 'stateCol_' + fieldType);
			});  */

		 /*
			$('*[name="billing_state"], *[name="delivery_state"]').each(function (){
				var processAddressFunction = checkoutClass.processBillingAddress;
				if ($(this).attr('name') == 'delivery_state'){
					processAddressFunction = checkoutClass.processShippingAddress;
				}
				
				var processFunction = function (){
					if ($(this).hasClass('required')){
						if (checkoutClass.fieldErrorCheck($(this)) == false){
							processAddressFunction.call(checkoutClass);
						}
					}else{
						processAddressFunction.call(checkoutClass);
					}
				}
			
				if ($(this).attr('type') == 'select-one'){
					$(this).change(processFunction);
				}else{
					$(this).blur(processFunction);
				}
				bindAutoFill($(this));
			});  */
		}
    
    $('#checkoutButton').on('mousedown', function(event) { // if "mousedown" first, we prevent "blur" from shippingAddress
        event.preventDefault();
    }).on('click', function() {
        $('select[name="'+addressTypeConfiguration.first.fullPrefix+'_country"], select[name="'+addressTypeConfiguration.first.fullPrefix+'_zone_id"], input', $('#'+ addressTypeConfiguration.first.id)).each(function (){
            checkoutClass.fieldErrorCheck($(this), false, false)
        });
			  checkoutClass.checkAllErrors();    
        return false;  
    });
     /*
        $('#checkoutButton').click(function() { 
				  checkoutClass.checkAllErrors();    
          return false;                                   
        });   */
        
      /*
        if (this.loggedIn == true && this.showAddressInFields == true){
            $('*[name="billing_state"]').trigger('change');
            $('*[name="delivery_state"]').trigger('change');
        }  */


        this.initializing = false;

        // off autocomplete for country field
        $('#shippingAddress input[type=select-one]').attr("autocomplete","none");
    }

}