<?php
	if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'){
		require('includes/application_top.php');
		switch ($_POST['request']) {
			case 'getBuyOnClickForm':
				ob_start();
        if (file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/quick_buy_form.tpl.php')) require_once DIR_WS_TEMPLATES . TEMPLATE_NAME.'/content/quick_buy_form.tpl.php';
        else require_once DIR_WS_CONTENT.'/quick_buy_form.tpl.php';
        
				$form = ob_get_contents();
				ob_end_clean();
				echo json_encode(array('html'=>$form));
			break;
			case 'getLoginForm':
				ob_start();
				if (file_exists(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/content/modal_login.tpl.php')) require_once DIR_WS_TEMPLATES . TEMPLATE_NAME.'/content/modal_login.tpl.php';
				else require_once DIR_WS_CONTENT.'/modal_login.tpl.php';

				$form = ob_get_contents();
				ob_end_clean();
				echo json_encode(array('html'=>$form));
			break;
			case 'QuickBuyProccess':
				$message = IMAGE_BUTTON_CONFIRM_ORDER.':<br>';
				$message .= ENTRY_TELEPHONE_NUMBER.' '.$_POST['phone'].'<br>';
				$message .= PREV_NEXT_PRODUCT.': '.tep_get_products_name(tep_db_prepare_input($_POST['products_id'])).'<br>';
				$message .= LOW_STOCK_TEXT2.': '.$_POST['model'];

				$sendto = CONTACT_US_LIST;
				if(tep_mail(STORE_OWNER, $sendto, QUICK_ORDER, $message, STORE_NAME, STORE_OWNER_EMAIL_ADDRESS)){
					$response = array('success'=>true,'message'=>'<span>'.QUICK_ORDER_SUCCESS.'</span>');
				}else{
					$response = array('success'=>false);
				}
				echo json_encode($response);
				die();
			break;
		}
	}

  ?>