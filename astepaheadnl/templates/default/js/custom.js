/****************************/
if($(window).width() <= '768') {
    $('.section_top_footer .h3, .contacts_info_footer .h3').click('click', function (event) {
        event.preventDefault();
        var arrow = $(this).parent().find('.toggle-xs');
        var h3 = $(this);

        $(arrow.attr('data-target')).slideToggle(); // show or hide block

        if (arrow.hasClass('open')) {
            arrow.removeClass('open');
            h3.removeClass('openh3');
        } else {
            arrow.addClass('open');
            h3.addClass('openh3');
        }
    });
}

$('.open-menu-xs').click( function(e) {
    $('body').toggleClass('no-scroll-y');
    $('.navbar-header').toggleClass('header-categories-animate');
    $(this).find('.fader').toggleClass('fader-menu-animate');
});
//open mobile_menu
$('.btn-mobile_menu').click( function(e) {
    $('.btn-mobile_menu').toggleClass('active_btn');
    $('.mobile_menu').toggleClass('active_menu');
    $('#search-form-fader').toggleClass('search-form-fader-open');
    $('body').toggleClass('modal-open');

});
//close search, mobile_menu
$('#search-form-fader').click( function(e) {
    $('#search-form-fader').removeClass('search-form-fader-open');
    $('.btn-mobile_menu').removeClass('active_btn');
    $('.mobile_menu').removeClass('active_menu');
    $('body').removeClass('modal-open');
    closeSearchForm();
});

//arrow rotate mobile_menu
$('.button-main-cursor').click( function(e) {

    $(this).parent().find('.navbar-nav, .menu_information, .menu_manuf').toggleClass('open_menu');
    if($(this).next().hasClass('open_menu')){
        $(this).find('.fa, svg').css('transform', 'rotate(90deg)');
    }
    else{
        $(this).find('.fa, svg').css('transform', 'rotate(0)');
    }
});
$('.show-sub_ul .down').click( function(e) {
    $(this).parent().toggleClass('active');
    if($(this).parents().hasClass('active')){
        $(this).find('.fa, svg').css('transform', 'rotate(90deg)');
    }
    else{
        $(this).find('.fa, svg').css('transform', 'rotate(0)');
    }
});
$("#manufacturers > div").owlCarousel({
    items: 4,
    responsive:{
        0:{items:3},
        600:{items:5}},
    nav: true,
    dots: false,
    loop:true,
    navText:['<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z"></path></svg>','<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z"></path></svg>'],
    slideSpeed: 200
});


function openSearchForm() {
    if ($('.navbar-header .main_search_form').hasClass('search-form-open')) {
        closeSearchForm();
    } else {
        $('.navbar-header .main_search_form').addClass('search-form-open');
        $('.navbar-header #searchpr1').addClass('search-form-input-open').select();
        $('.navbar-header #search-form-button1').addClass('search-form-button-open');
        $('.navbar-header #search-form-button-close1').addClass('search-form-button-close-open');
        $('.search-form-fader').toggleClass('search-form-fader-open');
        $('body').addClass('modal-open');

        if (!$('#search-form-fader').hasClass('search-form-fader-open')) {
            $('#search-form-fader').addClass('search-form-fader-open');
        }
    }
}

function closeSearchForm() {
    $('.navbar-header .main_search_form').removeClass('search-form-open');
    $('.navbar-header #searchpr1').removeClass('search-form-input-open');
    $('.navbar-header #search-form-button1').removeClass('search-form-button-open');
    $('.navbar-header #search-form-button-close1').removeClass('search-form-button-close-open');
    if ($('.mobile_menu.active_menu').length === 0) {
        $('body').removeClass('modal-open');
        $('#search-form-fader').removeClass('search-form-fader-open');
    }

    // if ($('#search-form-fader').hasClass('search-form-fader-open')) {
    //     $('#search-form-fader').removeClass('search-form-fader-open');
    //
    // }
}