<h2><center><?php echo HEADING_TITLE; ?></center></h2>

<p class="text-center"><?php echo TEXT_ACCOUNT_CREATED; ?></p>

<div class="form-group">
    <p class="text-center">
        <?php echo '<a class="btn btn-lg btn-default" href="' . $origin_href . '">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?>
    </p>
</div>

