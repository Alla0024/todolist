<style>
    body {
        font-family:Arial;
    }

    .products_table {
        width:100%;
        padding:20px 0;
    }

    .products_table td {
        padding:5px;
    }

    .order_confirm_title {
        font-family: Arial;
        position: absolute;
        top: 233px;
        left: 90px;
        background: #fff;
        padding: 5px 10px;
        font-style: italic;
        font-size: 18px;
        color: #242424;
        font-weight: bold;
        border-top-left-radius:10px;
        border-top-right-radius:10px;
        border-bottom-right-radius:10px;
        border-bottom-left-radius:10px;
        border:1px solid #242424;
    }
    .address_block {
        width: 100%;
    }
    .address_block .border_radius {
        border-top-left-radius:10px;
        border-top-right-radius:10px;
        border-bottom-right-radius:10px;
        border-bottom-left-radius:10px;
        border:1px solid #000;
        position: relative;
        display: block;
        width: 42%;
        float: left;
        padding: 10px 10px 0 20px;
        font-size: 13px;
    }
    .address_block .border_radius b {
        font-size: 14px;
    }
    .address_block .border_radius div {
        width: 50%;
        float: left;
    }
    .head_logo .logo_text {
        text-align: right;
        background-color: #1b302f;
        color: white;
        font-size: 30px;
        padding: 5px 10px;
        font-weight: 100;
        -webkit-print-color-adjust: exact;
        max-height: 30px;
    }
    .address_box__title {
        vertical-align: top;
    }
    .top_line {
        height: 20px;
        background-color: #1b302f;
        color: white;
        line-height: 20px;
        font-weight: 600;
        padding: 7px 9px;
    }
    .footer-line {
        background: #0090b5;
        border-color: #0090b5;
        color: #0090b5;
        height: 35px;
        -webkit-print-color-adjust: exact;
    }
</style>
<!--mpdf

<htmlpagefooter name="myfooter">
<table width="100%" style="padding-top:80px;">

    <tr style="width:100%; text-align:right;">
        <td width="70%" style="padding-bottom:20px;text-align:right;vertical-align: bottom;padding-right:15px;font-size:10px;border-right:1px solid #eee">
        </td>
        <td width="15%" style="padding-bottom:20px;text-align:left;font-size: 10px;padding-left:15px;">
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2">

        </td>
    </tr>
</table>
</htmlpagefooter>

<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->

<table style="padding-bottom: 20px;width: 100%;" class="print_header">
    <tr class="head_logo">
        <td width="50%" rowspan="2" class="logo">
            <img src="<?= HTTP_SERVER . "/" . LOGO_IMAGE?>" alt="logo" style="max-width: 40%">
        </td>
        <td width="30%"></td>
        <td width="18%" rowspan="2" class="logo_text">
            <?= DOCUMENT_TITLE; ?>
        </td>
    </tr>
</table>
<table class="address_box">
    <tr>
        <td class="address_box__title">
            <?= ADDRESS_TITLE ?>:
        </td>
        <td class="address_box__body" width="60%">
            <?= $order->customer["company"] . ' ' .$order->customer["name"] ?><br>
            <?= $order->customer["street_address"] . ', ' . $order->customer["postcode"] ?><br>
            <?= $order->customer["country"] ?><br>
        </td>
    </tr>
    <tr>
        <td style="height: 20px"></td>
    </tr>
    <tr>
        <td class="tel_box__title">
            <?= TEL_TITLE ?>:
        </td>
        <td class="tel_box__body" width="60%">
            <?= $order->customer["telephone"]; ?>
        </td>
    </tr>
    <tr>
        <td style="height: 20px"></td>
    </tr>
</table>
<table style="width: 100%">
    <tr>
        <td>
            <?= INVOICE_NUMBER_TITLE ?>:
        </td>
        <td>
            W<?= $order_info["invoice_number"] ?>
        </td>

        <td>
            <?= INVOICE_DATE_TITLE ?>:
        </td>
        <td>
            <?= date('d-m-y', strtotime($order->info['date_purchased'])); ?>
        </td>

        <td>
            <?= INVOICE_VAT_TITLE ?>:
        </td>
        <td>
            <?= INVOICE_VAT ?>
        </td>
    </tr>
    <tr>
        <td>
            <?= INVOICE_DEBITEUR_TITLE ?>:
        </td>
        <td>
            <?= $order_info["customers_id"] ?>
        </td>

        <td>
            <?= INVOICE_EXPIRATION_DATE ?>:
        </td>
        <td>
            <?= date('d-m-y', strtotime($order->info['date_purchased']) + 1209600) // + 14 days ?>
        </td>

        <td>
            <?= INVOICE_SHEET_TITLE ?>:
        </td>
        <td>
            <?= INVOICE_SHEET_NUMBER ?>
        </td>
    </tr>
    <tr>
        <td style="height: 20px"></td>
    </tr>
</table>

<table width="100%" style="font-size: 14px;">
    <tbody>
    <tr>
        <td colspan="2" align="center">
            <table border="0" cellspacing="0" cellpadding="2" class="products_table">
                <tr>
                    <td class="top_line" width="20" align="left"><b><?php echo ENTRY_QTY; ?></b></td>
                    <td class="top_line" width="60" align="left"><b><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></b></td>
                    <td class="top_line" align="left"><b><?php echo TABLE_HEADING_PRODUCTS; ?></b></td>
                    <td class="top_line" width="50" align="right"><b><?php echo TABLE_HEADING_REF; ?></b></td>
                    <td class="top_line" width="50" align="right"><b><?php echo TABLE_HEADING_INVOICE_PRODUCT_PRICE; ?></b></td>
                    <td class="top_line" width="50" align="right"><b><?php echo TABLE_HEADING_TOTAL_EXCLUDING_TAX; ?></b></td>
                </tr>
                <?php
                for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
                    echo '      <tr>' . "\n" .
                        '        <td valign="top" width="20" align="left">' . $order->products[$i]['qty'] . '</td>' . "\n" .
                        '        <td width="60" valign="top">' . $order->products[$i]['model'] . '</td>' . "\n" .
                        '        <td valign="top" align="left">' . $order->products[$i]['name'];

                    if (isset($order->products[$i]['attributes']) && (($k = sizeof($order->products[$i]['attributes'])) > 0)) {
                        for ($j = 0; $j < $k; $j++) {
                            echo '<br><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
                            if ($order->products[$i]['attributes'][$j]['price'] != '0') echo ' (' . $order->products[$i]['attributes'][$j]['prefix'] . $currencies->format($order->products[$i]['attributes'][$j]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . ')';
                            echo '</i></small></nobr>';
                        }
                    }


                    echo '          </td>' . "\n" ;
                    echo '          <td WIDTH="50" align="right" valign="top">' . number_format($order->products[$i]['tax'],0,'.','') . '%</td>' . "\n";
                    echo '          <td WIDTH="50" align="right" valign="top">' . $currencies->format($order->products[$i]['price'], true, $order->info['currency'], $order->info['currency_value']) . '</td>' . "\n" .
                        '          <td WIDTH="50" align="right" valign="top">' . $currencies->format($order->products[$i]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</td>' . "\n";
                    echo '         </tr>' . "\n";
                }
                ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td><?= SECOND_PART_TITLE ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php
                $lng = tep_get_languages();
                foreach ($lng as $lngItem) {
                    includeLanguages(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $lngItem['directory'] .'/modules/order_total/ot_subtotal.json');
                    includeLanguages(DIR_FS_CATALOG . DIR_WS_LANGUAGES . $lngItem['directory'] .'/modules/order_total/ot_total.json');
                }
                $excludeTotals = [
                    MODULE_ORDER_TOTAL_SUBTOTAL_TITLE,
                    MODULE_ORDER_TOTAL_TOTAL_TITLE,
                    'B.T.W. 21.0%',
                    'Total Excl. VAT:',
                    'Total incl. VAT',
                ];
                for ($k = 0, $n = sizeof($order->totals); $k < $n; $k++) {
                    $check = true;
                    foreach ($excludeTotals as $exclude) {
                        if (stripos($order->totals[$k]['title'], $exclude) !== false) {
                            $check = false;
                            break;
                        }
                    }
                    switch ($order->totals[$k]['class']) {
                        case 'ot_tax':
                            $VatTax = str_replace('.', '', $order->totals[$k]['text']);
                            $VatTax = (float) str_replace(['€', ','], ['', '.'], $VatTax);
                            break;

                        case 'ot_total':
                            $totals = str_replace('.', '', $order->totals[$k]['text']);
                            $totals = (float) str_replace(['<b>€', ','], ['', '.'], $totals);
                            break;
                    }
                    if ($check) {
                        $i++; ?>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><?= $order->totals[$k]['title']; ?></td>
                            <td></td>
                            <td></td>
                            <td align="right" valign="top"><?= $order->totals[$k]['text']; ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
            <table width="100%" style="font-size: 14px;" border="0" cellspacing="0" cellpadding="2" class="products_table">
                <tr>
                    <td class="top_line" width="20" align="left"><b><?php echo CREDIT_LIMIT; ?></b></td>
                    <td class="top_line" width="60"><b><?php echo OTHER; ?></b></td>
                    <td class="top_line" width="120" align="center"><b><?php echo SUBTOTAL; ?></b></td>
                    <td class="top_line" width="90" align="right"><b><?php echo VAT_SUBTOTAL; ?></b></td>
                    <td class="top_line" width="90" align="right"><b><?php echo INVOICE_TOTAL; ?></b></td>
                </tr>
                <tr>
                    <td width="20" align="left"><?php echo $currencies->format(0, true, $order->info['currency'], $order->info['currency_value']) ?></td>
                    <td width="60"><?php echo $currencies->format(0, true, $order->info['currency'], $order->info['currency_value']) ?></td>
                    <td width="120" align="center"><?php echo $currencies->format($totals - $VatTax, true, $order->info['currency'], $order->info['currency_value']) ?></td>
                    <td width="90" align="right"><?php echo $currencies->format($VatTax, true, $order->info['currency'], $order->info['currency_value']) ?></td>
                    <td width="90" align="right"><b><?php echo $currencies->format($totals, true, $order->info['currency'], $order->info['currency_value']) ?></b></td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<div class="test" style="display:block;position: absolute; bottom: 0">
<div class="before_footer_item"><?= PAYMENT_CONDITION_TITLE?> : <?= $order->info["payment_method"] ?></div>
<div class="before_footer_item"><?= PAYMENT_TERM_TITLE?> : <?= PAYMENT_TERM ?></div>
<div class="before_footer_item"><?= CREDIT_LIMITATION_TITLE?> : <?= CREDIT_LIMITATION ?></div>
<div class="before_footer_item"><?= COMPLAINTS?></div>
    <div style="width: 87%; margin-left:18px"><hr class="footer-line" color="black"></div>
<table class="footer_block">
    <tr>
        <td style="padding: 15px 15px 15px 0px; line-height: 20px;">
            <?= FOOTER_BLOCK1 ?>
        </td>
        <td style="padding: 15px 15px 15px 0px; line-height: 20px;">
            <?= FOOTER_BLOCK2 ?>
        </td>
        <td style="padding: 15px 15px 15px 0px; line-height: 20px;">
            <div><?= FOOTER_PHONE ?></div>
            <div><?= FOOTER_EMAIL ?></div>
            <div><?= FOOTER_SITE ?></div>
        </td>
        <td style="padding: 15px 15px 15px 0px; line-height: 20px;">
            <div><?= FOOTER_CC ?></div>
            <div><?= FOOTER_IBAN ?></div>
            <div><?= FOOTER_BIC ?></div>
            <div><?= FOOTER_VAT ?></div>
        </td>
    </tr>
</table>
</div>