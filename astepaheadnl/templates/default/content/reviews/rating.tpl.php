<div class="rating_wrapper">
    <div class="sp_rating">
        <div class="base">
            <div class="average" style="width: {{rating_value}}%;"></div>
        </div>
        <div class="status">
            <div class="score review_score">
                <a class="score1" data-val="1"></a>
                <a class="score2" data-val="2"></a>
                <a class="score3" data-val="3"></a>
                <a class="score4" data-val="4"></a>
                <a class="score5" data-val="5"></a>
            </div>
        </div>
    </div>
</div>