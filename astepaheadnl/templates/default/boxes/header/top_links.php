<!-- TOP LINKS -->
<nav class="main_nav clearfix">
  <ul>
    <?php
    $art_array = getArticles($config['id']['val']?:16, $config['limit']['val']?:7);
    if (is_array($art_array)) {
        foreach ($art_array as $articles) {
            $active = $articles['link']==HTTP_SERVER.$_SERVER['REQUEST_URI']?'class="active"':'';
            echo '<li><a '.$active.' href="' . $articles['link'] . '">' . $articles['name'] . '</a></li>';
        }
    }
    ?>
  </ul>
</nav>
<!-- END TOP LINKS -->