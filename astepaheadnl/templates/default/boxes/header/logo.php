<!-- LOGO -->

<?php
       if (LOGO_IMAGE) $logo_str = '<img class="img-responsive lazyload" alt="' . LOGO_IMAGE_TITLE . '" src="images/pixel_trans.png" data-src="' . HTTP_SERVER . '/' . str_replace("images/", "images/350x350/", LOGO_IMAGE) . '" width="200" height="100"/>';
       else $logo_str = '<p style="font-size: 18px;">' . STORE_NAME . '</p>';

       echo '<div class="logo"><a href="' . tep_href_link('/') . '">'.$logo_str.'</a></div>';
?>

<!-- END LOGO -->                