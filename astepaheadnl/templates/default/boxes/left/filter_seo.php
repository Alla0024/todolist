<?php
include(DIR_WS_MODULES . 'filters.php');



if (($_GET['cPath'] != '' or isset($_GET['manufacturers_id']) or isset($_GET['keywords'])) and ATTRIBUTES_PRODUCTS_MODULE_ENABLED == 'true' && $all_pids_string) { ?>
    <div id="filters_box" class="box filter_box">
        <div class="filter_box_in">
            <?php
            // Price Range Filter:
            if ($rmin != $rmax) {
                $r_content .= '<div class="dipcen">
                         <div><span class="filter_heading">' . COMP_PROD_PRICE . '</span></div>
                         <div id="slider-range"></div>
                         <span class="left slider-from">
                           <input type="text" name="rmin" id="range1" value="' . $rmin . '"  />
                         </span>
                         <span class="left slider-to">                                                                                                             
                           <input type="text" name="rmax" id="range2" value="' . $rmax . '" />
                         </span>
                         &nbsp;&nbsp;<span class="price_fltr">' . $price_fltr . '</span>
                       </div>';

                // show max price value for js:
                $slider_max = isset($_GET['rmax_current']) ? (int)$_GET['rmax_current'] : (int)$listing_sql_max['max_price'];
                $slider_min = isset($_GET['rmin_current']) ? (int)$_GET['rmin_current'] : (int)$listing_sql_max['min_price'];
                $r_content  .= '<input type="hidden" name="slider_max" value="' . $slider_max . '" />';
                $r_content  .= '<input type="hidden" name="slider_min" value="' . $slider_min . '" />';
                $r_content  .= '<div class="clear"></div>';
            }
            // if checkbox "all" is checked
            if ($_GET['filter_id']=='') $allchecked='checked';
            else $allchecked='';


            if (tep_db_num_rows($manuf_sql) && empty($_GET['manufacturers_id'])) {
                unset($tempSeoFilterInfo);

                if (!empty($_GET['keywords']) || $allchecked){
                    $allFilterLink = '<span>'. FILTER_ALL. '</span>';
                }else{
                    $link = getFilterUrl($_GET['cPath'],'',$redirectOptionsIdsArrayForCheck);
                    $allFilterLink ='<a href="'.$link .'">'. FILTER_ALL . '</a>';
                }
                $r_content .= '<div class="attrib_divs ajax">
                          <div id="ajax_search_brands" class="block">
                            <div class="filter_heading">' . FILTER_BRAND . '</div>
                              <div class="inner-scroll">
                                <div class="item">
                                  <input class="filter_all" type="checkbox" id="brand_all" ' . $allchecked . ' name="filter_id[]" value="not" />
                                  <label for="brand_all">'.$allFilterLink.'</label>
                                </div>';


                foreach ($filterManufacturers as $manufacturers_values){

                    if ((!empty($_GET['filter_id']) && $_GET['filter_id'] == $manufacturers_values['manufacturers_id']) || empty($manufacturers_values['href']) || !empty($_GET['keywords'])){
                        $manufacturerLink = '<span>' . $manufacturers_values['manufacturers_name'] . $manufacturers_values['count'].'</span>';
                        $disabledInput = empty($manufacturers_values['href']) ||  !empty($_GET['keywords']) ? '' : ' disabled';

                    }else{
                        $disabledInput = '';
                        $manufacturerLink = '<a href="'.$manufacturers_values['href'].'">' . $manufacturers_values['manufacturers_name'] . $manufacturers_values['count'].'</a>';
                    }

                    $r_content .= '<div class="item">';
                    $r_content .= '<input type="checkbox"'.$disabledInput.' id="brand_' . $manufacturers_values['manufacturers_id'] . '" name="filter_id[]" ' . $manufacturers_values['check'] . ' value="' . $manufacturers_values['manufacturers_id'] . '" />
                                   <label for="brand_' . $manufacturers_values['manufacturers_id'] . '">'.$manufacturerLink.'</label>';
                    $r_content .= '</div>';

                }

                $r_content .= '</div></div></div>';
            }

            // Attribute Filter:
            $r_content .= '<div class="filter_cont" id="attribs" ><noindex>';

            // sort attributes values as natural

            if (is_array($attrs_array)) {
                foreach ($attrs_array as $at_id) {
                    if (is_array($show_in_filter) and in_array($at_id, $show_in_filter)) {
                        //                               if(!empty($counts_array_true[$at_id])) {
                        $r_content .= '<span class="filter_heading">' . $attr_names_array[$at_id] . '</span>';
                        $r_content .= '<div class="attrib_divs">';

                        // if checkbox "all" is checked
                        if ($_GET[$at_id]=='')
                            $allchecked='checked';else $allchecked='';

                        $allOptionsVal = $redirectOptionsIdsArrayForCheck;
                        if (isset($allOptionsVal[$at_id])) unset($allOptionsVal[$at_id]);
                        if ($checkFilterNext || $checkFilterRobots || isset($_GET['manufacturers_id']) || $allchecked){
                            $filterText = FILTER_ALL;
                        }else{
                            $filterText = '<a href="' . getFilterUrl(
                                    $_GET['cPath'],
                                    $_GET['filter_id'],
                                    $allOptionsVal
                                ) . '">' . FILTER_ALL . '</a>';
                        }
                        // output checkbox "all"
                        $r_content .= '<div class="item"><input class="filter_all" id="filter_all_' . $at_id . '" type="checkbox" ' . $allchecked . ' name="' . $at_id . '" value="not" />
                            <label for="filter_all_' . $at_id . '">
                                    '.$filterText.'
                            </label></div>';
                        // get all values for current option (attribute)
                        if (is_array($attr_vals_array[$at_id])) {
                            foreach ($attr_vals_array[$at_id] as $at_val_id => $at_val_name) {
                                // check if current attribute value is checked
                                $optionValueData = getOptionValueData($at_id,$at_val_id,$at_val_name);
                                $r_content.='<div class="item'.($optionValueData['count']?'':' pointer_events_none').'">
                                     <input id="' . $at_val_id . '2" type="checkbox" name="' . $at_id . '" ' . $optionValueData['checked'] . ' value="' . $at_val_id . '" />
                                     <label for="' . $at_val_id . '2">'.$optionValueData['text'].'</label>                                    
                                   </div>';
                                //}
                            }
                        }

                        $r_content .= '</div>';
                        //  }
                    }
                }
            }

            $r_content .= ' 
            </noindex>
          </div>';

            echo $r_content;
            ?>
        </div>
    </div>
    <?php
}
?>