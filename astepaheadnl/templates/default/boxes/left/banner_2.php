<!-- LEFT BANNER 2 -->
<?php

$banner = renderArticle($config['id']['val']?:'banner_2', true);
if ($banner) {
    echo '<div class="bn_sidebar">';

    if ($banner['articles_description']) {
        echo $banner['articles_description'];
    } elseif ($banner['articles_image']) {
        echo '<img class="img-responsive lazyload" alt="' . $banner['articles_image'] . '" src="images/pixel_trans.png" data-src="' . HTTP_SERVER . '/getimage/' . $banner['articles_image'] . '" />';
    }
    echo '</div>';
}

