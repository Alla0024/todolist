<!-- FOOTER ARTICLES -->
<div class="col-sm-4 col-xs-12">
    <div class="section_top_footer">
        <div class="h3"><?= FOOTER_ARTICLES; ?></div>
        <a href="#" class="toggle-xs" data-target="#footer_articles"></a>
        <nav class="list_footer" id="footer_articles">
            <ul>
                <?php
                // 13 - id for Articles

                $art_array = getArticles($config['id']['val'] ? $config['id']['val'] : 13, $config['limit']['val'] ? $config['limit']['val'] : 7);
                $all_articles_string = '';
                if (is_array($art_array)) {
                    foreach ($art_array as $articles) {
                        $all_articles_string .= '<li><a href="' . $articles['link'] . '">' . $articles['name'] . '</a></li>';
                    }
                    echo $all_articles_string;
                }
                ?>
            </ul>
            <?php
            $art_array = getArticles($config['id']['val'] ?: 13, 10);
            if (is_array($art_array) && count($art_array) > 7) {
                echo '<a href="' . tep_href_link(FILENAME_ARTICLES, 'tPath=13') . '">' . SHOW_ALL_ARTICLES . '</a>';
            }
            ?>
        </nav>
    </div>
</div>
<!-- END FOOTER ARTICLES -->