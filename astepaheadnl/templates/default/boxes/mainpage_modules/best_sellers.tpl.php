<?php
if (isset($_POST['render'])) {
    $config = $template->checkConfig('MAINPAGE', 'M_BEST_SELLERS');
}
$item_limit_mobile = $config['limit_mobile']['val'] ? : 3;
$item_limit = $config['limit']['val'] ? : 3;
$module_is_ajax = isset($config['ajax']['val']) && $config['ajax']['val'] == '1' ? true : false;
    $tpl_settings = array(
      'id'=>'best_sellers',
      'classes'=>($config['slider']['val']==1)?array('product_slider'):array('front_section'),
      'cols'=>$config['cols']['val'] ?: 4,
      'limit'=>isMobile()?$item_limit_mobile:$item_limit,
      'title'=>MAIN_BESTSELLERS,
    );
if (isset($_POST['render']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' || !$module_is_ajax) {
    include(DIR_WS_MODULES . 'best_sellers.php');
}
if ($module_is_ajax) {
    echo '<div data-module-id="' . $tpl_settings['id'] . '.tpl" class="ajax-module-box lazy-data-block"><span class="lazy-data-loader"></span></div>';
}
?>