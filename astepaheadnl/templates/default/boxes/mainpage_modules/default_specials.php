<?php
$module_name = 'default_specials';
if (isset($_POST['render'])) {
    $config = $template->checkConfig('MAINPAGE', 'M_DEFAULT_SPECIALS');
}
$item_limit_mobile = $config['limit_mobile']['val'] ? : 5;
$item_limit = $config['limit']['val'] ? : 10;
$module_is_ajax = isset($config['ajax']['val']) && $config['ajax']['val'] == '1' ? true : false;
    $tpl_settings = array(
      'id'=>'specials',
      'classes'=>($config['slider']['val']==1)?array('product_slider'):array('front_section'),
      'cols'=>$config['cols']['val'] ?: 4,
      'limit' => isMobile()?$item_limit_mobile:$item_limit,
      'title'=>sprintf(BOX_HEADING_DEFAULT_SPECIALS, tep_date_long_translate(strftime('%B'))),
    );

if (isset($_POST['render']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' || !$module_is_ajax) {
    include(DIR_WS_MODULES . 'default_specials.php');
}
if ($module_is_ajax) {
    echo '<div data-module-id="' . $module_name . '" class="ajax-module-box lazy-data-block"><span class="lazy-data-loader"></span></div>';
}?>