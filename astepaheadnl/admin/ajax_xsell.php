<?php

require_once('includes/application_top.php');

if ($_GET['action']=='show') {
    $sql="SELECT distinct
          `p`.`products_id` as `id`, 
          `p`.`products_model` as `model`,
          `pd`.`products_name` as `label`
        FROM " . TABLE_PRODUCTS . " `p`
        left join " . TABLE_PRODUCTS_DESCRIPTION . " `pd` on pd.products_id=p.products_id
        WHERE (`pd`.`products_name` LIKE '%{$_GET['xsell_model']}%' OR `p`.`products_model` LIKE '%{$_GET['xsell_model']}%') and  `pd`.`language_id` ='{$_SESSION['languages_id']}' and `p` . `products_id` not in ('{$_GET['exclude']}') and `p` . `products_id` != '{$_GET['pid']}'";

    $sql=tep_db_query($sql);
    while ($row=mysqli_fetch_assoc($sql)) {
        $result[]=$row;
    }
    echo json_encode($result);
    exit;

}elseif ($_GET['action']=='add' and $_GET['pid']!='' and $_GET['xsell_model']!='') {
    $result=tep_db_query("insert into " . TABLE_PRODUCTS_XSELL . " (products_id, xsell_id, sort_order) values (" . (int)$_GET['pid'] . ", " . $_GET['xsell_model'] . ",1)");
    if ($result) {
        $msg=array('msg'=>true);
    }else {
        $msg=array('msg'=>false);
    }
    echo json_encode($msg);
    exit;

}else if ($_GET['action']=='del') {
    if ($_GET['pid']!='' and $_GET['xsell_id']!='') {
        tep_db_query("delete from " . TABLE_PRODUCTS_XSELL . " where products_id = " . (int)$_GET['pid'] . " and xsell_id = " . (int)$_GET['xsell_id'] . " ");
    }
    exit;
}
else if ($_GET['action']=='add_discount') {
    if ($_GET['pid']!='' and $_GET['xsell_id']!='') {
        tep_db_query("UPDATE " . TABLE_PRODUCTS_XSELL ." set discount=".$_GET['val'] ." where products_id = " . (int)$_GET['pid'] . " and xsell_id = " . (int)$_GET['xsell_id']);
    }
    exit;
}
else if ($_GET['action']=='change_sort_order') {
    if ($_GET['xsell_id']!='') {
        tep_db_query("UPDATE " . TABLE_PRODUCTS_XSELL ." set sort_order=".$_GET['val'] ." where products_id = " . (int)$_GET['pid'] . " and xsell_id = " . (int)$_GET['xsell_id']);
    }
    exit;
}
else if ($_GET['action']=='add_rec_link') {
    if ($_GET['pid']!='' and $_GET['xsell_id']!='') {
        if($_GET['val']=='true'){
            tep_db_query("insert into " . TABLE_PRODUCTS_XSELL . " (products_id, xsell_id, sort_order) values (" . (int)$_GET['xsell_id'] . ", " . $_GET['pid'] . ",1)");
        }else{
            tep_db_query("delete from " . TABLE_PRODUCTS_XSELL . " where products_id = " . (int)$_GET['xsell_id'] . " and xsell_id = " . (int)$_GET['pid']);
        }
    }
    exit;
}
/*if ($_GET['action']=='add') {
    if ($_GET['pid']!='' and $_GET['xsell_model']!='') {

        $product_query=tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and p.products_model = '" . $_GET['xsell_model'] . "' ");
        $product=tep_db_fetch_array($product_query);

        $xsell_id=$product['products_id'];
        tep_db_query("insert into " . TABLE_PRODUCTS_XSELL . " (products_id, xsell_id, sort_order) values (" . (int)$_GET['pid'] . ", " . (int)$xsell_id . ",1)");

        echo $product['products_name'];
    }
}elseif ($_GET['action']=='del') {
    if ($_GET['pid']!='' and $_GET['xsell_id']!='') {
        tep_db_query("delete from " . TABLE_PRODUCTS_XSELL . " where products_id = " . (int)$_GET['pid'] . " and xsell_id = " . (int)$_GET['xsell_id'] . " ");
    }
}*/


?>