<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE_PRINT_ORDER . $oID; ?></title>
    <base href="<?php echo HTTP_SERVER . DIR_WS_CATALOG; ?>">
    <link rel="stylesheet" type="text/css" href="admin/includes/print.css">
</head>

<body marginwidth="10" marginheight="10" topmargin="10" bottommargin="10" leftmargin="10" rightmargin="10">

<style>
    @page {
        margin: 20px;
    }

    body {
        font-family: Arial;
    }

    table {
        width: 90%;
        margin: 20px auto !important;
        border-collapse: collapse;
        font-size: 12px;
    }

    .logo {
        width: 200px;
    }

    .products td {
        padding: 15px;
        text-align: center;
    }

    .products tr {
        border: 1px solid #f5f5f5;
    }
</style>

<table>
    <tbody>
    <tr>
        <td>
            <div class="logo"><img style="width:200px" src="<?= HTTP_SERVER . "/" . LOGO_IMAGE ?>"></div>
        </td>
        <td align="right">
            <div class="logo_text"
                 style="font-size: 30px;background-color: #1b302f;color: white;padding: 5px 10px;font-weight: 100;text-align: center;min-width: 250px; width: 250px; display: block"><?= DOCUMENT_TITLE; ?></div>
        </td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 115px;" valign="top"><?= ADDRESS_TITLE ?></td>
        <td style="width: 10px;" valign="top">:</td>
        <td><?= $order->customer["company"] ?><br>
        	<?= $order->customer["name"] ?><br>
            <?= $order->customer["street_address"] ?><br>
            <?= $order->customer["postcode"] . ' ' . $order->customer["city"] ?><br>
            <?= $order->customer["country"] ?></td>
    </tr>
    <tr>
        <td style="width: 115px;"><?= TEL_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $order->customer["telephone"]; ?></td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 115px;"><?= INVOICE_NUMBER_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $oID ?></td>

        <td style="width: 100px;"><?= INVOICE_DATE_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= date('d-m-y', strtotime($order->info['date_purchased'])) ?></td>
    </tr>
    <tr>
        <td style="width: 100px;"><?= INVOICE_DEBITEUR_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $customer_number["customers_id"] ?></td>
        
        <td style="width: 100px;"><?= CONDITION_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $order->info["payment_method"] ?></td>
    </tr>
    </tbody>
</table>

<table class="products">
    <thead>
    <tr style="background-color: #333;border: 1px solid #333;">
        <td style="color: #fff;font-weight: normal;padding: 5px; text-align:left;"><?php echo TABLE_HEADING_PRODUCTS_QUANTITY; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px; text-align:left;"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px; text-align:left;"><?php echo DESCRIPTION_TITLE; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo WEIGHT_TITLE; ?></td>
    </tr>
    </thead>
    <tbody>
    <?php
    $totalWight = 0;
    for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
        ?>
        <tr>
            <td><?php echo $order->products[$i]['qty'] ?></td>
            <td style="padding: 5px;text-align:left;"><?php echo $order->products[$i]['model'] ?></td>
            <td style="padding: 5px;text-align:left;"><?php echo $order->products[$i]['name'] ?></td>
            <td><?php echo $order->products[$i]["weight"] * $order->products[$i]['qty'] ?></td>
        </tr>
        <?php $totalWight += $order->products[$i]["weight"] * $order->products[$i]['qty'];
    }
    ?>
    </tbody>
</table>

<table style="20px 20px 20px 68px!important;">
    <tr>
        <td><?= UNILEVER_INFO_TITLE ?> :</td>
    </tr>
    <tr>
        <td style="width: 150px"><?= TOTAL_WEIGHT_TITLE ?></td>
        <td><?= $totalWight ?> KG</td>
        <td><?= FOR_AGREEMENT ?><br/><br/></td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 150px"><?= WAITING_PAYMENT_TITLE ?></td>
        <td><?= WAITING_PAYMENT ?></td>
        <td><?= NAME ?>:______________________________<br/><br/></td>
    </tr>
    <tr>
        <td style="width: 150px"><?= RECEIVE_PAYMENT_TITLE ?></td>
        <td><?= RECEIVE_PAYMENT ?></td>
        <td></td>
    </tr>
    <tr>
        <td style="width: 150px"><?= ULEVEN_TITLE ?></td>
        <td><?= ULEVEN ?></td>
        <td><?= SIGNATURE ?>:___________________________<br/><br/></td>
    </tr>
</table>

<div class="test"  style="display:block;position: absolute; bottom: 0; width: 95%">
    <table style="width: 90%">
        <tbody>
        <tr>
            <td><?= PAYMENT_CONDITION_TITLE ?> : <?= $order->info["payment_method"] ?></td>
        </tr>
        <tr>
            <td><?= PAYMENT_TERM_TITLE ?> : <?= PAYMENT_TERM ?></td>
        </tr>
        <tr>
            <td><?= CREDIT_LIMITATION_TITLE ?> : <?= CREDIT_LIMITATION ?></td>
        </tr>
        <tr>
            <td><?= COMPLAINTS ?></td>
        </tr>
        <tr>
            <td style="text-align: right"><?= INVOICE_CREATED ?></td>
        </tr>
        </tbody>
    </table>

    <table  style="width: 90%" class="footer">
        <tr style="background-color: #e53f42;">
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"></td>
            <td style="padding: 5px;"></td>
        </tr>
        <tr style="background-color: #0090b5;">
            <td style="padding: 10px;"></td>
            <td style="padding: 10px;"></td>
            <td style="padding: 10px;"></td>
            <td style="padding: 10px;"></td>
        </tr>
        <tbody>
        <tr>
            <td style="padding: 15px 15px 15px 0px; line-height: 20px;"><?= FOOTER_BLOCK1 ?></td>
            <td style="padding: 15px 15px 15px 0px; line-height: 20px;"><?= FOOTER_BLOCK2 ?></td>
            <td style="padding: 15px 15px 15px 0px; line-height: 20px;"><?= FOOTER_PHONE ?><br/>
                <?= FOOTER_EMAIL ?><br/>
                <?= FOOTER_SITE ?></td>
            <td style="padding: 15px 0px 15px 0px; line-height: 20px;"><?= FOOTER_CC ?><br/>
                <?= FOOTER_IBAN ?><br/>
                <?= FOOTER_BIC ?><br/>
                <?= FOOTER_VAT ?><br/></td>
        </tr>
        </tbody>
    </table>

    <br/>
    <br/>
    <br/>
</div>
</body>

</html>
