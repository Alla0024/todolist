<?php
require('includes/application_top.php');

use admin\includes\solomono\app\models\seoTemplates\seoTemplates;

if (isset($_GET['info']) && !empty($_GET['info'])) {
    $info = $_GET['info'];
} else {
    $info = seoTemplates::MAIN_PAGE;
}

function getTreeOption($arr, $depth = 0, $selected_id = false) {
    $html = '';
    foreach ($arr as $v) {
        if (is_array($selected_id)){
            $selected = in_array($v['id'], $selected_id) ? 'selected' : '';
        } else {
            $selected = $selected_id == $v['id'] ? 'selected' : '';
        }
        $html .= '<option ' . $selected . ' value="' . $v['id'] . '">';
        $html .= str_repeat('&nbsp;', $depth * 3);
        $html .= $v['name'] . '</option>';
        if (array_key_exists('childs', $v)) {
            $html .= getTreeOption($v['childs'], $depth + 1, $selected_id);
        }
    }
    return $html;
}

$filename = 'seoTemplates';

$seo_templates = new seoTemplates($info);

if (isset($_GET['ajax_load']) && $_GET['ajax_load'] == 'show') {
    $seo_templates->query($_GET);
    echo json_encode($seo_templates->data);
    exit;
}

if ($seo_templates->isAjax()) {

    $action = $_GET['action'];
    switch ($action) {
        case "edit_$filename":
        case "new_$filename":
            $id = $_GET['id'] ? $_GET['id'] : false;
            $seo_templates->selectOne($id);
            $categories_arr = $seo_templates->setTree();
            $selected_manufacturers = $seo_categories = explode(',',$seo_templates->data['data']['include_ids']);
            $seo_templates->data['option']['seo_categories_selected'] = getTreeOption($categories_arr, 0, $seo_categories);
            $seo_templates->data['option']['seo_manufacturers_selected'] = getTreeOption($seo_templates->getManufacturers(),0,$selected_manufacturers);
            $seo_templates->data['descriptions'] = $seo_templates->getDescriptions($id);

            $html = $seo_templates->getView("seoTemplates/form");
            echo json_encode(array('html' => $html));
            exit;
            break;
        case "insert_$filename":

            if ($seo_templates->insert($_POST)) {
                $arr = array(
                    'success' => true,
                    'msg' => TEXT_SAVE_DATA_OK,
                );
            } else {
                $arr = array(
                    'success' => false,
                    'msg' => TEXT_ERROR
                );
            }
            echo json_encode($arr);
            exit;
            break;
        case "update_$filename":
            if ($seo_templates->update($_POST)) {
                $arr = array(
                    'success' => true,
                    'msg' => TEXT_SAVE_DATA_OK,
                );
            } else {
                $arr = array(
                    'success' => false,
                    'msg' => TEXT_ERROR
                );
            }
            echo json_encode($arr);
            exit;
            break;
        case 'translate':
            /*id language*/
            $from=$_GET['from']?:1;
            $to=$_GET['to']?:3;

            $arr=$seo_templates->yandexTranslate($_POST,$from,$to);
            echo json_encode($arr);
            exit;
            break;
    }
    $action = $_POST['action'];
    switch ($action) {
        case "delete_$filename":
            if ($seo_templates->delete($_POST['id'])) {
                $arr = array(
                    'success' => true,
                    'msg' => TEXT_SAVE_DATA_OK
                );
            } else {
                $arr = array(
                    'success' => false,
                    'msg' => TEXT_ERROR
                );
            }
            echo json_encode($arr);
            exit;
            break;
    }
    if(isset($_POST['status'])){
        if($seo_templates->statusUpdate($_POST['status'],$_POST['id'],$field = 'status', $table = 'seo_templates')){
            $array = array(
                'success' => true,
                'msg' => TEXT_SAVE_DATA_OK,
            );
        }else{
            $array = array(
                'success' => false,
                'msg' => TEXT_ERROR
            );
        }
        echo json_encode($array);
        exit;
    }
}

include_once('html-open.php');
include_once('header.php');

$tabs = [
    seoTemplates::MAIN_PAGE => META_TAGS_MAINPAGE_TITLE_SEO,
    seoTemplates::PRODUCT => META_TAGS_PRODUCT_TITLE_SEO,
    seoTemplates::CATEGORY => META_TAGS_CATEGORY_TITLE_SEO,
    seoTemplates::MANUFACTURER => META_TAGS_MANUFACTURER_TITLE_SEO,
    seoTemplates::SEARCH => META_TAGS_SEARCH_TITLE_SEO,
];

?>
<div class="container">
    <div class="nav_table">
        <?php foreach ($tabs as $page => $pageTranslate):?>
            <a data-template="<?= $page ?>" href="?info=<?= $page ?>"
               class="<?= $pageTranslate === $info ? 'active' : '' ?> link ">
                <?= $pageTranslate ?>
            </a>
        <?php endforeach; ?>
    </div>
    <script>
        var lang=<?php echo $seo_templates->getTranslation();?>;
    </script>
    <div class="container">
        <?php echo $seo_templates->getView();?>
    </div>
</div>
<script>
    function copyToContext(copyText) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(copyText).select();
        document.execCommand("copy");
        $temp.remove();
        show_tooltip("<?= TOOLTIP_TEXT_COPIED ?>:\n\r " + copyText, 2000, $('body'), false);
    }
</script>
<?php
include_once('footer.php');
include_once('html-close.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
// end of file
?>