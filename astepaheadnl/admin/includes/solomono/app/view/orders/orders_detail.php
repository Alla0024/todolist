<?php
//debug($data);
//debug($action);
//exit;
require(DIR_WS_CLASSES . 'currencies.php');
$currencies=new currencies();
?>
<?php $action_form = (!empty($data['data'])) ? "update_$action" : "insert_$action"; ?>

<div class="container modal_order_details">
    <div class="row">
        <div class="col-md-3">
            <h3><?php echo ENTRY_INFO; ?></h3>
            <?php foreach ($data['data']['INFO'] as $field => $v):
                echo $v ? '<p><b>' . $field . ':</b> ' . $v . '</p>' : '';
            endforeach; ?>
            <p><a href="/admin/edit_orders.php?oID=<?= $data['order_id']; ?>">edit order</a></b></p>
        </div>
        <div class="col-md-3">
            <h3><?php echo ENTRY_CUSTOMERS; ?></h3>
            <?php echo tep_address_format($data['data']['CUSTOMERS']['address_format_id'], $data['data']['CUSTOMERS'], 1, '', '<br>'); ?>
        </div>
        <div class="col-md-3">
            <h3><?php echo ENTRY_DELIVERY; ?></h3>
            <?php echo tep_address_format($data['data']['DELIVERY']['address_format_id'], $data['data']['DELIVERY'], 1, '', '<br>'); ?>
        </div>
        <div class="col-md-3">
            <h3><?php echo ENTRY_BILLING; ?></h3>
            <?php echo tep_address_format($data['data']['BILLING']['address_format_id'], $data['data']['BILLING'], 1, '', '<br>'); ?>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped <?php echo $action; ?>">
            <thead>
            <tr>
                <?php if (count($data['products'])): ?>
                    <?php foreach (current($data['products']) as $field => $v):  if ((isset($data['allowed_fields'][$field]['show']) && $data['allowed_fields'][$field]['show'] == false)): ?>
<!--                        --><?php //if($field == "orders_products_id") continue; ?>
<!--                        --><?php //if($field == "products_attr") continue; ?>
                        <th><?php echo $data['allowed_fields'][$field]['label'] ?: $field; ?></th>
                    <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['products'] as $k => $arr): ?>
                <tr>
                    <?php foreach ($arr as $field=>$v): if ((isset($data['allowed_fields'][$field]['show']) && $data['allowed_fields'][$field]['show'] == false)): ?>
                        <?php
//                            if($field == "orders_products_id") continue;
                            $products_attrs = "";
                            foreach ($arr['products_attr'] as $arrt) {
                                $products_attrs .= "<br><span class='attrs_product'><strong>".$arrt['products_options'].": </strong>".$arrt['products_options_values']."</span>";
                            }
//                            if($field == "products_attr") continue;
                        ?>
                        <td data-label="<?php echo $data['allowed_fields'][$field]['label'] ?: $field; ?>">
                            <?php
                                if ($field=='products_name') echo '<a target="_blank" href="/product_info.php?products_id='.$arr['products_id'].'">'.$v.'</a>'.$products_attrs;
                                elseif ($field=='products_price' or $field=='final_price') echo $currencies->format($v,true,$data['data']['INFO']['currency'],$data['data']['INFO']['currency_value']);
                                else echo $v;
                            ?>
                        </td>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <?php for ($i = 0; $i < count($data['orders_total']); $i++): ?>
            <p class="text-right"><?php echo $data['orders_total'][$i]['title'] . $data['orders_total'][$i]['text']; ?></p>
        <?php endfor; ?>
    </div>
    <div class="row form-group text-right">
<!--        <button type="button" class="btn btn-default send_confirmation" data-dismiss="modal" data-id="--><?php //echo $data['order_id'] ?><!--">Send confirmation</button>-->
        <a target="_blank" href="invoice.php?oID=<?php echo $data['order_id'] ?>&pdf=true"
           class="btn btn-default"><?php echo IMAGE_ORDERS_INVOICE ?></a>
        <a target="_blank" href="packingslip.php?oID=<?php echo $data['order_id'] ?>&pdf=true"
           class="btn btn-default"><?php echo IMAGE_ORDERS_PACKINGSLIP ?></a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
    </div>
</div>