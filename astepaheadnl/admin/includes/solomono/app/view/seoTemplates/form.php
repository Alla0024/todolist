<?php
//debug($data);
//debug($action);
$languages = tep_get_languages();
?>

<?php $action_form = (!empty($data['data'])) ? "update_$action" : "insert_$action"; ?>

<form class="form-horizontal <?php echo $action ?>"
      action="<?php echo ($_SERVER['SCRIPT_URL'] ?: $_SERVER['SCRIPT_NAME']) . '?action=' . $action_form; ?>"
      method="post" enctype="multipart/form-data">
    <?php if (!empty($data['data'])): ?>
        <input type="hidden" name="id" value="<?php echo $data['data']['id'] ?>">
    <?php endif; ?>
    <div class="col-md-12">
        <?php
        foreach ($data['allowed_fields'] as $field_name => $option) {

            if (!isset($option['type']) || $option['hideInForm'] === true) {
                continue;
            }
            if ($option['type'] == "select") {
                if (isset($option['option']['id']) && isset($data['data'][$option['option']['id']])) {
                    $val = $data['data'][$option['option']['id']];
                } else {
                    $val = $data['data'][$field_name];
                }
            } else {
                $val = (!empty($data['data'][$field_name])) ? $data['data'][$field_name] : '';
            }
            require('renderInput.php');
        }
        ?>
    </div>
    <div class="col-md-12">
        <ul class="nav nav-tabs">
            <?php foreach ($languages as $k => $v): ?>
                <li class="<?= (!$k ? 'active' : ''); ?>">
                    <a data-toggle="tab" href="#<?= $v['code'] ?>">
                        <input type="image" src="/includes/languages/<?= $v['directory'] ?>/images/icon.png" border="0"
                               title="<?= $v['name'] ?>">
                    </a>

                </li>
            <?php endforeach; ?>
        </ul>
        <div class="tab-content">
            <?php foreach ($languages as $k => $v): ?>
                <div id="<?= $v['code'] ?>" class=" col-md-8 form-group tab-pane <?= (!$k ? 'active' : ''); ?>">
                    <input class="form-control" placeholder="<?= TABLE_HEADING_SEO_META_TITLE; ?>" type="text"
                           name="seo_meta_title[<?= $v['id'] ?>]" size="255" value="<?= $data['descriptions'][$v['id']]['meta_title'] ?>">
                    <input class="form-control" placeholder="<?= TABLE_HEADING_SEO_META_DESCRIPTION; ?>" type="text"
                           name="seo_meta_description[<?= $v['id'] ?>]" size="255" value="<?= $data['descriptions'][$v['id']]['meta_description'] ?>">
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="col-md-12">
        <?php
        include_once '_examples.php';
        ?>
    </div>
    <div class="form-group text-right">
        <div class="col-sm-12">
            <input type="submit" value="OK" class="btn">
            <button type="button" class="btn btn-info saveInputData"><?php echo TEXT_MODAL_APPLY_ACTION ?></button>
            <button type="button" class="btn btn-default"
                    data-dismiss="modal"><?php echo TEXT_MODAL_CANCEL_ACTION ?></button>
        </div>
    </div>
</form>