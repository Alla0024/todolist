<?php
/**
 * Created by PhpStorm.
 * User: ILIYA
 * Date: 14.06.2017
 * Time: 15:22
 */

namespace admin\includes\solomono\app\models\manufacturers;

use admin\includes\solomono\app\core\Model;

include_once(DIR_ROOT . '/' . DIR_WS_CLASSES . 'seo.class.php');

class manufacturers extends Model {

    protected $allowed_fields=[
        'manufacturers_name'=>[
            'label'=>TEXT_MANUFACTURERS_NAME,
            'general'=>'text',
            'sort'=>true,
            'filter'=>true
        ],
        'manufacturers_image'=>[
            'label'=>TEXT_MANUFACTURERS_IMAGE,
            'general'=>'file',
        ],
        'sort'=>[
            'label'=>TEXT_MANUFACTURERS_SORT,
            'general'=>'number',
            'show'=>false,
        ],
        'manufacturers_seo_url'=>[
            'label'=>'Seo url',
            'general'=>'text',
            'show'=>false,
        ],
        'manufacturers_url'=>[
            'label'=>TABLE_HEADING_DESC,
            'type'=>'textarea',
            'show'=>false,
            'ckeditor' => true,
        ],
        'keywords'=>[
            'label'=>TABLE_HEADING_META_KEYWORDS,
            'type'=>'text',
            'show'=>false,
        ],
        'description'=>[
            'label'=>TABLE_HEADING_META_DESC,
            'type'=>'text',
            'show'=>false
        ],
        'title'=>[
            'label'=>TABLE_HEADING_META_TITLE,
            'type'=>'text',
            'show'=>false,
        ],
        'status'=>[
            'label'=>TABLE_HEADING_STATUS,
            'type'=>'status',
            'hideInForm'=>true
        ],
        'h1_manufacturer' => [
            'label' => TEXT_MANUFACTURERS_H1,
            'type' => 'text',
            'show' => false,
        ],
        'seo_text_top' => [
            'label' => TEXT_MANUFACTURERS_SEO_TOP,
            'type' => 'textarea',
            'show' => false,
            'ckeditor' => true,
        ],
        'date_added'=>[
            'label'=>TEXT_DATE_ADDED,
            'general'=>'disabled',
            'show'=>false,
        ],
        'last_modified'=>[
            'label'=>TEXT_LAST_MODIFIED,
            'general'=>'disabled',
            'show'=>false,
        ]
    ];

    private $seoUrl;
    protected $prefix_id='manufacturers_id';

    public function select($id=false) {
        $sql="SELECT
                  `m`.`manufacturers_id` as id,
                  `m`.`manufacturers_name`,
                  `m`.`manufacturers_seo_url`,
                  `m`.`manufacturers_image`,
                  `m`.`date_added`,
                  `m`.`last_modified`,
                  `m`.`status`,
                  `m`.`sort`,
                  `mi`.`languages_id`,
                  `mi`.`manufacturers_url`,
                  `mi`.`h1_manufacturer`,
                  `mi`.`seo_text_top`,
                   mt.keywords,
                   mt.description,
                   mt.title
                FROM `manufacturers` `m`
                  LEFT JOIN `manufacturers_info` `mi` ON `mi`.`manufacturers_id` = `m`.`manufacturers_id` 
                  LEFT JOIN `meta_tags` `mt` ON `mt`.`manufacturers_id` = `m`.`manufacturers_id` and  language_id = `mi`.`languages_id`
                  ";
        if ($id) {
            return $sql . " WHERE `m`.`manufacturers_id` = {$id}";
        }
        $sql.=" WHERE `mi`.`languages_id`='{$this->language_id}'";
        return $sql;
    }

    public function selectOne($id) {
        $sql=$this->select($id);
        if ($id) {
            $this->data['data']=$this->getResultKey($sql, 'languages_id');
        }
        $this->getLanguages();
    }

    public function update($data) {
        $this->seoUrl();
        $data['sort'] = (int) $data['sort'];
        $data['manufacturers_seo_url']=$this->seoUrl->strip($data['manufacturers_seo_url']);

        $meta_data = [];
        foreach ($data as $k => $v){
            if($k == 'keywords' || $k == 'description' || $k == 'title'){
                $meta_data[$k] = $v;
                unset($data[$k]);
            }
        }

        $id=$data['id'];
        unset($data['id']);

        $this->InsertUpdate($meta_data, $id, __FUNCTION__, 'language_id', 'meta_tags');

        $query=$this->prepareGeneralField($data);
        if ($this->InsertUpdate($data, $id, __FUNCTION__, 'languages_id', 'manufacturers_info')) {
            $sql="INSERT INTO `{$this->table}` SET {$query},`$this->prefix_id`='{$id}',`last_modified`=now() ON DUPLICATE KEY UPDATE {$query},`$this->prefix_id`='{$id}',`last_modified`=now()";
            if (!tep_db_query($sql)) {
                return false;
            }
        }
        return true;
    }

    public function insert($data) {
        $this->seoUrl();
        $data['sort'] = (int) $data['sort'];
        $data['manufacturers_seo_url']=$this->seoUrl->strip($data['manufacturers_seo_url']);

        $id=mysqli_fetch_assoc(tep_db_query("select max({$this->prefix_id})+1 as next_id from `{$this->table}`"))['next_id']?:1;

        $meta_data = [];
        foreach ($data as $k => $v){
            if($k == 'keywords' || $k == 'description' || $k == 'title'){
                $meta_data[$k] = $v;
                unset($data[$k]);
            }
        }
        $this->InsertUpdate($meta_data, $id, __FUNCTION__, 'language_id', 'meta_tags');
        
        $query=$this->prepareGeneralField($data);
        if ($this->InsertUpdate($data, $id, __FUNCTION__, 'languages_id', 'manufacturers_info')) {
            $sql="INSERT INTO `{$this->table}` SET {$query},`$this->prefix_id`='{$id}',`date_added`=now()";
            if (!tep_db_query($sql)) {
                return false;
            }
        }
        return true;
    }

    private function seoUrl() {
        $seo_urls=new \SEO_URL($this->language_id);
        $this->seoUrl=$seo_urls;
    }

    private function InsertUpdate($data, $id, $action, $lang='language_id', $table=null) {

        $table=$table ? : $this->table;
        $this->getLanguages();
        $languages=$this->data['languages'];

        foreach ($languages as $k=>$v) {
            $query='';
            foreach ($data as $key=>$value) {
                $value=tep_db_prepare_input($value[$k]);
                $query.="`{$key}` = " . '\'' . tep_db_input($value) . '\', ';
            }
            $query.="`{$this->prefix_id}`= {$id}, `{$lang}`={$k}";

            if ($action=='update') {
                $sql="INSERT INTO `{$table}` SET $query ON DUPLICATE KEY UPDATE {$query}";
            }elseif ($action=='insert') {
                $sql="INSERT INTO `{$table}` SET $query";
            }
            if (!tep_db_query($sql)) {
                return false;
            }
        }
        return true;
    }

    public function delete($id) {
        $this->delFile($id, 'manufacturers_image');
        if (tep_db_query("DELETE FROM {$this->table} WHERE `{$this->prefix_id}`={$id}")) {
            tep_db_query("DELETE FROM `meta_tags` WHERE `{$this->prefix_id}`={$id}");
            return tep_db_query("DELETE FROM `manufacturers_info` WHERE `{$this->prefix_id}`={$id}");
        }
        return false;
    }

}