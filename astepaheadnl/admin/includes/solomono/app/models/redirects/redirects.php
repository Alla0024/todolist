<?php

namespace admin\includes\solomono\app\models\redirects;

use admin\includes\solomono\app\core\Model;

class redirects extends Model{
    protected $allowed_fields = [
        'redirect_from' => [
            'label' => TEXT_REDIRECT_FROM,
            'type' => 'text',
        ],
        'redirect_to' => [
            'label' => TEXT_REDIRECT_TO,
            'type' => 'text',
        ],
        'status' => [
            'label' => TABLE_HEADING_STATUS,
        ],
        'date_added' => [
            'label' => TEXT_DATE_ADDED,
            'general' => 'disabled',

        ],
        'date_updated' => [
            'label' => TEXT_DATE_UPDATED,

        ]
    ];
    protected $prefix_id='id';
    public function select($id=false) {
        $sql = "SELECT DISTINCT 
                  `r`.`id`,
                  `r`.`redirect_from`,
                  `r`.`redirect_to`,     
                  `r`.`status`,
                  `r`.`date_added`,
                  `r`.`date_updated`
                FROM `redirects` `r`
             ";
        if ($id) {
            $sql.=" WHERE  `r`.id ='{$id}'";
        }
        return $sql;
    }
    public function selectOne($id) {
        if ($id) {
            $sql=$this->select($id);
            $this->data['data']=$this->getResult($sql)[0];
        }
    }

    public function update($data) {
        $id = $data['id'];
        unset($data['id']);


        $redirects_query = $this->prepareGeneralField($data);
        $sql = "UPDATE `redirects` SET `date_updated`=now(),{$redirects_query} WHERE {$this->prefix_id}={$id} ";
            if (!tep_db_query($sql)) {
                return false;
            }

        return true;
    }
}
