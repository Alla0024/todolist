<?php
/**
 * Created by PhpStorm.
 * User: ILIYA
 * Date: 14.06.2017
 * Time: 15:22
 */

namespace admin\includes\solomono\app\models\specials;

use admin\includes\solomono\app\core\Model;

class products extends Model {

    protected $allowed_fields = [
        'products_model' => [
            'label' => TABLE_HEADING_MODEL,
            'sort' =>true
        ],
        'products_name' => [
            'label' => TABLE_HEADING_NAME,
            'sort' =>true
        ],
        'products_price' => [
            'label' => TABLE_HEADING_PRICE,
            'sort' =>true
        ],
    ];
    protected $prefix_id = 'products_id';

    public function select($id=false,$in=false) {
        $sql="SELECT
                      p.products_id as id,
                      p.products_model,
                      p.products_price,
                      pd.products_name,
                      p.manufacturers_id,
                      p.products_parent_category,
                      ptc.categories_id,
                      cd.categories_name
                    FROM
                      products p
                      LEFT JOIN
                      products_description pd
                        ON pd.products_id = p.products_id
                      LEFT JOIN
                      products_to_categories ptc
                        ON ptc.products_id = p.products_id
                      LEFT JOIN
                      categories_description cd
                        ON cd.categories_id = ptc.categories_id";

        $sql.=$id ? " WHERE p.products_parent_category = {$id} ":'';
        $sql.=$in ? " OR ptc.categories_id IN ({$in}) ":'';
        return $sql;
    }

}