<?php
/**
 * Created by PhpStorm.
 * User: ILIYA
 * Date: 14.06.2017
 * Time: 15:22
 */

namespace admin\includes\solomono\app\models\mysqlperformance;

use admin\includes\solomono\app\core\Model;

include_once(DIR_ROOT . '/' . DIR_WS_CLASSES . 'seo.class.php');

class mysqlperformance extends Model {

    protected $allowed_fields=[
        '4'=>[
            'label'=>TABLE_HEADING_NUMBER,
            'general'=>'text',
            'thWidth' => '50'
        ],
        '0'=>[
            'label'=>TABLE_HEADING_QUERY,
            'general'=>'text',
        ],
        '1'=>[
            'label'=>TABLE_HEADING_QLOCATION,
            'general'=>'text',
            'thWidth' => '500'
        ],
        '2'=>[
            'label'=>TABLE_HEADING_QUERY_TIME,
            'general'=>'text',
            'thWidth' => '130'
        ],
        '3'=>[
            'label'=>TABLE_HEADING_DATE_CREATED,
            'general'=>'disabled',
            'thWidth' => '120'
        ],
    ];

    protected $prefix_id='manufacturers_id';

    public function select() {
        $sql="SELECT
                  `m`.`manufacturers_id` as id,
                  `m`.`manufacturers_name`,
                  `m`.`manufacturers_countries`,
                  `m`.`manufacturers_image`,
                  `m`.`status`
                FROM `manufacturers` `m`";
        return $sql;
    }

    public function query($request) {
        // File location (URL or server path)
        $FilePath = DIR_FS_CATALOG.'includes/slow_queries/slow_query_log.txt';
        // This will be a two dimensional array that holds the content nicely organized
        $Data = array();
        // We will use this as an index
        $performance_numrows = 0;
        if (file_exists($FilePath)) {
            // Stores the content of the file
            $Lines = @file($FilePath);
            if ($Lines){
                // Counts the number of lines
                $LineCount = count($Lines);
                // Loop through each line
                foreach($Lines as $Value){
                    // In the array store this line with values delimited by \t (tab) as separate array values
                    $Data[$performance_numrows] = explode("\t", $Value);
                    array_unshift($Data[$performance_numrows],$performance_numrows);
                    $Data[$performance_numrows]['id'] = $performance_numrows;
                    // Increase the line index
                    $performance_numrows++;
                }
                $performance_numrows = $LineCount;
            }
        }
        $recordsTotal = $request['count'] ?: $performance_numrows;
        $this->paginate($recordsTotal, $request['page'], $request['perPage']);

        $offset = ($request['page'] - 1) * $request['perPage'];
        $this->data['data'] = array_slice($Data, $offset, $request['perPage']);
    }

    private function getFileData(){

    }



    public function delete($id) {
        if (tep_db_query("DELETE FROM {$this->table} WHERE `{$this->prefix_id}`={$id}")) {
            $this->delFile($id, 'manufacturers_image');
            return tep_db_query("DELETE FROM `manufacturers_info` WHERE `{$this->prefix_id}`={$id}");
        }
        return false;
    }

}