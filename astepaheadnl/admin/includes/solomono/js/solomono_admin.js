var option = getGetParams();
var timeout = null;
var widthOfModal = null;
$(document).load(function () {

});
$(document).ready(function () {
    $('body').on('mouseenter', '.fa-question-circle', function () {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body',
            html:true,
            template: '<div class="left_m_tooltip tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
        });
    });


    //custom panel
    $(document).mouseup(function (e){
        var collapse_block = $('.custom_panel_block .collapse_li');
        // console.log(collapse_block)
        if (!collapse_block.is(e.target) && collapse_block.has(e.target).length === 0) {
            collapse_block.find('.drop_menu.in').collapse('hide');
            collapse_block.find('.collapse_btn').addClass('collapsed');
        }
    });

    $(document).on('click', '.open_custom_panel_btn', function () {
        if($('.custom_panel_block').hasClass('visible')) {
            $('.custom_panel_fader').removeClass('visible');
            $('.custom_panel_block .collapse_btn').addClass('collapsed');
            $('.custom_panel_block .drop_menu.in').collapse('hide');
            $('.custom_panel_block').removeClass('visible');
            $('.open_custom_panel_btn').addClass('anim');
        } else {
            $('.custom_panel_block').addClass('visible');
            $('.open_custom_panel_btn').removeClass('anim');
        }
    });
    $(document).on('click', '.custom_panel_close', function () {
        $('.custom_panel_block').removeClass('visible');
        $('.custom_panel_fader').removeClass('visible');
        $('.custom_panel_block .collapse_btn').addClass('collapsed');
        $('.custom_panel_block .drop_menu.in').collapse('hide');
        $('.open_custom_panel_btn').addClass('anim');
    });

    $('#change_pass').on('click', function (e) {
        e.preventDefault();
        $.get($(this).attr('href'), function (response) {
            modal({
                title: response.title,
                body: response.html,
                after: function (modal) {
                    var form = $(modal).find('form');
                    form.on('click', 'input[type="submit"]', function (e) {
                        var id = form.find('input[name="id"]').val();
                        e.preventDefault();
                        var formData = new FormData(form.get(0));
                        $.ajax({
                            url: form.attr('action'),
                            type: form.attr('method'),
                            dataType: 'json',
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                $(modal).modal('hide');
                                show_tooltip(response['msg'], 9999999,$('body'),true);
                            }
                        });
                    })
                }
            })
        }, "json");
    })

    $('body').on('focus', '.datepicker', function () {
        $(this).datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });
    });

    // Saves block state in database
    $(document).on("click", ".collapse_link", function() {
        var url = window.location.origin + "/admin/ajax_hide_block.php";
        $.ajax({
            url: url,
            type: "POST",
            data: {hide_block_id: this.hash, hide_block_value: $(this).hasClass("collapsed")?"hide":"show"}
        });
    });

    //---for all---//
    $('body').on('click', '#own_table .edit_row,#add', getForm);

    $('body').on('click', '#own_table .del_link', function () {
        var param = {
            msg: lang.all.TEXT_MODAL_CONFIRMATION_ACTION
        };
        var param_update = {};
        var $this = $(this), id = $this.closest('tr').data('id'), action = $this.data('action');

        if ($this.data('ajax')) {
            $.ajax({
                url: window.location.pathname,
                type: 'POST',
                data: {id: id, action: 'check'},
                success: function (data) {
                    console.log(data);
                    var params = JSON.parse(data);
                    if (params.msg) {
                        param_update = {
                            msg: params.msg
                        }
                    }
                },
                dataType: "json",
                async: false
            });
        }


        $.extend(param, param_update)

        var body = '<form>';
        body += '<p>' + param.msg + '</p>';
        body += '<input type="hidden" name="id" value="' + id + '">';
        if (action == 'delete_articles' && option.tPath !== undefined) {
            body += '<input type="hidden" name="tPath" value="' + option.tPath + '">';
        }
        body += '<input type="hidden" name="action" value="' + action + '">';
        if ($this.data('input')) {
            var matches = $this.data('input').match(/(.+)_(.+)/);
            body += '<p>' + lang.currentPage.TEXT_INFO_RESTOCK_PRODUCT_QUANTITY + '</p>';
            body += '<p><input type="' + matches[1] + '" name="' + matches[2] + '"></p>';
        }
        body += '<button type="button" class="btn btn-default" data-dismiss="modal">' + lang.all.TEXT_MODAL_CANCEL_ACTION + '</button>';
        body += '<button type="submit" class="btn btn-danger btn-confirm">OK</button>';
        body += '</form>';
        modal({
            id: 'confirm-delete',
            title: lang.all.TEXT_MODAL_CONFIRM_ACTION,
            body: body,
            width: '50%',
            after: function (modal) {
                $(modal).on('click', 'button.btn-confirm', function (e) {
                    e.preventDefault();
                    var form = $(modal).find('form');
                    var data = form.serializeArray();
                    $.post(window.location.pathname, data, function (msg) {
                        if (msg['success']) {
                            if ($this.closest('tbody').find('tr').length == 1) {
                                delete(option.count);
                                if (option.page == 1) {
                                    option.page = 1;
                                } else {
                                    option.page = option.page - 1;
                                }
                            }
                            $this.closest('tr').remove();
                            $('#own_pagination').pagination('selectPage', option.page);
                        }
                        $(modal).modal('hide');
                        var time = msg['time'] || 1500;
                        show_tooltip(msg['msg'], time);
                    }, "json");
                })
            }
        });
    });

    //---- Sidebar ----- //
    var boxWidth = $('.app-header-fixed #aside .aside-wrap').width();
    if(boxWidth > 55 && boxWidth < 65 ){
        triggerMenuItem('close');
    }

    $('#close-menu').on('click',function(e){
        $('nav.navi>ul>li>a').show();
        $('nav.navi>ul>li>ul>li').show();
        $('nav.navi>ul>li>ul').removeAttr('style');
        e.preventDefault();
        collapsLeftMenu();
    });
    var el = $('#aside div:first');
    $('#open-menu,nav ul li.item-menu').on('click',function(e){
        $('#close-menu').show();
        $('#open-menu').hide();
        if(el.hasClass('collapse-menu')){
            el.removeClass('collapse-menu');
        }
        if($(this).is('#open-menu')){
            triggerMenuItem('open');
        }else{
            $('#aside ul.nav-sub').find('li._active').attr('class', 'active');
            $('#aside ul.nav').find('li._active').attr('class', '');
        }

        $('.menu-form div.menu_search_form').css({'display':'inline-block'});
        $('.navi ul.nav li a .new_badge').css({'right':'14px','top':'14px','background':'#424756','width':'25px','height':'19px'});
        if($(window).width()<= 1499){
            $('.app-header-fixed #aside .aside-wrap').addClass('min-collapse-menu');
        };
        $('body').removeClass('wrap-collapse-menu');
        setTooltipForMenu();
        $.get('ajax_set_menu_location.php?action=set_menu_location&value=1');
    });

    setTooltipForMenu();
    //--------------------------//

    $('select#per_page option[value="' + option.perPage + '"]').prop("selected", true);
    $('select#manufacturers option[value="' + option.manufacturersId + '"]').prop("selected", true);
    $('select#category option[value="' + option.categoryId + '"]').prop("selected", true);
    $('#specials').prop("checked", (option.onlySpecials == 'yes') ? true : false);

    $('#manufacturers').on('change', function () {
        option.manufacturersId = $(this).val();
        option.page = 1;
        $('#own_pagination').pagination('selectPage', option.page);
    });

    $('#specials').on('change', function () {
        option.onlySpecials = $(this).is(':checked') ? 'yes' : 'no';
        option.page = 1;
        $('#own_pagination').pagination('selectPage', option.page);
    });

    $('body').on('click', '#own_table >thead>tr>th>i.fa', function () {
        var $this = $(this);
        $this.removeClass('fa-sort');
        if ($this.hasClass("fa-sort-desc")) {
            $this.removeClass('fa-sort-desc').addClass('fa-sort-asc');
        } else if ($this.hasClass("fa-sort-asc")) {
            $this.removeClass('fa-sort-asc').addClass('fa-sort');
        } else {
            $this.removeClass('fa-sort-asc').addClass('fa-sort-desc');
        }
        field = $this.parent('th').data('table');
        sort = $this.attr('class').match(/sort-(asc|desc)/);
        if (sort) {
            option.order = field + '-' + sort[1];
        } else {
            delete(option.order);
        }
        option.page = 1;
        $('#own_pagination').pagination('selectPage', option.page);
    });

    $('body').on('click', '.quick_updates_table th i.fa', function () {
        var $this = $(this);
        $this.removeClass('fa-sort');

        if ($this.hasClass("fa-sort-desc")) {
            $this.removeClass('fa-sort-desc').addClass('fa-sort-asc');
        } else if ($this.hasClass("fa-sort-asc")) {
            $this.removeClass('fa-sort-asc').addClass('fa-sort');
        } else {
            $this.removeClass('fa-sort-asc').addClass('fa-sort-desc');
        }
    });


    var locseach = location.search.substr(1);
    var href = new URLSearchParams(location.href);
    var show_sort = locseach.match('sort_by');
    if (show_sort) {
        var correct_class = href.get('sort_by').split(' ').shift();
        correct_class = correct_class.replace(".", "_");

        if (locseach.match(/(desc)/)){
            $('.quick_updates_table .' + correct_class + ' a').removeClass('active');
            $('.quick_updates_table .' + correct_class + ' a.sort_asc').addClass('active');
        } else if (locseach.match(/(asc)/)) {
            $('.quick_updates_table .' + correct_class + ' a').removeClass('active');
            $('.quick_updates_table .' + correct_class + ' a.sort_default').addClass('active');
        }
    } else {
        $('.quick_updates_table th a').removeClass('active');
        $('.sort_desc').addClass('active');
    }



    $('body').on('change', '#own_table .status input', function () {
        var data = {};
        var id = $(this).attr('id').match(/[0-9]+/)[0];
        var field = $(this).parents('td').attr('data-name');
        var status = $(this).prop('checked');
        if (!checkAllStatus($(this), status)) return;
        if (status == true) {
            status = 1;
        } else {
            status = 0;
        }
        data[field] = status;
        data['status'] = status;
        data['field'] = field;
        data['id'] = id;
        $.ajax({
            url: window.location.href,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (response) {
                show_tooltip(response.msg, 500);
            }
        });
    });

    if ($('#own_pagination').length != 0) {

        renderData('show');

        $('#own_pagination').pagination('selectPage', option.page);
    }

    $('body').on('click', '#debug>div >h3', function () {
        $(this).next().slideToggle(400);
    });
    $('body').on('click', '#debug i.fa-bug', function () {
        $(this).parent().toggleClass('active');
        $(this).toggleClass('active').next().slideToggle(300);
    });

    $('body').on('change', 'select#per_page', function () {
        option.perPage = $(this).val();
        option.page = 1;
        delete(option.count);
        $('#own_pagination').pagination('selectPage', option.page);
    });


    $('body').on('input', '#own_table >thead>tr>th>input.search', function () {
        filterTable($(this).parents('thead'));
    });

    $('body').on('change', '#own_table >thead>tr>th>select.search', function () {
        filterTable($(this).parents('thead'));
    });

    $('body').on('mouseenter', '#own_table', function () {
        $('[data-toggle="tooltip"]').tooltip();
    });
    $('body').on('mouseenter', '.fa-question-circle', function () {
        $('[data-toggle="tooltip"]').tooltip({
            container: 'body',
            html:true,
            offset: 20
        });
    });



    $('.navbar-header .open_menu').click( function() {
        $('.app-aside').toggleClass('off-screen');
        $('#overflow_admin').toggleClass('overflow_admin-open');
        $('body').toggleClass('modal-open');

    });
//close search, mobile_menu
    $('#overflow_admin').click( function() {
        $('.app-aside').removeClass('off-screen');
        $('#overflow_admin').removeClass('overflow_admin-open');
        $('body').removeClass('modal-open');
    });
    //открыто всегда только 1 меню
    $(document).on('click', 'header .collapse_btn, #new_settings_menu .collapse_btn', function (e) {
        var collapse_btn = $('header .collapse_btn, #new_settings_menu .collapse_btn');
        collapse_btn.each(function () {
            if(!$(this).hasClass('collapsed')){
                $('header').find('.open_btn').removeClass('open_btn');
                $(this).next('ul').collapse('hide');
            }
        });
        $(this).toggleClass('open_btn');
        // $(this).next('ul').collapse();
        $(this).next('ul').on('shown.bs.collapse',function () {
            var height_ul = $(this).prop('scrollHeight');
            $(this).css('height', height_ul);
        });

    });
    //скрываем открытые меню, если таргет на в нем
    $(document).mouseup(function (e){
        var collapse_block = $('header .header_drop_menu');
        if (!collapse_block.is(e.target) && collapse_block.has(e.target).length === 0) {
            collapse_block.find('ul').collapse('hide');
            collapse_block.find('.open_btn').removeClass('open_btn');
        }
    });
    renderCustomizationPanel();


    $(document).on('change', '.status input[type="checkbox"]', function () {
        if ($(this).prop("checked")) {
            $(this).closest('li').addClass('active');
        } else {
            $(this).closest('li').removeClass('active');
        }
    });

    $(document).on('change', '.load_file',function(e) {
        if ($(this).val() !=='') {
            addFileToList($(this).val());
            $(this).closest('.actions').find('.green_btn').attr('disabled', false);
        }
    });
    function addFileToList(val){
        var fileName = val.split('\\');
        val = fileName[fileName.length-1];
        $('#replacement_block').addClass('file_selected').html('<svg role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="#fff" d="M435.848 83.466L172.804 346.51l-96.652-96.652c-4.686-4.686-12.284-4.686-16.971 0l-28.284 28.284c-4.686 4.686-4.686 12.284 0 16.971l133.421 133.421c4.686 4.686 12.284 4.686 16.971 0l299.813-299.813c4.686-4.686 4.686-12.284 0-16.971l-28.284-28.284c-4.686-4.686-12.284-4.686-16.97 0z"></path></svg>' + val);
    }
});


//render data
function renderData($url_show) {
    $('#own_pagination').pagination({
        onPageClick: function (pageNum, e) {
            option.page = pageNum;

            for (var key in option) { //in view
                if (option[key] === undefined || option[key] == '') {
                    delete option[key];
                }
            }

            if ($('#own_table').hasClass('inner_loader')) {
                $('#own_table').after('<div class="ajax_load">' + '<span class="spin"></span>' + '<span>' + lang.all.TEXT_WAIT + '</span>' + '</div>');
            } else {
                $('#own_table').addClass('inner_loader').after('<div class="ajax_load">' + '<span class="spin"></span>' + '<span>' + lang.all.TEXT_WAIT + '</span>' + '</div>');
            }


            $.ajax({
                url: window.location.pathname + '?ajax_load=' + $url_show,
                type: "GET",
                dataType: 'json',
                data: option,
                success: function (response) {
                    if (response['modal']) {
                        getForm();
                    }
                    //console.log(response);
                    var link = '';
                    for (var key in option) {
                        if (key == 'search' || key == 'count') continue;
                        link += '&' + key + '=' + option[key];
                    }
                    link = link.replace("&", "?");
                    window.history.pushState(null, null, window.location.pathname + link);

                    $('.ajax_load').remove();
                    var paginate = response.paginate;
                    var data = response.data;
                    var allowed_fields = response.allowed_fields;
                    var show = '';
                    var buttonAction = $('#action').clone().removeAttr('style').html();
                    if($('input[name="number_of_rows"]')){
                        $('input[name="number_of_rows"]').val(paginate['count'])
                    }
                    $('input[name="page"]').val(paginate['current_page']);
                    option['count'] = paginate['count'];
                    var count_product = paginate['count'];
                    $('#count_prod').text('- ' + count_product);
                    if (data != null) {
                        var cnt_data = data.length;
                        for (var i = 0; i < cnt_data; i++) {
                            var style = (data[i]['background-color'] != "undefined") ? 'style="background-color:' + data[i]['background-color'] +'1a' + '"' : '';
                            show += '<tr ' + style + ' data-id="' + data[i]['id'] + '">';
                            for (var field in allowed_fields) {
                                if (allowed_fields[field]['show'] === false) continue;
                                var className = allowed_fields[field]['class'] != undefined ? 'class="' + allowed_fields[field]['class'] + '"' : '';
                                show += '<td ' + className + ' data-name="' + field + '" data-label="' + $('th[data-table='+field+']')[0].childNodes[0].textContent.trim() + '">';
                                if (field == 'status' || allowed_fields[field]['type'] == 'status') {
                                    var check = data[i][field] == 1 ? 'checked' : '';
                                    var id = field + '_' + data[i]['id'];
                                    show += '<div class="status">';
                                    show += '<input class="cmn-toggle cmn-toggle-round" type="checkbox" id="cmn-toggle-' + id + '"' + check + '>';
                                    show += '<label for="cmn-toggle-' + id + '"></label>';
                                    show += '</div>';
                                } else if (allowed_fields[field]['type'] == 'file' || allowed_fields[field]['general'] == 'file') {
                                    if (data[i][field] == null || data[i][field].length == 0) {
                                        show += '<img src="/images/default.png">';
                                    } else {
                                        show += '<img src="/getimage/' + data[i][field] + '">';
                                    }
                                } else if (allowed_fields[field]['change'] == true) {
                                    params = allowed_fields[field]['params'] !== undefined ? allowed_fields[field]['params'] : '';
                                    show += '<span class="change">' +
                                        '<input class="' + allowed_fields[field]['class'] + '"' + params + ' data-old="' + data[i][field] + '" name="' + field + '" type="' + allowed_fields[field]['type'] + '" value="' + data[i][field] + '"></span>';
                                } else if (allowed_fields[field]['type'] == 'select' && typeof(table_option) !== 'undefined') {
                                    show += '<span data-value="' + data[i][field] + '">' + table_option[field][data[i][field]] + '</span>';
                                } else if (field == 'dynamic') {
                                    show += '<input type="' + allowed_fields[field]['type'] + '">';
                                }
                                else if (data[i][field] == undefined) {
                                    show += '<div class="text-center">-</div>';
                                } else if (allowed_fields[field]['type'] == 'link') {
                                    show += '<a href="customers.php?id='+data[i]['cid']+'&action=edit_customers">'+data[i][field]+'</a>';
                                } else {
                                    show += data[i][field];
                                }
                                show += '</td>';
                            }
                            //  buttonAction = buttonAction.replace("data-title=","data-title='#"+data[i]['id']+"'");
                            show += buttonAction ? ('<td>' + buttonAction.replace("data-title=","data-title='#"+data[i]['id']+"'") + '</td>') : '';
                            show += '</tr>';
                        }
                        $('#own_pagination').pagination('updateItems', response['paginate']['total']);
                        $('#own_table >tbody').empty().append(show);
                    } else {
                        $('#own_table >tbody').empty();
                        $('#own_pagination').pagination('updateItems', 1);
                        show_tooltip('empty');
                    }
                    if (response['debug'] != undefined) {
                        show = '';
                        for (var key in response['debug']) {
                            if (typeof response['debug'][key] === 'object') {
                                show += '<h3>' + key + '</h3><pre>' + dump(response['debug'][key]) + '</pre>';
                            } else {
                                show += '<h3>' + key + '</h3>' + response['debug'][key];
                            }
                        }
                        $('#debug').remove();
                        $('<div id="debug"><i class="fa fa-bug fa-lg" aria-hidden="true"></i><div>' + show + '</div></div>').insertAfter($('#own_pagination'));
                    }
                    checkShowMoreBtnStatus();
                }
            });
        }
    });
}

function filterTable($thead) {
    option.search = {};
    option.page = 1;
    delete(option.count);
    var $filters = $($thead).find('input.search');
    $filters.each(function (i, e) {
        var key = $(e).parent('th').data('table');
        option.search[key] = e.value;
    });

    var $filters_select = $($thead).find('select.search');
    $filters_select.each(function (i, e) {
        var key = $(e).parent('th').data('table_select');
        option.search[key] = e.value;
        option[key] = e.value;
    });

    if (timeout) clearTimeout(timeout);
    timeout = setTimeout(function () {
        $('#own_pagination').pagination('selectPage', option.page);
    }, 300);

}

function checkAllStatus(obg, status) {
    if (obg.parents('td').hasClass("check_all")) {
        obg.parents('table').find('.check_all input').prop('checked', false);
        obg.prop('checked', true);
        if (status == false) return false;
    }
    return true;
}

function modal(options) {
    var settings = {
        id: Math.floor(Math.random() * (1000 - 1 + 1)) + 1,
        after: function () {
        },
        width: 0,
        before: function () {
        },
        hidden: function () {
        }
    }

    $.extend(true, settings, options);
    var width = '';
    if (settings.width != 0) width = 'style="width:auto;max-width:' + settings.width + '"';

    if (jQuery('.modal').length == 0) {
        var $html = '<div class="modal fade" id="modal_' + settings.id + '" tabindex="-1" role="dialog" aria-labelledby="' + settings.id + '_label" aria-hidden="true">';
        $html += '<div class="modal-dialog" ' + width + '><div class="modal-content">';
        $html += '<div class="modal-header">';
        $html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
        if (settings.title) {
            $html += '<h4 class="modal-title" id="modal_' + settings.id + '_label">' + settings.title + '</h4>';
        }
        $html += '</div>';
        $html += '<div class="modal-body">' + settings.body + '</div>\
        </div>\
      </div>\
      </div>';
        jQuery('body').append($html).promise().done(function () {
            var $modal = jQuery('#modal_' + settings.id);
            var $before = settings.before($modal);
            if ($before !== false) {
                if (settings.classes) {
                    $modal.addClass(settings.classes);
                }

                if (settings.title == null) {
                    $modal.addClass('no-title');
                }

                if ($modal.hasClass('valign-false') == false) {
                    centerModal($modal);
                }
                $modal.on('hidden.bs.modal', function (e) {
                    $modal.remove();
                    if (settings.hidden) {
                        settings.hidden($modal);
                    }
                });
                $modal.on('shown.bs.modal', function (e) {
                    /*---for ckeditor in mozilla  ----*/
                    $(document).off('focusin.modal');
                    if (settings.after) {
                        settings.after($modal);
                    }
                });
                $modal.modal();

            } else {
                $modal.remove();
            }

        });
    }

}

function centerModal(el) {
    $(el).css('display', 'block');
    var $dialog = $(el).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    var bottomMargin = $dialog.css('marginBottom');
    bottomMargin = parseInt(bottomMargin);
    if (offset < bottomMargin) offset = bottomMargin;

    $dialog.css("margin-top", "10px");
    //$dialog.css("margin-top", offset);
}

function removeTooltip(e) {
    $(e).closest('.tooltip_own').remove();
}

function show_tooltip(message, time, element, close) {
    close = close == true ? '<span onclick="removeTooltip(this)" class="close">X</span>' : '';
    time = time || 1500;
    element = element || $('body');
    element.prepend('<p class="tooltip_own">' + message + close + '</p>');
    setTimeout(function () {
        $('.tooltip_own').animate({opacity: 0}, time, function () {
            $(this).remove();
        });
    }, time)
}

function dump(v, s) {
    s = s || 1;
    var t = '';
    switch (typeof v) {
        case "object":
            t += "\n";
            for (var i in v) {
                t += Array(s).join(" ") + i + ": ";
                t += dump(v[i], s + 3);
            }
            break;
        default: //number, string, boolean, null, undefined
            t += v + " (" + typeof v + ")\n";
            break;
    }
    return t;
}

function getGetParams() {
    var search = window.location.search.substr(1),
        keys = {
            page: 1,
            perPage: 25
        };
    search.split('&').forEach(function (item) {
        item = item.split('=');
        keys[item[0]] = item[1];
    });
    return keys;
}

function delGetParams(Url, Prm) {
    var a = Url.split('?');
    var result = a[0];
    var str = a[1];
    Prm.forEach(function (item, i, arr) {
        var re = new RegExp('(\\?|&)' + item + '=[^&]+', 'g');
        str = ('?' + str).replace(re, '');
        str = str.replace(/^&|\?/, '');
    });
    return result + str == '' ? '' : '?' + str;
}

function changeLang() {
    var $langId = $(this).children('a').data('lang');
    var modalBody = $(this).parents('.modal-body');
    $(this).parent().find('li.active').removeClass('active');
    $(this).addClass('active');
    modalBody.find('form div[data-lang]').removeClass('active');
    modalBody.find('form div[data-lang="' + $langId + '"]').addClass('active');
}

function getForm(e) {

    var data = {};
    var $this = $(this);
    $('.modal').removeClass('fade');
    $('[data-dismiss="modal"]').click();
    var url = $this.attr('href') ? $this.attr('href') : window.location.href;//
    var title = $this.data('title') || lang.currentPage.HEADING_TITLE;
    if (url.indexOf('template_configuration.php') != -1) title += ' "'+$this.closest('tr').find('td[data-name=template_name]').text()+'"';
    data.action = $this.data('action');
    if (data.action == 'edit_template') widthOfModal = '90%';
    if ($this.hasClass('edit_row')) data.id = $this.closest('tr').attr('data-id');
    if ($this.hasClass('check_options_values')) data.id = $this.closest('tr').attr('data-id');
    var link = '';
    for (var key in data) {
        if (data[key] === undefined) continue;
        link += '&' + key + '=' + data[key];
    }

    $.ajax({
        url: url,
        type: "GET",
        data: data,
        dataType: 'json',
        success: function (response) {
            $('.tooltip_own').remove();

            window.history.pushState(null, null, url + link);
            modal({
                title: $this.data('title') || $this.closest('tr').find('td[data-name=articles_name]').text() || title || lang.currentPage.HEADING_TITLE,
                body: response.html,
                id: 'form',
                width: (widthOfModal != undefined) ? widthOfModal : '90%',
                before: function () {
                    ckFinderBuilder();
                },
                hidden: function (modal) {
                    var str = delGetParams(url, ['action', 'id']);
                    delete(option.action);
                    delete(option.id);
                    window.history.pushState(null, null, str);
                },
                after: function (modal) {
                    var form = $(modal).find('form');
                    var lang = $(modal).find('ul#lang>li');
                    var del_file = $(modal).find('form button .fa-trash-o');
                    del_file.on('click', delFile);
                    lang.on('click', changeLang);
                    form.on('click', 'input[type="submit"]', function (e) {
                        //$('[contenteditable]').remove();
                        e.preventDefault();
                        if (checkRequired(form)) {
                            var id = form.find('input[name="id"]').val();
                            var formData = new FormData(form.get(0));
                            $.ajax({
                                url: form.attr('action'),
                                type: form.attr('method'),
                                dataType: 'json',
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: function (response) {
                                    $('.tooltip_own').remove();
                                    delete(option.action);
                                    delete(option.id);
                                    $(modal).modal('hide');
                                    if (response.success == true) {
                                        delete(option.count);
                                        $('#own_pagination').pagination('selectPage', option.page);
                                    } else if (response.link !== undefined) {
                                        window.location.href = response.link;
                                    } else if (response.html) {
                                        getForm();
                                    } else {
                                        show_tooltip(response['msg']);
                                    }
                                }
                            });
                        }
                    })
                    form.on('click', 'button.saveInputData', function (e) {
                        //$('[contenteditable]').remove();
                        e.preventDefault();
                        if (checkRequired(form)) {
                            var id = form.find('input[name="id"]').val();
                            var formData = new FormData(form.get(0));
                            $.ajax({
                                url: form.attr('action'),
                                type: form.attr('method'),
                                dataType: 'json',
                                data: formData,
                                contentType: false,
                                processData: false,
                                success: function (response) {
                                    $('.tooltip_own').remove();
                                    show_tooltip(response['msg']);
                                }
                            });
                        }
                    })
                }
            })
            // activate language tabs for attributes
            $('#attributes-tabs').tabs({fx: {opacity: 'toggle', duration: 'fast'}});
        }
    });
}

function checkRequired(form) {
    var error = true;
    form.find('[required]').each(function (i) {
        if (!($(this).val())) {
            error = false;
            $(this).addClass('error');
        } else {
            $(this).removeClass('error')
        }
    });
    return error;
}

function ckFinderBuilder() {
    var textarea = $('textarea.ck_replacer');

    if (textarea.length !== 0) {
        $('.modal').removeAttr('tabindex');

        textarea.each(function (i, e) {
            var editor = CKEDITOR.replace($(e).attr('name'), {
                extraPlugins: 'colorbutton,font,showblocks,justify,codemirror,btgrid',
                startupFocus: true,
                height: '400px',
                removePlugins: 'sourcearea',
                on: {
                    instanceReady: function() {
                        this.dataProcessor.htmlFilter.addRules( {
                            elements: {
                                img: function( el ) {
                                    // Add an attribute.
                                    if ( !el.attributes.alt )
                                        el.attributes.alt = '';
                                    // Add some class.
                                    if (typeof el.attributes.class === 'undefined' || el.attributes.class.indexOf('img-responsive') == -1){
                                        el.addClass( ' img-responsive' );
                                    }
                                    if (typeof el.attributes.class === 'undefined' || el.attributes.class.indexOf('lazyload') == -1){
                                        el.addClass( ' lazyload' );
                                    }
                                    // el.attributes.style = '' ;
                                }
                            }
                        });
                    }
                }
            });
            editor.on('change', function (evt) {
                $(e).text(evt.editor.getData());
            });
            CKFinder.setupCKEditor(editor, 'includes/ckfinder/');
        });
    }

}

function tableOption(params) {
    //file: template_configuration
    var o = {
        arr: null,
        id: null,
        name: null,
        ajax_url: window.location.pathname,
        hide: function (e) {
            e.hide();
        },
        show: function (e) {
            e.show();
        },
        remove: function (e) {
            e.remove();
        },
        onChange: function (element) {
            element.change(function (event) {
                var selected = $(this).find(':selected'), val = selected.val(), text = selected.text();
                o.data = {val: val, name: o.name, id: o.id, text: text};
                o.sendAjax(o.data);
                o.remove($linkWrapper);
                o.show($this);
            });
        },
        ajaxSuccess: function () {
            $this.replaceWith('<span data-value="' + o.data.val + '">' + o.data.text + '</span>');
        },
        ajaxError: function () {

        },
        onInit: function () {
            // Callback triggered immediately after initialization
        },
        drawOption: function (arr) {

            for (var val in arr) {
                var selected = val == val_id ? 'selected' : '';
                show += '<option ' + selected + ' value="' + val + '">' + arr[val] + '</option>';
            }
            return show;
        },
        sendAjax: function (data) {
            $.ajax({
                url: o.ajax_url + '?action=change_' + o.name,
                type: "POST",
                data: data,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    show_tooltip(response.msg, 500);
                    if (response.result) {
                        o.ajaxSuccess();
                    } else {
                        o.ajaxError();
                    }
                }
            });
        }
    };
    $.extend(true, o, params);
    var $this = $(this), val_id = $this.data('value'), td = $this.parent(), show, $linkWrapper = $('<select></select>');
    o.onInit();
    o.hide($this);

    show = o.drawOption(o.arr[o.name]);

    $linkWrapper.addClass('form-control');

    o.onChange($linkWrapper);

    $linkWrapper.append(show);
    td.append($linkWrapper);
}

function delFile(e) {
    e.preventDefault();
    var $this = $(this);
    var $divBlock = $this.closest('div');
    var ID = $('form input[name="id"]').val();
    var field_name = $divBlock.find('input[type="file"]').attr('name');
    $.ajax({
        url: window.location.pathname,
        type: "POST",
        data: {ID: ID, field_name: field_name, action: 'delete_image'},
        dataType: 'json',
        success: function (response) {
            $divBlock.find('img').remove();
            $divBlock.find('#image_name').remove();
            $divBlock.find('span').show();
            $divBlock.find('button').remove();

        }
    });
}

function scrollToElement(element){
    var $el = jQuery(element);
    if ($el.length) {
        var $elOffset_top = $el.offset().top - 200;
        jQuery('body,html').stop(false, false).animate({scrollTop: $elOffset_top}, {
            duration: 600,
            easing: 'swing'
        }, 800);
    }
}

function setTooltipForMenu(){
    if($(window).width()<= 767){
        return true;
    }
    var boxWidth = $('.app-header-fixed #aside .aside-wrap').width();
    if(boxWidth > 55 && boxWidth < 65 ){
        $('#aside li.item-menu').tooltip({
            items:'[data-title]',
            content: function(){
                return 	$(this).attr( "data-title" );
            },
            position: {
                my: "left",
                at: "left+70",
                collision: "none",
                //of:'.item-menu',
                using: function(position, feedback){
                    var tooltip = $(this);
                    tooltip.css({
                        border: 'none',
                        textTransform: 'none',
                        minWidth: '120px',
                        maxWidth: '120px',
                        height: '31px',
                        boxShadow: '0 5px 10px 0 rgba(0, 0, 0, 0.1)',
                        backgroundColor: '#2d3343',
                        color: '#fff',
                        fontSize: '13px'
                    })
                    tooltip.css(position);
                    tooltip.addClass("arrow-tt111").appendTo(tooltip);
                }
            }
        });
    }else{
        try{
            $('#aside li.item-menu').tooltip('destroy');
        }catch(e){
            return true;
        }
    }
}

function triggerMenuItem(action,event){
    if(action == 'close'){
        $('#aside li.active').find('ul.nav-sub').css('display', 'none');
        $('#aside li.active').map(function(idx, el){
            $(el).removeClass('active').addClass('_active');
        });
    }else{
        $('#aside li._active').map(function(idx, el){
            $(el).removeClass('_active').addClass('active');
        });
        $('#aside li.active').find('ul.nav-sub').css('display', 'block');

    }
    return true;
}

function collapsLeftMenu() {
    if($(window).width()<= 767){
        $('.app-header-fixed #aside').removeClass('off-screen');
        return false;
    }
    $('#close-menu').hide();
    $('#open-menu').show();
    $('.app-header-fixed #aside div.fix-width').addClass('collapse-menu');
    $('.app-header-fixed #aside .aside-wrap').removeClass('min-collapse-menu');
    $('body').addClass('wrap-collapse-menu');
    triggerMenuItem('close');
    $('.menu-form div.menu_search_form').css({'display':'none'});
    $('.app-aside-dock .app-aside .navi > ul > li > a > svg').css({'margin':'0 4px 0 0px'});
    $('.app-aside-dock .app-aside .navi > ul > li img.img-filter_1').css({'filter':'inherit','filter':'initial','filter':'unset'});
    $('.app-aside-dock .app-aside .navi > ul > li > a img').css({'margin':'0 20px 0 15px'});
    $('.navi ul.nav li a .new_badge').css({'right':'224px','top':'-2px','background':'#ff112b','width':'21px','height':'15px','transition':'0s'});
    setTooltipForMenu();
    if($(window).width()<= 1599){
        $('.navi ul.nav li a .new_badge').css({'right':'139px','top':'-2px','background':'#ff112b','width':'21px','height':'15px','transition':'0s'});
    };
    $.get('ajax_set_menu_location.php?action=set_menu_location&value=2');
}
$(document).on('keyup','.menu_search_form input[type="search"]',function(e){
    var search = e.target.value.toLowerCase();
    if(search.length > 0){
        $('nav.navi>ul>li>a').hide();
        $('nav.navi>ul>li>ul>li').each(function(){
            var currText = $(this).text().toLowerCase();
            if(currText.indexOf(search) == -1){
                $(this).hide();
            }else{
                $(this).parent().parent().find('a').show();
                $(this).show();
            }
        });
        $('nav.navi>ul>li>ul').css({'display':'block','opacity':1});
    }else{
        $('nav.navi>ul>li>a').show();
        $('nav.navi>ul>li>ul>li').show();
        $('nav.navi>ul>li>ul').removeAttr('style');

    }
});

function renderCustomizationPanel() {

    $.ajax({
        url: '/admin/customization_panel.php?side=admin',
        success: function (response) {
            $('body').append(response);
        }
    });
}
