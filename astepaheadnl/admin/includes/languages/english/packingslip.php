<?php
/*
  $Id: packingslip.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TABLE_HEADING_COMMENTS', 'Comments');
define('TABLE_HEADING_PRODUCTS_MODEL', 'Product model');
define('TABLE_HEADING_PRODUCTS', 'Products');
define('TABLE_HEADING_PRODUCTS_QUANTITY', 'Amount');
define('TITLE_PRINT_ORDER', 'Packing slip');

define('ENTRY_SOLD_TO', 'Sold to:');
define('ENTRY_SHIP_TO', 'Ship to:');
define('ENTRY_PAYMENT_METHOD', 'Payment Method:');

define('PACKINGSLIP_CLOSE_BTN', 'Close');
define('PACKINGSLIP_SELLER_TITLE', 'Seller:');
define('PACKINGSLIP_SELLER_NAME', 'Solomono Sergei');
define('PACKINGSLIP_SELLER_EMAIL', 'solomono.net');
define('PACKINGSLIP_BOX_ORDER', 'Order#');
define('PACKINGSLIP_BOX_ORDER_DATE', 'Order date:');
define('PACKINGSLIP_BOX_ORDER_DELIVERY', 'Delivery:');
define('PACKINGSLIP_GETTING_GOODS', 'Goods received. No complaints about quantity and quality');
define('PACKINGSLIP_PAINTING_GOODS', 'Your signature here');

define('DOCUMENT_TITLE', 'Packing Slip');
define('ADDRESS_TITLE', 'Delivery address');
define('ADDRESS', 'Vibrafit<br>
t.a.v.<br>
Lindenstr, 44<br>
35606 Solms<br>
Germany<br>');
define('TEL_TITLE', 'Telephone');
define('TELEPHONE', '+49 6442 92130');
define('TEL_FAX_TITLE', 'Fax');
define('TELEPHONE_FAX', '+49 6442 92130');
define('INVOICE_NUMBER_TITLE', 'Pakbonnummer');
define('INVOICE_DEBITEUR_TITLE', 'Debtor number');
define('INVOICE_DATE_TITLE', 'Pakbondatum');
define('INVOICE_SHEET_TITLE', 'Sheet number');
define('INVOICE_SHEET_NUMBER', '1');
define('CONDITION_TITLE', 'Bet. condition');
define('CONDITION', 'Pay in advance / Forward');
define('EPISODE_TITLE', 'Episode');
define('EPISODE', '1');
define('NUMBER_TITLE', 'Number');
define('CODE_TITLE', 'Item code');
define('DESCRIPTION_TITLE', 'Description');
define('WEIGHT_TITLE', 'GWeight KG');
define('REFERENCE_CUSTOMER_TITLE', 'Referentie klant');

define('UNILEVER_INFO_TITLE', 'Unilever info');
define('TOTAL_WEIGHT_TITLE', 'Totaal gewicht');
define('WAITING_PAYMENT_TITLE', 'Waiting for payment');
define('WAITING_PAYMENT', 'Yes / No');
define('RECEIVE_PAYMENT_TITLE', 'Receive payment');
define('RECEIVE_PAYMENT', 'Yes / No');
define('ULEVEN_TITLE', 'Uleven');
define('ULEVEN', 'Yes / No');
define('FOR_AGREEMENT', 'For agreement');
define('NAME', 'Name');
define('SIGNATURE', 'Signature');

define('PAYMENT_CONDITION_TITLE', 'Payment Condition');
define('PAYMENT_CONDITION', 'Pay In Advance / Prepayment');
define('PAYMENT_TERM_TITLE', 'Payment term');
define('PAYMENT_TERM', 'When paying within 14 days, you can deduct the credit limit.');
define('CREDIT_LIMITATION_TITLE', 'Credit limitation');
define('CREDIT_LIMITATION', '100% payment within 14 days, stating: of the invoice and debtor number');
define('COMPLAINTS', 'Complaints must be in our possession within 8 days of the invoice date.');
define('INVOICE_CREATED', 'Invoice created by : AstepAhead');
define('FOOTER_BLOCK1', '<b>DRAX Europe / ASA B.V</b><br>
Distribution & Service<br>
Ondernemingsweg 45<br>
2404 HN Alpen aan den Rijn<br>
The Netherlands<br>');
define('FOOTER_BLOCK2', '<b>DRAX Europe</b><br>
Showroom & Life Setting<br>
Snijdersbergweg 97<br>
1105 AN Amsterdam<br>
The Netherlands<br>');
define('FOOTER_PHONE', 'Phone : +31 85 7501025');
define('FOOTER_EMAIL', 'Email : sales@draxfit.eu');
define('FOOTER_SITE', 'Website : www.draxfit.eu');
define('FOOTER_CC', 'CC no : 27362484');
define('FOOTER_IBAN', 'IBAN : NL57 RABO 01528857 73');
define('FOOTER_BIC', 'BIC : RABONNL2U');
define('FOOTER_VAT', 'VAT no : NL821969778B01');