<?php
/*
  $Id: packingslip.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('TABLE_HEADING_COMMENTS','Opmerkingen');
define('TABLE_HEADING_PRODUCTS_MODEL','Artikelnr.');
define('TABLE_HEADING_PRODUCTS','Producten');
define('TABLE_HEADING_PRODUCTS_QUANTITY', 'Aantal');
define('TITLE_PRINT_ORDER', 'Pakbon');

define('ENTRY_SOLD_TO','Koper:');
define('ENTRY_SHIP_TO','Levering adres:');
define('ENTRY_PAYMENT_METHOD','Betaalmethode:');

define('PACKINGSLIP_CLOSE_BTN', 'Sluiten');
define('PACKINGSLIP_SELLER_TITLE', 'Verkoper:');
define('PACKINGSLIP_SELLER_NAME', 'Solomono Sergey');
define('PACKINGSLIP_SELLER_EMAIL', 'solomono.net');
define('PACKINGSLIP_BOX_ORDER', 'Bestel#');
define('PACKINGSLIP_BOX_ORDER_DATE', 'Besteldatum:');
define('PACKINGSLIP_BOX_ORDER_DELIVERY', 'Levering:');
define('PACKINGSLIP_GETTING_GOODS', 'Item ontvangen. Geen klachten over kwantiteit en kwaliteit');
define('PACKINGSLIP_PAINTING_GOODS', 'Uw handtekening hier');

define('DOCUMENT_TITLE', 'Pakbon');
define('ADDRESS_TITLE', 'Afleveradres');
define('ADDRESS', 'Vibrafit<br>
t.a.v.<br>
Lindenstr, 44<br>
35606 Solms<br>
Germany<br>');
define('TEL_TITLE', 'Telefoon');
define('TELEPHONE', '+49 6442 92130');
define('TEL_FAX_TITLE', 'Telefax');
define('TELEPHONE_FAX', '+49 6442 92130');
define('INVOICE_NUMBER_TITLE', 'Pakbonnummer');
define('INVOICE_DEBITEUR_TITLE', 'Debiteurennummer');
define('INVOICE_DATE_TITLE', 'Pakbondatum');
define('INVOICE_SHEET_TITLE', 'Bladnummer');
define('INVOICE_SHEET_NUMBER', '1');
define('CONDITION_TITLE', 'Bet. condition');
define('CONDITION', 'Pay in advance / Vooruit');
define('EPISODE_TITLE', 'Aflevering');
define('EPISODE', '1');

define('NUMBER_TITLE', 'Aantal');
define('CODE_TITLE', 'Artikelcode ');
define('DESCRIPTION_TITLE', 'Omschrijving');
define('WEIGHT_TITLE', 'Gewicht in KG');
define('REFERENCE_CUSTOMER_TITLE', 'Referentie klant');

define('UNILEVER_INFO_TITLE', 'Unilever info');
define('TOTAL_WEIGHT_TITLE', 'Totaal gewicht');
define('WAITING_PAYMENT_TITLE', 'Wacht op betaling');
define('WAITING_PAYMENT', 'Ja / Nee');
define('RECEIVE_PAYMENT_TITLE', 'Betaling ontvangen');
define('RECEIVE_PAYMENT', 'Ja / Nee');
define('ULEVEN_TITLE', 'Uleveen');
define('ULEVEN', 'Ja / Nee');
define('FOR_AGREEMENT', 'Voor akkoord');
define('NAME', 'Naam');
define('SIGNATURE', 'Handtekening');

define('PAYMENT_CONDITION_TITLE', 'Betalingsconditie');
define('PAYMENT_CONDITION', 'Pay In Advance / Vooruitbetaling ');
define('PAYMENT_TERM_TITLE', 'Betalingstermijn');
define('PAYMENT_TERM', 'Bij betaling binnen 14 dagen kunt u de kredietbeperking in mindering brengen.');
define('CREDIT_LIMITATION_TITLE', 'Krediet beperking');
define('CREDIT_LIMITATION', '100% betaling binnen 14 dogen o.v.v. van het en factuur- en debiteuren nummer');
define('COMPLAINTS', 'Reclamaties dienen binnen 8 dagen na factuurdatum in ons bezit te zijn.');
define('INVOICE_CREATED', 'Factuur is gemaakt door : AstepAhead');
define('FOOTER_BLOCK1', '<b>AStepAhead B.V</b><br>
Distribution & Service<br>
Ondernemingsweg 46<br>
2404HN Alpen aan den Rijn<br>
The Netherlands<br>');
define('FOOTER_BLOCK2', '<b>Experienceroom</b><br>
Showroom<br>
Euromarkt 6<br>
2408BL Alphen aan den Rijn<br>
The Netherlands<br>');
define('FOOTER_PHONE', 'Phone : +31 85 7501025');
define('FOOTER_EMAIL', 'Email : info@astepahead.nl');
define('FOOTER_SITE', 'Website : www.astepahead.nl');
define('FOOTER_CC', 'KVK nr : 27362484');
define('FOOTER_IBAN', 'IBAN : NL57 RABO 01528857 73');
define('FOOTER_BIC', 'BIC : RABONNL2U');
define('FOOTER_VAT', 'BTW nr : NL821969778B01');