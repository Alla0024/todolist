<?php
define('HEADING_TITLE', 'Omleiding');
define('TEXT_REDIRECT_FROM', 'Omleiden van');
define('TEXT_REDIRECT_TO', 'Omleiden naar');
define('TABLE_HEADING_STATUS', 'Toestand');
define('TEXT_DATE_ADDED', 'Datum toegevoegd');
define('TEXT_DATE_UPDATED', 'Datum bijgewerkt');
