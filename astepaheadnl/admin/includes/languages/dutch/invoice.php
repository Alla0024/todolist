<?php
define('TITLE_PRINT_ORDER','BESTELLEN Nr.');
define('TABLE_HEADING_COMMENTS','Opmerkingen');
define('TABLE_HEADING_PRODUCTS_MODEL','Artnr.');
define('TABLE_HEADING_INVOICE_PRODUCT_PRICE', 'Prijs excl. BTW');
define('TABLE_HEADING_PRODUCTS','Omschrijving');
define('TABLE_HEADING_REF', '');
define('TABLE_HEADING_TAX','Belasting');
define('TABLE_HEADING_TOTAL','Gewoon');
define('TABLE_HEADING_PRICE_EXCLUDING_TAX','Prijs');
define('TABLE_HEADING_PRICE_INCLUDING_TAX','Prijs (inclusief btw)');
define('TABLE_HEADING_TOTAL_EXCLUDING_TAX','Totaal excl. BTW');
define('TABLE_HEADING_TOTAL_INCLUDING_TAX','Sub total');

define('ENTRY_SOLD_TO','KOPER:');
define('ENTRY_SHIP_TO','LEVERING ADRES:');
define('ENTRY_PAYMENT_METHOD','Betaalmethode:');
define('ENTRY_SUB_TOTAL','Voorafgaand Bedrag:');
define('ENTRY_TAX','Belastingen:');
define('ENTRY_SHIPPING','Levering:');
define('ENTRY_TOTAL','Totaal:');
define('ENTRY_SELLER','VERKOPER:');
define('ENTRY_QTY','Aantal');
define('ENTRY_QTY_SUM','Van de items in de volgorde.');
define('ENTRY_SUMBIT1','Bestelling is ontvangen.');
define('ENTRY_SUMBIT2','Vorderingen op kwantiteit en kwaliteit niet.');
define('ENTRY_SIGN','de handtekening van de klant');
define('DOCUMENT_TITLE', 'Factuur');
define('ADDRESS_TITLE', 'Factuur adres');
define('ADDRESS', 'Mens Mentis SRL<br>
Attn : Accounts Payable / t.a.v. de Administratie<br>
Ep.M Pavel street 23/5<br>
410210 Oradea Bihor Romania<br>');
define('TEL_TITLE', 'Telefoon');
define('TELEPHONE', '+40 359 444 443');
define('INVOICE_NUMBER_TITLE', 'Factuurnummer');
define('INVOICE_DEBITEUR_TITLE', 'Debiteurennummer');
define('INVOICE_DATE_TITLE', 'Factuurdatum');
define('INVOICE_EXPIRATION_DATE', 'Vervaldatum');
define('INVOICE_VAT_TITLE', 'BTW # klant');
define('INVOICE_VAT', '');
define('INVOICE_SHEET_TITLE', 'Bladnummer');
define('INVOICE_SHEET_NUMBER', '1');
define('PRODUCT_LINK', 'Koppeling');
define('SECOND_PART_TITLE', '');

define('INVOICE_CLOSE_BTN', 'Sluiten');
define('INVOIC_BOX_ORDER_DATE', 'Besteldatum:');

define('CREDIT_LIMIT', 'Kredietbeperking');
define('OTHER', 'Overige');
define('SUBTOTAL', 'Subtotaal excl. BTW');
define('VAT_SUBTOTAL', 'BTW');
define('INVOICE_TOTAL', 'Totaal incl. BTW');

define('PAYMENT_CONDITION_TITLE', 'Betalingsconditie');
define('PAYMENT_CONDITION', 'Pay In Advance / Vooruitbetaling ');
define('PAYMENT_TERM_TITLE', 'Betalingstermijn');
define('PAYMENT_TERM', 'Bij betaling binnen 14 dagen kunt u de kredietbeperking in mindering brengen.');
define('CREDIT_LIMITATION_TITLE', 'Krediet beperking');
define('CREDIT_LIMITATION', '100% betaling binnen 14 dogen o.v.v. van het en factuur- en debiteuren nummer');
define('COMPLAINTS', 'Reclamaties dienen binnen 8 dagen na factuurdatum in ons bezit te zijn.');
define('INVOICE_CREATED', 'Factuur is gemaakt door : AstepAhead');
define('FOOTER_BLOCK1', '<b>AStepAhead B.V</b><br>
Distribution & Service<br>
Ondernemingsweg 46<br>
2404HN Alpen aan den Rijn<br>
The Netherlands<br>');
define('FOOTER_BLOCK2', '<b>Experience room</b><br>
Showroom<br>
Euromarkt 6<br>
2408BL Alphen aan den Rijn<br>
The Netherlands<br>');
define('FOOTER_PHONE', 'Tel : +31 85 7501025');
define('FOOTER_EMAIL', 'Email : info@astepahead.nl');
define('FOOTER_SITE', 'Website : www.astepahead.nl');
define('FOOTER_CC', 'CC no : 27362484');
define('FOOTER_IBAN', 'IBAN : NL57 RABO 0152885773');
define('FOOTER_BIC', 'BIC : RABONNL2U');
define('FOOTER_VAT', 'BTWnr. : NL821969778B01');