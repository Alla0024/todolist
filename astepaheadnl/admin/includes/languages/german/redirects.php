<?php
define('HEADING_TITLE', 'Weiterleitungen');
define('TEXT_REDIRECT_FROM', 'Weiterleiten von');
define('TEXT_REDIRECT_TO', 'Weiterleiten an');
define('TABLE_HEADING_STATUS', 'Status');
define('TEXT_DATE_ADDED', 'Datum hinzugefügt');
define('TEXT_DATE_UPDATED', 'Aktualisierungsdatum');