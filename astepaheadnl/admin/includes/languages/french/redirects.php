<?php
define('HEADING_TITLE', 'Redirects');
define('TEXT_REDIRECT_FROM', 'Rediriger depuis');
define('TEXT_REDIRECT_TO', 'Rediriger vers');
define('TABLE_HEADING_STATUS', 'Status');
define('TEXT_DATE_ADDED', 'Date ajoutée');
define('TEXT_DATE_UPDATED', 'Date de mise à jour');