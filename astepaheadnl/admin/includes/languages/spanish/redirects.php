<?php
define('HEADING_TITLE', 'Redirecciones');
define('TEXT_REDIRECT_FROM', 'Redirigir desde');
define('TEXT_REDIRECT_TO', 'Redirigir a');
define('TABLE_HEADING_STATUS', 'Estado');
define('TEXT_DATE_ADDED', 'Fecha de adición');
define('TEXT_DATE_UPDATED', 'Fecha de actualización');