<?php
define('HEADING_TITLE', 'Ανακατευθύνσεις');
define('TEXT_REDIRECT_FROM', 'Ανακατεύθυνση από');
define('TEXT_REDIRECT_TO', 'Ανακατεύθυνση σε');
define('TABLE_HEADING_STATUS', 'Κατάσταση');
define('TEXT_DATE_ADDED', 'Ημερομηνία προσθήκης');
define('TEXT_DATE_UPDATED', 'Ημερομηνία ενημέρωσης');

