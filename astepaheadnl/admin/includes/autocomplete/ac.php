<?php 
    // Solomono jQuery Autocomplete module for osCommerce. 2017

    // go to catalog folder level
    chdir('../../');
    $rootPath = dirname(dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME']))));
    include('includes/application_top.php');
    

    // if this is ajax query
    if($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
      
      if($_GET['term']) { 
        $result = array();
      
        require(DIR_WS_CLASSES . 'currencies.php');
        $currencies = new currencies();
        
        // main search sql query
        $search_query=tep_db_query('SELECT DISTINCT pd.products_id, pd.products_name, p.products_image,	p.products_price, p.products_model 
    																FROM products p LEFT JOIN products_description pd ON pd.products_id=p.products_id 
    																WHERE (LOWER(pd.products_name) LIKE "%'.strtolower($_GET['term']).'%" or LOWER(p.products_model) LIKE "%'.strtolower($_GET['term']).'%")    
    																AND (p.products_status=1) and pd.language_id = ' . $languages_id . '
    																ORDER BY p.products_quantity desc
                                    LIMIT 10;');
    
        // store received data to array
        while($search_product = tep_db_fetch_array($search_query)){
          $result[] = array('id'=>$search_product['products_id'], 
                            'name'=>$search_product['products_name'], 
                            'model'=>$search_product['products_model'], 
                            'price'=>$currencies->format($search_product['products_price']), 
                            'image'=>$search_product['products_image']);
        }                                                             

        
        if(!empty($result)) echo json_encode($result); // send full array to js in JSON format
  
    	}
    }

    require(DIR_WS_INCLUDES . 'application_bottom.php');
?>