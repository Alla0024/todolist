<?php
/*
  $Id: packingslip.php,v 1.2 2003/09/24 15:18:15 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

require('includes/application_top.php');

require(DIR_WS_CLASSES . 'currencies.php');
$currencies = new currencies();

$oID = tep_db_prepare_input($_GET['oID']);

$customerNumberSql = 'SELECT o.customers_id,
                             i.invoice_number
                     FROM ' . TABLE_ORDERS . ' o
                     LEFT JOIN invoices i on i.order_id = o.orders_id
                     WHERE o.orders_id = ' . $oID;
$customer_number_query = tep_db_query($customerNumberSql);
$customer_number = tep_db_fetch_array($customer_number_query);

include(DIR_WS_CLASSES . 'order.php');
$order = new order($oID);

if ($_GET['pdf'] == 'true') {
    require_once '../' . DIR_WS_INCLUDES . 'mpdf/examples/vendor/autoload.php';
    define('_MPDF_TTFONTPATH', '../' . DIR_WS_INCLUDES . 'mpdf/examples/vendor/mpdf/mpdf/ttfonts');
    $mpdf = new \Mpdf\Mpdf([
        'margin_left' => 15,
        'margin_right' => 15,
        'margin_bottom' => 55,
        'margin_footer' => 10
    ]);

    ob_start();
    require_once "packingslip.tpl.php";
    $output = ob_get_contents();
    ob_end_clean();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $mpdf->WriteHTML($output);


    $mpdf->Output();
    require(DIR_WS_INCLUDES . 'application_bottom.php');

    exit();
}

?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
    <title><?php echo TITLE_PRINT_ORDER . $oID; ?></title>
    <base href="<?php echo HTTP_SERVER . DIR_WS_CATALOG; ?>">
    <link rel="stylesheet" type="text/css" href="admin/includes/print.css">
</head>

<body marginwidth="10" marginheight="10" topmargin="10" bottommargin="10" leftmargin="10" rightmargin="10">

<style>
    @page {
        margin: 20px;
    }

    body {
        font-family: Arial;
    }

    table {
        width: 90%;
        margin: 20px auto !important;
        border-collapse: collapse;
        font-size: 14px;
    }

    .logo {
        width: 200px;
    }

    .products td {
        padding: 15px;
        text-align: center;
    }

    .products tr {
        border: 1px solid #f5f5f5;
    }
</style>

<table>
    <tbody>
    <tr>
        <td>
            <div class="logo"><img style="width:200px" src="<?= HTTP_SERVER . "/" . LOGO_IMAGE ?>"></div>
        </td>
        <td align="right">
            <div class="logo_text"
                 style="font-size: 30px;background-color: #1b302f;color: white;padding: 5px 10px;font-weight: 100;text-align: center;min-width: 250px; width: 250px; display: block"><?= DOCUMENT_TITLE; ?></div>
        </td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 115px;"><?= ADDRESS_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $order->customer["company"] . ' ' . $order->customer["name"] ?><br>
            <?= $order->customer["street_address"] . ', ' . $order->customer["postcode"] ?><br>
            <?= $order->customer["country"] ?></td>
    </tr>
    <tr>
        <td style="width: 115px;"><?= TEL_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $order->customer["telephone"]; ?></td>
    </tr>
    <tr>
        <td style="width: 115px;"><?= TEL_FAX_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td><?= $order->customer["fax"] ?></td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td style="width: 115px;"><?= INVOICE_NUMBER_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= $oID ?></td>

        <td style="width: 115px;"><?= INVOICE_DATE_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= date('d-m-y', strtotime($order->info['date_purchased'])) ?></td>

        <td style="width: 115px;"><?= CONDITION_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= $order->info["payment_method"] ?></td>
    </tr>
    <tr>
        <td style="width: 115px;"><?= INVOICE_DEBITEUR_TITLE ?></td>
        <td style="width: 10px;">:</td>
        <td style="width: 115px;"><?= $customer_number["customers_id"] ?></td>
    </tr>
    </tbody>
</table>

<table class="products">
    <thead>
    <tr style="background-color: #333;border: 1px solid #333;">
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo TABLE_HEADING_PRODUCTS_QUANTITY; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px; text-align:left;"><?php echo DESCRIPTION_TITLE; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo WEIGHT_TITLE; ?></td>
        <td style="color: #fff;font-weight: normal;padding: 5px;"><?php echo REFERENCE_CUSTOMER_TITLE; ?></td>
    </tr>
    </thead>
    <tbody>
    <?php
    $totalWight = 0;
    for ($i = 0, $n = sizeof($order->products); $i < $n; $i++) {
        ?>
        <tr>
            <td><?php echo $order->products[$i]['qty'] ?></td>
            <td><?php echo $order->products[$i]['model'] ?></td>
            <td><?php echo $order->products[$i]['name'] ?></td>
            <td><?php echo $order->products[$i]["weight"] * $order->products[$i]['qty'] ?></td>
        </tr>
        <?php $totalWight += $order->products[$i]["weight"] * $order->products[$i]['qty'];
    }
    ?>
    </tbody>
</table>

<table style="width: 25%;display: inline-table;margin: 20px 20px 20px 68px !important;">
    <tr>
        <td><?= UNILEVER_INFO_TITLE ?> :</td>
    </tr>
    <tr>
        <td><?= TOTAL_WEIGHT_TITLE ?></td>
        <td><?= $totalWight ?> KG</td>
    </tr>
    <tr>
        <td><?= WAITING_PAYMENT_TITLE ?></td>
        <td><?= WAITING_PAYMENT ?></td>
    </tr>
    <tr>
        <td><?= RECEIVE_PAYMENT_TITLE ?></td>
        <td><?= RECEIVE_PAYMENT ?></td>
    </tr>
    <tr>
        <td><?= ULEVEN_TITLE ?></td>
        <td><?= ULEVEN ?></td>
    </tr>
</table>

<table style="width: 25%;display: inline-table;margin: 20px 20px 20px 68px !important;">
    <tr>
        <td> </td>
    </tr>
    <tr>
        <td><?= FOR_AGREEMENT ?><br/><br/></td>
    </tr>
    <tr>
        <td><?= NAME ?>:______________________________<br/><br/></td>
    </tr>
    <tr>
        <td><?= SIGNATURE ?>:___________________________<br/><br/></td>
    </tr>
</table>

<table>
    <tbody>
    <tr>
        <td><?= PAYMENT_CONDITION_TITLE ?> : <?= $order->info["payment_method"] ?></td>
    </tr>
    <tr>
        <td><?= PAYMENT_TERM_TITLE ?> : <?= PAYMENT_TERM ?></td>
    </tr>
    <tr>
        <td><?= CREDIT_LIMITATION_TITLE ?> : <?= CREDIT_LIMITATION ?></td>
    </tr>
    <tr>
        <td><?= COMPLAINTS ?></td>
    </tr>
    <tr>
        <td style="text-align: right"><?= INVOICE_CREATED ?></td>
    </tr>
    </tbody>
</table>

<table class="footer">
    <tr style="background-color: #0090b5;">
        <td style="padding: 15px;"></td>
        <td style="padding: 15px;"></td>
        <td style="padding: 15px;"></td>
        <td style="padding: 15px;"></td>
    </tr>
    <tbody>
    <tr>
        <td><?= FOOTER_BLOCK1 ?></td>
        <td><?= FOOTER_BLOCK2 ?></td>
        <td><?= FOOTER_PHONE ?><br/>
            <?= FOOTER_EMAIL ?><br/>
            <?= FOOTER_SITE ?></td>
        <td><?= FOOTER_CC ?><br/>
            <?= FOOTER_IBAN ?><br/>
            <?= FOOTER_BIC ?><br/>
            <?= FOOTER_VAT ?><br/></td>
    </tr>
    </tbody>
</table>

<br/>
<br/>
<br/>

</body>

</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
