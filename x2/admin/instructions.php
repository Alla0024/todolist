<?php

require_once __DIR__ . "/includes/application_top.php";
require_once __DIR__ . '/html-open.php';
require_once __DIR__ . '/header.php';
require_once __DIR__ . "/Documentation.html";
require_once __DIR__ . '/footer.php';
require_once __DIR__ . '/html-close.php';
require_once DIR_WS_INCLUDES . 'application_bottom.php';