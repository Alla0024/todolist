<?php
/*
  $Id: modules.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE_MODULES_PAYMENT', 'Moduły płatności');
define('HEADING_TITLE_MODULES_SHIPPING', 'Moduły dostawy');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Moduły zamówień');
define('TEXT_INSTALL_INTRO', 'Czy na pewno chcesz zainstalować ten moduł?');
define('TEXT_DELETE_INTRO', 'Czy na pewno chcesz usunąć ten moduł??');


define('TABLE_HEADING_MODULES', 'Moduły');
define('TABLE_HEADING_MODULE_DESCRIPTION', 'Opis');
define('TABLE_HEADING_SORT_ORDER', 'Kolejność sortowania');
define('TABLE_HEADING_ACTION', 'Działanie');
define('TEXT_MODULE_DIRECTORY', 'Katalog modułów:');
define('TEXT_CLOSE_BUTTON', 'Zamknij');

define('MODULE_PAYMENT_CC_STATUS_TITLE', 'Zezwalaj na moduł płatności Karta kredytowa');
define('MODULE_PAYMENT_CC_STATUS_DESC', 'Czy chcesz akceptować płatności kartami kredytowymi?');
define('MODULE_PAYMENT_CC_EMAIL_TITLE', 'Adres E-Mail');
define('MODULE_PAYMENT_CC_EMAIL_DESC', 'Jeśli podany jest adres e-mail, to średnia cyfra z numeru karty kredytowej zostanie wysłana na podany adres e-mail (w bazie danych zostanie zapisany pełny numer karty kredytowej, z wyjątkiem średnich cyfr)');
define('MODULE_PAYMENT_CC_ZONE_TITLE', 'Strefa');
define('MODULE_PAYMENT_CC_ZONE_DESC', 'Jeśli strefa jest wybrana, to ten moduł płatności będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_PAYMENT_CC_ORDER_STATUS_ID_TITLE', 'Status zamówienia');
define('MODULE_PAYMENT_CC_ORDER_STATUS_ID_DESC', 'Zamówienia przetworzone przy użyciu tego modułu płatności otrzymają określony status.');
define('MODULE_PAYMENT_CC_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_PAYMENT_CC_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_PAYMENT_COD_STATUS_TITLE', 'Zezwalaj na moduł płatności Płatność przy odbiorze');
define('MODULE_PAYMENT_COD_STATUS_DESC', 'Czy chcesz zezwolić na korzystanie z modułu podczas składania zamówień?');
define('MODULE_PAYMENT_COD_ZONE_TITLE', 'Strefa');
define('MODULE_PAYMENT_COD_ZONE_DESC', 'Jeśli strefa jest wybrana, to ten moduł płatności będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_TITLE', 'Status zamówienia');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_DESC', 'Zamówienia przetworzone przy użyciu tego modułu płatności otrzymają określony status.');
define('MODULE_PAYMENT_COD_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_PAYMENT_COD_SORT_ORDER_DESC', 'Kolejność sortowania modułu');

define('MODULE_PAYMENT_FREECHARGER_STATUS_TITLE', 'Zezwalaj na moduł Darmowe pobieranie');
define('MODULE_PAYMENT_FREECHARGER_STATUS_DESC', 'Czy chcesz zezwolić na moduł `Bezpłatne pobranie?');
define('MODULE_PAYMENT_FREECHARGER_ZONE_TITLE', 'Strefa');
define('MODULE_PAYMENT_FREECHARGER_ZONE_DESC', 'Jeśli strefa jest wybrana, to ten moduł płatności będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_PAYMENT_FREECHARGER_ORDER_STATUS_ID_TITLE', 'Status zamówienia');
define('MODULE_PAYMENT_FREECHARGER_ORDER_STATUS_ID_DESC', 'Zamówienia przetworzone przy użyciu tego modułu płatności otrzymają określony status.');
define('MODULE_PAYMENT_FREECHARGER_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_PAYMENT_FREECHARGER_SORT_ORDER_DESC', 'Kolejność sortowania modułu');

define('MODULE_PAYMENT_LIQPAY_STATUS_TITLE', 'Zezwalaj na moduł płatności LiqPAY');
define('MODULE_PAYMENT_LIQPAY_STATUS_DESC', 'Zezwalaj na moduł płatności LiqPAY?');
define('MODULE_PAYMENT_LIQPAY_ID_TITLE', 'ID Sprzedawcy');
define('MODULE_PAYMENT_LIQPAY_ID_DESC', 'Podaj swój numer identyfikacyjny (sprzedawca id).');
define('MODULE_PAYMENT_LIQPAY_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_PAYMENT_LIQPAY_SORT_ORDER_DESC', 'Kolejność sortowania modułów.');
define('MODULE_PAYMENT_LIQPAY_ZONE_TITLE', 'Strefa opłaty');
define('MODULE_PAYMENT_LIQPAY_ZONE_DESC', 'Jeśli strefa jest wybrana, to ten moduł płatności będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_PAYMENT_LIQPAY_SECRET_KEY_TITLE', 'Hasło sprzedawcy (podpis)');
define('MODULE_PAYMENT_LIQPAY_SECRET_KEY_DESC', 'W tej opcji określ hasło (podpis) określone w ustawieniach w serwisie LiqPAY.');
define('MODULE_PAYMENT_LIQPAY_ORDER_STATUS_ID_TITLE', 'Wprowadź opłacony status zamówienia');
define('MODULE_PAYMENT_LIQPAY_ORDER_STATUS_ID_DESC', 'Wprowadź opłacony status zamówienia');

define('MODULE_PAYMENT_LIQPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Ustaw domyślny status zamówienia');
define('MODULE_PAYMENT_LIQPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Ustaw domyślny status zamówienia');

define('MODULE_PAYMENT_BANK_TRANSFER_STATUS_TITLE', 'Przedpłata na rachunek');
define('MODULE_PAYMENT_BANK_TRANSFER_STATUS_DESC', 'Czy chcesz korzystać z modułu Przedpłata na koncie? 1 - tak, 0 - nie');
define('MODULE_PAYMENT_BANK_TRANSFER_1_TITLE', 'Nazwa banku');
define('MODULE_PAYMENT_BANK_TRANSFER_1_DESC', 'Wprowadź nazwę banku');
define('MODULE_PAYMENT_BANK_TRANSFER_2_TITLE', 'Rachunek bieżący');
define('MODULE_PAYMENT_BANK_TRANSFER_2_DESC', 'Wprowadź swój rachunek bieżący');
define('MODULE_PAYMENT_BANK_TRANSFER_3_TITLE', 'Kod identyfikacyjny banku');
define('MODULE_PAYMENT_BANK_TRANSFER_3_DESC', 'Wprowadź kod identyfikacyjny banku');
define('MODULE_PAYMENT_BANK_TRANSFER_4_TITLE', 'Konto korespondencyjne');
define('MODULE_PAYMENT_BANK_TRANSFER_4_DESC', 'Wprowadź konto korespondencyjne banku');
define('MODULE_PAYMENT_BANK_TRANSFER_5_TITLE', 'NIP');
define('MODULE_PAYMENT_BANK_TRANSFER_5_DESC', 'Wprowadź NIP');
define('MODULE_PAYMENT_BANK_TRANSFER_6_TITLE', 'Odbiorca');
define('MODULE_PAYMENT_BANK_TRANSFER_6_DESC', 'Odbiorca płatności');
define('MODULE_PAYMENT_BANK_TRANSFER_7_TITLE', 'Kod przyczyny oświadczenia');
define('MODULE_PAYMENT_BANK_TRANSFER_7_DESC', 'Wprowadź kod przyczyny oświadczenia ');
define('MODULE_PAYMENT_BANK_TRANSFER_8_TITLE', 'Cel płatności');
define('MODULE_PAYMENT_BANK_TRANSFER_8_DESC', 'Wprowadź cel płatności');
define('MODULE_PAYMENT_BANK_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_PAYMENT_BANK_SORT_ORDER_DESC', 'Kolejność sortowania modułu');

define('MODULE_PAYMENT_WEBMONEY_STATUS_TITLE', 'Płatność przez WebMoney');
define('MODULE_PAYMENT_WEBMONEY_STATUS_DESC', 'Czy chcesz korzystać z modułu płatności przez WebMoney? 1 - tak, 0 - nie');
define('MODULE_PAYMENT_WEBMONEY_1_TITLE', 'Twój identyfikator WM');
define('MODULE_PAYMENT_WEBMONEY_1_DESC', 'Wpisz swój identyfikator WM');
       define('MODULE_PAYMENT_WEBMONEY_2_TITLE', 'Numer twojego R portfelu');
define('MODULE_PAYMENT_WEBMONEY_2_DESC', 'Wprowadź numer twojego R portfelu');
define('MODULE_PAYMENT_WEBMONEY_3_TITLE', 'Numer twojego Z portfelu');
define('MODULE_PAYMENT_WEBMONEY_3_DESC', 'Wprowadź numer twojego Z portfelu');
define('MODULE_PAYMENT_WEBMONEY_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_PAYMENT_WEBMONEY_SORT_ORDER_DESC', 'Kolejność sortowania modułu');

// -----------------------SHIPPING!!!!!---------------------------//

       define('MODULE_SHIPPING_EXPRESS_STATUS_TITLE', 'Pozwól na moduł Dostawa kurierem');
define('MODULE_SHIPPING_EXPRESS_STATUS_DESC', 'Czy chcesz pozwolić na moduł Dostawa kurierem?');
define('MODULE_SHIPPING_EXPRESS_COST_TITLE', 'Koszt');
define('MODULE_SHIPPING_EXPRESS_COST_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_EXPRESS_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_EXPRESS_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_SHIPPING_EXPRESS_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_EXPRESS_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_EXPRESS_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_EXPRESS_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_FLAT_STATUS_TITLE', 'Pozwól na moduł Dostawa kurierem');
define('MODULE_SHIPPING_FLAT_STATUS_DESC', 'Czy chcesz pozwolić na moduł Dostawa kurierem?');
define('MODULE_SHIPPING_FLAT_COST_TITLE', 'Koszt');
define('MODULE_SHIPPING_FLAT_COST_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_SHIPPING_FLAT_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_FLAT_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_FREESHIPPER_STATUS_TITLE', 'Pozwól na moduł Dostawa bezpłatna');
define('MODULE_SHIPPING_FREESHIPPER_STATUS_DESC', 'Czy chcesz pozwolić na moduł Dostawa bezpłatna?');
define('MODULE_SHIPPING_FREESHIPPER_COST_TITLE','Koszt');
define('MODULE_SHIPPING_FREESHIPPER_COST_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_FREESHIPPER_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_FREESHIPPER_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_SHIPPING_FREESHIPPER_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_FREESHIPPER_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_FREESHIPPER_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_FREESHIPPER_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_ITEM_STATUS_TITLE', 'Pozwól na moduł Na jednostkę');
define('MODULE_SHIPPING_ITEM_STATUS_DESC', 'Czy chcesz pozwolić na moduł Na jednostkę');
define('MODULE_SHIPPING_ITEM_COST_TITLE', 'Koszt dostawy');
define('MODULE_SHIPPING_ITEM_COST_DESC', 'Koszt wysyłki zostanie pomnożony przez liczbę pozycji w zamówieniu.');
define('MODULE_SHIPPING_ITEM_HANDLING_TITLE', 'Koszt');
define('MODULE_SHIPPING_ITEM_HANDLING_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_SHIPPING_ITEM_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_ITEM_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_NWPOCHTA_STATUS_TITLE', 'Pozwól na moduł Nowa Poczta');
define('MODULE_SHIPPING_NWPOCHTA_STATUS_DESC', 'Czy chcesz pozwolić na moduł Nowa Poczta');
define('MODULE_SHIPPING_NWPOCHTA_CUSTOM_NAME_TITLE', 'Nazwa niestandardowego modułu');
define('MODULE_SHIPPING_NWPOCHTA_CUSTOM_NAME_DESC', 'Pozostaw puste, jeśli chcesz używać domyślnej nazwy modułu');
define('MODULE_SHIPPING_NWPOCHTA_COST_TITLE', 'Koszt');
define('MODULE_SHIPPING_NWPOCHTA_COST_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_NWPOCHTA_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_NWPOCHTA_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_SHIPPING_NWPOCHTA_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_NWPOCHTA_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_NWPOCHTA_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_NWPOCHTA_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_CUSTOMSHIPPER_STATUS_TITLE', 'Pozwól na moduł');
define('MODULE_SHIPPING_CUSTOMSHIPPER_NAME_TITLE', 'Nazwa modułu');
define('MODULE_SHIPPING_CUSTOMSHIPPER_WAY_TITLE', 'Opis');
define('MODULE_SHIPPING_CUSTOMSHIPPER_COST_TITLE', 'Koszt');
define('MODULE_SHIPPING_CUSTOMSHIPPER_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_CUSTOMSHIPPER_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_CUSTOMSHIPPER_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_CUSTOMSHIPPER_SORT_ORDER_TITLE', 'Kolejność sortowania');

define('MODULE_SHIPPING_PERCENT_STATUS_TITLE', 'Pozwól na moduł Dostawa procentowa');
define('MODULE_SHIPPING_PERCENT_STATUS_DESC', 'Czy chcesz pozwolić na moduł Dostawa procentowa?');
define('MODULE_SHIPPING_PERCENT_RATE_TITLE', 'Procent');
define('MODULE_SHIPPING_PERCENT_RATE_DESC', 'Koszt dostawy tego modułu jako procent całkowitej wartości zamówienia, wartości od .01 do .99');
define('MODULE_SHIPPING_PERCENT_LESS_THEN_TITLE', 'Koszt płaski dla zamówień do');
define('MODULE_SHIPPING_PERCENT_LESS_THEN_DESC', 'Płaski koszt wysyłki dla zamówień, jest kosztem do określonej wartości.');
define('MODULE_SHIPPING_PERCENT_FLAT_USE_TITLE', 'Płaski procentowy koszt');
define('MODULE_SHIPPING_PERCENT_FLAT_USE_DESC', 'Płaskie koszty wysyłki, jako procent całkowitej wartości zamówienia, ważne dla wszystkich zamówień.');
define('MODULE_SHIPPING_PERCENT_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_PERCENT_TAX_CLASS_DESC', 'Korzystać z podatku');
define('MODULE_SHIPPING_PERCENT_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_PERCENT_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_PERCENT_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_PERCENT_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_SAT_STATUS_TITLE', 'Pozwól na moduł Dostawa kurierem');
define('MODULE_SHIPPING_SAT_STATUS_DESC', 'Chcesz pozwolić na moduł Dostawa kurierem?');
define('MODULE_SHIPPING_SAT_COST_TITLE', 'Koszt');
define('MODULE_SHIPPING_SAT_COST_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_SAT_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_SAT_TAX_CLASS_DESC', 'Korzystaj z podatku.');
define('MODULE_SHIPPING_SAT_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_SAT_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_SAT_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_SAT_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_TABLE_STATUS_TITLE', 'Pozwól na moduł "Bez dostawy"');
define('MODULE_SHIPPING_TABLE_STATUS_DESC', 'Chcesz pozwolić na moduł "Bez dostawy" ');
define('MODULE_SHIPPING_TABLE_COST_TITLE', 'Koszt dostawy');
define('MODULE_SHIPPING_TABLE_COST_DESC', 'Koszt wysyłki jest obliczany na podstawie całkowitej masy zamówienia lub całkowitego kosztu zamówienia. Na przykład: 25: 8.50, 50: 5.50 itd. ... Oznacza to, że do 25 dostaw kosztuje 8,50, od 25 do 50 kosztuje 5,50, itd.');
define('MODULE_SHIPPING_TABLE_MODE_TITLE', 'Sposób obliczania');
define('MODULE_SHIPPING_TABLE_MODE_DESC', 'Koszt obliczenia dostawy w oparciu o całkowitą masę zamówienia (waga) lub na podstawie całkowitego kosztu zamówienia (cena).');
define('MODULE_SHIPPING_TABLE_HANDLING_TITLE', 'Koszt');
define('MODULE_SHIPPING_TABLE_HANDLING_DESC', 'Koszt korzystania z tego sposobu dostawy.');
define('MODULE_SHIPPING_TABLE_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_TABLE_TAX_CLASS_DESC', 'Korzystaj z podatku.');
define('MODULE_SHIPPING_TABLE_ZONE_TITLE', 'Strefa');
define('MODULE_SHIPPING_TABLE_ZONE_DESC', 'Jeśli wybrana jest strefa, ten moduł dostawy będzie widoczny tylko dla kupujących z wybranej strefy.');
define('MODULE_SHIPPING_TABLE_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_TABLE_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');

define('MODULE_SHIPPING_ZONES_STATUS_TITLE', 'Pozwól na moduł Stawki dla strefy');
define('MODULE_SHIPPING_ZONES_STATUS_DESC', 'Czy chcesz pozwolić na moduł Stawki dla strefy');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_SHIPPING_ZONES_COUNTRIES_1_TITLE', 'Kraje 1 strefy');
define('MODULE_SHIPPING_ZONES_COUNTRIES_1_DESC', 'Lista krajów przez przecinki dla strefy 1.');
define('MODULE_SHIPPING_ZONES_COST_1_TITLE', 'Koszt dostawy dla 1 strefy');
define('MODULE_SHIPPING_ZONES_COST_1_DESC', 'Koszt dostawy dla 1 strefy na podstawie maksymalnego kosztu zamówienia. Na przykład: 3: 8,50,7: 10,50, ... Oznacza to, że koszty dostawy dla zamówień ważących do 3 kg. będzie kosztować 8,50 dla kupujących z krajów pierwszej strefy.');
define('MODULE_SHIPPING_ZONES_HANDLING_1_TITLE', 'Koszt dla 1 strefy');
define('MODULE_SHIPPING_ZONES_HANDLING_1_DESC', 'Koszt korzystania z tego sposobu dostawy.');

// -----------------------ORDER TOTAL!!!!!---------------------------//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_STATUS_TITLE', 'Pozwól na moduł Powiązany rabat');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_STATUS_DESC', 'Czy chcesz pozwolić na moduł Powiązany rabat?');
define('MODULE_ORDER_TOTAL_OT_BETTER_TOGETHER_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_BETTER_TOGETHER_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_INC_TAX_TITLE', 'Include Tax');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_INC_TAX_DESC', 'Korzystać z podatku');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_CALC_TAX_TITLE', 'Re-calculate Tax');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_CALC_TAX_DESC', 'Przelicz podatek');

define('MODULE_ORDER_TOTAL_COUPON_STATUS_TITLE', 'Pokaż wszystko');
define('MODULE_ORDER_TOTAL_COUPON_STATUS_DESC', 'Czy chcesz wyświetlić wartość kuponu?');
define('MODULE_ORDER_TOTAL_OT_COUPON_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_COUPON_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_TITLE', 'Uwzględniać dostawę');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_DESC', 'Uwzględnij w obliczeniach dostawę.');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_TITLE', 'Uwzględnij podatek');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_DESC', 'Uwzględnij podatek w obliczeniach.');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_TITLE', 'Przelicz podatek');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_DESC', 'Przelicz podatek.');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_DESC', 'Użyj podatku dla kuponów.');

define('MODULE_ORDER_TOTAL_GV_STATUS_TITLE', 'Pokaż wszystko');
define('MODULE_ORDER_TOTAL_GV_STATUS_DESC', 'Czy chcesz wyświetlić wartość kuponu?');
define('MODULE_ORDER_TOTAL_OT_GV_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_GV_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_ORDER_TOTAL_GV_QUEUE_TITLE', 'Aktywacja karty podarunkowej');
define('MODULE_ORDER_TOTAL_GV_QUEUE_DESC', 'Czy chcesz ręcznie aktywować zakupione karty podarunkowe?');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_TITLE', 'Uwzględniać dostawę');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_DESC', 'Uwzględnić w obliczeniach dostawę.');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_TITLE', 'Uwzględnij podatek');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_DESC', 'Uwzględnij podatek w obliczeniach.');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_TITLE', 'Przelicz podatek');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_DESC', 'Przelicz podatek.');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_DESC', 'Korzystać z podatku.');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_TITLE', 'Podatek karty podarunkowej');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_DESC', 'Dodaj podatek do kupionych kart podarunkowych.');
define('MODULE_ORDER_TOTAL_GV_ORDER_STATUS_ID_TITLE', 'Status zamówienia');
define('MODULE_ORDER_TOTAL_GV_ORDER_STATUS_ID_DESC', 'Zamówienia złożone przy użyciu karty podarunkowej pokrywającej pełny koszt zamówienia będą miały wskazany status.');

define('MODULE_LEV_DISCOUNT_STATUS_TITLE', 'Pokaż rabat');
define('MODULE_LEV_DISCOUNT_STATUS_DESC', 'Zezwalać na rabaty');
define('MODULE_ORDER_TOTAL_OT_LEV_DISCOUNT_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_LEV_DISCOUNT_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_LEV_DISCOUNT_TABLE_TITLE', 'Procent rabatu');
define('MODULE_LEV_DISCOUNT_TABLE_DESC', 'Ustaw limity cenowe i procenty zniżki, oddzielając je przecinkiem.');
define('MODULE_LEV_DISCOUNT_INC_SHIPPING_TITLE', 'Uwzględniać dostawę');
define('MODULE_LEV_DISCOUNT_INC_SHIPPING_DESC', 'Uwzględnij w obliczeniach dostawę.');
define('MODULE_LEV_DISCOUNT_INC_TAX_TITLE', 'Uwzględnij podatek');
define('MODULE_LEV_DISCOUNT_INC_TAX_DESC', 'Uwzględnij podatek w obliczeniach.');
define('MODULE_LEV_DISCOUNT_CALC_TAX_TITLE', 'Przelicz podatek');
define('MODULE_LEV_DISCOUNT_CALC_TAX_DESC', 'Przelicz podatek.');

define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_TITLE', 'Pokaż niską wartość zamówienia');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_DESC', 'Czy chcesz pokazać niski koszt zamówienia?');
define('MODULE_ORDER_TOTAL_OT_LOWORDERFEE_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_LOWORDERFEE_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_TITLE', 'Zezwalaj na niskie koszty zamówienia');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_DESC', 'Czy chcesz pozwolić na moduł niskiego kosztu zamówienia?');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_TITLE', 'Niski koszt zamówień do');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_DESC', 'Niska wartość zamówienia dla zamówień do określonej wartości.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_TITLE', 'Opłata');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_DESC', 'Opłata');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_TITLE', 'Dodaj opłatę do zamówienia');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_DESC', 'Dodaj opłatę do następujących zamówień.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_TITLE', 'Podatek');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_DESC', 'Korzystać z podatku.');

define('MODULE_PAYMENT_DISC_STATUS_TITLE', 'Pozwól na moduł');
define('MODULE_PAYMENT_DISC_STATUS_DESC', 'Aktywować moduł?');
define('MODULE_ORDER_TOTAL_OT_PAYMENT_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_PAYMENT_SORT_ORDER_DESC', 'Porządek sortowania modułu musi być koniecznie niższy niż moduł Total.');
define('MODULE_PAYMENT_DISC_PERCENTAGE_TITLE', 'Rabat');
define('MODULE_PAYMENT_DISC_PERCENTAGE_DESC', 'Minimalna kwota zamówienia, aby otrzymać rabat.');
define('MODULE_PAYMENT_DISC_MINIMUM_TITLE', 'Minimalna kwota zamówienia');
define('MODULE_PAYMENT_DISC_MINIMUM_DESC', 'Minimalna kwota zamówienia, aby otrzymać rabat.');
define('MODULE_PAYMENT_DISC_TYPE_TITLE', 'Sposób płatności');
define('MODULE_PAYMENT_DISC_TYPE_DESC', 'Tutaj musisz podać nazwę klasy modułu płatności, klasa znajduje się na przykład w pliku modułu /includes/modules/payment/webmoney.php. Od góry jest widoczny class webmoney, oznacza, że jeśli chcemy dać zniżkę przy płatności za pośrednictwem WebMoney, piszemy webmoney.');
define('MODULE_PAYMENT_DISC_INC_SHIPPING_TITLE', 'Uwzględniać dostawę');
define('MODULE_PAYMENT_DISC_INC_SHIPPING_DESC', 'Uwzględnij koszty wysyłki w obliczeniach');
define('MODULE_PAYMENT_DISC_INC_TAX_TITLE', 'Uwzględnij podatek');
define('MODULE_PAYMENT_DISC_INC_TAX_DESC', 'Uwzględnij podatek w obliczeniach.');
define('MODULE_PAYMENT_DISC_CALC_TAX_TITLE', 'Oblicz podatek');
define('MODULE_PAYMENT_DISC_CALC_TAX_DESC', 'Uwzględnij podatek przy obliczaniu rabatów.');

define('MODULE_QTY_DISCOUNT_STATUS_TITLE', 'Pokaż rabat na ilość');
define('MODULE_QTY_DISCOUNT_STATUS_DESC', 'Czy chcesz zezwolić na rabaty od ilości?');
define('MODULE_ORDER_TOTAL_OT_QTY_DISCOUNT_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_QTY_DISCOUNT_SORT_ORDER_DESC', 'Kolejność sortowania modułu.');
define('MODULE_QTY_DISCOUNT_RATE_TYPE_TITLE', 'Rodzaj rabatu');
define('MODULE_QTY_DISCOUNT_RATE_TYPE_DESC', 'Wybierz rodzaj rabatu - procent (percentage) lub płaska (flat rate)');
define('MODULE_QTY_DISCOUNT_RATES_TITLE', 'Rabat');
define('MODULE_QTY_DISCOUNT_RATES_DESC', 'Rabat jest obliczany na podstawie całkowitej liczby zamówionych pozycji. Na przykład: 10: 5.20: 10 ... i tak dalej. Oznacza to, że zamawiając 10 lub więcej sztuk towarów, kupujący otrzymuje rabat w wysokości 5% lub 5 USD; 20 lub więcej jednostek - 10% rabatu lub 10 USD; w zależności od typu');
define('MODULE_QTY_DISCOUNT_INC_SHIPPING_TITLE', 'Uwzględniać dostawę');
define('MODULE_QTY_DISCOUNT_INC_SHIPPING_DESC', 'Uwzględnić w obliczeniach dostawę.');
define('MODULE_QTY_DISCOUNT_INC_TAX_TITLE', 'Uwzględnij podatek');
define('MODULE_QTY_DISCOUNT_INC_TAX_DESC', 'Uwzględnij podatek w obliczeniach.');
define('MODULE_QTY_DISCOUNT_CALC_TAX_TITLE', 'Przelicz podatek');
define('MODULE_QTY_DISCOUNT_CALC_TAX_DESC', 'Przelicz podatek.');

define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_TITLE', 'Pokaż dostawę');
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_DESC', 'Czy chcesz pokazać koszt dostawy?');
define('MODULE_ORDER_TOTAL_OT_SHIPPING_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_SHIPPING_SORT_ORDER_DESC', 'Kolejność  sortowania modułu.');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_TITLE', 'Pozwól na bezpłatną dostawę');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_DESC', 'Chcesz umożliwić bezpłatną dostawę?');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_TITLE', 'Bezpłatna wysyłka dla zamówień powyżej');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_DESC', 'W przypadku zamówień przekraczających podaną wartość dostawa będzie bezpłatna.');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_TITLE', 'Bezpłatna dostawa przy zamówieniach');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_DESC', 'Określ, dla których konkretnych zamówień darmowa dostawa będzie działać.');

define('MODULE_ORDER_TOTAL_SUBTOTAL_STATUS_TITLE', 'Pokaż koszt produktu');
define('MODULE_ORDER_TOTAL_SUBTOTAL_STATUS_DESC', 'Chcesz pokazać koszt produktu?');
define('MODULE_ORDER_TOTAL_OT_SUBTOTAL_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_SUBTOTAL_SORT_ORDER_DESC', 'Kolejność  sortowania modułu.');

define('MODULE_ORDER_TOTAL_TAX_STATUS_TITLE', 'Pokaż podatek');
define('MODULE_ORDER_TOTAL_TAX_STATUS_DESC', 'Czy chcesz pokazać podatek?');
define('MODULE_ORDER_TOTAL_OT_TAX_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_TAX_SORT_ORDER_DESC', 'Kolejność  sortowania modułu.');

define('MODULE_ORDER_TOTAL_TOTAL_STATUS_TITLE', 'Pokaż wszystko');
define('MODULE_ORDER_TOTAL_TOTAL_STATUS_DESC', 'Czy chcesz pokazać całkowity koszt zamówienia?');
define('MODULE_ORDER_TOTAL_OT_TOTAL_SORT_ORDER_TITLE', 'Kolejność sortowania');
define('MODULE_ORDER_TOTAL_OT_TOTAL_SORT_ORDER_DESC', 'Kolejność  sortowania modułu.');

?>
