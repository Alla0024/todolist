<?php
define('IMPORTEXPORT_TAB_IMPORT', 'Importar');
define('IMPORTEXPORT_TAB_EXPORT', 'Exportar');
define('IMPORTEXPORT_ORDER_OWN_VARIANT', 'Ordene su opción');
define('IMPORTEXPORT_UNDER_ORDER', 'En orden');
define('IMPORTEXPORT_IF_INTERESTED', 'Si está interesado en <span>exportar</span> a un servicio que no figura en la lista anterior');
define('IMPORTEXPORT_CONTACT_US', 'contáctenos');
define('COPY_BUTTON', 'Copiar');
define('COPY_TEXT', 'Copiado');
define('OSC_IMPORT_BLOCKED_TEXT', 'La migración solo está disponible en su alojamiento');