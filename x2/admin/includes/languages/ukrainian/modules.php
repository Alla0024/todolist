<?php
/*
  $Id: modules.php,v 1.2 2003/09/24 13:57:08 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE_MODULES_PAYMENT', 'Модулі Оплати');
define('HEADING_TITLE_MODULES_SHIPPING', 'Модулі Доставки');
define('HEADING_TITLE_MODULES_ORDER_TOTAL', 'Модулі Замовлень');
define('TEXT_INSTALL_INTRO', 'Ви дійсно хочете встановити цей модуль?');
define('TEXT_DELETE_INTRO', 'Ви дійсно хочете видалити цей модуль?');


define('TABLE_HEADING_MODULES', 'Модулі');
define('TABLE_HEADING_MODULE_DESCRIPTION', 'Опис');
define('TABLE_HEADING_SORT_ORDER', 'Порядок Сортування');
define('TABLE_HEADING_ACTION', 'Дія');
define('TEXT_MODULE_DIRECTORY', 'Директорія модулів:');
define('TEXT_CLOSE_BUTTON', 'Закрити');

define('MODULE_PAYMENT_CC_STATUS_TITLE', 'Дозволити модуль оплати кредитна картка');
define('MODULE_PAYMENT_CC_STATUS_DESC', 'Чи хочете Ви приймати платежі за допомогою кредитних карток?');
define('MODULE_PAYMENT_CC_EMAIL_TITLE', 'E-Mail Адреса');
define('MODULE_PAYMENT_CC_EMAIL_DESC', 'Якщо вказано e-mail адрес, то на вказаний e-mail адрес будуть відправлятися середні цифри з номера кредитної картки (в базі даних буде зберігатися повний номер кредитної картки, за винятком даних середніх цифр)');
define('MODULE_PAYMENT_CC_ZONE_TITLE', 'Зона');
define('MODULE_PAYMENT_CC_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_PAYMENT_CC_ORDER_STATUS_ID_TITLE', 'Статус замовлення');
define('MODULE_PAYMENT_CC_ORDER_STATUS_ID_DESC', 'Замовлення, оформлені з використанням даного модуля оплати прийматимуть вказаний статус.');
define('MODULE_PAYMENT_CC_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_CC_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_PAYMENT_COD_STATUS_TITLE', 'Дозволити модуль оплати Готівкою при отриманні');
define('MODULE_PAYMENT_COD_STATUS_DESC', 'Ви бажаєте дозволити використання модуля при оформленні замовлень?');
define('MODULE_PAYMENT_COD_ZONE_TITLE', 'Зона');
define('MODULE_PAYMENT_COD_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_TITLE', 'Статус замовлення');
define('MODULE_PAYMENT_COD_ORDER_STATUS_ID_DESC', 'Замовлення, оформлені з використанням даного модуля оплати прийматимуть вказаний статус.');
define('MODULE_PAYMENT_COD_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_COD_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_PAYMENT_FREECHARGER_STATUS_TITLE', 'Дозволити модуль Безкоштовне завантаження');
define('MODULE_PAYMENT_FREECHARGER_STATUS_DESC', 'Ви бажаєте дозволити модуль безкоштовне завантаження?');
define('MODULE_PAYMENT_FREECHARGER_ZONE_TITLE', 'Зона');
define('MODULE_PAYMENT_FREECHARGER_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_PAYMENT_FREECHARGER_ORDER_STATUS_ID_TITLE', 'Статус замовлення');
define('MODULE_PAYMENT_FREECHARGER_ORDER_STATUS_ID_DESC', 'Замовлення, оформлені з використанням даного модуля оплати прийматимуть вказаний статус.');
define('MODULE_PAYMENT_FREECHARGER_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_FREECHARGER_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_PAYMENT_LIQPAY_STATUS_TITLE', 'Дозволити модуль оплати LiqPAY');
define('MODULE_PAYMENT_LIQPAY_STATUS_DESC', 'Дозволити модуль оплати LiqPAY?');
define('MODULE_PAYMENT_LIQPAY_ID_TITLE', 'Мерчант ID');
define('MODULE_PAYMENT_LIQPAY_ID_DESC', 'Вкажіть Ваш ідентіфікаціонниий номер (мерчант id).');
define('MODULE_PAYMENT_LIQPAY_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_LIQPAY_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_PAYMENT_LIQPAY_ZONE_TITLE', 'Зона оплати');
define('MODULE_PAYMENT_LIQPAY_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_PAYMENT_LIQPAY_SECRET_KEY_TITLE', 'Мерчант пароль (підпис)');
define('MODULE_PAYMENT_LIQPAY_SECRET_KEY_DESC', 'У даній опції вкажіть пароль (підпис), зазначений в настройках на сайті LiqPAY.');
define('MODULE_PAYMENT_LIQPAY_ORDER_STATUS_ID_TITLE', 'Вкажіть оплачений статус замовлення');
define('MODULE_PAYMENT_LIQPAY_ORDER_STATUS_ID_DESC', 'Вкажіть оплачений статус замовлення');

define('MODULE_PAYMENT_LIQPAY_DEFAULT_ORDER_STATUS_ID_TITLE', 'Вкажіть статус замовлення замовчуванням');
define('MODULE_PAYMENT_LIQPAY_DEFAULT_ORDER_STATUS_ID_DESC', 'Вкажіть статус замовлення замовчуванням');


define('MODULE_PAYMENT_RUS_BANK_STATUS_TITLE', ''); // Безготівковий розрахунок
define('MODULE_PAYMENT_RUS_BANK_STATUS_DESC', 'Ви бажаєте використовувати модуль "Безготівковий розрахунок"? 1 - так, 0 - ні');
define('MODULE_PAYMENT_RUS_BANK_1_TITLE', 'Отримувач');
define('MODULE_PAYMENT_RUS_BANK_1_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_2_TITLE', 'ЄДРПОУ/ІПН');
define('MODULE_PAYMENT_RUS_BANK_2_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_3_TITLE', 'Назва банку');
define('MODULE_PAYMENT_RUS_BANK_3_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_4_TITLE', 'Номер рахунку/карти');
define('MODULE_PAYMENT_RUS_BANK_4_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_5_TITLE', 'МФО банку');
define('MODULE_PAYMENT_RUS_BANK_5_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_6_TITLE', 'Призначення платежу');
define('MODULE_PAYMENT_RUS_BANK_6_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_7_TITLE', 'додаткове поле 1');
define('MODULE_PAYMENT_RUS_BANK_7_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_8_TITLE', 'додаткове поле 2');
define('MODULE_PAYMENT_RUS_BANK_8_DESC', '');
define('MODULE_PAYMENT_RUS_BANK_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_RUS_BANK_SORT_ORDER_DESC', '');

define('MODULE_PAYMENT_WEBMONEY_STATUS_TITLE', 'Оплата через систему WebMoney');
define('MODULE_PAYMENT_WEBMONEY_STATUS_DESC', 'Ви бажаєте використовувати модуль Оплата через систему WebMoney? 1 - так, 0 - ні');
define('MODULE_PAYMENT_WEBMONEY_1_TITLE', 'Ваш WM Ідентифікатор');
define('MODULE_PAYMENT_WEBMONEY_1_DESC', 'Введіть Ваш WM ідентифікатор');
define('MODULE_PAYMENT_WEBMONEY_2_TITLE', 'Номер Вашого R гаманця');
define('MODULE_PAYMENT_WEBMONEY_2_DESC', 'Введіть номер Вашого R гаманця');
define('MODULE_PAYMENT_WEBMONEY_3_TITLE', 'Номер Вашого Z гаманця');
define('MODULE_PAYMENT_WEBMONEY_3_DESC', 'Введіть номер Вашого Z гаманця');
define('MODULE_PAYMENT_WEBMONEY_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_PAYMENT_WEBMONEY_SORT_ORDER_DESC', 'Порядок сортування модуля');

// -----------------------SHIPPING!!!!!---------------------------//

define('MODULE_SHIPPING_EXPRESS_STATUS_TITLE', 'Дозволити модуль кур\'єрська доставка');
define('MODULE_SHIPPING_EXPRESS_STATUS_DESC', 'Ви бажаєте дозволити модуль кур\'єрська доставка?');
define('MODULE_SHIPPING_EXPRESS_COST_TITLE', 'Вартість');
define('MODULE_SHIPPING_EXPRESS_COST_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_EXPRESS_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_EXPRESS_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_EXPRESS_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_EXPRESS_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_EXPRESS_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_EXPRESS_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_FLAT_STATUS_TITLE', 'Дозволити модуль кур\'єрська доставка');
define('MODULE_SHIPPING_FLAT_STATUS_DESC', 'Ви бажаєте дозволити модуль кур\'єрська доставка?');
define('MODULE_SHIPPING_FLAT_COST_TITLE', 'Вартість');
define('MODULE_SHIPPING_FLAT_COST_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_FLAT_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_FLAT_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_FLAT_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_FLAT_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_FREESHIPPER_STATUS_TITLE', 'Дозволити безкоштовну доставку');
define('MODULE_SHIPPING_FREESHIPPER_STATUS_DESC', 'Ви бажаєте дозволити модуль безкоштовна доставка?');
define('MODULE_SHIPPING_FREESHIPPER_COST_TITLE', 'Вартість');
define('MODULE_SHIPPING_FREESHIPPER_COST_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_FREESHIPPER_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_FREESHIPPER_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_FREESHIPPER_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_FREESHIPPER_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_FREESHIPPER_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_FREESHIPPER_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_ITEM_STATUS_TITLE', 'Дозволити модуль на одиницю');
define('MODULE_SHIPPING_ITEM_STATUS_DESC', 'Ви бажаєте дозволити модуль на одиницю?');
define('MODULE_SHIPPING_ITEM_COST_TITLE', 'Вартість доставки');
define('MODULE_SHIPPING_ITEM_COST_DESC', 'Вартість доставки буде помножена на кількість одиниць товару в замовленні.');
define('MODULE_SHIPPING_ITEM_HANDLING_TITLE', 'Вартість');
define('MODULE_SHIPPING_ITEM_HANDLING_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_ITEM_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_ITEM_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_ITEM_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_ITEM_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_NWPOCHTA_STATUS_TITLE', 'Дозволити модуль Нова Пошта');
define('MODULE_SHIPPING_NWPOCHTA_STATUS_DESC', 'Ви бажаєте дозволити модуль Нова Пошта?');
define('MODULE_SHIPPING_NWPOCHTA_CUSTOM_NAME_TITLE', 'Спец. Назва');
define('MODULE_SHIPPING_NWPOCHTA_CUSTOM_NAME_DESC', 'Залиште поле порожнім якщо хочете використовувати назву за замовчуванням');
define('MODULE_SHIPPING_NWPOCHTA_COST_TITLE', 'Вартість');
define('MODULE_SHIPPING_NWPOCHTA_COST_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_NWPOCHTA_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_NWPOCHTA_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_NWPOCHTA_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_NWPOCHTA_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_NWPOCHTA_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_NWPOCHTA_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_CUSTOMSHIPPER_STATUS_TITLE', 'Дозволити модуль кур\'єрська доставка');   
define('MODULE_SHIPPING_CUSTOMSHIPPER_NAME_TITLE', 'Назва перевізника');
define('MODULE_SHIPPING_CUSTOMSHIPPER_WAY_TITLE', 'Опис перевізника');
define('MODULE_SHIPPING_CUSTOMSHIPPER_COST_TITLE', 'Вартість');
define('MODULE_SHIPPING_CUSTOMSHIPPER_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_CUSTOMSHIPPER_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_CUSTOMSHIPPER_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_CUSTOMSHIPPER_SORT_ORDER_TITLE', 'Порядок сортування');

define('MODULE_SHIPPING_PERCENT_STATUS_TITLE', 'Дозволити модуль процентна доставка');
define('MODULE_SHIPPING_PERCENT_STATUS_DESC', 'Ви бажаєте дозволити модуль процентна доставка?');
define('MODULE_SHIPPING_PERCENT_RATE_TITLE', 'Відсоток');
define('MODULE_SHIPPING_PERCENT_RATE_DESC', 'Вартість доставки даними модулем у відсотках від загальної вартості замовлення, значення від .01 до .99');
define('MODULE_SHIPPING_PERCENT_LESS_THEN_TITLE', 'Пряма вартість для замовлень до');
define('MODULE_SHIPPING_PERCENT_LESS_THEN_DESC', 'Пряма вартість доставки для замовлень, вартістю до зазначеного розміру.');
define('MODULE_SHIPPING_PERCENT_FLAT_USE_TITLE', 'Пряма процентна вартість');
define('MODULE_SHIPPING_PERCENT_FLAT_USE_DESC', 'Пряма вартість доставки в процентах від загальної вартості замовлення, дійсно для всіх замовлень.');
define('MODULE_SHIPPING_PERCENT_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_PERCENT_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_PERCENT_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_PERCENT_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_PERCENT_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_PERCENT_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_SAT_STATUS_TITLE', 'Дозволити модуль кур\'єрська доставка');
define('MODULE_SHIPPING_SAT_STATUS_DESC', 'Ви бажаєте дозволити модуль кур\'єрська доставка?');
define('MODULE_SHIPPING_SAT_COST_TITLE', 'Вартість');
define('MODULE_SHIPPING_SAT_COST_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_SAT_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_SAT_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_SAT_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_SAT_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_SAT_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_SAT_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_TABLE_STATUS_TITLE', 'Разрешить модуль "Без доставки"');
define('MODULE_SHIPPING_TABLE_STATUS_DESC', 'Вы хотите разрешить модуль доставки "Без доставки"?');
define('MODULE_SHIPPING_TABLE_COST_TITLE', 'Вартість доставки');
define('MODULE_SHIPPING_TABLE_COST_DESC', 'Вартість доставки розраховується на основі загальної ваги замовлення або загальної вартості замовлення. Наприклад: 25:8.50,50:5.50, і т.д ... Це означає, що до 25 доставка буде коштувати 8.50, від 25 до 50 буде коштувати 5.50 і т.д.');
define('MODULE_SHIPPING_TABLE_MODE_TITLE', 'Метод розрахунку');
define('MODULE_SHIPPING_TABLE_MODE_DESC', 'Вартість розрахунку доставки виходячи з загальної ваги замовлення (weight) або виходячи із загальної вартості замовлення (price).');
define('MODULE_SHIPPING_TABLE_HANDLING_TITLE', 'Вартість');
define('MODULE_SHIPPING_TABLE_HANDLING_DESC', 'Вартість використання даного способу доставки.');
define('MODULE_SHIPPING_TABLE_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_TABLE_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_TABLE_ZONE_TITLE', 'Зона');
define('MODULE_SHIPPING_TABLE_ZONE_DESC', 'Якщо обрана зона, то даний модуль оплати буде видно тільки покупцям з обраної зони.');
define('MODULE_SHIPPING_TABLE_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_TABLE_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_SHIPPING_ZONES_STATUS_TITLE', 'Дозволити модуль тарифи для зони');
define('MODULE_SHIPPING_ZONES_STATUS_DESC', 'Ви бажаєте дозволити модуль тарифи для зони?');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_TITLE', 'Податок');
define('MODULE_SHIPPING_ZONES_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_SHIPPING_ZONES_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_SHIPPING_ZONES_COUNTRIES_1_TITLE', 'Країни 1 зони');
define('MODULE_SHIPPING_ZONES_COUNTRIES_1_DESC', 'Список країн через кому для зони 1.');
define('MODULE_SHIPPING_ZONES_COST_1_TITLE', 'Вартість доставки для 1 зони');
define('MODULE_SHIPPING_ZONES_COST_1_DESC', 'Вартість доставки через кому для зони 1 на базі максимальної вартість замовлення. Наприклад: 3:8.50,7:10.50, ... Це означає, що вартість доставки для замовлень, вагою до 3 кг. буде коштувати 8.50 для покупців з країн 1 зони.');
define('MODULE_SHIPPING_ZONES_HANDLING_1_TITLE', 'Вартість для 1 зони');
define('MODULE_SHIPPING_ZONES_HANDLING_1_DESC', 'Вартість використання даного способу доставки.');

// -----------------------ORDER TOTAL!!!!!---------------------------//

define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_STATUS_TITLE', 'Дозволити модуль Супутня знижка');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_STATUS_DESC', 'Ви бажаєте дозволити модуль Супутня знижка?');
define('MODULE_ORDER_TOTAL_OT_BETTER_TOGETHER_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_BETTER_TOGETHER_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_INC_TAX_TITLE', 'Include Tax');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_INC_TAX_DESC', 'Використовувати податок');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_CALC_TAX_TITLE', 'Re-calculate Tax');
define('MODULE_ORDER_TOTAL_BETTER_TOGETHER_CALC_TAX_DESC', 'Перераховувати податок');

define('MODULE_ORDER_TOTAL_COUPON_STATUS_TITLE', 'Показувати всього');
define('MODULE_ORDER_TOTAL_COUPON_STATUS_DESC', 'Ви бажаєте показувати номінал купона?');
define('MODULE_ORDER_TOTAL_OT_COUPON_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_COUPON_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_TITLE', 'Враховувати доставку');
define('MODULE_ORDER_TOTAL_COUPON_INC_SHIPPING_DESC', 'Включати в розрахунок доставку.');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_TITLE', 'Враховувати податок');
define('MODULE_ORDER_TOTAL_COUPON_INC_TAX_DESC', 'Включати в розрахунок податок.');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_TITLE', 'Перераховувати податок');
define('MODULE_ORDER_TOTAL_COUPON_CALC_TAX_DESC', 'Перераховувати податок.');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_TITLE', 'Податок');
define('MODULE_ORDER_TOTAL_COUPON_TAX_CLASS_DESC', 'Використовувати податок для купонів.');

define('MODULE_ORDER_TOTAL_GV_STATUS_TITLE', 'Показувати всього');
define('MODULE_ORDER_TOTAL_GV_STATUS_DESC', 'Ви бажаєте показувати номінал подарункового сертифіката?');
define('MODULE_ORDER_TOTAL_OT_GV_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_GV_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_ORDER_TOTAL_GV_QUEUE_TITLE', 'Активація сертифікатів');
define('MODULE_ORDER_TOTAL_GV_QUEUE_DESC', 'Ви бажаєте вручну активувати куплені подарункові сертифікати?');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_TITLE', 'Враховувати доставку');
define('MODULE_ORDER_TOTAL_GV_INC_SHIPPING_DESC', 'Включати в розрахунок доставку.');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_TITLE', 'Враховувати податок');
define('MODULE_ORDER_TOTAL_GV_INC_TAX_DESC', 'Включати в розрахунок податок.');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_TITLE', 'Перераховувати податок');
define('MODULE_ORDER_TOTAL_GV_CALC_TAX_DESC', 'Перераховувати податок.');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_TITLE', 'Податок');
define('MODULE_ORDER_TOTAL_GV_TAX_CLASS_DESC', 'Використовувати податок.');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_TITLE', 'Податок сертифіката');
define('MODULE_ORDER_TOTAL_GV_CREDIT_TAX_DESC', 'Додавати податок до до придбаних подарункових сертифікатів.');
define('MODULE_ORDER_TOTAL_GV_ORDER_STATUS_ID_TITLE', 'Статус замовлення');
define('MODULE_ORDER_TOTAL_GV_ORDER_STATUS_ID_DESC', 'Заказы, оформленные с использованием подарочного сертификата, покрывающего полную стоимость заказа, будут иметь указанный статус.');

define('MODULE_LEV_DISCOUNT_STATUS_TITLE', 'Показувати знижку');
define('MODULE_LEV_DISCOUNT_STATUS_DESC', 'Дозволити знижки?');
define('MODULE_ORDER_TOTAL_OT_LEV_DISCOUNT_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_LEV_DISCOUNT_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_LEV_DISCOUNT_TABLE_TITLE', 'Відсоток знижки');
define('MODULE_LEV_DISCOUNT_TABLE_DESC', 'Встановіть цінові межі і відсотки знижки, через кому.');
define('MODULE_LEV_DISCOUNT_INC_SHIPPING_TITLE', 'Враховувати доставку');
define('MODULE_LEV_DISCOUNT_INC_SHIPPING_DESC', 'Включати в розрахунок доставку.');
define('MODULE_LEV_DISCOUNT_INC_TAX_TITLE', 'Враховувати податок');
define('MODULE_LEV_DISCOUNT_INC_TAX_DESC', 'Включати в розрахунок податок.');
define('MODULE_LEV_DISCOUNT_CALC_TAX_TITLE', 'Перераховувати податок');
define('MODULE_LEV_DISCOUNT_CALC_TAX_DESC', 'Перераховувати податок.');

define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_TITLE', 'Показувати нижчу вартість замовлення');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_STATUS_DESC', 'Ви хочете показати нижчу вартість замовлення?');
define('MODULE_ORDER_TOTAL_OT_LOWORDERFEE_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_LOWORDERFEE_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_TITLE', 'Дозволити нижчу вартість замовлення');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_LOW_ORDER_FEE_DESC', 'Ви бажаєте дозволити модуль низької вартість замовлення?');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_TITLE', 'Низька вартість для замовлень до');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_ORDER_UNDER_DESC', 'Низька вартість замовлень для замовлень до зазначеного розміру.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_TITLE', 'Плата');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_FEE_DESC', 'Плата');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_TITLE', 'Додавати плату до замовлення');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_DESTINATION_DESC', 'Додавати плату до наступних замовленнях.');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_TITLE', 'Податок');
define('MODULE_ORDER_TOTAL_LOWORDERFEE_TAX_CLASS_DESC', 'Використовувати податок.');

define('MODULE_PAYMENT_DISC_STATUS_TITLE', 'Дозволити модуль');
define('MODULE_PAYMENT_DISC_STATUS_DESC', 'Активувати модуль?');
define('MODULE_ORDER_TOTAL_OT_PAYMENT_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_PAYMENT_SORT_ORDER_DESC', 'Порядок сортування модуля обов\'язково має бути нижче ніж модуль Всього.');
define('MODULE_PAYMENT_DISC_PERCENTAGE_TITLE', 'Знижка');
define('MODULE_PAYMENT_DISC_PERCENTAGE_DESC', 'Мінімальна сума замовлення для отримання знижки.');
define('MODULE_PAYMENT_DISC_MINIMUM_TITLE', 'Мінімальна сума замовлення');
define('MODULE_PAYMENT_DISC_MINIMUM_DESC', 'Мінімальна сума замовлення для отримання знижки.');
define('MODULE_PAYMENT_DISC_TYPE_TITLE', 'Спосіб оплати');
define('MODULE_PAYMENT_DISC_TYPE_DESC', 'Тут потрібно вказати назву класу модуля оплати, клас можо дізнатися в файлі модуля, наприклад /includes/modules/payment/webmoney.php. Зверху видно class webmoney, значить якщо ми хочемо дати знижку при оплаті через WebMoney, пишемо webmoney.');
define('MODULE_PAYMENT_DISC_INC_SHIPPING_TITLE', 'Враховувати доставку');
define('MODULE_PAYMENT_DISC_INC_SHIPPING_DESC', 'Включати доставку в розрахунки');
define('MODULE_PAYMENT_DISC_INC_TAX_TITLE', 'Враховувати податок');
define('MODULE_PAYMENT_DISC_INC_TAX_DESC', 'Включати податок в розрахунки.');
define('MODULE_PAYMENT_DISC_CALC_TAX_TITLE', 'Рахувати податок');
define('MODULE_PAYMENT_DISC_CALC_TAX_DESC', 'Враховувати податок при підрахунку знижки.');

define('MODULE_QTY_DISCOUNT_STATUS_TITLE', 'Показувати знижку від кількості');
define('MODULE_QTY_DISCOUNT_STATUS_DESC', 'Ви бажаєте дозволити знижки від кількості?');
define('MODULE_ORDER_TOTAL_OT_QTY_DISCOUNT_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_QTY_DISCOUNT_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_QTY_DISCOUNT_RATE_TYPE_TITLE', 'Тип знижки');
define('MODULE_QTY_DISCOUNT_RATE_TYPE_DESC', 'Виберіть тип знижки - процентна (percentage) або пряма (flat rate)');
define('MODULE_QTY_DISCOUNT_RATES_TITLE', 'Знижка');
define('MODULE_QTY_DISCOUNT_RATES_DESC', 'Знижка вважається виходячи із загальної кількості замовлених одиниць товару. Наприклад: 10:5,20:10 ... і т.д. Це означає, що замовивши 10 або більше одиниць товару, покупець отримує знижку 5% або $5; 20 або більше одиниць - знижка 10% або $10; в залежності від типу');
define('MODULE_QTY_DISCOUNT_INC_SHIPPING_TITLE', 'Враховувати доставку');
define('MODULE_QTY_DISCOUNT_INC_SHIPPING_DESC', 'Включати в розрахунок доставку.');
define('MODULE_QTY_DISCOUNT_INC_TAX_TITLE', 'Враховувати податок');
define('MODULE_QTY_DISCOUNT_INC_TAX_DESC', 'Включати в розрахунок податок.');
define('MODULE_QTY_DISCOUNT_CALC_TAX_TITLE', 'Перераховувати податок');
define('MODULE_QTY_DISCOUNT_CALC_TAX_DESC', 'Перераховувати податок.');

define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_TITLE', 'Показувати доставку');
define('MODULE_ORDER_TOTAL_SHIPPING_STATUS_DESC', 'Ви бажаєте показувати вартість доставки?');
define('MODULE_ORDER_TOTAL_OT_SHIPPING_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_SHIPPING_SORT_ORDER_DESC', 'Порядок сортування модуля.');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_TITLE', 'Дозволити безкоштовну доставку');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_DESC', 'Ви бажаєте дозволити безкоштовну доставку?');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_TITLE', 'Безкоштовна доставка для замовлень понад');
define('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER_DESC', 'Для замовлень, понад зазначеної величини, доставка буде безкоштовною ..');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_TITLE', 'Безкоштовна доставка для замовлень');
define('MODULE_ORDER_TOTAL_SHIPPING_DESTINATION_DESC', 'Вкажіть, для яких саме замовлень буде дійсна безкоштовна доставка.');

define('MODULE_ORDER_TOTAL_SUBTOTAL_STATUS_TITLE', 'Показувати вартість товару');
define('MODULE_ORDER_TOTAL_SUBTOTAL_STATUS_DESC', 'Ви бажаєте показувати вартість товару?');
define('MODULE_ORDER_TOTAL_OT_SUBTOTAL_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_SUBTOTAL_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_ORDER_TOTAL_TAX_STATUS_TITLE', 'Показувати податок');
define('MODULE_ORDER_TOTAL_TAX_STATUS_DESC', 'Ви бажаєте показувати податок?');
define('MODULE_ORDER_TOTAL_OT_TAX_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_TAX_SORT_ORDER_DESC', 'Порядок сортування модуля.');

define('MODULE_ORDER_TOTAL_TOTAL_STATUS_TITLE', 'Показувати всього');
define('MODULE_ORDER_TOTAL_TOTAL_STATUS_DESC', 'Ви бажаєте показувати загальну вартість замовлення?');
define('MODULE_ORDER_TOTAL_OT_TOTAL_SORT_ORDER_TITLE', 'Порядок сортування');
define('MODULE_ORDER_TOTAL_OT_TOTAL_SORT_ORDER_DESC', 'Порядок сортування модуля.');

?>
