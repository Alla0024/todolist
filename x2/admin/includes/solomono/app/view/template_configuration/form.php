<?php
$checkModuleExist = [
    'P_COMPARE' => 'COMPARE_MODULE_ENABLED',
    'P_WISHLIST' => 'WISHLIST_MODULE_ENABLED'
];
//debug($data);
//debug($action);
//exit;
/// test1
?>
<?php global $login_id; ?>
<?php $action_form = (!empty($data['data'])) ? "update_$action" : "insert_$action"; ?>
<?php $cnt = ceil(12 / count($data['data'])); ?>
<form class="form-horizontal <?php echo  $action ?>" action="<?php echo  ($_SERVER['SCRIPT_URL']?:$_SERVER['SCRIPT_NAME']) . '?action=' . $action_form; ?>" method="post"
      enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo  $data['id'] ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <ul class="nav nav-tabs col-md-3 padder template_conf_list">
                    <?php $first=true; foreach ($data['data'] as $const => $arr): ?>
                        <li<?=$first?' class="active" ':''?> style="float:none"><a data-toggle="tab" href="#tab-<?=$const?>"><?php echo  constant($const . '_MODULES'); ?></a></li>
                    <?php $first = false; endforeach; ?>
                    <?php if ($login_id == '1'): ?>
                        <li style="float:none"><a data-toggle="tab" href="#tab-addModule"><?= ADD_MODULE_MODULES ?></a></li>

                    <?php endif; ?>
                </ul>
                <div class="tab-content content col-md-9 padder">
                    <?php $first = true; if(array_key_exists('MAINCONF',$data['data'])) $first = false; ?>
                    <?php foreach ($data['data'] as $const => $arr): ?>
                        <?php if ($const == 'MAINCONF'): ?>

                            <div id="tab-<?=$const?>" data-name="<?=$const?>" class="tab-pane fade <?=($const == 'MAINCONF')?'active in' :''?>">
                                <h4><?php echo constant($const . '_MODULES'); ?></h4>
                                <?php foreach ($arr as $value) : ?>                    
                                    <div class="form-group padder">
                                      <?php $infobox_data = unserialize($value['infobox_data']); ?>

                                      <?php if($infobox_data['type'] == 'checkbox'){ ?>
                                          <span ><?=defined($value['infobox_define'])?constant($value['infobox_define']):$value['infobox_define'];?>
                                              <?php if($value['callback_data'] == 'NEED_MINIFY'){ ?>
		                                          <a href="javascript:void(0);" title="<?= INFO_ICON_NEED_MINIFY?>">
			                                          <span class="glyphicon glyphicon-info-sign" style="color: #2172ff"></span>
		                                          </a>
                                              <?php } ?>
                                          </span>
                                      <?php } else { ?>
                                          <label for="<?=$value['infobox_define']?>">
	                                          <?=defined($value['infobox_define'])?constant($value['infobox_define']):$value['infobox_define'];?>
                                              <?php
                                              if($value['callback_data'] == 'NEED_MINIFY'){ ?>
		                                          <a href="javascript:void(0);" title="<?= INFO_ICON_NEED_MINIFY?>">
			                                          <span class="glyphicon glyphicon-info-sign" style="color: #2172ff"></span>
		                                          </a>
                                              <?php } ?>
                                          </label>
                                      <?php } ?>
                                      <?php if ($infobox_data['type'] == 'select'): ?>
                                          <select class="form-control" id="<?=$value['infobox_define']?>" data-callback="<?php echo  $value['callback_data']; ?>">
                                            <?php foreach ($infobox_data['data'] as $k=>$v) : ?>
                                                <option value="<?=$k?>"<?=$infobox_data['val']==$k?' selected':''?>><?=defined($v)?constant($v):$v?></option>
                                            <?php endforeach; ?>
                                          </select>
                                        <?php elseif($infobox_data['type'] == 'checkbox'): ?>
                                            <input type="<?=$infobox_data['type']?>" class="cmn-toggle cmn-toggle-round" data-callback="<?php echo  $value['callback_data']; ?>" id="<?=$value['infobox_define']?>" value="<?=$infobox_data['val']?>"<?= $infobox_data['val']?' checked':''?>>
                                            <label for="<?=$value['infobox_define']?>"></label>
                                        <?php elseif($infobox_data['type'] == 'color'): ?>
                                            <input type="<?=$infobox_data['type']?>" class="color form-control" id="<?=$value['infobox_define']?>" value="<?=$infobox_data['val']?>">
                                        <?php else: ?>
                                            <input type="<?=$infobox_data['type']?>" class="form-control" id="<?=$value['infobox_define']?>" value="<?=$infobox_data['val']?>">
                                        <?php endif; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                    <?php else: ?>
                    <div id="tab-<?=$const?>" class="tab-pane fade<?=$first?' active in' :''?>">
                        <?php $first = false; ?>
                        <h4><?php echo  constant($const . '_MODULES'); ?></h4>
                        <ul id="<?php echo  $const ?>" class="list-group <?php echo  ($const == 'LEFT' || $const == 'MAINPAGE') ? 'sortable' : ''; ?> <?php echo  $action; ?>">
                            <?php foreach ($arr as $k => $v): ?>
                                <?php

                                // START CHECK IF MODULE ENABLED
                                if (array_key_exists ($v["infobox_define"], $checkModuleExist)) {
                                    if (getConstantValue($checkModuleExist[$v["infobox_define"]]) == 'false') {
                                        continue;
                                    }
                                } // END CHECK IF MODULE ENABLED?>

                                <li class="list-group-item animated" data-location="<?php echo  $v['location']; ?>"
                                    data-id="<?php echo  $v['id']; ?>" data-callback="<?php echo  $v['callback_data']; ?>">
                                    <?php echo constant($v['infobox_define']); ?>
                                    <div class="status">
                                        <input class="cmn-toggle cmn-toggle-round" type="checkbox"
                                               id="cmn-toggle-<?php echo  $v['id']; ?>" <?php echo  ($v['infobox_display'] == 1) ? 'checked' : '' ?>>
                                        <label for="cmn-toggle-<?php echo  $v['id']; ?>"></label>
                                    </div>
                                    <?php // if (isset($v['infobox_data'])): ?>
                                        <?php require "template_config.php"; ?>
                                    <?php // endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                    <?php $first = false; endif; endforeach;?>
                    <div id="tab-addModule" class="tab-pane fade">
                        <h4><?= ADD_MODULE_MODULES; ?></h4>
                        <ul id="addModule" class="list-group">
                            <div class="form-group padder">
                                <label for="infobox_file_name">infobox_file_name</label>
                                <input type="text" class="form-control" id="infobox_file_name" name="infobox_file_name">
                            </div>
                            <div class="form-group padder">
                                <label for="infobox_define">infobox_define</label>
                                <input type="text" class="form-control" id="infobox_define" name="infobox_define">
                            </div>
                            <div class="form-group padder">
                                <label for="display_in_column">display_in_column</label>
                                <select class="form-control" id="display_in_column" name="display_in_column">
                                    <?php foreach ($this->getObject()->getDisplayInColumn() as $option): ?>
                                        <option value="<?=$option?>"><?=$option?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <p>infobox_data example:</p>
                            <pre>
                                    <?php var_export(['id'=>['val'=>5,'label'=>'name','callable'=>'method']]) ?>
                             </pre>
                            <div class="form-group padder">
                                <label for="infobox_data">infobox_data</label>
                                <textarea rows="5" class="form-control" id="infobox_data" name="infobox_data"></textarea>
                            </div>
                        </ul>
                        <button class="addModuleButton btn btn-default" type="button">Add module</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="form-group text-right">
        <div class="col-sm-12">
            <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
        </div>
    </div>
</form>
<script>

    $("form.template .changeable input,form.template .changeable select").on('change', function () {
        var block = $(this).parents('.val');
        var val = $(this).val()
        if ($(this).attr('type') == 'checkbox'){
            val = $(this).prop('checked') ? 1 : 0;
        }
        var name = {
            module: block.data('module'),
            const: block.data('const'),
            field: block.data('field'),
            val: val
        };
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            dataType: 'json',
            data: {action: 'changeable', name: name, template_id: $('form.template [name="id"]').val()},
            beforeSend: function () {
                show_tooltip('<span style="font-size: 5em;" class="ajax-loader"></span>', 55555);
            },
            success: function (response) {
                console.log(response);
                $('.tooltip_own').remove();
                if (response.success == true) {
                    show_tooltip(response.msg, 500);
                } else {
                    show_tooltip(response.msg, 99999, $('body'), true);
                }
            }
        });
    });

    $('.status>input.cmn-toggle').on('change', function () {
        var status = $(this).prop('checked');
        var callbackData = $(this).parents('li').data('callback');
        if (status == true) {
            status = 1;
        } else {
            status = 0;
        }
        var data = {
            action: 'infobox_display',
            id: $(this).parents('li').data('id'),
            callback: callbackData,
            status: status
        };
        $.ajax({
            url: window.location.pathname,
            type: "POST",
            data: data,
            dataType: 'json',
            success: function (response) {
                show_tooltip(response.msg, 500);
                if(typeof callbackData != 'undefined'){
                    setAndShowCriticalWindow();
                }
            }
        });
    });

    $(".sortable").sortable({
        update: function (event, ui) {
            var ul = event.target;
            var form = $(ul).closest('form');
            var li = $(ul).find('li');
            var data = {};
            $(li).each(function (i, e) {
                $(e).data('location', i + 1);
                data[$(e).data('id')] = $(e).data('location');
            });
            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                dataType: 'json',
                data: data,
                success: function (response) {
                    if (response.success == false) {
                        $('#modal_form').modal('hide');
                        show_tooltip(response['msg']);
                    }
                }
            });
        }
    });
    $('body').on('click', 'input.form-control.color', function (e) {
        var $this = $(this);

        e.preventDefault();
        $this.ColorPicker({
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                var formData = {
                    action: 'changeable',
                    template_id: $('form.template [name="id"]').val(),
                    name: {
                        module: 'MAINCONF',
                        const: $this.attr('id'),
                        val: $this.val(),
                    }
                }

                $.ajax({
                    url: window.location.pathname,
                    type: 'POST',
                    dataType: 'json',
                    data: formData,
                    beforeSend: function () {
                        show_tooltip('<span style="font-size: 5em;" class="ajax-loader"></span>', 55555);
                    },
                    success: function (response) {
                        $('.tooltip_own').remove();
                        if (response.success == true) {
                            show_tooltip(response.msg, 500);
                        } else {
                            show_tooltip(response.msg, 99999, $('body'), true);
                        }
                    }
                });
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $this.val('#' + hex);
            }
        });
        $this.click();
    });
    $('body').on('change','.tab-pane[data-name] input,.tab-pane[data-name] select',function(){
        var val = $(this).attr('type') == 'checkbox' ? ($(this).prop('checked')?1:0) : $(this).val();
        var callbackData = $(this).data('callback');
        var formData = {
            action: 'changeable',
            template_id: $('form.template [name="id"]').val(),
			callback: callbackData,
            name: {
                module: $(this).parents('.tab-pane[data-name]').data('name'),
                const: $(this).attr('id'),
                val: val,
            }
        }
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            dataType: 'json',
            data: formData,
            beforeSend: function () {
                show_tooltip('<span style="font-size: 5em;" class="ajax-loader"></span>', 55555);
            },
            success: function (response) {
                $('.tooltip_own').remove();
                if (response.success == true) {
                    show_tooltip(response.msg, 500);
                    if(typeof callbackData != 'undefined'){
                        setAndShowCriticalWindow();
                    }
                } else {
                    show_tooltip(response.msg, 99999, $('body'), true);
                }
            }
        });
    });

    <?php if ($login_id == "1"): ?>
    $('.changeable_admin .save_admin_block').on('click',function(){
        var $this = $(this).parent().find('textarea');
        var formData = {
            action: 'changeable_admin',
            template_id: $('form.template [name="id"]').val(),
            module: $(this).parents('ul').attr('id'),
            const: $this.attr('name'),
            val: $this.val(),
        }
        $.ajax({
            url: window.location.pathname,
            type: 'POST',
            dataType: 'json',
            data: formData,
            beforeSend: function () {
                show_tooltip('<span style="font-size: 5em;" class="ajax-loader"></span>', 55555);
            },
            success: function (response) {
                $('.tooltip_own').remove();
                if (response.success == true) {
                    show_tooltip(response.msg, 500);
                } else {
                    show_tooltip(response.msg, 99999, $('body'), true);
                }
            }
        })
    })
    $('.addModuleButton').on('click',function(){
        var formData = {'action':'addModule','template_id':$('form.template [name="id"]').val()};
        $('#addModule input,#addModule select,#addModule textarea').each(function(){
            if ($(this).val() == '') {
                return;
            } else {
                formData[$(this).attr('name')] = $(this).val();
            }
        });
    $.ajax({
        url: window.location.pathname,
        type: 'POST',
        dataType: 'json',
        data: formData,
        beforeSend: function () {
            show_tooltip('<span style="font-size: 5em;" class="ajax-loader"></span>', 55555);
        },
        success: function (response) {
            $('.tooltip_own').remove();
            if (response.success == true) {
                show_tooltip(response.msg, 500);
                $('#addModule input,#addModule select,#addModule textarea').each(function(){
                    $(this).val('');
                });
            } else {
                show_tooltip(response.msg, 99999, $('body'), true);
            }
        }
    });
    })

    <?php endif;?>
    setTimeout(()=>
    console.clear(),1000);
</script>