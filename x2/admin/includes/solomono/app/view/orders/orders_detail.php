<?php
//debug($data);
//debug($action);
//exit;
require(DIR_WS_CLASSES . 'currencies.php');
$currencies=new currencies();
$language = $_SESSION['language'];
?>
<?php $action_form = (!empty($data['data'])) ? "update_$action" : "insert_$action"; ?>

<div class="container modal_order_details">
    <div class="row">
        <div class="col-md-3">
            <h3><?php echo ENTRY_INFO; ?></h3>
            <?php foreach ($data['data']['INFO'] as $field => $v):
                switch ($field){
                    case 'shipping_method_code':
                        break;
                    default:
                            echo $v ? '<p><b>' . addDoubleDot($field) . '</b> ' . $v . '</p>' : '';
                }

            endforeach; ?>
            <p><a href="/admin/edit_orders.php?oID=<?= $data['order_id']; ?>">edit order</a></b></p>
        </div>
        <div class="col-md-3">
            <h3><?php echo ENTRY_CUSTOMERS; ?></h3>
            <?php echo tep_address_format($data['data']['CUSTOMERS']['address_format_id'], $data['data']['CUSTOMERS'], 1, '', '<br>'); ?>
        </div>
        <div class="col-md-3">
            <h3><?php echo ENTRY_DELIVERY; ?></h3>
            <?php echo tep_address_format($data['data']['DELIVERY']['address_format_id'], $data['data']['DELIVERY'], 1, '', '<br>'); ?>
        </div>
        <div class="col-md-3">
            <h3><?php echo ENTRY_BILLING; ?></h3>
            <?php echo tep_address_format($data['data']['BILLING']['address_format_id'], $data['data']['BILLING'], 1, '', '<br>'); ?>
        </div>
    </div>
    <div class="row">
        <table class="table table-striped <?php echo $action; ?>">
            <thead>
            <tr>
                <?php if (count($data['products'])): ?>
                    <?php foreach (current($data['products']) as $field => $v):  if ((isset($data['allowed_fields'][$field]['show']) && $data['allowed_fields'][$field]['show'] == false)): ?>
<!--                        --><?php //if($field == "orders_products_id") continue; ?>
<!--                        --><?php //if($field == "products_attr") continue; ?>
                        <th><?php echo $data['allowed_fields'][$field]['label'] ?: $field; ?></th>
                    <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['products'] as $k => $arr): ?>
                <tr>
                    <?php foreach ($arr as $field=>$v): if ((isset($data['allowed_fields'][$field]['show']) && $data['allowed_fields'][$field]['show'] == false)): ?>
                        <?php
//                            if($field == "orders_products_id") continue;
                            $products_attrs = "";
                            foreach ($arr['products_attr'] as $arrt) {
                                $products_attrs .= "<br><span class='attrs_product'><strong>".$arrt['products_options'].": </strong>".$arrt['products_options_values']."</span>";
                            }
//                            if($field == "products_attr") continue;
                        ?>
                        <td data-label="<?php echo $data['allowed_fields'][$field]['label'] ?: $field; ?>">
                            <?php
                                if ($field=='products_name') echo '<a target="_blank" href="/product_info.php?products_id='.$arr['products_id'].'">'.$v.'</a>'.$products_attrs;
                                elseif ($field=='products_price' or $field=='final_price') echo $currencies->format($v,true,$data['data']['INFO']['currency'],$data['data']['INFO']['currency_value']);
                                else echo $v;
                            ?>
                        </td>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <?php
        //associated "order_total" modules to their titles
        $modulesTitles = [
                'ot_better_together'=>  'MODULE_ORDER_TOTAL_BETTER_TOGETHER_TITLE',
                'ot_country_discount'=> 'MODULE_ORDER_TOTAL_COUNTRY_DISCOUNT_TEXT_TITLE',
                'ot_coupon'=>           'MODULE_ORDER_TOTAL_COUPON_TITLE',
                'ot_gv'=>               'MODULE_ORDER_TOTAL_GV_TITLE',
                'ot_lev_discount'=>     'MODULE_LEV_DISCOUNT_TITLE',
                'ot_loworderfee'=>      'MODULE_ORDER_TOTAL_LOWORDERFEE_TITLE',
                'ot_payment'=>          'MODULE_PAYMENT_DISC_TITLE',
                'ot_qty_discount'=>     'MODULE_QTY_DISCOUNT_TITLE',
                'ot_shipping'=>         'MODULE_ORDER_TOTAL_SHIPPING_TITLE',
                'ot_subtotal'=>         'MODULE_ORDER_TOTAL_SUBTOTAL_TITLE',
                'ot_tax'=>              'MODULE_ORDER_TOTAL_TAX_TITLE',
                'ot_total'=>            'MODULE_ORDER_TOTAL_TOTAL_TITLE'
        ];

        for ($i = 0; $i < count($data['orders_total']); $i++): ?>
            <p class="text-right"><?php
                //default title from DB->orders_total
                $title = $data['orders_total'][$i]['title'];
                //change title on title of appropriate module
                switch ($data['orders_total'][$i]['class']) {
                    case 'ot_shipping':
                        //get active shipping module
                        $activeShippingModule = $data['data']['INFO']['shipping_method_code'];
                        if(!empty($activeShippingModule)){
                            //init appropriate shipping module
                            if (is_file(DIR_FS_CATALOG_MODULES_SHIPPING . $activeShippingModule . '.php')) {
                                include_once(DIR_FS_CATALOG_MODULES_SHIPPING . $activeShippingModule . '.php');
                                includeLanguages(DIR_FS_CATALOG_LANGUAGES . $language .'/modules/shipping/' . $activeShippingModule . '.php');
                            } else {
                                include_once(DIR_FS_EXT . 'shipping/' . $activeShippingModule . '/' . $activeShippingModule . '.php');
                                includeLanguages(DIR_FS_EXT . 'shipping/' . $activeShippingModule . '/languages/' . $language . '/' . $activeShippingModule . '.json');
                            }
                            $class = $activeShippingModule;
                            $module = new $class;
                            $title = $module->title . ': ';
                        }
                        break;
                    default:
                        $moduleTitle = $modulesTitles[$data['orders_total'][$i]['class']];
                        if(isset($moduleTitle)){
                            includeLanguages(DIR_FS_CATALOG_LANGUAGES . $language .'/modules/order_total/' . $data['orders_total'][$i]['class'].'.php');
                            $title = getConstantValue($moduleTitle) . ': ';
                        }
                }
                echo $title . $data['orders_total'][$i]['text'];
                ?></p>
        <?php endfor; ?>
    </div>
    <div class="row form-group text-right">
<!--        <button type="button" class="btn btn-default send_confirmation" data-dismiss="modal" data-id="--><?php //echo $data['order_id'] ?><!--">Send confirmation</button>-->
        <a target="_blank" href="invoice.php?oID=<?php echo $data['order_id'] ?>"
           class="btn btn-default"><?php echo IMAGE_ORDERS_INVOICE ?></a>
        <a target="_blank" href="packingslip.php?oID=<?php echo $data['order_id'] ?>"
           class="btn btn-default"><?php echo IMAGE_ORDERS_PACKINGSLIP ?></a>
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
    </div>
</div>