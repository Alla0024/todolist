<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 19.09.2016
 * Time: 12:13
 */
class App
{
    private static $env;


    /**
     * @return mixed
     */
    public static function getEnv()
    {
        return self::$env;
    }

    /**
     * @param mixed $env
     */
    public static function setEnv($env)
    {
        self::$env = $env;
    }
    private static $instance = null;

    /**
     * @return mixed
     */
    public static function getInstance()
    {
        if(self::$instance === null){
            self::$instance = new self;
        }
    }

    public function __construct(){}

    /**
     * @return mixed
     */
    public static function isProduction()
    {
        return self::getEnv() === 'production';
    }

    function __destruct(){}
    function __clone(){}
}