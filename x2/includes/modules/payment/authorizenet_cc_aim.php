<?php

$rootDirectory = __DIR__ . "/../../../";
$liqpay_path   = 'ext/acquiring/authorizenet_cc_aim/authorizenet_cc_aim.php';

if (CARDS_ENABLED == 'true' && file_exists($rootDirectory . $liqpay_path)) {
  require_once $rootDirectory . $liqpay_path;
} else {
  class authorizenet_cc_aim {
    var $code, $title, $description, $enabled;

    function __construct() {
    }

    function check() {
      return false;
    }
  }
}