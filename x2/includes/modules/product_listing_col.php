<?php

//$time_start = microtime(true);

if (!is_array($tpl_settings)) {
    $tpl_settings = array(
        'request' => $listing_query,
        'id' => 'r_spisok',
        //'classes' => array(),
        //'title' => '',   
        //'cols' => array('xs'=>$cols[0],'sm'=>$cols[1],'md'=>$cols[2],'lg'=>$cols[0]),//'cols' => array('xs'=>'2','sm'=>'2','md'=>'3','lg'=>'4'),
        'wishlist' => true,
        'compare' => true
    );
}
$default_cols = explode(';', $template->getMainconf('MC_PRODUCT_QNT_IN_ROW')); // XS, SM, MD, LG
//$default_cols = unserialize(RTPL_COLS);
$default_cols[4] = is_null($default_cols[4]) ? 6 : $default_cols[4];
if ($tpl_settings['cols'] != '') {
    $tpl_cols = explode(';', $tpl_settings['cols']); // XS, SM, MD, LG
    if (count($tpl_cols) > 1) {
        $blocks_num_xs = 12 / ($tpl_cols[0] ?: 12);
        $blocks_num_sm = 12 / ($tpl_cols[1] ?: 12);
        $blocks_num_md = 12 / ($tpl_cols[2] ?: 12);
        $blocks_num_lg = 12 / ($tpl_cols[3] ?: 12);
        $blocks_num_xl = 12 / ($tpl_cols[4] ?: 6);
    } else {
        $blocks_num_xs = $blocks_num_sm = $blocks_num_md = $blocks_num_lg = $blocks_num_xl = $default_cols[2] = 12 / $tpl_settings['cols'];
    }
} else {

    $blocks_num_xs = 12 / ($default_cols[0] ?: 2);//default value if empty
    $blocks_num_sm = 12 / ($default_cols[1] ?: 3);//default value if empty
//  $default_cols[2] = $tpl_settings['cols']?:$default_cols[2];
    $blocks_num_md = 12 / ($default_cols[2] ?: 4);//default value if empty
    $blocks_num_lg = 12 / ($default_cols[3] ?: 4);//default value if empty
    $blocks_num_xl = 12 / ($default_cols[4] ?: 6);//default value if empty
}
$blocks_num_xs = is_float($blocks_num_xs) ? str_replace('.', '-', $blocks_num_xs) : $blocks_num_xs;
$blocks_num_sm = is_float($blocks_num_sm) ? str_replace('.', '-', $blocks_num_sm) : $blocks_num_sm;
$blocks_num_md = is_float($blocks_num_md) ? str_replace('.', '-', $blocks_num_md) : $blocks_num_md;
$blocks_num_lg = is_float($blocks_num_lg) ? str_replace('.', '-', $blocks_num_lg) : $blocks_num_lg;
$blocks_num_xl = is_float($blocks_num_xl) ? str_replace('.', '-', $blocks_num_xl) : $blocks_num_xl;

if ($listing_split->number_of_rows > 0 or $tpl_settings['id'] != 'r_spisok') {
    $vue_array = array();
    if ($tpl_settings['id'] != 'r_spisok') $display = 'block'; // $display = PRODUCT_LISTING_DISPLAY_STYLE;

    if ($display == 'list') { // display LIST ------------------------------------------------------
        $pmodel_class = 'l_list';
        $stock_pull = 'pull-left';
//        $compare_class = 'compare_list';
        $listing_layout = file_get_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/listing_list.html');
        $attr_listing = RTPL_PRODUCTS_ATTR_LISTING_LIST;
        if (is_array($attrs_array) and !isMobile()) $attr_body = RTPL_PRODUCTS_ATTR_BODY_LIST;
    } else {  // display COLUMNS ------------------------------------------------------
        $pmodel_class = 'p_list_model';
        $stock_pull = 'pull-right';
//        $compare_class = 'compare';
        $listing_layout = file_get_contents(DIR_WS_TEMPLATES . TEMPLATE_NAME . '/listing_columns.html');
        $attr_listing = RTPL_PRODUCTS_ATTR_LISTING_COL;
        if (is_array($attrs_array) and !isMobile()) $attr_body = RTPL_PRODUCTS_ATTR_BODY_COL;
    }

    $attr_listing_values = RTPL_PRODUCTS_ATTR_LISTING_DEFAULT_VALUES;
    $categories_id = $current_category_id;
    $catalog_path = tep_href_link(FILENAME_DEFAULT);

    if (is_array($tpl_settings['classes'])) $render_class = implode(' ', $tpl_settings['classes']);
    else $render_class = '';

    // listing_header:
    $listing_header = '';

    if ($tpl_settings['title'] != '') {
        $listing_header .= '<div class="like_h2">' . $tpl_settings['title'];
        if (isset($tpl_settings['title_link']) && $tpl_settings['title_link'] != '') $listing_header .= '<span class="title-link">' . $tpl_settings['title_link'] . '</span>';
        if ($tpl_settings['description'] != '') $listing_header .= '<p class="like_p">' . $tpl_settings['description'] . '</p>';
        if ($tpl_settings['additional_title_block'] != '') $listing_header .= $tpl_settings['additional_title_block'];
        $listing_header .= '</div>';
    }

    // if its slider:
    if (is_array($tpl_settings['classes']) and in_array('product_slider', $tpl_settings['classes'])) {
        $listing_header .= sprintf(RTPL_LISTING_HEADER_SLIDER, $tpl_settings['id'], $render_class);
    } else {
        $listing_header .= sprintf(RTPL_LISTING_HEADER_NORMAL, $render_class, $tpl_settings['id']);
    }
    // END listing_header


    // listing_footer:
    $listing_footer = '</div>';
    $block_is_slider = false;
    if (is_array($tpl_settings['classes']) and in_array('product_slider', $tpl_settings['classes'])) {
        $block_is_slider = true;
        $listing_footer .= '</div>';
    }

    if ($tpl_settings['id'] == 'r_spisok') {
        if ($listing_split->number_of_rows > $row_by_page) {
            $listing_footer .= sprintf(RTPL_NUMBER_OF_ROWS, $listing_split->number_of_rows);
            if ($template->show('LIST_LOAD_MORE')) {
                if (
                    file_exists("ext/show_more/init.php") &&
                    $listing_split->current_page_number < $listing_split->number_of_pages
                ) {
                    $listing_footer .= require_once "ext/show_more/init.php";
                }
            }
            if ($template->show('LIST_NUMBER_OF_ROWS')) {
                $listing_footer .= sprintf(RTPL_PAGES_HTML, $listing_split->display_links(10, tep_get_all_get_params(array('page', 'info', 'x', 'y', 'ajaxloading', 'language'))));
            }
        }
        if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true' && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && empty($_GET['manufacturers_id'])) {
            $isFilter = true;

            if (!isset($redirectOptionsIdsArrayForCheck)) {
                $redirectOptionsIdsArrayForCheck = array_filter(array_keys($_GET), 'is_numeric');
                $redirectOptionsIdsArrayForCheck = array_intersect_key($_GET, array_flip($redirectOptionsIdsArrayForCheck));
            }
        }
    }
    // END listing_footer
    if (isset($tpl_settings['wishlist']) && defined('WISHLIST_MODULE_ENABLED') && WISHLIST_MODULE_ENABLED == 'true') {
        $wish_enabled = $tpl_settings['wishlist'];
    } else {
        $wish_enabled = WISHLIST_MODULE_ENABLED == 'true';
    }
    if (isset($tpl_settings['compare']) && defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true') {
        $compa_enabled = $tpl_settings['compare'];
    } else {
        $compa_enabled = COMPARE_MODULE_ENABLED == 'true';
    }
    if (isset($content) && $content === '404') {
        $pdf_link = '';
    } else {
        $pdf_link = tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('language')) . 'pdf=true');
    }
    // one global array:
    $vue_array['globals'] = array('categories_id' => $categories_id,
        'cart_incart_text' => IMAGE_BUTTON_IN_CART,
        'cart_addto_cart' => IMAGE_BUTTON_ADDTO_CART,
        'compa_enabled' => $compa_enabled,
        'promUrls' => $promUrls,
        'compa_text_in' => GO_COMPARE,
        'compa_text' => COMPARE,
        'wish_enabled' => $wish_enabled,
        'wish_text_in' => IN_WHISHLIST,
        'wish_text' => WHISH,
        'product_stock_text_in' => LIST_TEMP_INSTOCK,
        'product_stock_text' => LIST_TEMP_OUTSTOCK,
        'display' => $display,
        'pdf_link' => $pdf_link,
        'pmodel_class' => $pmodel_class,
        'compare_class' => $compare_class,
        'attr_listing' => $attr_listing,
        'attr_listing_values' => $attr_listing_values,
        'attr_body' => $attr_body,
        'stock_pull' => $stock_pull,
        'catalog_path' => $catalog_path,
        'blocks_num' => array('xs' => $blocks_num_xs, 'sm' => $blocks_num_sm, 'md' => $blocks_num_md, 'lg' => $blocks_num_lg, 'xl' => $blocks_num_xl),
        'listing_header' => $listing_header,
        'listing_footer' => $listing_footer,
        'listing_layout' => $listing_layout,
        'img_end' => SMALL_IMAGE_WIDTH . 'x' . SMALL_IMAGE_HEIGHT,
        'img_default' => 'default.png',
        'all_attribs' => $attr_names_array);

    if ($template->getMainconf('MC_THUMB_FIT') != 0) $vue_array['globals']['add_classes'] .= ' object_fit';
    if (defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true') {
        $compare_page_link = tep_href_link('compare.php');
    }
    $i = 1;
    $productsIds = $productsPrices = [];
    while ($listing = tep_db_fetch_array($tpl_settings['request'])) {
        $id = $listing['products_id'];
        $product_name = strip_tags($listing['products_name']);

        if ($listing['products_url'] != '') {
            $product_href = $listing['products_url']; // if "products_url" is set, show link without DB query to get names
        } else {
            $products_url = $seo_urls->strip($product_name);
            tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_url = '" . $products_url . "' where language_id = '" . $languages_id . "' and products_id = '" . tep_db_input($id) . "'");
            $product_href = $products_url ?: '-'; // if "products_url" empty, then get name from DB
        }

        $product_href = getCPathUrlPart($id) . preg_replace('|' . $catalog_path . '|i', '', $product_href);

        if ($listing['products_image'] != '') $product_image = explode(';', $listing['products_image'])[0];
        else $product_image = '';

        if ($template->getMainconf('MC_SHOW_THUMB2') == 1) $product_image2 = explode(';', $listing['products_image'])[1];

        $product_model = $listing['products_model'];
        $product_stock = $listing['products_quantity'];
        $cart_incart = false;
        foreach (array_keys($cart->contents) as $cart_key) {
            if (strpos($cart_key, $id) === 0) {  // product_id on first position in string cart_key
                $cart_incart = true;
                break;
            }
        }

        // PRICES
        $currencies->taxWrapper = 'span';
        $currencies->enableCurrencies = true;

        $old_price = $currencies->display_price($listing['products_price'], tep_get_tax_rate($listing['products_tax_class_id']));
        // PRICES with discounts
        if ($listing['specials_new_products_price']) $spec_price = $listing['specials_new_products_price'];
        elseif($salemakers_array[$id]) $spec_price = $salemakers_array[$id];
        elseif($spec_array[$id]) $spec_price = $spec_array[$id];
        else $spec_price = '';

        if ($spec_price) $new_price = $currencies->display_price($spec_price, tep_get_tax_rate($listing['products_tax_class_id']));
        else $new_price = '';
        // END -- PRICES

        // LABELS
        if (PRODUCT_LABELS_MODULE_ENABLED == 'true') {
            $label_arr = getLabel($listing);
            $label = $label_arr['name'];
            $label_class = $label_arr['class'];
        } else {
            $label = '';
            $label_class = '';
        }
        // END -- LABELS

        // COMPARE & WISHLIST
        if (defined('COMPARE_MODULE_ENABLED') && COMPARE_MODULE_ENABLED == 'true') {

            $compa_arr = getCompare($id);
            $compa_checked = $compa_arr['checked'];
            $compa_text = $compa_arr['text'];

        }

        if (defined('WISHLIST_MODULE_ENABLED') && WISHLIST_MODULE_ENABLED == 'true') {
            $wish_arr = getWishList($id);
            $wish_checked = $wish_arr['checked'];
            $wish_text = $wish_arr['text'];
        }

        // END COMPARE & WISHLIST

        // ATTRIBUTES
        $listing_product_attributes = $product_attributes_number = array();
        $attr_limit = 4; // attributes in product hover
        if (is_array($r_pid_attr_array[$id])) {
            foreach ($attrs_array as $at_id) {
                if (!empty($r_pid_attr_array[$id][$at_id]) and $attr_limit != 0 and is_array($show_in_product_listing) and in_array($at_id, $show_in_product_listing)) {
                    ksort($r_pid_attr_array[$id][$at_id]);
                    $listing_product_attributes[$at_id] = implode(', ', $r_pid_attr_array[$id][$at_id]);
                    if (!empty($pid_attr_array[$id][$at_id]) && count($pid_attr_array[$id][$at_id]) > 1) {
                        ksort($pid_attr_array[$id][$at_id]);
                        $product_attributes_number[$at_id] = $pid_attr_array[$id][$at_id];
                    }
                    $attr_limit--;
                }
            }
        }
        // END -- ATTRIBUTES

        // array for every product:
        $product_modal_button = ($template->show("LIST_MODAL_ON") == 'true') ?
            '<svg class="product-modal-button" xmlns="http://www.w3.org/2000/svg" height="28px" viewBox="0 0 512 512.00049" width="28px"><path d="m90.742188 180.734375c-5.519532 0-10 4.476563-10 9.996094s4.480468 10 10 10c5.519531 0 10-4.480469 10-10s-4.480469-9.996094-10-9.996094zm0 0"/><path d="m370.921875 300.21875c-1.871094-1.875-4.417969-2.929688-7.070313-2.929688-.101562.019532-3.945312-.195312-7.070312 2.929688l-7.066406 7.066406-22.976563-22.972656c50.703125-71.074219 42.992188-169.117188-19.449219-231.554688-70.335937-70.335937-184.1875-70.347656-254.53125 0-70.335937 70.332032-70.347656 184.1875 0 254.53125 62.285157 62.289063 160.289063 70.28125 231.550782 19.449219l22.976562 22.976563-7.066406 7.066406c-3.902344 3.902344-3.90625 10.238281 0 14.144531l126.421875 126.410157c19.535156 19.546874 51.15625 19.558593 70.695313.003906 19.546874-19.53125 19.554687-51.15625 0-70.695313zm-49.496094 35.355469-21.410156-21.410157c2.488281-2.230468 4.921875-4.519531 7.273437-6.875 2.355469-2.351562 4.644532-4.785156 6.875-7.269531l21.410157 21.410157zm-254.527343-42.425781c-62.523438-62.519532-62.53125-163.722657 0-226.25 62.519531-62.523438 163.722656-62.53125 226.25 0 62.382812 62.378906 62.699218 163.558593 0 226.25-63.40625 63.414062-164.582032 61.671874-226.25 0zm296.953124 28.28125 28.277344 28.28125-42.417968 42.417968-28.28125-28.277344zm119.34375 161.769531c-11.71875 11.730469-30.683593 11.734375-42.410156 0l-76.933594-76.929688 42.417969-42.417969 76.929688 76.9375c11.726562 11.714844 11.738281 30.679688-.003907 42.410157zm0 0"/><path d="m279.011719 81.042969c-54.683594-54.683594-143.273438-54.714844-197.980469-.007813-54.570312 54.582032-54.570312 143.394532 0 197.980469 54.714844 54.710937 143.253906 54.714844 197.972656 0 54.582032-54.585937 54.585938-143.394531.007813-197.972656zm-14.148438 183.828125c-46.894531 46.894531-122.785156 46.90625-169.691406.003906-46.773437-46.789062-46.773437-122.914062 0-169.703125 46.871094-46.867187 122.8125-46.878906 169.699219.011719 46.78125 46.78125 46.777344 122.902344-.007813 169.6875zm0 0"/><path d="m180.023438 80.042969c-26.726563 0-51.835938 10.398437-70.703126 29.277343-10.9375 10.9375-18.988281 23.859376-23.925781 38.414063-1.773437 5.226563 1.023438 10.90625 6.253907 12.679687 5.238281 1.777344 10.910156-1.035156 12.683593-6.253906 3.941407-11.621094 10.378907-21.949218 19.128907-30.703125 15.09375-15.097656 35.179687-23.414062 56.5625-23.414062 5.523437 0 10-4.476563 10-10 0-5.523438-4.476563-10-10-10zm0 0"/></svg>' : '';
        $vue_array[$i] = array('p_id' => $id,
            'p_name' => stripslashes(htmlspecialchars($product_name)),
            'p_href' => $product_href,
            'p_img' => $product_image,
            'p_qty' => $product_stock,
            'show_button' => STOCK_SHOW_BUY_BUTTON,
            'cat_name' => stripslashes($cat_names[$prodToCat[$id]]),
            'p_price' => $old_price,
            'modal_button' => $product_modal_button);

        if ($cart_incart) $vue_array[$i]['cart_incart'] = $cart_incart;
        if ($label != '') $vue_array[$i]['lbl'] = $label;
        if ($label_class != '') $vue_array[$i]['l_class'] = $label_class;
        if ($compa_checked != '') {
            $vue_array[$i]['compare'] = $compa_checked;
            $vue_array[$i]['compare_link_before'] = '<a href="' . $compare_page_link . '">';
            $vue_array[$i]['compare_link_after'] = '</a>';
        } else {
            $vue_array[$i]['compare_link_before'] = '';
            $vue_array[$i]['compare_link_after'] = '';
        }
        if ($product_image2 != '') $vue_array[$i]['p_img2'] = $product_image2;
        if ($wish_checked != '') $vue_array[$i]['wish'] = $wish_checked;
        if ($new_price != '') $vue_array[$i]['p_specprice'] = $new_price;
        if ($listing['products_info'] != '') $vue_array[$i]['p_info'] = $listing['products_info'];
        if (is_array($listing_product_attributes) and isset($listing_product_attributes)) $vue_array[$i]['p_attr'] = $listing_product_attributes;
        if (is_array($product_attributes_number) and isset($product_attributes_number)) $vue_array[$i]['attr'] = $product_attributes_number;
        $vue_array[$i]['p_model'] = $template->show('LIST_MODEL') ? $product_model : '';
        $productsIds[] = $id;
        $productsPrices[] = $new_price ? (float)preg_replace('/[^0-9,.]/i', '', $new_price) : (float)preg_replace('/[^0-9,.]/i', '', $old_price);
        $i++;
    }

    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' && !isset($_GET['get-module'])) {
        if (defined('SEO_FILTER') && constant('SEO_FILTER') == 'true') {
            ob_start();

            $filesToRequire = [
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/left/filter_seo.php',
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/left/filter.php',
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/listing/filter_seo.php',
                DIR_WS_TEMPLATES . TEMPLATE_NAME . '/boxes/listing/filter.php',
                DIR_WS_TEMPLATES . 'default/boxes/left/filter_seo.php',
            ];

            foreach ($filesToRequire as $fileToRequire) {
                if (file_exists($fileToRequire)) {
                    require $fileToRequire;
                    break;
                }
            }

            $vue_array['globals']['filtersBlock'] = ob_get_contents();
            $addPage = true;
            unset($tempSeoFilterInfo);
            if ($isFilter && empty($_GET['manufacturers_id']) && empty($_GET['keywords'])) {
                if (empty($_GET['filter_id']) && empty($redirectOptionsIdsArrayForCheck)) {
                    $sortParameters = tep_get_all_get_params([
                        'language',
                        'currency',
                        'cPath'
                    ]);
                    $sortParameters = $sortParameters ? "&" . $sortParameters : "";
                    $vue_array['globals']['currentHref'] = HTTP_SERVER . '/' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . ($_GET['cPath'] ?: 0) . $sortParameters);
                } else {
                    $vue_array['globals']['currentHref'] = HTTP_SERVER . getFilterUrl(
                            $_GET['cPath'] ?: 0,
                            (isset($_GET['filter_id']) ? $_GET['filter_id'] : ''),
                            $redirectOptionsIdsArrayForCheck
                        );
                }
            } elseif (isset($_GET['cPath'])) {
                $vue_array['globals']['currentHref'] = $isFilter ?: tep_href_link(FILENAME_DEFAULT, 'cPath=' . $_GET['cPath']);
            }
            $addPage = false;

            ob_end_clean();
        }
        echo json_encode($vue_array);
        die;
    } // if its ajax-request
    else { // if its usual request

//        if(RTPL_PDF_ENABLED and $tpl_settings['id']=='r_spisok' && $current_category_id!=0 ) $output = sprintf(RTPL_LISTING_HEADER, $vue_array['globals']['pdf_link']);
//        else $output = '';
        $output = '';
        $output .= $vue_array['globals']['listing_header'];

        // show each product
        foreach ($vue_array as $key => $listing) {

            if ($key != 'globals') {
                $id = $listing['p_id'];
                $product_name = $listing['p_name'];
                $product_info_text = $listing['p_info'];
                $cat_name = $listing['cat_name'];
                if ($promUrls) {
                    $product_href = $vue_array['globals']['catalog_path'] . 'p' . $id . '-' . $listing['p_href'] . '.html';
                } else {
                    $product_href = $vue_array['globals']['catalog_path'] . $listing['p_href'] . '/p-' . $id . '.html';
                }
                $product_image = sprintf(RTPL_PRODUCTS_IMAGE, $vue_array['globals']['img_end'], $listing['p_img'] ?: $vue_array['globals']['img_default'], $product_name, $product_name, ($listing['p_img2'] ? 'data-hover="getimage/' . $vue_array['globals']['img_end'] . '/products/' . $listing['p_img2'] . '"' : ''));
                $pmodel = ($listing['p_model'] != '' ? sprintf(RTPL_PRODUCTS_MODEL, $vue_array['globals']['pmodel_class'], $listing['p_model']) : '');
                $stock = ($listing['p_qty'] > 0 ? sprintf(RTPL_PRODUCTS_STOCK, $vue_array['globals']['stock_pull']) : sprintf(RTPL_PRODUCTS_OUTSTOCK, $vue_array['globals']['stock_pull']));
                $label = $listing['lbl'] != '' ? sprintf(RTPL_LABEL, $listing['l_class'], $listing['lbl']) : '';
                $add_classes = $vue_array['globals']['add_classes'] ?: '';
                $add_classes = !$block_is_slider ? $add_classes . ' not-slider' : $add_classes;
                if($listing['p_qty'] <= 0) $add_classes .= ' nostock';

                $spec_price = $listing['p_specprice'];
                $spec_price = $spec_price == '-' ? '' : $spec_price;
                $old_price = $listing['p_price'];
                $old_price = $old_price == '-' ? '' : $old_price;
                $final_price = ($spec_price != '' ? sprintf(RTPL_PRODUCTS_SPEC_PRICE, $spec_price, $old_price) : sprintf(RTPL_PRODUCTS_PRICE, $old_price));

                $new_price = sprintf(RTPL_PRODUCTS_PRICE, ($spec_price ?: $old_price)); // separated new price
                $old_price = $spec_price ? sprintf(RTPL_PRODUCTS_OLD_PRICE, $old_price) : ''; // separated old price

                //  $cart_button = tep_draw_hidden_field('cart_quantity', 1) . tep_draw_hidden_field('products_id', $id);

                if ($listing['p_qty'] <= 0) {
                    if (STOCK_SHOW_BUY_BUTTON == "false") {
                        $cart_button = "";
                    } else {
                        $cart_button = $listing['cart_incart'] ? sprintf(RTPL_CART_BUTTON, $id) : sprintf(RTPL_ADD_TO_CART_BUTTON, $id, 1);
                    }
                } else {
                    $cart_button = $listing['cart_incart'] ? sprintf(RTPL_CART_BUTTON, $id) : sprintf(RTPL_ADD_TO_CART_BUTTON, $id, 1);
                }


                // Compare & Wishlist
                $compare_output = $wishlist_output = '';
                if ($vue_array['globals']['compa_enabled'] == 'true' or $vue_array['globals']['wish_enabled'] == 'true') {
                    if ($vue_array['globals']['compa_enabled'] == 'true' and !isMobile()) {
                        if (TEMPLATE_NAME === 'default' || TEMPLATE_NAME === 'molla') {
                            $compare_output .= sprintf(
                                RTPL_PRODUCTS_COMPARE,
                                $id,
                                $id,
                                $id,
                                $listing['compare'],
                                $listing['compare_link_before'],
                                $id,
                                ($listing['compare'] ? $vue_array['globals']['compa_text_in'] : $vue_array['globals']['compa_text']),
                                $listing['compare_link_after']

                            );
                        } else {
                            $compare_output .= sprintf(
                                RTPL_PRODUCTS_COMPARE,
                                $id,
                                $id,
                                $id,
                                $listing['compare'],
                                $id,
                                ($listing['compare'] ? $vue_array['globals']['compa_text_in'] : $vue_array['globals']['compa_text'])
                            );
                        }
                    }
                    if ($vue_array['globals']['wish_enabled'] == 'true' and !isMobile()) $wishlist_output .= sprintf(RTPL_PRODUCTS_WISHLIST, $id, $id, $id, $listing['wish'], $id, $listing['wish'] ? $vue_array['globals']['wish_text_in'] : $vue_array['globals']['wish_text']);
                }
                //   $compare_output = ($compare_output!=''?sprintf(RTPL_COMPARE_OUTPUT, $vue_array['globals']['compare_class'], $compare_output):'');
                $listing_product_attributes = $product_attributes_number = '';
                if (is_array($listing['p_attr']) && !empty($listing['p_attr'])) {

                    foreach ($listing['p_attr'] as $ana_name => $ana_vals) $listing_product_attributes .= sprintf($vue_array['globals']['attr_listing'], $vue_array['globals']['all_attribs'][$ana_name], $ana_vals);
                    if (is_array($listing['attr']) && !empty($listing['attr'])) {
                        foreach ($listing['attr'] as $attr_key => $attr_vals) $product_attributes_number .= sprintf($vue_array['globals']['attr_listing_values'], $attr_key, array_shift($attr_vals));
                    }
                    $cart_button .= $product_attributes_number;
                    $listing_product_attributes = sprintf($vue_array['globals']['attr_body'], $listing_product_attributes);
                }
                $product_modal_button = ($template->show("LIST_MODAL_ON") == 'true') ?
                    '<svg class="product-modal-button" xmlns="http://www.w3.org/2000/svg" height="28px" viewBox="0 0 512 512.00049" width="28px"><path d="m90.742188 180.734375c-5.519532 0-10 4.476563-10 9.996094s4.480468 10 10 10c5.519531 0 10-4.480469 10-10s-4.480469-9.996094-10-9.996094zm0 0"/><path d="m370.921875 300.21875c-1.871094-1.875-4.417969-2.929688-7.070313-2.929688-.101562.019532-3.945312-.195312-7.070312 2.929688l-7.066406 7.066406-22.976563-22.972656c50.703125-71.074219 42.992188-169.117188-19.449219-231.554688-70.335937-70.335937-184.1875-70.347656-254.53125 0-70.335937 70.332032-70.347656 184.1875 0 254.53125 62.285157 62.289063 160.289063 70.28125 231.550782 19.449219l22.976562 22.976563-7.066406 7.066406c-3.902344 3.902344-3.90625 10.238281 0 14.144531l126.421875 126.410157c19.535156 19.546874 51.15625 19.558593 70.695313.003906 19.546874-19.53125 19.554687-51.15625 0-70.695313zm-49.496094 35.355469-21.410156-21.410157c2.488281-2.230468 4.921875-4.519531 7.273437-6.875 2.355469-2.351562 4.644532-4.785156 6.875-7.269531l21.410157 21.410157zm-254.527343-42.425781c-62.523438-62.519532-62.53125-163.722657 0-226.25 62.519531-62.523438 163.722656-62.53125 226.25 0 62.382812 62.378906 62.699218 163.558593 0 226.25-63.40625 63.414062-164.582032 61.671874-226.25 0zm296.953124 28.28125 28.277344 28.28125-42.417968 42.417968-28.28125-28.277344zm119.34375 161.769531c-11.71875 11.730469-30.683593 11.734375-42.410156 0l-76.933594-76.929688 42.417969-42.417969 76.929688 76.9375c11.726562 11.714844 11.738281 30.679688-.003907 42.410157zm0 0"/><path d="m279.011719 81.042969c-54.683594-54.683594-143.273438-54.714844-197.980469-.007813-54.570312 54.582032-54.570312 143.394532 0 197.980469 54.714844 54.710937 143.253906 54.714844 197.972656 0 54.582032-54.585937 54.585938-143.394531.007813-197.972656zm-14.148438 183.828125c-46.894531 46.894531-122.785156 46.90625-169.691406.003906-46.773437-46.789062-46.773437-122.914062 0-169.703125 46.871094-46.867187 122.8125-46.878906 169.699219.011719 46.78125 46.78125 46.777344 122.902344-.007813 169.6875zm0 0"/><path d="m180.023438 80.042969c-26.726563 0-51.835938 10.398437-70.703126 29.277343-10.9375 10.9375-18.988281 23.859376-23.925781 38.414063-1.773437 5.226563 1.023438 10.90625 6.253907 12.679687 5.238281 1.777344 10.910156-1.035156 12.683593-6.253906 3.941407-11.621094 10.378907-21.949218 19.128907-30.703125 15.09375-15.097656 35.179687-23.414062 56.5625-23.414062 5.523437 0 10-4.476563 10-10 0-5.523438-4.476563-10-10-10zm0 0"/></svg>' : '';
                // array to replace variables from html template:
                $array_from_to = array(
                    '{{blocks_num_xl}}' => $vue_array['globals']['blocks_num']['xl'],
                    '{{blocks_num_lg}}' => $vue_array['globals']['blocks_num']['lg'],
                    '{{blocks_num_md}}' => $vue_array['globals']['blocks_num']['md'],
                    '{{blocks_num_sm}}' => $vue_array['globals']['blocks_num']['sm'],
                    '{{blocks_num_xs}}' => $vue_array['globals']['blocks_num']['xs'],
                    '{{product_href}}' => $product_href,
                    '{{label}}' => $label,
                    '{{product_image}}' => $product_image,
                    '{{products_model}}' => $pmodel,
                    '{{stock}}' => $stock,
                    '{{final_price}}' => $final_price,
                    '{{new_price}}' => $new_price,
                    '{{old_price}}' => $old_price,
                    '{{category_name}}' => $cat_name,
                    '{{product_modal_button}}' => $product_modal_button,
                    '{{id}}' => $id,
                    '{{product_name}}' => $product_name,
                    '{{product_info}}' => $product_info_text,
                    '{{product_attributes}}' => $listing_product_attributes,
                    '{{compare_output}}' => $compare_output,
                    '{{wishlist_output}}' => $wishlist_output,
                    '{{cart_button}}' => $cart_button,
                    '{{not_available}}' => ($listing['p_qty'] == 0 ? ' not_available' : ''),
                    '{{add_classes}}' => $add_classes);

                $output .= strtr($vue_array['globals']['listing_layout'], $array_from_to);
                //  $output .= strtr(constant($vue_array['globals']['listing_layout']), $array_from_to);
            }
        }

        $output .= $vue_array['globals']['listing_footer'];
        $output .= '<input type="hidden" class="productsPricesForAnalytics" value="[\'' . implode('\',\'', $productsPrices) . '\']">';
        $output .= '<input type="hidden" class="productsIdsForAnalytics" value="[\'' . implode('\',\'', $productsIds) . '\']">';
        echo $output;

    }

} else {
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') echo json_encode(array());
    else echo '';
}
?>